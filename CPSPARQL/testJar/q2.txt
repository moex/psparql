prefix foaf: <http://xmlns.com/foaf/0.1/>  
 prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix ex: <example.org>  
 select *  
from <testGraph.ttl>
 where {  
  ex:Abbeville +( -ex:from . ex:to  ) ?o .  
}               
