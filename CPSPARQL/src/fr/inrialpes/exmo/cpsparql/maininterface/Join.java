/*
 * Join.java
 *
 * Created on July 10, 2006, 11:56 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;

import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.VarImage;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Solution;

/**
 *
 * @author faisal
 */
public class Join {
    
    /**
     * Creates a new instance of Join 
     */
    public Join(){
    }
    
    public Join(Vector setOfMaps1,Vector setOfMaps2) {
        outerJoinSetOfMaps=outerJoin(setOfMaps1,setOfMaps2);
    }
    
    public Vector getOuterJoin(){
        return outerJoinSetOfMaps;
    }
    
    public Vector getJoinResult(){
        return joinSetOfMaps;
    }
    
    public Vector join(Vector setOfMaps1,Vector setOfMaps2){
       /*if (setOfMaps1.size()==0)
           return setOfMaps2;
       if (setOfMaps2.size()==0)
           return setOfMaps1;*/
       //System.out.println(setOfMaps1.size()+"   "+setOfMaps2.size());
       resultSol=new Solution();
       Vector joinSetOfMaps = new Vector();
       compatible = new boolean[setOfMaps1.size()];
       for(int i=0;i<setOfMaps1.size();i++)
           compatible[i]=false;
       for(int i=0;i<setOfMaps1.size();i++)
           for(int j=0;j<setOfMaps2.size();j++)
               if (compatible((Solution)setOfMaps1.get(i),(Solution)setOfMaps2.get(j))){
                   //System.out.println("yes");
                   compatible[i]=true;
                   joinSetOfMaps.add(resultSol);                               
               }
       //System.out.println(joinSetOfMaps.size());
       return joinSetOfMaps;
    }
    
    public Vector outerJoin(Vector setOfMaps1,Vector setOfMaps2){
       outerJoinSetOfMaps = new Vector();
       compatible = new boolean[setOfMaps1.size()];
       for(int i=0;i<setOfMaps1.size();i++)
           compatible[i]=false;
      /* if (setOfMaps1.size()==0)
           return setOfMaps2;
       if (setOfMaps2.size()==0)
           return setOfMaps1;*/
       outerJoinSetOfMaps =join(setOfMaps1,setOfMaps2);
       //System.out.println(outerJoinSetOfMaps.size());
       for(int i=0;i<setOfMaps1.size();i++)
           if (!compatible[i])
               outerJoinSetOfMaps.add(setOfMaps1.get(i));
       //System.out.println(outerJoinSetOfMaps.size());
       return outerJoinSetOfMaps;
    }
    
     public boolean compatible(Solution sol1, Solution sol2){
        boolean consistent=true;
        int i=0;
        int location;
        resultSol= new Solution(sol2);
        Vector map1Concepts= sol1.getConcepts();
        Vector map1Images= sol1.getImages();
        Vector map2Concepts= sol2.getConcepts();
        Vector map2Images= sol2.getImages();
        Vector resultSolConcepts= resultSol.getConcepts();
        Vector resultSolImages= resultSol.getImages();
        while((consistent) &&(i<map1Concepts.size())){
            String map1Variable,map1Image;
            map1Variable = (String)map1Concepts.get(i);
            map1Image=(String)map1Images.get(i);
            location = hasImage(map2Concepts,map1Variable);
            if(location>=0){
                String map2Image;
                map2Image=(String)map2Images.get(location);
                if(!map1Image.equals(map2Image))
                    consistent=false;
            }
            else {
                resultSolConcepts.add(map1Variable);
                resultSolImages.add(map1Image);
            }
            i++;
        }
        return consistent;
    }

     public int hasImage(Vector v, String var){
        boolean found =false;
        int i=0;
        String vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(String)(v.get(i));
            if (var.equals(vi)){
                found =true;
                location=i;
            }
            i++;
        }
        return location;
    }
    
    
    private boolean compatible[];
    private Vector joinSetOfMaps,outerJoinSetOfMaps;
    private Solution resultSol;    
}
