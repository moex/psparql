/*
 * PSPARQLEngine.java
 *
 * Created on April 6, 2006, 2:45 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;


import java.util.LinkedList;
import javax.swing.text.BadLocationException;
import org.w3c.dom.*;
import fr.inrialpes.exmo.cpsparql.query.*;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Graph;
import fr.inrialpes.exmo.cpsparql.findpathprojections.MultiGraph;
import fr.inrialpes.exmo.cpsparql.findpathprojections.FindPathProjections;
import fr.inrialpes.exmo.cpsparql.parser.QueryParser;
import fr.inrialpes.exmo.cpsparql.parser.QueryLexer;
import fr.inrialpes.exmo.cpsparql.parser.TokenBuffer;
import fr.inrialpes.exmo.cpsparql.turtleParser.TurtleDocumentParser;
import java.util.Vector;


/**
 *
 * @author faisal
 */
public class PSPARQLEngine {
    
    
    
    /**
     * Creates a new instance of PSPARQLEngine 
     */
    public PSPARQLEngine() {
        initialize();
        //buildRun();
    }
 

    public PSPARQLEngine(String queryText, boolean transformQuery, boolean checkInverse) {
        initialize();
        buildRun(queryText,transformQuery,checkInverse);
    }            
    
    public void initialize(){
        query = new Query();
        mappings  = new Vector();
    }

    public String getResult(){
        return result;
    }

     public long getElapsedTimeMillis(){
        return elapsedTimeMillis;
    }
     
     public long getElapsedLoadTimeS(){
        return elapsedLoadTimeS;
    }
     
     public long getElapsedTimeMSFirstS(){
        return elapsedTimeMSFirstS;
    }
     
    public String buildRun(String queryText, boolean transformQuery, boolean checkInverseMode){
            //System.out.println("running..."+checkInverseMode);
            TokenBuffer finalTokenBuffer = new TokenBuffer(0);
            query1 = queryText;
            int i=queryText.length();                
            query1+='\0';
            transformedQuery = query1; 
            QueryLexer lex = new QueryLexer(query1);
            QueryParser p;
            boolean queryContainInverseFlag = false;
            if(transformQuery)
                queryContainInverseFlag = false;
            if (checkInverseMode){
                boolean rewriteQuery = true;
                p = new QueryParser(lex.getTokenBuffer().tokenBuffer,false,true,rewriteQuery,queryContainInverseFlag);         
            }
            else{
                p = new QueryParser(lex.getTokenBuffer().tokenBuffer,false,true,transformQuery,queryContainInverseFlag);
                transformedQuery = p.getTransformedQuery();
                //System.out.println("Running Transformed Query...\n"+p.getTransformedQuery());
            }         
            
            if(checkInverseMode){
                 queryContainInverseFlag = p.getQueryContainInverseFlag();
                 lex = new QueryLexer(query1);
                 p = new QueryParser(lex.getTokenBuffer().tokenBuffer,false,true,false,queryContainInverseFlag);         
            }
            else if(transformQuery){
                queryContainInverseFlag = p.getQueryContainInverseFlag();
                lex = new QueryLexer(transformedQuery);
                p = new QueryParser(lex.getTokenBuffer().tokenBuffer,false,true,false,queryContainInverseFlag);         
                //System.out.println("Running Transformed Query...\n"+transformedQuery);
            }
             
             String textQuerySet = p.getTextGraphSet();                
             query = p.getQuery();
             elapsedTimeMillis = p.getElapsedTimeMillis();
             elapsedLoadTimeS = p.getElapsedLoadTimeS();
             elapsedTimeMSFirstS = p.getElapsedTimeMSFirstS();

             String str1="",str="";                                 

             Vector v =new Vector(p.getErrorsExceptions()); 
             if (v.isEmpty()){
                 str="";                                 
                 //GraphPattern [] GPS=query.getGraphSet().getGraphPattern();
                 //int gpNom = query.getGraphSet().getGraphPatternNom();
                  //MultiGraph graphG=loadRDFGraphs();


                  Vector constraints = new Vector();
                  str=queryFormat(p.getMappings(), constraints);


             }
             else{
                 str+="QUERY NOT CORRECT";
                 for (int count=0;count<v.size();count++)
                     str+=(String)v.get(count)+'\n';
             }
             return str;                
    }
    
    
    public String refine(String q, int leng){
        int i=0;
        String str=new String("");
        while(i<=leng){            
            if (q.charAt(i)!='\n')
                str+=q.charAt(i++);
            else i++;
                
        }
        return str;
    }   
    
    public MultiGraph loadRDFGraphs(boolean qContainInverseOperator){
        Vector rdfGraphsURIs = new Vector();
        if (query.getFromIRIs().size()>=1)
            for(int i=0;i<query.getFromIRIs().size();i++)
                rdfGraphsURIs.add((String)query.getFromIRIs().get(i));
        else rdfGraphsURIs.add("<http://barbara.inrialpes.fr/exmo/people/alkhateeb/RDFgraphs/graphG1.txt>");
        TurtleDocumentParser docParser = new TurtleDocumentParser(rdfGraphsURIs,qContainInverseOperator);
        return docParser.getGraph();
    }
    
    
    public String getGraphName(){
        if (query.getFromIRIs().size()>=1)
            return (String)query.getFromIRIs().get(0);
        else return "<http://barbara.inrialpes.fr/exmo/people/alkhateeb/RDFgraphs/graphG1.txt>";
    }
    
    public String queryFormat(Vector solutions,Vector constraints){
        //System.out.println("running...");
        String str=new String("");
        int limit=query.Limit();  
        int offset=query.Offset(); 
        Vector solAfter;
        //applying constraints here
        /*if (constraints.size()!=0){
            solAfter=new Vector(applyingConstraints(solutions,constraints));            
            solutions = solAfter;
        }*/
        // this method will apply the order criteria
        if (query.getOrder())
            solutions =orderBy(solutions);   
        //mappings = solutions;
        ResultForm RF = new ResultForm(solutions,limit,offset,query);
        mappings = RF.getFinalSolutions();
        str= RF.Result();     
        listOfResults = RF.getListOfResults();
        result = str;
        return str;
    }
    
    public Vector applyingConstraints(Vector sols,Vector cons){
        Vector solutionsAfter;
        ApplyConstraint applyConstraints = new ApplyConstraint(sols, cons);
        solutionsAfter= applyConstraints.getSolutionsAfter(); 
        return solutionsAfter;
    }
    
    public Vector orderBy(Vector sols){
        ApplyOrderBy AOBy = new ApplyOrderBy(sols,query.getOrderConditions());
        return  AOBy.getSolutions();
    }
    
    public Vector getMappings(){
        return mappings;
    }
    
    public LinkedList<Node> getListOfResults(){
        return listOfResults;
    }
    
    public String getTransformedQuery(){
        transformedQuery+= '\0';
        return transformedQuery.substring(0,transformedQuery.indexOf('\0'));
    }
    
    private Query query;
    private String query1 = new String("");
    private String transformedQuery;
    private String result = "Verify the query, it may contain errors...";
    private long elapsedTimeMillis; 
    private long elapsedLoadTimeS; 
    private long elapsedTimeMSFirstS;
    private Vector mappings;
    private LinkedList<Node> listOfResults;
}

