/*
 * ApplyConstraint.java
 *
 * Created on March 25, 2006, 9:10 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;

import java.lang.Float;
import fr.inrialpes.exmo.cpsparql.parser.Token;
import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Solution;
import java.util.regex.*;
import java.lang.NumberFormatException;

/**
 *
 * @author faisal
 */
public class ApplyConstraint {
    
    /** Creates a new instance of ApplyConstraint */
    public ApplyConstraint(Vector Solutions,Vector consts) {
        Debug=false;
        if (Debug) {
            System.out.println(" we apply constraints");
            printConstraint(consts);
        }
        solutions=Solutions;
        constraints=consts;
        constraintIndex=0;
        currentToken=-1;
        constraint=(Vector)constraints.get(constraintIndex);
        solIndex=0;
        solutionsAfter = new Vector();
        //System.out.println("$"+Double.parseDouble("-3.76")+"$  $"+Double.toToken(-375.987)+"$");
        start();
    }
    
    public Vector getSolutionsAfter(){
        //System.out.println(solutionsAfter.size());
        return solutionsAfter;
    }
    
    public void start(){
        for(int i=0;i<solutions.size();i++){
            currentToken=-1;
            if(evaluateConstraint(i)){
                //System.out.println(" yes");
                solutionsAfter.add(solutions.get(i));
            }
        }
    }
    
    public void printConstraint(Vector cs){
        //System.out.println(" printing the constraints");
        Vector c;
        Token token;
        String s=new String("");
        for(int i=0;i<cs.size();i++){
            c= (Vector)cs.get(i);
            for(int j=0;j<c.size();j++){
                token =(Token)c.get(j);
                s+=token.text+" :"+token.type+"  ";
            }
            if (Debug) System.out.println(" constraint ["+i+"]="+s);          
        }
    }
    
    public boolean evaluateConstraint(int index){
        solIndex =index;
        Token c=new Token();
         if ((LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS")))
            c=new Token(FunctionCall());
        else if (LA(1).type.equals("LPAREN"))
            c=new Token(BrackettedExpression());
        else if(!LA(1).type.equals("EOF"))
            c=new Token(BuiltInCall());
        //System.out.println(" evaluate="+c);
        if (c.text.indexOf("\"\"\"")>=0)
            c.text=c.text.substring(3, c.text.length()-3);
        if (c.type.indexOf("^^")>=0)
            c.type=c.type.substring(2);
        if (c.text.equals("error"))
            return false;        
        else if (effectiveBooleanValue(c))
            return true;
        else return false;
    }
    
    

    public boolean effectiveBooleanValue(Token exp1){
        //System.out.println(" we are at effectiveVBooleanValue():"+exp1.text+exp1.type);
        boolean value;
        if (exp1.text.indexOf("\"\"\"")>=0)
            exp1.text=exp1.text.substring(3, exp1.text.length()-3);
        if (exp1.type.indexOf("^^")>=0)
            exp1.type=exp1.type.substring(2);
        if (exp1.type.equals("<http://www.w3.org/2001/XMLSchema#boolean>")){
            if (exp1.text.equals("true"))
                value= true;
            else value= false;
        }
        else{
            
            if ((exp1.type.equals("<http://www.w3.org/2001/XMLSchema#string>"))||
                    (exp1.type.equals("NON")))
                if (exp1.text.equals(""))
                    value= false;
                else value= true;
            else if ((isNumericType(exp1.type))){
                if (exp1.text.equals("true"))
                    value= true;
                else if (exp1.text.equals("false"))
                    value= false;
                else if (parseValue(exp1.text)!=0)
                    value= true;
                else value= false;
            }
            else value= false;   // To be verified for the effective value
        }
        //System.out.println(" we are at effectiveVBooleanValue():"+exp1.text+exp1.type+value);
        return value;
    }// end of effectiveBooleanValue()
    
    public double parseValue(String s){
        double value=0;
        /*if (type.equals("<http://www.w3.org/2001/XMLSchema#double>") ||
                type.equals("DOUBLE"))*/
        value = Double.parseDouble(s);
        return value;
    }//end of parseValue()
    
    public boolean isNumericType(String type){
        if (type.equals("<http://www.w3.org/2001/XMLSchema#decimal>") ||
                type.equals("<http://www.w3.org/2001/XMLSchema#double>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#float>") ||
                type.equals("<http://www.w3.org/2001/XMLSchema#integer>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#short>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#long>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#int>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#byte>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>")||
                type.equals("INTEGER") ||type.equals("DOUBLE") ||
                type.equals("DECIMAL"))
            return true;
        else return false;
                    
    }// end of isNumericType()
        
    public Token LA(int i){
        return (Token)constraint.get(currentToken+i); //          tokenBuffer[currentToken+i];
    }/* end LA */
    
    public Token search(String var){
        Solution s1= (Solution)solutions.get(solIndex);
        int loc=-1,i=0;
        boolean  found=false;
        String concept,image=new String("");
        while ((!found) &&(i<s1.getConcepts().size())){
            concept =(String)s1.getConcepts().get(i);
            if (concept.equals(var)){
                found=true;
                loc=i;
                image =(String)s1.getImages().get(i);
            }
            i++;
        }
        Token t;
        if (image.length()>0)
            t =token(image);
        else t = new Token(" ", " ", 78, -1);
        return t;        
    }
    
    public Token token(String image){
        if(Debug)  System.out.println(" we are at token"+currentToken+image);
   
        Token token;
        String text,type;
        int tI,index;
        switch (image.charAt(0)){
            case '_':
                token= new Token('"'+image+'"', "BLANK", 65, -1);
                break;
            case '<':
                token= new Token('"'+image+'"', "IRI", 63, -1);
                break;
            /*case ' ': 
                      //token = " "; 
                      break;*/
            default:
                if ((image.charAt(0)=='"')&& (image.charAt(1)=='"') && (image.charAt(2)=='"')){
                    int loc= image.indexOf("\"\"\"", 3);
                     text=image.substring(0,loc+3);
                    type = image.substring(loc+3);
                    if (type.equals("<http://www.w3.org/2001/XMLSchema#decimal>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#float>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#double>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#integer>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#short>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#long>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#int>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#byte>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>")||
                        type.equals("INTEGER") ||type.equals("DOUBLE") ||
                        type.equals("DECIMAL"))
                         text=image.substring(3,loc);
                    tI=80;
                }
                else if ((image.charAt(0)=='\'')&& (image.charAt(1)=='\'') && (image.charAt(2)=='\'')){
                    int loc= image.indexOf("'''", 3);
                    text=image.substring(0,loc+3);
                    type = image.substring(loc+3);
                    if (type.equals("<http://www.w3.org/2001/XMLSchema#decimal>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#double>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#float>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#integer>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#short>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#long>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#int>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#byte>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>")||
                        type.equals("INTEGER") ||type.equals("DOUBLE") ||
                        type.equals("DECIMAL"))
                         text=image.substring(3,loc);
                    tI=79;
                }
                else if (image.charAt(0)=='"'){
                    int loc= image.indexOf('"', 1);
                    text=image.substring(0,loc+1);
                    type = image.substring(loc+1);
                    if (type.equals("<http://www.w3.org/2001/XMLSchema#decimal>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#double>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#float>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#integer>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#short>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#int>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#byte>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#long>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>")||
                        type.equals("INTEGER") ||type.equals("DOUBLE") ||
                        type.equals("DECIMAL"))
                         text=image.substring(1,loc);
                    tI=78;
                }
                else {
                    int loc= image.indexOf('\'', 1);
                    text=image.substring(0,loc+1);
                    type = image.substring(loc+1);
                    if (type.equals("<http://www.w3.org/2001/XMLSchema#decimal>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#double>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#float>") ||
                        type.equals("<http://www.w3.org/2001/XMLSchema#integer>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#short>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#long>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#int>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#byte>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>")||
                        type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>")||
                        type.equals("INTEGER") ||type.equals("DOUBLE") ||
                        type.equals("DECIMAL"))
                         text=image.substring(3,loc);
                    tI=77;
                }
                token= new Token(text, type, tI, -1);
                break;                
        }
                
        return token;
    }
 
    /* [41]  */
    public Token Var(String input){
        if(Debug)  
            System.out.println(" we are at Var"+input);
        if (LA(1).type.equals("VAR1")) 
            currentToken++;                //match("VAR1");
        else currentToken++;                //match("VAR2");
        //System.out.println(" we are at Var"+currentToken);
        Token image =new Token(search(LA(0).text+input));
        //image.type = datatype(LA(0)).text;
        //System.out.println(" we are at Var: "+image);
        if (image.text.indexOf("\"\"\"")>=0)
            image.text=image.text.substring(3, image.text.length()-3);
        if (image.type.indexOf("^^")>=0)
            image.type=image.type.substring(2);
        /*if (input.equals("SUM")) 
            image.type = "INTEGER";*/
        if(Debug)  
             System.out.println(" we exit at Var"+image.text+"$"+image.type);
        return image;
    }/* end Var */
    
    
       /* [43]  */
    public Token Expression(){
        Token exp;
        if(Debug)  System.out.println(" we are at Expression"+currentToken);
        exp =new Token(ConditionalOrExpression());  
        if(Debug)  System.out.println(" we exit at Expression:"+exp.text+"$");
        return exp;
    }/* end Expression */
    
    
    /* [44]  */
    public Token ConditionalOrExpression(){
        Token exp1,exp2;
        if(Debug)  System.out.println(" we are at CondionalOrEXpression"+currentToken);
        exp1=new Token(ConditionalAndExpression()); 
        while (LA(1).type.equals("SC_OR")){
            currentToken++;                //match("SC_OR");
            exp2=new Token(ConditionalAndExpression());
            exp1=LogicalOr(exp1, exp2);
        }
        return exp1;
    }/* end ConditionalOrExpression */
   
    public Token LogicalOr(Token c1,Token c2){
        Token t;
        if((effectiveBooleanValue(c1)) || (effectiveBooleanValue(c2)))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else 
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        return t;
    }
    
    /* [45]  */
    public Token ConditionalAndExpression(){
        Token exp1,exp2;
        if(Debug)  System.out.println(" we are at ConditionalAndExpression"+currentToken);
        exp1=new Token(ValueLogical());
        while (LA(1).type.equals("SC_AND")){
            currentToken++;                //match("SC_AND");
            exp2=new Token(ValueLogical());
            exp1=LogicalAnd(exp1, exp2);
        }
        return exp1;
    }/* end ConditionalAndExpression */
    
     public Token LogicalAnd(Token c1,Token c2){
        Token t;
        if((effectiveBooleanValue(c1)) && (effectiveBooleanValue(c2)))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else 
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        return t;
    }
   
    
    /* [46]  */
    public Token ValueLogical(){
        Token exp;
        if(Debug)  System.out.println(" we are at ValueLogical"+currentToken);
        exp=new Token(RelationalExpression());
        return exp;
    }/* end ValueLogical */
    
    
    /* [47]  */
    public Token RelationalExpression(){
        Token exp1,exp2;
        int op; 
        if(Debug) System.out.println(" we are at RelationalExpression"+currentToken);
        exp1= new Token(NumericExpression());
        if ((LA(1).type.equals("EQ"))||(LA(1).type.equals("NE"))||(LA(1).type.equals("GT"))
               ||(LA(1).type.equals("LT")) ||(LA(1).type.equals("LE")) ||(LA(1).type.equals("GE"))){
            op = LA(1).typeIndex;
            currentToken++;                //match(LA(1).type);
            exp2=new Token(NumericExpression());
            if (Debug)    System.out.println("we are at RelationalExpression$"+exp1.text+exp1.type+"$"+exp2.text+"$"+exp2.type);
            exp1 =relational(exp1,exp2,op);
        }
        return exp1;
    }/* end RelationalExpression */
    
     public Token relational(Token c1,Token c2,int op){
         return compare(c1, c2,op);
    }
     
     
   
     public Token compare(Token c1,Token c2,int op){
         if (Debug)    System.out.println("we are at compare(): op="+op);
         int comp,compType=0;
         if (Debug) 
             System.out.println("we are at compare():"+c1.text+" "+c2.text+" "+c1.type+"  "+" "+c2.type+"$");
         if(c1.text.indexOf("\"\"\"")>=0)
             c1.text=c1.text.substring(3, c1.text.length()-3);
         else if(c1.text.indexOf("\"")>=0)
             c1.text=c1.text.substring(1, c1.text.length()-1);
         if(c2.text.indexOf("\"\"\"")>=0)
             c2.text=c2.text.substring(3, c2.text.length()-3);
         else if(c2.text.indexOf("\"")>=0)
             c2.text=c2.text.substring(1, c2.text.length()-1);
         int pos = c1.type.indexOf("^^");
         if(pos>=0)
             c1.type=c1.type.substring(pos+2);
         pos = c2.type.indexOf("^^");
         if(pos>=0)
             c2.type=c2.type.substring(pos+2);
         
         if (c1.type.equals("STRING_LITERAL1") || c1.type.equals("STRING_LITERAL2")||
                 c1.type.equals("STRING_LITERAL_LONG1") || c1.type.equals("STRING_LITERAL_LONG2"))
                 c1.type="<http://www.w3.org/2001/XMLSchema#string>";
         if (c2.type.equals("STRING_LITERAL1") || c2.type.equals("STRING_LITERAL2")||
                 c2.type.equals("STRING_LITERAL_LONG1") || c2.type.equals("STRING_LITERAL_LONG2"))
                 c2.type="<http://www.w3.org/2001/XMLSchema#string>";
         
         
         if (c1.type.equals("<http://www.w3.org/2001/XMLSchema#boolean>"))
             if (c1.text.equals("0"))
                 c1.text="false";
         if (c1.type.equals("<http://www.w3.org/2001/XMLSchema#boolean>"))
             if (c1.text.equals("1"))
                 c1.text="true";
         if (c2.type.equals("<http://www.w3.org/2001/XMLSchema#boolean>"))
             if (c2.text.equals("0"))
                 c2.text="false";
         if (c2.type.equals("<http://www.w3.org/2001/XMLSchema#boolean>"))
             if (c2.text.equals("1"))
                 c2.text="true";
         if (Debug) 
             System.out.println("we are at compare():"+c1.text+"$"+c2.text+"$"+c1.type+"$"+"$"+c2.type+"$");
        if ((((!c1.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))&&
                 (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#float>")) &&
                 (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))&&
                 (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))&&
                 (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))&&
                (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))&&
                (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))&&
                (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))&&
                 (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))&&
                (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))&&
                 (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))&&
                (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))&&
                (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))&&
                (!c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))&&
                (!c1.type.equals("DOUBLE"))&& 
                 (!c1.type.equals("INTEGER"))&& 
                (!c1.type.equals("DECIMAL")))
                
                ||
                (((!c2.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))&&
                 (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))&&
                 (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#float>")) &&
                 (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))&&
                 (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))&&
                (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))&&
                (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))&&
                (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))&&
                 (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))&&
                (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))&&
                 (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))&&
                (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))&&
                (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))&&
                (!c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))&&
                (!c2.type.equals("DOUBLE")))&& 
                 (!c2.type.equals("INTEGER")))&& 
                ((!c2.type.equals("DECIMAL"))))){
             
             if (Debug) System.out.println("before"+c1.type+c2.type);
             //System.out.println("faisal");
             comp =c1.text.compareTo(c2.text);
             if ((c2.type.equals("QNAME"))||(c2.type.equals("QNAME_NS"))||(c2.type.equals("Q_IRI_REF")))
                 c2.type="IRI";
             if ((c1.type.equals("QNAME"))||(c1.type.equals("QNAME_NS"))||(c1.type.equals("Q_IRI_REF")))
                 c1.type="IRI";
             if (Debug) System.out.println("after:"+c1.type+c2.type);
             if (c1.type.equals("NON") ||c2.type.equals("NON"))
                 compType=0;
             else compType =c1.type.compareTo(c2.type);  //compType comparing the type
        }
        
         else if (((c1.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))||
                (c1.type.equals("DOUBLE")))|| ((c2.type.equals("DOUBLE"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#double>")))){
             Double v,v1,v2;
             //System.out.println("faisal");
             v1 = Double.parseDouble(c1.text);
             v2 = Double.parseDouble(c2.text);
             comp =v1.compareTo(v2);
             if (c1.type.equals("DOUBLE"))
                 c1.type="<http://www.w3.org/2001/XMLSchema#double>";
             if (c2.type.equals("DOUBLE"))
                 c2.type="<http://www.w3.org/2001/XMLSchema#double>";
             compType = c1.type.compareTo(c2.type);  //compType comparing the type
        }
        else if (((c1.type.equals("<http://www.w3.org/2001/XMLSchema#float>"))||
                (c1.type.equals("FLOAT")))|| ((c2.type.equals("FLOAT"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#float>")))){
             Float v,v1,v2;
             v1 = Float.parseFloat(c1.text);
             v2 = Float.parseFloat(c2.text);
             comp =v1.compareTo(v2);
             if (c1.type.equals("FLOAT"))
                 c1.type="<http://www.w3.org/2001/XMLSchema#float>";
             if (c2.type.equals("FLOAT"))
                 c2.type="<http://www.w3.org/2001/XMLSchema#float>";
             //c1.type.compareTo(c2.type);  //compType comparing the type
        }
        
         else if (((c1.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))||
                (c1.type.equals("DECIMAL")))|| ((c2.type.equals("DECIMAL"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>")))){
             //System.out.println("faisal");
             Float v,v1,v2;
             v1 = Float.parseFloat(c1.text);
             v2 = Float.parseFloat(c2.text);
             comp =v1.compareTo(v2);
             if (c1.type.equals("DECIMAL"))
                 c1.type="<http://www.w3.org/2001/XMLSchema#decimal>";
             if (c2.type.equals("DECIMAL"))
                 c2.type="<http://www.w3.org/2001/XMLSchema#decimal>";
             c1.type.compareTo(c2.type);  //compType comparing the type
        }
        else  if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))||
                
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))||
                 (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))||
                
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#long>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#long>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#int>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#int>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))||
                (c1.type.equals("INTEGER"))|| (c2.type.equals("INTEGER"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))){
             Integer v,v1,v2;
             //System.out.println(c1.text+c1.type);
             v1 = Integer.parseInt(c1.text); //Integer.parseInt(c1.text.substring(3, c1.text.length()-3))
             v2 = Integer.parseInt(c2.text);
             //System.out.println(v1+"    "+v2);
             comp =v1.compareTo(v2);
             if (c1.type.equals("INTEGER"))
                 c1.type="<http://www.w3.org/2001/XMLSchema#integer>";
             if (c2.type.equals("INTEGER"))
                 c2.type="<http://www.w3.org/2001/XMLSchema#integer>";
             c1.type.compareTo(c2.type);  //compType comparing the type
        }
        else comp=-1;   // To be verified later for this
         Token t;
         switch (op){
             case 1:
                 if ((comp==0)&&(compType==0))  //compType comparing the same type
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 2:
                 if (Debug)
                     System.out.println("NE: "+comp+" "+compType);                     
                 if ((comp!=0)||(compType!=0))  //compType comparing the same type
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 3:
                 if ((comp>0)&&(compType==0))  //compType comparing the same type
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 4:
                 if ((comp<0)&&(compType==0))  //compType comparing the same type
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 5:
                 if (Debug)
                     System.out.println("LE: "+comp+" "+compType);                     
                 if ((comp<=0)&&(compType==0))  //compType comparing the same type
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             default: // op=6
                 if ((comp>=0)&&(compType==0))  //compType comparing the same type
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
         }
         return t;
    }
   
   public Token compare(Double c1,Double c2,int op){
         int comp =c1.compareTo(c2);
         Token t;
         switch (op){
             case 1:
                 if (comp==0)
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 2:
                 if (comp!=0)
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 3:
                 if (comp>0)
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 4:
                 if (comp<0)
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             case 5:
                 if (comp<=0)
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
             default: // op=6
                 if (comp>=0)
                     t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
                 break;
         }
         return t;
    }
   
     
    /* [48]  */
    public Token NumericExpression(){
        Token exp;
        if(Debug)  System.out.println(" we are at NumericExpression"+currentToken);
        exp=new Token(AdditiveExpression());
        return exp;
    }/* end NumericExpression */
    
    /* [49]  */
    public Token AdditiveExpression(){
        Token exp1,exp2;
        int op;
        if(Debug)  System.out.println(" we are at AdditiveExpression"+currentToken);
        exp1=new Token(MultiplicativeExpression());
        while ((LA(1).type.equals("MINUS"))||(LA(1).type.equals("PLUS"))){
            op =LA(1).typeIndex;
            currentToken++;                //match(LA(1).type);
            exp2=new Token(MultiplicativeExpression());            
            exp1 = additive(exp1,exp2, op);
        }
        if(Debug)  System.out.println(" we exit AdditiveExpression"+exp1.text+"$"+exp1.type);
        return exp1;
    }/* end AdditiveExpression */
    
   
    public Token additive(Token c1,Token c2,int op){
        if(Debug)  
            System.out.println(" we are at Additive():"+c1.text+c1.type+c2.text+c2.type);
        if (c1.type.charAt(0)=='^') 
            c1.type=c1.type.substring(2);
        if (c2.type.charAt(0)=='^') 
            c2.type=c2.type.substring(2);
        if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))||
                (c1.type.equals("DOUBLE"))|| (c2.type.equals("DOUBLE"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))){
             Double v,v1,v2;
             v1 = Double.parseDouble(c1.text.substring(3,c1.text.length()-3));  //before c1.text  """1"""
             v2 = Double.parseDouble(c2.text.substring(3,c2.text.length()-3));  // c2.text
             switch (op){
                 case 12:
                     v=v1+v2;
                     break;
                 default:
                     v =v1-v2;
                     break;
             }
             c1.type="<http://www.w3.org/2001/XMLSchema#double>";             
             c1.text= v.toString();        
        }
         else if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#float>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#float>"))){
             Float v,v1,v2;
             v1 = Float.parseFloat(c1.text.substring(3,c1.text.length()-3));
             v2 = Float.parseFloat(c2.text.substring(3,c2.text.length()-3));
             switch (op){
                 case 12:
                     v=v1+v2;
                     break;
                 default:
                     v =v1-v2;
                     break;
             }
             c1.type="<http://www.w3.org/2001/XMLSchema#float>";
             c1.text= v.toString();        
        }
        else if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))||
                (c1.type.equals("DECIMAL"))|| (c2.type.equals("DECIMAL"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))){
             Float v,v1,v2;
             v1 = Float.parseFloat(c1.text.substring(3,c1.text.length()-3));
             v2 = Float.parseFloat(c2.text.substring(3,c2.text.length()-3));
             switch (op){
                 case 12:
                     v=v1+v2;
                     break;
                 default:
                     v =v1-v2;
                     break;
             }
             c1.type="<http://www.w3.org/2001/XMLSchema#decimal>";
             c1.text= v.toString();        
        }
        else  if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#long>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#long>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#int>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#int>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))||
                
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))||
                (c1.type.equals("INTEGER"))|| (c2.type.equals("INTEGER"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))){
             Integer v,v1,v2;
             v1 = Integer.parseInt(c1.text.substring(3,c1.text.length()-3));
             v2 = Integer.parseInt(c2.text.substring(3,c2.text.length()-3));
             switch (op){
                 case 12:
                     v=v1+v2;
                     break;
                 default:
                     v =v1-v2;
                     break;
             }
             c1.type="<http://www.w3.org/2001/XMLSchema#integer>";
             c1.text= v.toString();        
        }
        else{
            c1.type="error";
        }
        if(Debug) 
            System.out.println(" we exit Additive():"+c1.text+c1.type);
        return c1;
    }
   
    /* [50]  */
    public Token MultiplicativeExpression(){
        Token exp1,exp2;
        int op;
        if(Debug)  System.out.println(" we are at MultiplicativeExpression"+currentToken);
        exp1=new Token(UnaryExpression());
        while ((LA(1).type.equals("SLASH"))||(LA(1).type.equals("STAR"))){
            op = LA(1).typeIndex;
            currentToken++;                //match(LA(1).type);
            exp2= new Token(UnaryExpression());
            exp1 = multiplicative(exp1,exp2, op);
        }
        if(Debug)  System.out.println(" we exit MultiplicativeExpression"+exp1.text+"$"+exp1.type);
        return exp1;
    }/* end MultiplicativeExpression */
   
    public Token multiplicative(Token c1,Token c2,int op){
        if(Debug)  System.out.println(" we are at multiplicative():"+c1.text+c2.text);
        if (c1.type.charAt(0)=='^') 
            c1.type=c1.type.substring(2);
        if (c2.type.charAt(0)=='^') 
            c2.type=c2.type.substring(2);
        if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))||
                (c1.type.equals("DOUBLE"))|| (c2.type.equals("DOUBLE"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))){
             Double v,v1,v2;
             v1 = Double.parseDouble(c1.text.substring(3,c1.text.length()-3));
             v2 = Double.parseDouble(c2.text.substring(3,c2.text.length()-3));
             switch (op){
                 case 14:
                     v=v1*v2;
                     break;
                 default:
                     v =v1/v2;
                     break;
             }
             c1.type="<http://www.w3.org/2001/XMLSchema#double>";             
             c1.text= v.toString();        
        }
        else if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))||
                (c1.type.equals("DECIMAL"))|| (c2.type.equals("DECIMAL"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))){
             Float v,v1,v2;
             v1 = Float.parseFloat(c1.text.substring(3,c1.text.length()-3));
             v2 = Float.parseFloat(c2.text.substring(3,c2.text.length()-3));
             switch (op){
                 case 14:
                     v=v1*v2;
                     break;
                 default:
                     v =v1/v2;
                     break;
             }
             c1.type="<http://www.w3.org/2001/XMLSchema#decimal>";
             c1.text= v.toString();        
        }
        else  if ((c1.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#short>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#long>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#long>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#int>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#int>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#byte>"))||
                
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>"))||
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>"))||
                
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>"))||
                
                
                (c1.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>"))||
                (c1.type.equals("INTEGER"))|| (c2.type.equals("INTEGER"))||
                (c2.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))){
             Integer v,v1,v2;
             v1 = Integer.parseInt(c1.text.substring(3,c1.text.length()-3));
             v2 = Integer.parseInt(c2.text.substring(3,c2.text.length()-3));
             switch (op){
                 case 14:
                     v=v1*v2;
                     break;
                 default:
                     v =v1/v2;
                     break;
             }
             c1.type="<http://www.w3.org/2001/XMLSchema#integer>";
             c1.text= v.toString();        
        }
        else{
            c1.type="error";
        }
        if(Debug)  System.out.println(" we exit muliplicative():"+c1.text+c1.type);
         return c1;
    }
   
    /* [51]  */
    public Token UnaryExpression(){
        Token exp1=LA(1);
        if(Debug)  System.out.println(" we are at UnaryExpression "+currentToken);
        if (LA(1).type.equals("BANG")){
            currentToken++;                //match("BANG");
            exp1= new Token(PrimaryExpression());
            if (exp1.text.equals("error"))
                exp1.text="error";
            else if(effectiveBooleanValue(exp1))
                exp1.text="false";
            else exp1.text="true";
            exp1.type="<http://www.w3.org/2001/XMLSchema#boolean>";            
        }
        else if (LA(1).type.equals("PLUS")){
            currentToken++;                //match("PLUS");
            exp1= new Token(PrimaryExpression());
        }
        else if (LA(1).type.equals("MINUS")){
            currentToken++;                //match("MINUS");
            exp1=new Token(PrimaryExpression());
            Double value = Double.parseDouble(exp1.text.substring(3,exp1.text.length()-3));
            value *=-1;
            exp1.text =value.toString();
        }
        else if(!LA(1).type.equals("EOF"))
            exp1=new Token(PrimaryExpression());
        if(Debug)  System.out.println(" we exit UnaryExpression"+exp1.text+"$"+exp1.type);
        return exp1;
    }/* end UnaryExpression */
    
    
    /* [52]  */
    public Token  BuiltInCall(){
        Token exp=new Token();
        if(Debug)  System.out.println(" we are at BuiltInCall"+currentToken);
        if (LA(1).type.equals("STR")){
            currentToken++;                //match("STR");
            currentToken++;                //match("LPAREN");
            Token s=new Token(Expression());
            exp= new Token(str(s));
            currentToken++;                //match("RPAREN"); 
        }
        else if(LA(1).type.equals("LANG")){
            currentToken++;                //match("LANG");
            currentToken++;                //match("LPAREN");
            Token s=new Token(Expression());
            exp= new Token(lang(s));
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("LANGMATCHES")){
            currentToken++;                //match("LANGMATCHES");
            currentToken++;                //match("LPAREN");
            Token s1=new Token(Expression());
            currentToken++;                //match("COMMA");
            Token s2=new Token(Expression());
            exp= new Token(langmatches(s1,s2));
            currentToken++;                //match("RPAREN"); 
        }
        else if(LA(1).type.equals("DATATYPE")){
            currentToken++;                //match("DATATYPE");
            currentToken++;                //match("LPAREN");
            Token s=new Token(Expression());
            exp= new Token(datatype(s));
            currentToken++;                //match("RPAREN"); 
        }
        else if(LA(1).type.equals("BOUND")){
            currentToken++;                //match("BOUND");
            currentToken++;                //match("LPAREN");
            Token var=Var("");
            exp=new Token(bound(var));
            //System.out.println(" we are at BOUND"+exp);
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("ISURI")){
            currentToken++;                //match("ISURI");
            currentToken++;                //match("LPAREN");Token s;
            Token s=new Token(Expression());
            exp=new Token(isURI(s));
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("ISIRI")){
            currentToken++;                //match("ISIRI");
            currentToken++;                //match("LPAREN");
            Token s;
            s=new Token(Expression());
            exp=new Token(isIRI(s));
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("ISBLANK")){
            currentToken++;                //match("ISBLANK");
            currentToken++;                //match("LPAREN");
            Token s;
            s=new Token(Expression());
            exp=new Token(isBLANK(s));
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("ISLITERAL")){
            currentToken++;                //match("ISLITERAL");
            currentToken++;                //match("LPAREN");
            Token s;
            s=new Token(Expression());
            exp=new Token(isLITERAL(s));
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("SUM")){
            currentToken++;                //match("SUM");
            currentToken++;                //match("LPAREN");
            Token s;
            exp = new Token(Var("SUM"));
            //exp=new Token(isLITERAL(s));
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("AVG")){
            currentToken++;                //match("AVG");
            currentToken++;                //match("LPAREN");
            Token s;
            exp = new Token(Var("AVG"));
            //exp=new Token(isLITERAL(s));
            currentToken++;                //match("RPAREN");
        }
        else if(LA(1).type.equals("COUNT")){
            currentToken++;                //match("AVG");
            currentToken++;                //match("LPAREN");
            Token s;
            exp = new Token(Var("COUNT"));
            //exp=new Token(isLITERAL(s));
            currentToken++;                //match("RPAREN");
        }
        else exp=new Token(RegexExpression());
        if(Debug)
            System.out.println(" we exit UnaryExpression"+exp.text+"$"+exp.type);
        return exp;
    }/* end BuiltInCall */
    
    
    public int endOfLexical(String image){
        int location=-1;
        if((image.charAt(0)=='\'') && (image.charAt(1)=='\'') &&(image.charAt(2)=='\'')){
            location =image.indexOf("'''", 3);
            if(location>=0) 
                location+=3;
        }
        else  if((image.charAt(0)=='"') && (image.charAt(1)=='"') &&(image.charAt(2)=='"')){
            location =image.indexOf("\"\"\"", 3);
            if(location>=0) 
                location+=3;
        }
        else  if(image.charAt(0)=='\''){
            location =image.indexOf('\'', 1);
            if(location>=0) 
                location+=1;
        }
        else  if(image.charAt(0)=='"'){
            location =image.indexOf('"', 1);
            if(location>=0) 
                location+=1;
        }
        return location;
    }
    
    public String copy(int from, int to, String image){
        String s=new String("");
        if(from <0)
            from=0;
        if (to>image.length())
            to =image.length();
        for(int i=from;i<to;i++)
            s+=image.charAt(i);
        return s;
    }
    
    public Token str(Token image){
        if (Debug) System.out.println(" we are at str"+image);
        Token exp=new Token();
        exp.type="<http://www.w3.org/2001/XMLSchema#string>";
        int loc=endOfLexical(image.text);
        if (isLITERAL(image).text.equals("true"))            
            if ((loc>=0)|| (loc==-1)){     
                if (loc==-1) loc=image.text.length();            
                exp.text=copy(0, loc, image.text);
            }
            else exp.text="";
        else if (isIRI(image).text.equals("true")){
            // TO DO  return codepoint of URI see DRAFT
        }
        else exp.text="";
        if (Debug) System.out.println(" we are at str:"+exp+"$");
        return exp;
    }
    
    public Token lang(Token image){
        if (Debug) 
            System.out.println(" we are at lang"+image.text+"$"+image.type+"$");
        Token exp=image;
       /* exp.type ="LANGTAG";
        exp.typeIndex=17;
        int loc=endOfLexical(image.text);
        if (isLITERAL(image).equals("true"))            
            if ((loc>=0)&&(image.text.charAt(loc)=='@')){            
                loc+=1;
                exp.text="\""+copy(loc, image.text.length(), image.text)+"\"";
            }
            else exp.text="";
        else exp.text="";*/
        if (image.text.length()>0){
            if (isLITERAL(image).text.equals("true")){// && (image.type.charAt(0)=='@'))            
                if ((image.type.charAt(0)=='@')){   
                    exp.text=image.type.substring(1);
                    exp.type="NON";        
                }
                else {exp.text="";  
                      exp.type="NON"; 
                }
            }
            else {exp.text=image.text; exp.type="IRI";   }
        }
        else {exp.text=image.text; exp.type="IRI";   }
        if (Debug) 
            System.out.println(" we are at lang:"+exp.text+"$"+exp.type);
        return exp;
    }
    
    public Token langmatches(Token exp1,Token exp2){
        if (Debug) System.out.println(" we are at langmatches():"+exp1.text+"$"+exp2.text);
        Token exp=exp1;
        
        if (isLITERAL(exp1).text.equals("true") && isLITERAL(exp2).text.equals("true")){
                if (Debug) System.out.println(" we are at langmatches(): true");

                if (exp2.text.indexOf("\"")>=0)
                    exp2.text=exp2.text.substring(1, exp2.text.length()-1);
                if (exp2.text.equals("*")&&(!exp1.text.equals(""))){
                    exp.text="true";
                    exp.type="<http://www.w3.org/2001/XMLSchema#boolean>";
                    if (Debug) System.out.println(" we are at langmatches(): "+exp.text+exp.type);
                    return exp;
                }
                else if (exp1.text.toLowerCase().indexOf(exp2.text.toLowerCase())>=0){
                    exp.text="true";
                    exp.type="<http://www.w3.org/2001/XMLSchema#boolean>";
                    return exp;
                }
                else{
                    exp.text="false";
                    exp.type="<http://www.w3.org/2001/XMLSchema#boolean>";
                    return exp;
                }
        }
        else {
            if (Debug) System.out.println(" we are at langmatches(): error");
            exp.text="error";
            exp.type="<http://www.w3.org/2001/XMLSchema#boolean>";
            return exp;
        }
       // return exp;
    }
    
    public Token datatype(Token image){
        //System.out.println(" we are at datatype"+image.text+image.type);
        Token exp=image;
        /*int loc=endOfLexical(image);
        if (isLITERAL(image).equals("true"))            
            if ((loc>=0)&&(image.charAt(loc)=='^')){            
                loc+=2;
                exp=copy(loc, image.length(), image);
            }
            else exp="";
        else exp="";*/
        if (isLITERAL(image).text.equals("true"))
            if(image.type.charAt(0)=='^')            
                exp.text=image.type.substring(2);
            else exp.text=image.type;
        else exp.text="";        
        exp.type ="Q_IRI_REF";
        if (Debug)
            System.out.println(" we are at datatype:"+exp.text+"$");
        return exp;
    }
    
    
    public Token isIRI(Token image){
        //System.out.println(" we are at isIRI"+image);
        Token t;
        if (image.text.equals(""))            
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((image.text.indexOf('<')>0)&& ((image.text.charAt(image.text.indexOf('<')-1)=='\"')
                  ||(image.text.charAt(image.text.indexOf('<')-1)=='\'')))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
            
        else
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        return t;
    }
    
    public Token isURI(Token image){
        //System.out.println(" we are at isURI"+image);
        Token t;
        if (image.text.equals(""))            
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((image.text.indexOf('<')>0)&& ((image.text.charAt(image.text.indexOf('<')-1)=='\"')
                  ||(image.text.charAt(image.text.indexOf('<')-1)=='\'')))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
            
        else
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        return t;
    }
    
    public Token isBLANK(Token image){
        //System.out.println(" we are at isBLANK"+image.text);
        Token t;
        if (image.text.equals(""))            
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((image.text.indexOf('_')>0)&& ((image.text.charAt(image.text.indexOf('_')-1)=='\"')
                  ||(image.text.charAt(image.text.indexOf('_')-1)=='\'')))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
            
        else
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        return t;        
    }
    
    public Token isLITERAL(Token image){
        if (Debug) 
            System.out.println(" we are at isLITERAL"+image.text+image.type);
        Token t=new Token();
        t.text=image.text.toUpperCase();
        int size=image.text.length();
        //System.out.println(" we are at isLITERAL$"+image.type+"$");
        if (isIRI(image).text.equals("true"))
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if (isURI(image).text.equals("true"))
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if (isBLANK(image).text.equals("true"))
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        
        else if (isNumericType(image.text) ||
                (image.text.equals("true")) ||
                (image.text.equals("false")))
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else 
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        
        
        /*else if (image.type.equals("<http://www.w3.org/2001/XMLSchema#string>") ||
                (image.type.equals("NON")))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if (image.text.length()==0)
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((image.text.charAt(0)=='\'')&&(image.text.charAt(size-1)=='\''))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((image.text.charAt(0)=='"')&&(image.text.charAt(size-1)=='"'))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((image.text.charAt(0)<='9') && (image.text.charAt(0)>='0'))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((image.text.charAt(0)<='.'))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else if ((t.text.equals("TRUE")) || (image.text.equals("FALSE")))
            t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);*/
        if (Debug)
              System.out.println(" we are at isLITERAL"+t.text);
        return t;
    }
    
    public Token bound(Token varImage){
        //System.out.println(" we are at bound"+varImage);
        Token t=varImage;
        if (varImage.text.equals(" "))
            t=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        else t=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        return t;
    }
    
    public Token regex(Token exp1,Token exp2,Token exp3){
        //System.out.println(" we are at regexexpression"+image);
        Token exp=exp1;
        //TO DO @@ see the sparql draft
        return exp;
    }
    
    
    public Token regex(Token exp1,Token exp2){
        if (Debug) System.out.println(" we are at regexexpression"+exp1+exp2);
        Token exp=exp1;
        boolean result=false;;
        exp2.text=value(exp2.text);//       .substring(1, exp2.text.length()-1);
        // Create a pattern to match cat
    //    if ((isLITERAL(exp2).text.equals("true")) && (isLITERAL(exp1).text.equals("true"))){
            Pattern p = Pattern.compile(exp2.text);  //if not case sensitive exp2.text.toLowerCase()
            // Create a matcher with an input Token
            Matcher m = p.matcher(exp1.text);  ////if not case sensitive exp2.text.toLowerCase()
            StringBuffer sb = new StringBuffer();
            result = m.find();
            if (Debug) System.out.println(" we are at regexexpression"+result+m+p);
            if(result) exp=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
            else exp=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
            //to do see sparql draft
      /*  }
        else exp=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);*/
        if (Debug) System.out.println(" we are at regexexpression:"+result);
        return exp;
    }
    
    /* [53]  */
    public Token RegexExpression(){
        Token exp =new Token();
        if(Debug)  System.out.println(" we are at RegexExpression"+currentToken);
        currentToken++;                //match("REGEX");
        currentToken++;                //match("LPAREN");
        Token exp1=new Token(Expression());
        currentToken++;                //match("COMMA");
        Token exp2=new Token(Expression());
        if (LA(1).type.equals("COMMA")){
            currentToken++;                //match("COMMA");
            Token exp3=new Token(Expression());
            exp=new Token(regex(exp1,exp2,exp3));
        }
        else exp=new Token(regex(exp1,exp2));
        currentToken++;                //match("RPAREN");
              if(Debug)  System.out.println(" we are at RegexExpression"+exp);
        return exp; 
    }/* end RegexExpression */
    
    /* [54]  */
    public Token FunctionCall(){
        Token exp=new Token();
        Vector argList=new Vector();  //Object [] argList=null;
        Token iri=new Token();;
        if(Debug)  System.out.println(" we are at FunctionCall"+currentToken);
        iri=new Token(IRIref());
        argList=ArgList();
        FunctionCall funCall=new FunctionCall(iri, argList);  
        exp = (Token)funCall.getReturnValue();
        return exp;
    }/* end FunctionCall */
    
    
    /* [55]  */
    public Token IRIrefOrFunction(){
        Token exp=new Token();
        Token iri=new Token();;
        Vector argList=new Vector();   //Object [] argList=null;
        if(Debug)  System.out.println(" we are at IRIrefOrFunction"+currentToken);
        iri=new Token(IRIref());
        if ((LA(1).type.equals("NIL")) || (LA(1).type.equals("LPAREN")))
            argList=ArgList();
        else return iri;
        FunctionCall funCall=new FunctionCall(iri, argList);
        exp = (Token)funCall.getReturnValue();
        return exp;
    }/* end IRIrefOrFunction */
    
    /* [56]  */
    public Vector ArgList(){
        if(Debug)  System.out.println(" we are at ArgList"+currentToken);
        int i=0;
        Vector argList = new Vector();    //Object [] argList=null;
        if (LA(1).type.equals("NIL")){
            currentToken++;                //match("NIL");
            return argList;
        }
        else {
            currentToken++;                //match("LPAREN");
            argList.add(new Token(Expression()));   //argList[i]=Expression();
            while (LA(1).type.equals("COMMA")){
                currentToken++;                //match("COMMA");
                argList.add(new Token(Expression()));   //argList[i]=Expression();
            }
            currentToken++;                //match("RPAREN");
            return argList;
        }
    }/* end ArgList */
    
    
    /* [57]  */
    public Token BrackettedExpression(){
        Token exp;
        if(Debug)  System.out.println(" we are at BrackettedExpression"+currentToken);
        currentToken++;                //match("LPAREN");
         exp = new Token(Expression());
        currentToken++;                //match("RPAREN");
        //System.out.println(" we are at BrackettedExpression:"+exp+"$");
        return exp;
    }/* end BrackettedExpressuion */
    
    
    /* [58]  */
    public Token PrimaryExpression(){
        Token exp=new Token();
        if(Debug)  System.out.println(" we are at PrimaryExpression"+currentToken);
        if (LA(1).type.equals("LPAREN"))
            exp=new Token(BrackettedExpression());
        else if ((LA(1).type.equals("STR")) || (LA(1).type.equals("LANG")) || (LA(1).type.equals("LANGMATCHES"))
              || (LA(1).type.equals("DATATYPE")) || (LA(1).type.equals("BOUND"))|| (LA(1).type.equals("ISIRI"))
              || (LA(1).type.equals("ISURI")) || (LA(1).type.equals("ISBLANK")) || (LA(1).type.equals("ISLITERAL"))
              || (LA(1).type.equals("REGEX"))|| (LA(1).type.equals("SUM"))|| (LA(1).type.equals("AVG"))
              || (LA(1).type.equals("COUNT")))
            exp=BuiltInCall();
        else if (LA(1).type.equals("VAR1")|| (LA(1).type.equals("VAR2")))
            exp=Var("");
        else if (LA(1).type.equals("TRUE")|| (LA(1).type.equals("FALSE")))
            exp=BooleanLiteral();
        else if (LA(1).type.equals("BLANK_NODE_LABEL") || (LA(1).type.equals("ANON")))
            exp=BlankNode();
        else if (LA(1).type.equals("INTEGER") || (LA(1).type.equals("DECIMAL")) || (LA(1).type.equals("DOUBLE")))
            exp=NumericLiteral();
        else if ((LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2"))
                  || (LA(1).type.equals("STRING_LITERAL_LONG1")) || (LA(1).type.equals("STRING_LITERAL_LONG2")))
            exp=RDFLiteral();
        else exp=IRIrefOrFunction();
        
         if(Debug)  System.out.println(" we exit UnaryExpression"+exp.text+"$"+exp.type);
         return exp;
    }/* end PrimaryExpression */
    
    /* [59]  */
    public Token NumericLiteral(){
        if(Debug)  System.out.println(" we are at NumericLiteral"+currentToken);
        Token t=new Token();
        if (LA(1).type.equals("INTEGER")){
            currentToken++;                //match("INTEGER");
            t.type =LA(0).type; //"<http://www.w3.org/2001/XMLSchema#integer>"
            t.typeIndex=107;
        }
        else if (LA(1).type.equals("DECIMAL")){
            currentToken++;                //match("DECIMAL");
            t.type=LA(0).type;  // "<http://www.w3.org/2001/XMLSchema#decimal>"
            t.typeIndex=108;
        }
        else if (LA(1).type.equals("DOUBLE")){
            currentToken++;                //match("DOUBLE");
            t.type=LA(0).type;  //"<http://www.w3.org/2001/XMLSchema#double>"
            t.typeIndex=109;
        }
        else {
            currentToken++;                //match("DOUBLE");
            t.type="TYPE ERROR";  //"<http://www.w3.org/2001/XMLSchema#double>"
            t.typeIndex=-1;
        }
        /*else match("Numericliteral");*/
        t.text =value(LA(0).text);
        if(Debug)  System.out.println(" we exit NumericLiteral():"+t.text+"$"+t.type);
        return t;
    }/* end NumericLiteral */
    
  
    public String value(String image){
        String s=new String("");
        int location=-1;
        if((image.charAt(0)=='\'') && (image.charAt(1)=='\'') &&(image.charAt(2)=='\'')){
            location =image.indexOf("'''", 3);
            s=image.substring(3, location);
        }
        else  if((image.charAt(0)=='"') && (image.charAt(1)=='"') &&(image.charAt(2)=='"')){
            location =image.indexOf("\"\"\"", 3);
            s=image.substring(3, location);
        }
        else  if(image.charAt(0)=='\''){
            location =image.indexOf('\'', 1);
            s=image.substring(1, location);
        }
        else  if(image.charAt(0)=='"'){
            location =image.indexOf('"', 1);
            s=image.substring(1, location);
        }
        else {
            s=image;
        }
        if(Debug)  System.out.println(" we exit value:("+image+")="+s);
        return s;
    }
    
    /* [60]  */
    public Token RDFLiteral(){
        if(Debug)  System.out.println(" we are at RDFLiteral"+currentToken);
        Token exp=new Token();
        exp=string();
        if (LA(1).type.equals("LANGTAG")){
            exp.type=LA(1).text;
            currentToken++;                //match("LANGTAG");
        }
        else if (LA(1).type.equals("DTYPE")){
            exp.type=LA(1).text;
            currentToken++;                //match("DTYPE");
            exp.type+=IRIref().text;
        }
        if(Debug)  
            System.out.println(" we are at RDFLiteral()"+exp.text+exp.type);
         return exp;
    }/* end RDFLiteral */
    
    /* [61]  */
    public Token BooleanLiteral(){
        if(Debug)  System.out.println(" we are at BooleanLiteral"+currentToken);
        Token exp=new Token();
        if (LA(1).type.equals("TRUE")){
            currentToken++;                //match("TRUE");
            exp=new Token("true", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        }
        else if (LA(1).type.equals("FALSE")){
            currentToken++;                //match("FALSE");
            exp=new Token("false", "<http://www.w3.org/2001/XMLSchema#boolean>", 61, -1);
        }
         return exp;
    }/* end BooleanLiteral */
    
    /* [62]  */
    public Token string(){
        if(Debug)  System.out.println(" we are at string()"+currentToken);
        Token exp=new Token();
        exp.text=LA(1).text;
        if(Debug)  System.out.println(" we are at Token"+currentToken);
        if (LA(1).type.equals("STRING_LITERAL1")){
            currentToken++;                //match("Token_LITERAL1");
            exp.text=exp.text.substring(1,exp.text.length()-1);
            /*exp="\""+exp+"\"";        */
        }
        else if (LA(1).type.equals("STRING_LITERAL2")){
            currentToken++;                //match("Token_LITERAL2");
            exp.text=exp.text.substring(1,exp.text.length()-1);
            /*exp="\""+exp+"\"";        */
        }
        else if (LA(1).type.equals("STRING_LITERAL_LONG1")){
            currentToken++;                //match("Token_LITERAL_LONG1");
            exp.text=exp.text.substring(3,exp.text.length()-3);
            /*exp="\""+exp+"\"";        */
        }
        else if (LA(1).type.equals("STRING_LITERAL_LONG2")){
            currentToken++;                //match("Token_LITERAL_LONG2");
            exp.text=exp.text.substring(3,exp.text.length()-3);
           /* exp="\""+exp+"\"";        */
        }
        if (LA(1).text.charAt(0)=='^')   
            exp.type=LA(2).text;
        else exp.type = "NON"; 
        if(Debug)  
            System.out.println(" we are at string():"+exp.text+exp.type);
        return exp;
    }/* end string */
    
    /* [63]  */
    public Token IRIref(){
        if(Debug)  System.out.println(" we are at IRIref");
        Token exp=new Token();
        if (LA(1).type.equals("Q_IRI_REF")){
            currentToken++;                //match("Q_IRI_REF");            
            exp=LA(0);
        } 
        else exp=QName();
        //System.out.println(base1);
        return exp;
    }/* end IRIref */
    
    
    /* [64]  */
    public Token QName(){
        if(Debug)  System.out.println(" we are at QName"+currentToken);
        Token exp=new Token();
        if (LA(1).type.equals("QNAME")){
            currentToken++;                //match("QNAME");                       
        }        
        else if (LA(1).type.equals("QNAME_NS"))
            currentToken++;                //match("QNAME_NS");
        else {
            if (LA(1).type.equals("NCNAME_PREFIX"))
               currentToken++;                //match("NCNAME_PREFIX");
            currentToken++;                //match(":");
            if (LA(1).type.equals("NCNAME"))
                currentToken++;                //match("NCNAME");
        }
        //System.out.println(base1);
        exp.text=LA(0).text;
        exp.type=LA(0).type;
        return exp;
    }/* end QName */
    
    /* [65]  */
    public Token BlankNode(){
        if(Debug)  System.out.println(" we are at BlankNode"+currentToken);
        Token exp =LA(1);
        if (LA(1).type.equals("BLANK_NODE_LABEL"))
            currentToken++;                //match("BLANK_NODE_LABEL");
        else {
            currentToken++;                //match("ANON");           
        }
        return exp;
    }/* end BlankNode */

    
    
    private boolean Debug;
    private Vector constraints;
    private Vector constraint;
    private Vector solutions;
    private int constraintIndex;
    private int currentToken;
    private int solIndex;
    private Vector solutionsAfter;
}

