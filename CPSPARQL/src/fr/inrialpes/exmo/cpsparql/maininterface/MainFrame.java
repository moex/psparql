/*
 * Frame.java
 *
 * Created on December 20, 2005, 2:01 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;
import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import javax.swing.*;
//import javax.swing.j
import java.util.Vector;
import javax.swing.JMenuBar.*;
import javax.swing.border.*;
import javax.swing.JPanel.*;
import java.io.*;
import javax.swing.text.Highlighter;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.BadLocationException;
import org.w3c.dom.*;
import fr.inrialpes.exmo.cpsparql.query.*;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Graph;
import fr.inrialpes.exmo.cpsparql.findpathprojections.MultiGraph;
import fr.inrialpes.exmo.cpsparql.findpathprojections.FindPathProjections;
import fr.inrialpes.exmo.cpsparql.parser.QueryParser;
import fr.inrialpes.exmo.cpsparql.parser.QueryLexer;
import fr.inrialpes.exmo.cpsparql.parser.TokenBuffer;
import fr.inrialpes.exmo.cpsparql.turtleParser.TurtleDocumentParser;
/**
 *
 * @author faisal
 */
public class MainFrame extends JFrame{
    private String[] items1 = {"-- Insert Statement --", "insert BASE", 
                          "insert FROM clause", "insert FROM NAMED clause", "insert Triple Pattern",
                          "insert Group Graph Pattern", "insert UNION Graph Pattern", 
                          "insert FILTER clause", "insert OPTIONAL clause", "insert Named Graph pattern",
                          "insert Order By clause", "insert OFFSET clause", "insert LIMIT clause"};
        
    private String[] items2 = {"-- Query Template --", "ASK Template","CONSTRUCT Template", 
                          "DESCRIBE Template", "SELECT Template"};
        
    private String[] items3 = {"-- Insert Prefixe --", "foaf", "owl", "rdf","rdfs","xsd"};
    private boolean transformQuery = false;
    private boolean checkInverseOperator = true;
    private Query query;
    //private String query1 = new String("");
    private String textQuerySet=new String("");
    private JTextArea outputTextArea=new JTextArea("", 10,100 );
    private JTextArea queryTextArea = new JTextArea("" +
                " prefix foaf: <http://xmlns.com/foaf/0.1/>  \n" +
                " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  \n" +
                " select *  \n" +
                " from <rdfgraph.ttl>  \n" + 
                " where {  \n" +
                " ?s ?p ?o .  \n" +
                "}               ", 15,100);
    private int maxlength = 20;
   // private String transformedQuery;
    private String [] textQueries = new String[maxlength]; 
    int undoIndex = 0, startIndex=0;
    //private String currentTextQ ;
    private JButton undojbutton1;
    private JButton savejbutton1;
    private JButton savealljbutton1;
    private JButton clearjbutton1;
    private JButton stopRunjbutton1;
    private boolean stoprunning;
    private RunThread runthread;
    
    
    /** Creates a new instance of Frame */
    public MainFrame() {
        
        initialize();
      
        ActionListener exitlistener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
             // runthread.stop();
              System.exit(0);
          }  
        };
        
         
        ActionListener clearListener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              m();
              outputTextArea.setText("");
              queryTextArea.setText("");
              savejbutton1.setEnabled(true);
              savealljbutton1.setEnabled(true);
              clearjbutton1.setEnabled(false);
              //initUndoButton();
              //undoIndex++;
              //undojbutton1.setEnabled(true);
              
          }  
        };
        
        
        
        ActionListener runAction = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              checkInverseOperator = true;
              stopRunjbutton1.setEnabled(true);
              /*stoprunning = false;
              enableStopRunning();
              JDialog jd = new JDialog();
              jd.setLocation(200, 200);
              jd.setPreferredSize(new Dimension (200,100));
              jd.setTitle("Program Running");
              jd.pack();
              jd.setVisible(true);
              run();*/
              startingTheRunThread();//buildRun();
              savealljbutton1.setEnabled(true);
              //stopRunjbutton1.setEnabled(false);
              //jd.setVisible(false);
          }  
        };
      
        ActionListener rewriteAndRunActionListener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              checkInverseOperator = false;
              stopRunjbutton1.setEnabled(true);
              transformQuery = true;
              startingTheRunThread();//buildRun();
              savealljbutton1.setEnabled(true);
              //stopRunjbutton1.setEnabled(false);
              //jd.setVisible(false);
          }  
        };
        
         ActionListener stoprunlistener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              //stoprunning = true;//do stop running
              runthread.interrupt();
              //runthread = null;
              runthread.stop();
              //System.out.println("stop running ..."+stoprunning);
              stopRunjbutton1.setEnabled(false);
          }  
        };
        
        ActionListener cblistener1 = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              JComboBox cb = (JComboBox)ae.getSource();
    
              // Get the new item
              Object newItem = cb.getSelectedItem();
              insertTextToQueryTextArea((String)newItem);
              
              
          }  
        };
        
        ActionListener undolistener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              undoQueryTextArea();
              clearjbutton1.setEnabled(true);
              
          }  
        };
        
        ActionListener openlistener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              //do open
              String path = createFileDialog("Save",FileDialog.LOAD);
              openFile(path);
              if(!path.equals("nullnull")){
                  initUndoButton();
                  savejbutton1.setEnabled(true);
              }              
          }  
        };
        
        ActionListener savelistener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              //do saving
              String path = createFileDialog("Save",FileDialog.SAVE);
              saveFile(path,false);
              //System.out.println(path);
              if(!path.equals("nullnull")){
                  initUndoButton();
                  savejbutton1.setEnabled(false);
              }
              
          }  
        };
        
        ActionListener savealllistener = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              //do saving all
              
              String path = createFileDialog("Save All",FileDialog.SAVE);
              saveFile(path,true);
              //System.out.println(path);
              if(!path.equals("nullnull")){
                  savejbutton1.setEnabled(false);
                  savealljbutton1.setEnabled(false);
                  initUndoButton();
              }
              
          }  
        };
        
        ActionListener cblistener2 = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
              JComboBox cb = (JComboBox)ae.getSource();
    
              // Get the new item
              Object newItem = cb.getSelectedItem();
              insertTemplateToQueryTextArea((String)newItem);
              
              
          }  
        };
        
        JButton exitButton = new JButton("Exit"); 
        JButton clearJButton = new JButton("Clear");
        clearJButton.addActionListener(clearListener);
        JButton runJButton = new JButton("Run");
        runJButton.addActionListener(runAction);
        exitButton.addActionListener(exitlistener);
         
        // creating the run button 
        ImageIcon iicon1 = new ImageIcon("images/runImage.jpg");//createImageIcon();
        JButton runjbutton1 = new JButton(iicon1);
        runjbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        runjbutton1.setToolTipText("Run Query");
        runjbutton1.setIcon(iicon1);
        runjbutton1.addActionListener(runAction);
        // end 
       
        // creating the clear button 
        ImageIcon cleariicon1 = new ImageIcon("images/clearImage0.jpg");//createImageIcon();
        clearjbutton1 = new JButton(cleariicon1);
        clearjbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        clearjbutton1.setToolTipText("Clear Query");
        clearjbutton1.setIcon(cleariicon1);
        clearjbutton1.addActionListener(clearListener);
        // end 
        
       // creating the exit button 
        ImageIcon exitiicon1 = new ImageIcon("images/exitImage1.jpg");//createImageIcon();
        JButton exitjbutton1 = new JButton(exitiicon1);
        exitjbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        exitjbutton1.setToolTipText("Exit");
        exitjbutton1.setIcon(exitiicon1);
        exitjbutton1.addActionListener(exitlistener);
        // end  
        
       // creating the rewriteAndRun button 
        ImageIcon rewriteAndRuniicon1 = new ImageIcon("images/rewriteAndRunImage1.jpg");//createImageIcon();
        JButton rewriteAndRunjbutton1 = new JButton(rewriteAndRuniicon1);
        rewriteAndRunjbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        rewriteAndRunjbutton1.setToolTipText("Rewrire and Run");
        rewriteAndRunjbutton1.setIcon(rewriteAndRuniicon1);
        rewriteAndRunjbutton1.addActionListener(rewriteAndRunActionListener);
        // end 
       
        // creating the undo button 
        ImageIcon undoiicon1 = new ImageIcon("images/undoImage3.jpg");//createImageIcon();
        undojbutton1 = new JButton(undoiicon1);
         undojbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        undojbutton1.setToolTipText("Undo");
        undojbutton1.setIcon(undoiicon1);
        //undojbutton1.setDisabledSelectedIcon(undoiicon1);
        undojbutton1.setEnabled(false);
        undojbutton1.addActionListener(undolistener);
        // end 
       
        // creating the save button 
        ImageIcon saveiicon1 = new ImageIcon("images/saveImage.jpg");//createImageIcon();
        savejbutton1 = new JButton(saveiicon1);
         savejbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        savejbutton1.setToolTipText("Save Query");
        savejbutton1.setIcon(saveiicon1);
        //undojbutton1.setDisabledSelectedIcon(undoiicon1);
        //savejbutton1.setEnabled(false);
        savejbutton1.addActionListener(savelistener);
        // end 
        
        
        // creating the saveall button 
        ImageIcon savealliicon1 = new ImageIcon("images/saveallImage.jpg");//createImageIcon();
        savealljbutton1 = new JButton(savealliicon1);
         savealljbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        savealljbutton1.setToolTipText("Save Query and Output");
        savealljbutton1.setIcon(savealliicon1);
        //undojbutton1.setDisabledSelectedIcon(undoiicon1);
        savealljbutton1.setEnabled(false);
        savealljbutton1.addActionListener(savealllistener);
        // end 
        
        // creating the open button 
        ImageIcon openiicon1 = new ImageIcon("images/openImage.jpg");//createImageIcon();
        JButton openjbutton1 = new JButton(openiicon1);
        openjbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        openjbutton1.setToolTipText("Open Query From File");
        openjbutton1.setIcon(openiicon1);
        openjbutton1.addActionListener(openlistener);
        // end 
        
         // creating the stoprunning button 
        ImageIcon stopruniicon1 = new ImageIcon("images/stopRunImage.jpg");//createImageIcon();
        stopRunjbutton1 = new JButton(stopruniicon1);
        stopRunjbutton1.setPreferredSize(new java.awt.Dimension( 30, 25 ));
        //jclearbutton1.setBackground(Color.lightGray);
        stopRunjbutton1.setToolTipText("Stop Running");
        stopRunjbutton1.setIcon(stopruniicon1);
        stopRunjbutton1.setEnabled(false);
        stopRunjbutton1.addActionListener(stoprunlistener);
        stopRunjbutton1.addKeyListener(new MyKeyAdapter(this));
        // end 
        
        JComboBox cb1 =  createJComboBox(items1);
        JComboBox cb2 =  createJComboBox(items2);
        JComboBox cb3 =  createJComboBox(items3);
        
        cb1.addActionListener(cblistener1);
        cb2.addActionListener(cblistener2);
        cb3.addActionListener(cblistener1);
        
        /*JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(250, 30));*/
        //menuBar.add(cb);
        JPanel insertPanel = new JPanel();
        insertPanel.add(clearjbutton1);
        insertPanel.add(openjbutton1);
        insertPanel.add(savejbutton1);
        insertPanel.add(savealljbutton1);
        insertPanel.add(undojbutton1);
        insertPanel.add(rewriteAndRunjbutton1);
        insertPanel.add(runjbutton1);
        insertPanel.add(stopRunjbutton1);
        insertPanel.add(exitjbutton1);
        insertPanel.add(cb1,BorderLayout.EAST);
        insertPanel.add(cb2,BorderLayout.WEST);
        insertPanel.add(cb3);
        
        //testing whether the query is changed
        //if(!queryTextArea.getText().equals(currentTextQ))
        //    System.out.println("changed");
        //end
        
        //JPanel wherePanel = new JPanel();
        //wherePanel.add(queryTextArea,BorderLayout.SOUTH);
        //wherePanel.add(insertPanel,BorderLayout.CENTER);
        JScrollPane queryScrollP = new JScrollPane(queryTextArea);//wherePanel);
        queryScrollP.setBorder(new TitledBorder("Query"));
        
        //JPanel compilerPanel = new JPanel();
        //compilerPanel.add(outputTextArea);
        outputTextArea.setEditable(false);
        JScrollPane outputScrollP = new JScrollPane(outputTextArea);//compilerPanel);
        outputScrollP.setBorder(new TitledBorder("Output"));
        
        //Create a split pane for the change log and the text area.
        JSplitPane splitPane = new JSplitPane(
                                       JSplitPane.VERTICAL_SPLIT,
                                       queryScrollP, outputScrollP);
        splitPane.setOneTouchExpandable(true);

        
        
        JPanel controlPanel = new JPanel();
        controlPanel.add(exitButton,BorderLayout.EAST);
        controlPanel.add(clearJButton,BorderLayout.WEST);
        controlPanel.add(runJButton);
        
        JPanel  outerPanel = new JPanel(new BorderLayout());
        //outerPanel.add(scroll2, BorderLayout.CENTER);
        //outerPanel.add(outputScroll, BorderLayout.SOUTH);
        outerPanel.add(splitPane, BorderLayout.CENTER);
        outerPanel.setVisible(true);
        EmptyBorder emptyBorder;
        emptyBorder = new EmptyBorder(5,5,5,5);
        //Font font = new Font("CPSPARQL Query Language",1,14);
        outerPanel.setBorder(new TitledBorder(""));
        
        
        JMenuBar menuBar = new JMenuBar();
        menuBar.setBackground(Color.lightGray);
        //java.net.URL imgURL = mainFrame.class.getResource("/images/runImage");
        JPanel buttonsPanel =  createPanelOfButtons();
        //menuBar.add(buttonsPanel);
        
        
        outerPanel.add(insertPanel,BorderLayout.NORTH);
        //outerPanel.add(cb2,BorderLayout.NORTH);
          this.setSize(825,600);       
         setTitle("CPSPARQL Query Language");
         //resize(600,600);
         //add(buttonsPanel,BorderLayout.NORTH);
         add(outerPanel, BorderLayout.CENTER);
         //add(controlPanel,"South");
         setVisible(true);//show();
    }
    
    public void enableStopRunning(){
        stopRunjbutton1.setEnabled(true);
         System.out.println("stop running button enabled ...");
    }
    
    public void startingTheRunThread(){
        runthread = new RunThread(this);
        runthread.start();      
    }
    
    public void run(){
        stopRunjbutton1.setEnabled(true);
        //MyKeyAdapter myKeyA = new MyKeyAdapter(this);
        while(!stoprunning){
            System.out.println("running ...");
        }
         System.out.println("stop running ...");
    }
            
    public void initUndoButton(){
        undojbutton1.setEnabled(false);
        undoIndex = 0;
        startIndex=0;
        textQueries[0]=queryTextArea.getText();
        //System.out.println(textQueries[0]);
    }
    
    public JPanel createPanelOfButtons(){
        ActionListener runAction = new ActionListener(){
          public void actionPerformed(ActionEvent ae){
                 buildRun();
              
              
          }  
        };
        
        ImageIcon iicon1 = new ImageIcon("images/runImage.jpg");//createImageIcon();
        JButton jbutton1 = new JButton(iicon1);
        jbutton1.setPreferredSize(new java.awt.Dimension( 30, 30 ));
        jbutton1.setToolTipText("Run Query");
        jbutton1.setIcon(iicon1);
        jbutton1.addActionListener(runAction);
       
        //JButton jbutton2 = new JButton("Edit");
        
        JPanel jp = new JPanel();               
        jp.add(jbutton1);
        //jp.add(jbutton2);
        return jp;
    }
    
    public ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = null;//getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } 
        else {
            System.err.println("Couldn't find file: " + imgURL);
            return null;
        }
    }
    
    public JComboBox createJComboBox(String [] items){
        // Create a read-only combobox
        JComboBox cb = new JComboBox(items);
        cb.setSize(2,1);
        cb.setBounds(1, 1, 1, 1);
        cb.setBackground(Color.white);

        // Get current value
        Object obj = cb.getSelectedItem(); // item1

        return cb;
    } 
    
    public void insertTextToQueryTextArea(String choice){
        removeHighlights(queryTextArea);
        String query1 = queryTextArea.getText();
        String textToBeInserted = new String(choice);
        int position =queryTextArea.getCaretPosition();
        //System.out.println(position);
        String firstPart = query1.substring(0, position);
        String secondPart = " "+ query1.substring(position, queryTextArea.getDocument().getLength());
        textToBeInserted = textOfChoice(choice);
        String newLine ="";
        if(secondPart.charAt(0)!='\n')
            newLine ="\n";
        queryTextArea.setText(firstPart+ "\n"+textToBeInserted+newLine+secondPart);
    }
    
    public String createFileDialog(String title,int mode){
        //System.out.print("creating");
        FileDialog fileDialog = new FileDialog(this,title,mode);
        fileDialog.setVisible(true);
        String directory = fileDialog.getDirectory();
        String selectedFile = fileDialog.getFile();
        String fullpath = directory+selectedFile;
        //System.out.println(directory+selectedFile);
        return fullpath;
    }
    
    public String openFile(String path) {
        File turtleFile = new File(path);
        
        InputStream fr = null;
        try{fr = new FileInputStream(turtleFile);}catch(FileNotFoundException e1){}
        fr = new BufferedInputStream(fr);
        StringBuffer s = new StringBuffer();
        try{                
                long lg = turtleFile.length(), i = 0;
               
                while(i++ < lg){
                    s.append((char) fr.read());
                }
                fr.close();               
        }catch(IOException e){System.out.println(e);}
        //System.out.println("end reading the file");
        queryTextArea.setText(s.toString());
        return s.toString();
    }
    
    public void saveFile(String fullpath,boolean all){
        try{
            FileOutputStream fo=new FileOutputStream(fullpath);
            PrintStream ps=new PrintStream(fo);
            ps.println(queryTextArea.getText());
            if(all){
                ps.println("______________________________________________________________\n"
                        +outputTextArea.getText());
            }
            ps.close();
            fo.close();
        }
        catch(Exception ex){}
    }
    
    
    public String textOfChoice(String choice){
        m();
        String textToBeReturned = new String("");
        if(choice.equals("insert BASE"))
            textToBeReturned = " BASE <examplebase.com/mybase>";
        else if (choice.equals("insert FROM clause"))
            textToBeReturned ="FROM <http://www.inrialpes.fr/exmo/people/alkhateeb/RDFgraphs/turtle1.txt>";
        else if(choice.equals("insert FROM NAMED clause"))
                textToBeReturned =" FROM NAMED <http://www.inrialpes.fr/exmo/people/alkhateeb/RDFgraphs/turtle1.txt>";
        else if (choice.equals("insert Triple Pattern"))
                textToBeReturned ="\t ?s1 ?p1 ?o1 . ";
        else if (choice.equals("insert Group Graph Pattern"))
                textToBeReturned ="\t { ?s1 ?p1 ?o1 . }";
        else if (choice.equals("insert UNION Graph Pattern"))
            textToBeReturned ="\t UNION \n \t { ?s1 ?p1 ?o1 . }";
        else if (choice.equals("insert FILTER clause"))
            textToBeReturned =" \t FILTER ( ?s = <ex/Albania>  ) ";
        else if (choice.equals("insert OPTIONAL clause"))
            textToBeReturned =" \t OPTIONAL { ?s ?p1 ?o1 . }";
        else if (choice.equals("insert Named Graph pattern"))
            textToBeReturned =" \t GRAPH ?s { ?s1 ?p1 ?o1.  }";
        else if (choice.equals("insert Order By clause"))
            textToBeReturned =" ORDER BY ?s";
        else if (choice.equals("insert OFFSET clause"))
            textToBeReturned =" OFFSET 3";
        else if (choice.equals( "insert LIMIT clause"))
            textToBeReturned =" LIMIT 5";
        else if (choice.equals("foaf"))
            textToBeReturned =" PREFIX foaf: <http://xmlns.com/foaf/0.1/>";
        else if (choice.equals("owl"))
            textToBeReturned =" PREFIX owl: <http://www.w3.org/2002/07/owl#>";
        else if (choice.equals("rdf"))
            textToBeReturned =" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>";
        else if (choice.equals("rdfs"))
            textToBeReturned =" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>";
        else if (choice.equals("xsd"))
            textToBeReturned = " PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>";
        else  textToBeReturned ="";
        return textToBeReturned;
    }
    
    
    public void insertTemplateToQueryTextArea(String text){
        m();
        removeHighlights(queryTextArea);
        if(text.equals("ASK Template"))
            queryTextArea.setText(" ASK \n FROM <localfile:testRDFGraph#10.txt> \n " +
                    "WHERE {?s ?p ?o }");
        else if(text.equals("CONSTRUCT Template"))
            queryTextArea.setText(" CONSTRUCT {?s ?p ?o } \n FROM <localfile:testRDFGraph#10.txt> \n " +
                    "WHERE {?s ?p ?o }");
        else if(text.equals("DESCRIBE Template"))
            queryTextArea.setText(" DESCRIBE ?s } \n FROM <localfile:testRDFGraph#10.txt> \n " +
                    "WHERE {?s ?p ?o }");
        else if(text.equals("SELECT Template"))
            queryTextArea.setText(" SELECT ?s ?p ?o \n FROM <localfile:testRDFGraph#10.txt> \n " +
                    "WHERE {?s ?p ?o }");
    }
    
    public void undoQueryTextArea(){
        //System.out.println("undo"+" startIndex="+startIndex+" undoIndex="+undoIndex);
        if((undoIndex > 0)&& (undoIndex !=startIndex)){
          //System.out.println("undo"+undoIndex+textQueries[0]);
          String previousState = textQueries[undoIndex--];
          queryTextArea.setText(previousState);  
          //System.out.println("1"+" startIndex="+startIndex+" undoIndex="+undoIndex);
        }
        else if (undoIndex == startIndex){
            undojbutton1.setEnabled(false);
            //System.out.println("2"+" startIndex="+startIndex+" undoIndex="+undoIndex);
        }
        else {
            undoIndex = maxlength-1;
            //System.out.println("3"+" startIndex="+startIndex+" undoIndex="+undoIndex);
        }
        if(undoIndex == startIndex){
            //System.out.println("not undo:"+undoIndex);
            undojbutton1.setEnabled(false);
        }
    }
    
    public void m(KeyEvent e){
        stoprunning = true;
        StringBuffer stringB = new StringBuffer(queryTextArea.getText());
        //System.out.println("modified: "+stringB.toString());
        clearjbutton1.setEnabled(true);
        savejbutton1.setEnabled(true);
        //savejbutton1.setEnabled(true);
        undojbutton1.setEnabled(true);
        if(undoIndex < (maxlength-1))
            if(undoIndex > (startIndex-1))
                textQueries[++undoIndex] = stringB.toString();
            else {
            //undoIndex++;
            textQueries[++undoIndex] = stringB.toString();
            if(startIndex < (maxlength-1))
                startIndex++;
            else startIndex = 0;
            }
        else {
            undoIndex = 0;
            textQueries[undoIndex] = stringB.toString();
            if(startIndex == 0)
                startIndex++;
        }
                
        //textQueries.add(++undoIndex,queryTextArea.getText());
        //if (e.getKeyChar() == 'A')
        //System.out.println("Mod");
        if (stringB.toString().equals(""))
            clearjbutton1.setEnabled(false);
        
    }
    
    public void m(){
        StringBuffer stringB = new StringBuffer(queryTextArea.getText());
        //System.out.println("modified: "+stringB.toString());
        clearjbutton1.setEnabled(true);
        savejbutton1.setEnabled(true);
        //savejbutton1.setEnabled(true);
        undojbutton1.setEnabled(true);
        if(undoIndex < (maxlength-1))
            if(undoIndex > (startIndex-1))
                textQueries[++undoIndex] = stringB.toString();
            else {
            //undoIndex++;
            textQueries[++undoIndex] = stringB.toString();
            if(startIndex < (maxlength-1))
                startIndex++;
            else startIndex = 0;
            }
        else {
            undoIndex = 0;
            textQueries[undoIndex] = stringB.toString();
            if(startIndex == 0)
                startIndex++;
        }
                
        //textQueries.add(++undoIndex,queryTextArea.getText());
        //if (e.getKeyChar() == 'A')
        //System.out.println("Mod");
        if (stringB.toString().equals(""))
            clearjbutton1.setEnabled(false);
        
    }
    

    
    public void initialize(){
       for(int i=0;i<maxlength;i++)
             textQueries[i] = "";
        query = new Query();
        textQueries[0]=queryTextArea.getText(); //undoIndex++;
        //currentTextQ = queryTextArea.getText();
         queryTextArea.addKeyListener(new MyKeyAdapter(this));
        //System.out.println(currentTextQ);
          
    }

    public void buildRun(){
        removeHighlights(queryTextArea);
        String query1 = queryTextArea.getText();    
        //checkInverseOperator = false;
        PSPARQLEngine mainClass = new PSPARQLEngine(query1,transformQuery,checkInverseOperator); 
        outputTextArea.setText(mainClass.getResult());
        queryTextArea.setText(mainClass.getTransformedQuery());
        //addHighlighting(p.getErrorNom());
    }
    
    public void buildRunOld(boolean qContainIverseOperator, boolean queryContainInverse){
                removeHighlights(queryTextArea);
                TokenBuffer finalTokenBuffer = new TokenBuffer(0);
                String query1 = queryTextArea.getText();    
                int i=queryTextArea.getText().length();                
                query1+='\0';
                QueryLexer lexer = new QueryLexer(query1);
                

                 QueryParser p = new QueryParser(lexer.getTokenBuffer().tokenBuffer,false,true,transformQuery,queryContainInverse);                        
                 textQuerySet = p.getTextGraphSet();                
                 query = p.getQuery();
                
                 outputTextArea.setText("");
                 String str1="",str="";                                 
                 
                 Vector v =new Vector(p.getErrorsExceptions()); 
                 if (v.isEmpty()){
                     outputTextArea.setText("");
                     str="";                                 
                     GraphPattern [] GPS=query.getGraphSet().getGraphPattern();
                     int gpNom = query.getGraphSet().getGraphPatternNom();
                     MultiGraph graphG=loadRDFGraphs(qContainIverseOperator);
    
                 
                     //String graphG =getGraphName();
                     str=queryFormat(query.Type(), GPS, gpNom, graphG); 
                     
                 }
                 else{
                     str+="QUERY NOT CORRECT";
                     for (int count=0;count<v.size();count++)
                         str+=(String)v.get(count)+'\n';
                 }
                 StringBuffer stringB = new StringBuffer(str);
                 outputTextArea.setText(stringB.toString());
                 addHighlighting(p.getErrorNom());

    }
    
    
    public String refine(String q, int leng){
        int i=0;
        String str=new String("");
        while(i<=leng){            
            if (q.charAt(i)!='\n')
                str+=q.charAt(i++);
            else i++;
                
        }
        return str;
    }   
    
    public String getGraphName(){
        if (query.getFromIRIs().size()>=1)
            return (String)query.getFromIRIs().get(0);
        else return "<graphG1.txt>";
    }
    
    
    
    public MultiGraph loadRDFGraphs(boolean qContainIverseOperator){
        Vector rdfGraphsURIs = new Vector();
        if (query.getFromIRIs().size()>=1)
            for(int i=0;i<query.getFromIRIs().size();i++)
                rdfGraphsURIs.add((String)query.getFromIRIs().get(i));
        else rdfGraphsURIs.add("<graphG1.txt>");
        TurtleDocumentParser docParser = new TurtleDocumentParser(rdfGraphsURIs,qContainIverseOperator);
        return docParser.getGraph();
    }
    
    
    public String queryFormat(int type,GraphPattern [] GPS, int gpNom, MultiGraph G){
        String str=new String("");
        Graph H;
        GraphPattern GP;
        FindPathProjections fpbc;
        Vector solutions=new Vector();
        int limit=query.Limit();  
        int offset=query.Offset(); 
        Vector constraints;
        Vector solAfter;
        //TurtleDocumentParser docParser = new TurtleDocumentParser(graphG); // to be deleted
        for(int i=0;i<=gpNom;i++){
            GP=GPS[i];
            H = GP.getDefaultGraph();             
            fpbc=new FindPathProjections(H,G);   
            
            //applying constraints here
            constraints = H.getConstraints();
            if (constraints.size()!=0){
                solAfter=new Vector(applyingConstraints(fpbc.Solutions(),constraints));
                solutions.addAll(solAfter);                         
            }
            else 
                solutions.addAll(fpbc.Solutions());                         
        }
        
        // this method will apply the order criteria
        if (query.getOrder())
            solutions =orderBy(solutions);   
        
        
        
        ResultForm RF = new ResultForm(solutions,limit,offset,query); 
        str= RF.Result();     
        return str;
    }
    
    public Vector applyingConstraints(Vector sols,Vector cons){
        Vector solutionsAfter;
        ApplyConstraint applyConstraints = new ApplyConstraint(sols, cons);
        solutionsAfter= applyConstraints.getSolutionsAfter(); 
        return solutionsAfter;
    }
    
    public Vector orderBy(Vector sols){
        ApplyOrderBy AOBy = new ApplyOrderBy(sols,query.getOrderConditions());
        return  AOBy.getSolutions();
    }
    
    public void addHighlighting(int errors){
         File errorIndexFile = new File("index.txt");
         FileReader inerrorIndex;
         BufferedReader in;
         if (errors >0){
                    try{
                       inerrorIndex = new FileReader(errorIndexFile);
                       in = new BufferedReader(inerrorIndex);    
                       int pos1,pos2;
                       String str;
                      /* while((str=in.readLine()) !=null){*/
                           str=in.readLine();
                           pos1=Integer.parseInt(str);
                           str=in.readLine();
                           pos2=Integer.parseInt(str);
                          /* System.out.println("errors+" "+pos1+"  "+pos2);*/
                           
                           try {
                               Highlighter hilite = queryTextArea.getHighlighter();
                               // An instance of the private subclass of the default highlight painter
                               Highlighter.HighlightPainter myHighlightPainter = new MyHighlightPainter(Color.red);
                               hilite.addHighlight(pos1, pos2, myHighlightPainter);                                                    
                           } catch (BadLocationException e) { }

                      /*}*/
                     inerrorIndex.close();
                    } catch (IOException e) {System.out.println(e); } 
                    
         }

    }
    
    // Removes only our private highlights
    public void removeHighlights(JTextArea textComp) {
        Highlighter hilite = textComp.getHighlighter();
        Highlighter.Highlight[] hilites = hilite.getHighlights();
    
        for (int i=0; i<hilites.length; i++) {
            if (hilites[i].getPainter() instanceof MyHighlightPainter) {
                hilite.removeHighlight(hilites[i]);
            }
        }
    }

     // A private subclass of the default highlight painter
    class MyHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {
        public MyHighlightPainter(Color color) {
            super(Color.red);
        }
    }
}
