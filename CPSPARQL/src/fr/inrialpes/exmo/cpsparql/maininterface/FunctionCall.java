/*
 * FunctionCall.java
 *
 * Created on April 3, 2006, 10:31 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;

import fr.inrialpes.exmo.cpsparql.parser.Token;
import java.util.Vector;
import java.lang.reflect.*;


/**
 *
 * @author faisal
 */
public class FunctionCall {
    
    /** Creates a new instance of FunctionCall */
    public FunctionCall(Token methodName, Vector argList) {
        methodName.text="sum";
        init(methodName, argList);
    }
    
    /** Creates a new instance of FunctionCall */
    public FunctionCall() {
    }
    
    
    public void init(Token methodName, Vector argList){
        Debug =false;
        exceptions= new Vector();
        object = new Object();
        Class [] paramTypes = getParamTypes(argList);
        try{
            FunctionCall o= new FunctionCall();
            object=invokeMethod(o,methodName.text,argsList,paramTypes);
        }
        catch(Exception e){
            if (Debug) System.out.println(" Function not Found...."+methodName.text);
            Token t=new Token();
            t.text = " Function not Found...."+methodName.text;
            object =t;
            exceptions.add(e);
        }
    }
    
    public Class [] getParamTypes(Vector argList){
        if (Debug) System.out.println(" we are at getParamTypes()....");
        Class [] paramTypes = null;
        Token token;
        Object [] arg=null;
        if (!argList.isEmpty()){
            paramTypes = new Class[argList.size()];
            arg = new Object [argList.size()];
            for (int i=0;i < argList.size(); i++){
                token = (Token)argList.get(i);
                if ((token.type.equals("<http://www.w3.org/2001/XMLSchema#integer>"))|| 
                        token.type.equals("INTEGER")){
                    if (Debug) System.out.println(" INTEGER parameter....");
                    paramTypes[i] = Integer.TYPE;
                    try{
                        arg[i] = Integer.parseInt(token.text);
                    }
                    catch(NumberFormatException e){
                        exceptions.add(e);
                    }
                    
                }
                else if ((token.type.equals("<http://www.w3.org/2001/XMLSchema#decimal>"))|| 
                        token.type.equals("DECIMLAL")){
                    if (Debug) System.out.println(" DECIMAL parameter....");
                    paramTypes[i] = Float.TYPE;                    
                    try{
                        arg[i] = Float.parseFloat(token.text);
                    }
                    catch(NumberFormatException e){
                        exceptions.add(e);
                    }
                }
                else if ((token.type.equals("<http://www.w3.org/2001/XMLSchema#double>"))|| 
                        token.type.equals("DOUBLE")){
                    if (Debug) System.out.println(" DOUBLE parameter....");
                    paramTypes[i] = Double.TYPE;
                    try{
                        arg[i] = Double.parseDouble(token.text);
                    }
                    catch(NumberFormatException e){
                        exceptions.add(e);
                    }
                }
                
                else if ((token.type.equals("<http://www.w3.org/2001/XMLSchema#boolean>"))|| 
                        token.type.equals("TRUE") || token.type.equals("FALSE")) {
                    if (Debug) System.out.println(" BOOLEAN parameter....");
                    paramTypes[i] = Boolean.TYPE;
                    try{
                        arg[i] = Boolean.getBoolean(token.text);                    
                    }
                    catch(NumberFormatException e){
                        exceptions.add(e);
                    }
                }
                else{
                    if (Debug) System.out.println(" STRING parameter....");
                    paramTypes[i] = String.class;
                    arg[i] = token.text;
                }
            }
        }
        argsList =arg;
        if (Debug) System.out.println(" we exit getParamTypes()....");
        return paramTypes;
    }
    
    public Object invokeMethod(Object o, String methodName, Object [] argsList,Class [] paramTypes) throws Exception {
        if (Debug) System.out.println(" we are at invokeMethod()...."+methodName+argsList.length+argsList[0].toString());
        if (Debug) System.out.println(" we are at invokeMethod()...."+paramTypes.length+paramTypes[0].toString());
        
        Method m = o.getClass().getMethod(methodName, paramTypes);
        if (Debug) System.out.println(" we are at invokeMethod()...."+m.toString());
        Object ob = m.invoke(object,argsList);
        if (Debug) System.out.println(" we are at invokeMethod()...."+ob.toString());
        return ob;
    }
    
    public Token getReturnValue(){
        if (Debug) System.out.println(" we are at getreturnValue()....");
        return (Token)object;
    }
    
    public Vector getExceptions(){
        return exceptions;
    }
    
   
    public Object sum(int i){
        Object ob =null;
        System.out.println(i);
        return ob;
    }
    
    
    
    private Object object;
    private Object[] argsList;
    private Vector exceptions;
    private boolean Debug;
}
