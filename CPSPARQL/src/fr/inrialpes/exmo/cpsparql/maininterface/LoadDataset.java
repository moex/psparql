/*
 * LoadDataset.java
 *
 * Created on February 6, 2007, 10:10 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;

import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Dataset;
import fr.inrialpes.exmo.cpsparql.turtleParser.TurtleDocumentParser;

/**
 *
 * @author faisal
 */
public class LoadDataset {
    
    /** Creates a new instance of LoadDataset */
    public LoadDataset(Vector fromIris, Vector fromNamedIris) {
        dataset = new Dataset();
        loadDefaultGraph(fromIris,false);
        loadNamedGraph(fromNamedIris,false);
    }
    
    /** Creates a new instance of LoadDataset */
    public LoadDataset(Vector fromIris, Vector fromNamedIris, boolean qContainInverseOpertor) {
        dataset = new Dataset();
        loadDefaultGraph(fromIris,qContainInverseOpertor);
        loadNamedGraph(fromNamedIris,qContainInverseOpertor);
    }
    
    public void loadDefaultGraph(Vector fromIris, boolean qContainInverseOpertor){
        tDocParser = new TurtleDocumentParser(fromIris, qContainInverseOpertor);
        dataset.setDefaultGraph(tDocParser.getGraph());
    }
    
    public void loadNamedGraph(Vector fromNamedIris, boolean qContainInverseOpertor){
        Vector namedIris;
        for(int i=0;i<fromNamedIris.size();i++){
            namedIris = new Vector();
            namedIris.add(fromNamedIris.get(i));
            tDocParser = new TurtleDocumentParser(namedIris, qContainInverseOpertor);
            dataset.addNamedGraph(tDocParser.getGraph(),(String)fromNamedIris.get(i));
        }
    }
    
    public Dataset getDataset(){
        return dataset;
    }
    
    
    private Dataset dataset;
    private TurtleDocumentParser tDocParser;
    
}
