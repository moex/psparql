/*
 * ApplyOrderBy.java
 *
 * Created on March 2, 2006, 11:31 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;

import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Solution;
import fr.inrialpes.exmo.cpsparql.query.OrderCondition;
/**
 *
 * @author faisal
 */
public class ApplyOrderBy {
    
    /**
     * Creates a new instance of ApplyOrderBy 
     */
    public ApplyOrderBy(Vector sols, Vector orderconditions) {
        solutions =sols;
        orderConditions = orderconditions;
        maxSize=0; Solution s;
        int index=0;
        for (int i=0;i<sols.size();i++){
            s=(Solution)sols.get(i);
            if (s.getConcepts().size()>maxSize){
                maxSize=s.getConcepts().size();
                index =i;
            }            
        }
        if (sols.size()>0){
            findLocations(index);            
            quickSort(solutions);
        }
    }
    
    public Vector getSolutions(){
        return solutions;
    }
    
    
    public int search(Solution s1,String str){
        int loc=-1,i=0;
        boolean  found=false;
        String concept,image;
        while ((!found) &&(i<s1.getConcepts().size())){
            concept =(String)s1.getConcepts().get(i);
            if (concept.equals(str)){
                found=true;
                loc=i;
            }
            i++;
        }
        return loc;        
    }
    
    
    public boolean compare1(Solution s1,Solution s2){
        boolean comp=true, equal =true;
        int i=0;
        String str1,str2;
        int direction,loc1,loc2;
        int com;
        while ((equal)&&(i<orderConditions.size())){
            orderCond = (OrderCondition)orderConditions.get(i);
            if ((s1.getConcepts().size()==maxSize)&&(s2.getConcepts().size()==maxSize))
                loc1=loc2 =orderCond.getLocation();
            else {
                loc1=search(s1, orderCond.getExpression());
                loc2=search(s2, orderCond.getExpression());
            }
            //System.out.println(" we are at compare1():"+loc);
            direction =orderCond.getDirection();
            if (loc1 >=0)
                str1= (String)s1.getImages().get(loc1);
            else str1="\0";
            if (loc2 >=0)
                str2= (String)s2.getImages().get(loc2);
            else str2="\0";
            //if (loc >=0){
                switch (direction){
                    case 1 : 
                    {
                        //System.out.println(" we are at compare1():"+str1+str2);
                        com=compareTo(str1,str2);   //str1.compareTo(str2);
                        if (com > 0){
                            equal =false;
                            comp= true;
                        }                
                        else if (com < 0){
                            equal =false;
                            comp= false;
                        }                
                        break;
                    }
                    case 0 : 
                        com=compareTo(str1,str2);   //str1.compareTo(str2);
                        if (com < 0){
                            equal =false;
                            comp= true;
                        }                
                        else if (com > 0){
                            equal =false;
                            comp= false;
                        }                
                }
            //}
            i++;
        }
        return comp;
    }
    
    
    public boolean compare2(Solution s1,Solution s2){
        boolean comp=true, equal =true;
        int i=0;
        String str1="",str2="";
        int direction,loc1,loc2;
        int com;
        while ((equal)&&(i<orderConditions.size())){
            orderCond = (OrderCondition)orderConditions.get(i);
            if ((s1.getConcepts().size()==maxSize)&&(s2.getConcepts().size()==maxSize))
                loc1=loc2 =orderCond.getLocation();
            else {
                loc1=search(s1, orderCond.getExpression());
                loc2=search(s2, orderCond.getExpression());
            }
            //System.out.println(" we are at compare1():"+loc);
            direction =orderCond.getDirection();
            if (loc1>=0)
                str1= (String)s1.getImages().get(loc1);
            if (loc2>=0)
                str2= (String)s2.getImages().get(loc2);
            //if (loc >=0){
                switch (direction){
                    case 0 : 
                    {
                        com=compareTo(str1,str2);   //str1.compareTo(str2);
                        if (com > 0){
                            equal =false;
                            comp= true;
                        }                
                        else if (com < 0){
                            equal =false;
                            comp= false;
                        }                
                        break;
                    }
                    case 1 : 
                        com=compareTo(str1,str2);   //str1.compareTo(str2);
                        if (com < 0){
                            equal =false;
                            comp= true;
                        }                
                        else if (com > 0){
                            equal =false;
                            comp= false;
                        }                
                }
            //}
            i++;
        }
        return comp;
    }
    
    public int compareTo(String str1,String str2){
        String copyStr1=str1;
        String copyStr2=str2;
        //System.out.println(" we are at compareTo():"+str1+str2);
        String type1="",type2="";
        int loc1,loc2;
        loc1 = str1.indexOf("^^");
        if (loc1>=0){
            type1=str1.substring(loc1+2);
            str1 = str1.substring(3, loc1-3);
            //System.out.println(" we are at compareTo():"+str1+type1);
        }
        loc2 = str2.indexOf("^^");
        if (loc2>=0){
            type2=str2.substring(loc2+2);
            str2 = str2.substring(3, loc2-3);
            //System.out.println(" we are at compareTo():"+str2+type2);
        }
        int typeOfstr1=type(str1,type1),typeOfstr2=type(str2,type2);
        //System.out.println(" we are at compareTo():  str1="+str1+" str2="+str2+"   "+typeOfstr1+typeOfstr2);                    
        switch (typeOfstr1){
            case 1: switch (typeOfstr2){
                case 2: return -1;
                case 3:
                case 4:
                case 5: return -1;
                default: return -1;
            }
            case 2: switch (typeOfstr2){
                case 2: return copyStr1.compareTo(copyStr2);
                case 3:
                case 4:
                case 5: return -1;
                default: return -1;
            }
            case 3: switch (typeOfstr2){
                case 2: return 1;
                case 3: return copyStr1.compareTo(copyStr2);
                case 4:
                case 5: return -1;
                default: return -1;
            }
            case 4: switch (typeOfstr2){
                case 2: return 1;
                case 3: return 1;
                case 4:
                    double v1=Double.parseDouble(str1);
                    double v2=Double.parseDouble(str2);
                    if (v1 > v2)
                        return 5;
                    else return -5;
                case 5: return -1;
                default: return -1;
            }
            case 5: switch (typeOfstr2){
                case 2: return 1;
                case 3: return 1;
                case 4: return 1;                    
                case 5: return copyStr1.compareTo(copyStr2);
                default: return -1;
            }
            default: return -1;
        }
    }
    
    public int type(String str,String type){
        if ((str+type).equals("\0"))
            return 1;
        if (isNumericType(type))
            return 4;
        else if (isBLANK(str))
            return 2;
        else if (isIRI(str))
            return 3;
        else return 5;
    }
    
    
    public void findLocations(int index){
        int loc=-1;
        Solution sol = (Solution)solutions.get(index);
        for(int i=0;i<orderConditions.size();i++){
            orderCond = (OrderCondition)orderConditions.get(i);
            loc = search(sol, orderCond.getExpression());
            //System.out.println(loc);
            orderCond.setLocation(loc);
        }
    }

    
    public void quickSort(Vector sols){
        int size =sols.size();
        q_sort(sols, 0, size-1);
    }

    public void q_sort(Vector sols, int left, int right){
        int  l_hold, r_hold;
        Solution pivot;
        l_hold = left;
        r_hold = right;
        pivot = (Solution)sols.get(left);
        while (left < right){            
            while ((compare1((Solution)sols.get(right),pivot)) && (left < right))
                right--;
            if (left != right){
                sols.set(left, sols.get(right));      
                left++;
            } 
            while ((compare2((Solution)sols.get(left),pivot)) && (left < right))    
                left++;
             
            if (left != right){
                sols.set(right, sols.get(left));      //sols[right] = sols[left];
                right--;
            }
          } 
        sols.set(left,pivot);       //sols[left] = pivot;
        int Pivot =left;       //pivot = left;
        left = l_hold;
        right = r_hold;
        if (left < Pivot)
            q_sort(sols, left, Pivot-1);
        if (right > Pivot)
            q_sort(sols, Pivot+1, right);
    }

    public double parseValue(String s){
        double value=0;
        /*if (type.equals("<http://www.w3.org/2001/XMLSchema#double>") ||
                type.equals("DOUBLE"))*/
        value = Double.parseDouble(s);
        return value;
    }//end of parseValue()
    
    public boolean isNumericType(String type){
        if (type.equals("<http://www.w3.org/2001/XMLSchema#decimal>") ||
                type.equals("<http://www.w3.org/2001/XMLSchema#float>") ||
                type.equals("<http://www.w3.org/2001/XMLSchema#double>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#integer>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#short>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#long>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#int>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#byte>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedLong>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedInt>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedShort>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#unsignedByte>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#positiveInteger>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#nonPositiveInteger>")||
                type.equals("<http://www.w3.org/2001/XMLSchema#negativeInteger>")||
                type.equals("INTEGER") ||type.equals("DOUBLE") ||
                type.equals("DECIMAL"))
            return true;
        else return false;
                    
    }// end of isNumericType()

    
    public boolean isIRI(String image){
        //System.out.println(" we are at isIRI"+image);
        if (image.indexOf('<')==0)
            return true;            
        else return false;
    }
    
    public boolean isBLANK(String image){
        if (image.indexOf('_')==0)
            return true;
        else  return false;
    }
    
    private Vector solutions;
    private Vector orderConditions;
    private OrderCondition orderCond;
    private int condCounter;
    private int maxSize;
}
