/*
 * ResultForm.java
 *
 * Created on February 27, 2006, 11:38 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;

import java.io.File;
import java.net.URI;
import fr.inrialpes.exmo.cpsparql.query.Query;
import java.util.LinkedList;
import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Graph;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Triple;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Solution;
import fr.inrialpes.exmo.sparqlmm2.TemplateParser;
import org.w3c.dom.Node;

/**
 *
 * @author faisal
 */
public class ResultForm {
    
    /** Creates a new instance of ResultForm */
    public ResultForm(Vector Solutions, int lim, int off,Query q) {
        query =q;
        nBS=0;
        nBDS =0;
        solutions=Solutions;
        limit =lim;
        offset =off;
        QueryFormat(); 
    }
    
    
    public String QueryFormat(){
        String str= new String("");
        switch(query.Type()){
                case 1:str=select(solutions, limit,offset);break;
                case 2:str=construct(solutions,  limit, offset);break;
                case 3:str=ask(solutions);break;
                case 4: str=describe(solutions,  limit, offset);break;
            }
        result =str;
        return str;
    }
    
    public String Result(){
        return result;
    }
    
    public int location(Vector cons,String c){
        int loc=-10;
        boolean found=false;
        int i=0;
        String concept;
        while((!found) &&(i<cons.size())){
            concept = (String)(cons.get(i));
            if(concept.equals(c)){
                loc=i;
                found=true;
            }
            i++;
        }
        return loc;
    }
       
    
    public boolean InSolution(Vector solutions, Solution solution){
        boolean found =false;
        int i=0;
        Solution sol;
        Vector images;
        Vector concepts;
        while((i<solutions.size())&&(!found)){
            sol=(Solution)solutions.get(i);
            if ((sol.getConcepts().equals(solution.getConcepts()))
            && (sol.getImages().equals(solution.getImages())))
                found =true;
            i++;
        }
        return found;
    }
    
    
    public String select( Vector solutions, int limit,int offset){
        //String str = new String("{\n");
        String tempStr = new String("");
        Solution sol;
        Solution tempSol= new Solution();                    
        Vector tempSolutions =new Vector();
        finalSols = new Vector();
        int varNom = query.varNom();  
        String [] selectVars = query.selectVar();   
        int pos;
        String image;                    
        String concept;
        //System.out.println(nBDS);
       
         if(offset <=0)
            offset = 0;
        if (limit < 0)
            limit=solutions.size();
        else if((limit+offset)>=solutions.size())
            limit=solutions.size();
        else 
            limit+=offset;
        StringBuffer strBuffer = new StringBuffer();
        for(int i=offset;i< limit;i++){//for(int i=0;i< solutions.size();i++){
            sol = (Solution)(solutions.get(i));
             switch (query.getSelectIndexStar()){ 
                case 1:{  // select *
                    if (!query.distinct()){// not distict answers
                        for(int v=0;v<sol.getConcepts().size();v++){
                            concept= (String)sol.getConcepts().get(v);
                            image= (String)sol.getImages().get(v);
                             if (image.indexOf("\"\"\"") >=0)
                                image = standardLabel(image);
                            strBuffer.append("("+concept+","+image+"), ");
                        }
                        nBS++;                        
                        strBuffer.append("\n");
                        finalSols.add(sol); //finalSols = solutions;
                    }
                    else // apply distict for select * 
                        if(!InSolution(tempSolutions,sol)){
                        tempSolutions.add(sol);
                        for(int v=0;v<sol.getConcepts().size();v++){
                            concept= (String)sol.getConcepts().get(v);
                            image= (String)sol.getImages().get(v);
                             if (image.indexOf("\"\"\"") >=0)
                                image = standardLabel(image);
                            strBuffer.append("("+concept+","+image+"), ");
                        }
                        nBS++;                        
                        strBuffer.append("\n");
                        finalSols = tempSolutions;
                    }
                    
                    break;
                }
                case 0:{ // selected variables
                    if (query.distinct()){ // apply distinct
                        tempStr = distinct(varNom,sol,selectVars);
                        strBuffer.append(tempStr);
                    }
                    else { // not distinct solutions
                        //finalSols = solutions;
                        nBS++;
                        //nBDS++;
                        Vector concepts = new Vector();
                        Vector images = new Vector();
                        Solution tempSolution=new Solution();
                        for(int v=0;v<=varNom;v++){
                            pos =location(sol.getConcepts(), selectVars[v]);
                            if(pos>=0){
                                image= (String)sol.getImages().get(pos);
                                 if (image.indexOf("\"\"\"") >=0)
                                    image = standardLabel(image);
                                concepts.add( selectVars[v]);
                                images.add(image);
                               strBuffer.append("("+selectVars[v]+","+image+"), ");
                            }
                            else {
                                strBuffer.append("("+selectVars[v]+","+""+"), ");
                                concepts.add(selectVars[v]);
                                images.add("");
                                //sol.addConcept(selectVars[v]);
                                //sol.addImage("");
                            }
                         }
                        tempSolution.setConcepts(concepts);
                        tempSolution.setImages(images);
                        finalSols.add(tempSolution);
                    }
                }   
            }            
            if ((!query.distinct()) && (query.getSelectIndexStar()!=1))
                strBuffer.append("\n");
        }        
       /* //System.out.println(nBDS+"  "+finalSols.size());
         if(offset <=0)
            offset =1;
        if (limit <0)
            limit=finalSols.size();
        else if((limit+offset)>finalSols.size())
            limit=finalSols.size();
        else 
            limit+=offset-1;
        if ((limit - offset+1) < finalSols.size())
            strBuffer.append("{\n"+applyOffsetLimit(finalSols,limit,offset));
        
        strBuffer.append("}\n");*/
        if (nBS >0){
            strBuffer.append("\n"+(limit-offset)+" Solutions out of :"+solutions.size()+"  Solutions");  //NBS
            strBuffer.append("\n"+" Solutions from offset: "+(offset+1)+" to "+(limit));
        }
        else
            strBuffer.append("\n"+nBDS+" Solutions from: "+(offset+1));
        return strBuffer.toString();
        
        
    }
    
    public String applyOffsetLimit(Vector solutions, int limit, int offset){
        //System.out.println(solutions.size()+"  "+limit+"  "+offset);
        String str = new String("");
        Vector tS = new Vector(solutions);
        Solution sol=new Solution();
        String concept,image;
        for(int i=offset-1;i < limit;i++){
            //System.out.println(nBDS+"  "+finalSols.size());
            sol = (Solution)solutions.get(i);
            for(int j=0;j < sol.getConcepts().size();j++){
                concept= (String)sol.getConcepts().get(j);
                image= (String)sol.getImages().get(j);
                //
                str+="("+concept+","+image+"), ";
            }
            str+='\n';
            tS.add(finalSols.get(i));
        }
        //System.out.println("apply Offset limit");
        //System.out.println(nBDS+"  "+finalSols.size());
        finalSols = tS;
        //System.out.println(tS.size());
       return str;
    }
    
    public String distinct(int varNom,Solution sol,String [] selectVars){
        Vector concepts = new Vector();
        Vector images = new Vector();
        int pos;
        Solution tempSol=new Solution();
        String image,str=new String(""),tempStr=new String("");
        for(int v=0;v<=varNom;v++){
            pos =location(sol.getConcepts(), selectVars[v]);
            if(pos>=0){
                image= (String)sol.getImages().get(pos);
                if (image.indexOf("\"\"\"") >=0)
                    image = standardLabel(image);
                concepts.add( selectVars[v]);
                images.add(image);
                tempStr+="("+selectVars[v]+","+image+"), ";
            }
        }
        tempSol.setConcepts(concepts);
        tempSol.setImages(images);
        if(!InSolution(finalSols,tempSol)){
            str=tempStr+"\n";
            finalSols.add(tempSol);
            nBS++;
            nBDS++;
        }
        return str;
    }
     
    
    public String standardLabel(String str){
       // System.out.println("we are at standardLabel:"+str);
        int loc =str.indexOf("\"\"\"");
        if (loc >=0)
            return str.substring(loc, str.indexOf("\"\"\"", 1)+3);
        else return str;
    }
        
    public String construct( Vector solutions, int limit,int offset){
        String str = new String("{\n");
        Vector tempSolutions = new Vector();
        Solution sol;
        Graph template;
        template = query.Template();  
        Triple triple; 
        int pos;
        String node,image;                    
        if(offset <=0)
            offset =0;
        if (limit <0)
            limit=solutions.size();
        else if((limit+offset)>=solutions.size())
            limit=solutions.size();
        else 
            limit+=offset;
        //System.out.println("test the construct template uri = "+query.getConstructTemplateUri());
        if(!query.getConstructTemplateUri().equals("")){
            tempSolutions = new Vector(); 
            for(int i=offset;i< limit;i++){
                tempSolutions.add(solutions.get(i));
            }
            solutions = tempSolutions ;
            substituteVarValuesInConstructTemplate(tempSolutions);
            str+=tempSolutions.size()+" files constructed according to the template "+query.getConstructTemplateUri()+"\n}";
            
        }
        else {
        for(int i=offset;i< limit;i++){
            sol = (Solution)(solutions.get(i));
            tempSolutions.add(sol);
            str+="\n";
            for(int v=0;v<template.size();v++){
                triple = template.TripleAt(v, 0);
                node =triple.getSubject();
                if(variable(node)){
                    pos =location(sol.getConcepts(), node);
                    if(pos>=0){
                        image= (String)sol.getImages().get(pos);
                         if (image.indexOf("\"\"\"") >=0)
                            image = standardLabel(image);
                        str+=image+"  ";
                    }
                }
                else if (node.charAt(0)=='_'){
                    node+=i;
                    str+=node+"  ";
                }
                else 
                    str+=node+"  ";
                node =triple.getPredicate();
                if(variable(node)){
                    pos =location(sol.getConcepts(), node);
                    if(pos>=0){
                        image= (String)sol.getImages().get(pos);
                         if (image.indexOf("\"\"\"") >=0)
                            image = standardLabel(image);
                        str+=image+"  ";
                    }
                }
                else if (node.charAt(0)=='_'){
                    node+=i;
                    str+=node+"  ";
                }
                else 
                    str+=node+"  ";
                node =triple.getobject();
                if(variable(node)){
                    pos =location(sol.getConcepts(), node);
                    if(pos>=0){
                        image= (String)sol.getImages().get(pos);
                         if (image.indexOf("\"\"\"") >=0)
                            image = standardLabel(image);
                        str+=image+"  .\n  ";
                    }
                }
                else if (node.charAt(0)=='_'){
                    node+=i;
                    if(v<(template.size()-1))
                        str+=node+"  .\n";
                    else str+=node+"  .";
                }
                else if(v<(template.size()-1))
                    str+=node+" .\n";
                else str+=node+" .";
            }
            //str+="\n";            
        }        
        str+="}";
        }
        finalSols = tempSolutions ;
        return str;
        
    }
        
    public void substituteVarValuesInConstructTemplate(Vector solutions){
        //System.out.println("Create files");
        File file; 
        String pathstr = query.getConstructTemplateUri();
        pathstr = pathstr.substring(1,pathstr.length()-1);           
        try{
           URI uri = new URI(pathstr);
           file = new File(uri);
        }
        catch(Exception e){
            //System.out.println(e);
            file = new File(pathstr);
            if (!file.isAbsolute()) {
                file = new File(file.getAbsolutePath());
            }
        }
        TemplateParser tempParser = new TemplateParser(file);
        listOfResults = tempParser.solve(solutions);
        //listOfResults = tempParser.setTemplateVarsValuesToNodes(solutions);
        //LinkedList<File> listOfFiles = tempParser.setTemplateVarsValuesToFiles(solutions); 
    }
    
     public boolean variable(String var){
        if ((var.charAt(0)=='?') ||(var.charAt(0)=='$'))  // ||(var.charAt(0)=='_')
            return true;
        else return false;
    }
   
    
    public String ask( Vector solutions){
        //System.out.println("ask me-------------------------------------------");
        String str = new String("");
        int size =solutions.size(); 
        if (size>0)
            str+="true:    ";
        else str+="false:    ";
        str+=size+ ": solutions found";
        //System.out.println(str);
        finalSols = solutions;
        return str;
        
    }
    public String describe( Vector solutions, int limit,int offset){
        String str = new String("");
        if(offset <0)
            offset =0;
        if (limit <0)
            limit=solutions.size();
        else if((limit+offset)>=solutions.size())
            limit=solutions.size();
        else 
            limit+=offset;
        for(int i=offset;i< limit;i++){
            
        }
        
        return str;
        
    }
    
    public Vector getFinalSolutions(){
        return finalSols;
    }
    
    public LinkedList<Node> getListOfResults(){
        return listOfResults;
    }
    
    private LinkedList<Node> listOfResults;
    private Vector solutions;
    private int limit;
    private int offset;
    private Query query;
    private String result;
    private Vector finalSols;
    private int nBS;    
    private int nBDS;
}

