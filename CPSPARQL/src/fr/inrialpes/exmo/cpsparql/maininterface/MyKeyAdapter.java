/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.inrialpes.exmo.cpsparql.maininterface;
import java.awt.event.*;
/**
 *
 * @author Faisal
 */
public class MyKeyAdapter extends java.awt.event.KeyAdapter {
    
   private MainFrame myFrame;

   MyKeyAdapter(MainFrame myFrame) {
     this.myFrame = myFrame;
   }
   public void keyTyped(KeyEvent e) {
     myFrame.m(e);
   }

}
