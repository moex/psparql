/*
 * RandomRDFGraphConstn.java
 *
 * Created on September 27, 2007, 6:08 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.ontologycreation;

import java.util.Vector;
import java.io.*;
import java.util.Random;

/**
 *
 * @author faisal
 */
public class RandomRDFGraphConstn {
    
    /** Creates a new instance of RandomRDFGraphConstn */
    public RandomRDFGraphConstn(long seed) {
        if (Debug)
            System.out.println("start RDF graph construction...");
        citiesList = new Vector();
        citiesInFranceList = new Vector();
        countriesList = new Vector();
        rdfGraph = new Vector();
        openLocalFile("citiesInFrance.txt", "citiesFranceRDFGraph.txt", "cityIn", citiesInFranceList, countriesList);
        countriesList = new Vector();
        openLocalFile("citiesCountriesList.txt", "citiesCountriesRDFGraph.txt", "capitalOf", citiesList, countriesList);
        //System.out.println(citiesInFranceList.size());
        createRandomRDFGraph(citiesList, countriesList, citiesInFranceList, 3,seed);
        writeRDFGraphToFile(rdfGraph, "testRDFGraphc#20.ttl");
    }
    
    public void start(long extSeed){
        if (Debug)
            System.out.println("start RDF graph construction...");
        citiesList = new Vector();
        citiesInFranceList = new Vector();
        countriesList = new Vector();
        rdfGraph = new Vector();
        openLocalFile("citiesInFrance.txt", "citiesFranceRDFGraph.txt", "cityIn", citiesInFranceList, countriesList);
        countriesList = new Vector();
        openLocalFile("citiesCountriesList.txt", "citiesCountriesRDFGraph.txt", "capitalOf", citiesList, countriesList);
        //System.out.println(citiesInFranceList.size());
        createRandomRDFGraph(citiesList, countriesList, citiesInFranceList, 41,extSeed);
        writeRDFGraphToFile(rdfGraph, "testRDFGraphc#5000.txt");
    }
    
    public void createRandomRDFGraph(Vector cities, Vector countries, Vector citiesInFrance, int nTriples,long extSeed){
        Random gen = new Random(extSeed);
        long seed = gen.nextInt();
        Random generator = new Random();
        generator.setSeed(seed);
        int index1, index2, transportType;
        String city1, city2;
        String [] transports = {"bus", "taxi", "train", "plane"}; 
        if (nTriples > citiesInFrance.size())
            nTriples = citiesInFrance.size();
        for(int j=0; j < (nTriples); j++){//citiesInFrance.size()
            for(int i=0; i< (nTriples); i++){
              
              //index1 = generator.nextInt(citiesInFrance.size());
                index2 = generator.nextInt(nTriples);//citiesInFrance.size()
                while ((index2==j)&&(nTriples >2)){
                 //seed = gen.nextInt();
                 //generator.setSeed(seed);
                 index2 = generator.nextInt(nTriples);//citiesInFrance.size()  
              }
              //System.out.println(j+"faisal \t"+index2);
              seed = gen.nextInt();
              generator.setSeed(seed);transportType = generator.nextInt(4);  
              city1 = (String)citiesInFrance.get(j);
              city2 =  (String)citiesInFrance.get(index2);
              //rdfGraph.add("<"+city1+">\t<"+base+transports[transportType]+">\t<"+city2+">  .");
              int flight = generator.nextInt(); 
              rdfGraph.add("<"+"_:b"+flight+">\t<"+base+"from"+">\t<"+city1+">  .");
              rdfGraph.add("<"+"_:b"+flight+">\t<"+base+"to"+">\t<"+city2+">  .");
              rdfGraph.add("<"+"_:b"+flight+">\t<"+"http://www.w3.org/1999/02/22-rdf-syntax-ns#type"+">\t<"+base+transports[transportType]+">  . \n");
              //System.out.println("<"+city1+">\t<"+transports[transportType]+">\t<"+city2+">  .");
            }
        }
        for(int j=0; j < 0; j++){//cities.size()
            for(int i=1; i<= (nTriples); i++){
              //index1 = generator.nextInt(citiesInFrance.size());  
              index2 = generator.nextInt(cities.size());  
              transportType = generator.nextInt(4);  
              city1 =  (String)cities.get(j);
              city2 =  (String)cities.get(index2);
             //rdfGraph.add("<"+city1+">\t<"+base+transports[transportType]+">\t<"+city2+">  .");
              int flight = generator.nextInt(); 
              rdfGraph.add("<"+"_:b"+flight+">\t<"+base+"from"+">\t<"+city1+">  .");
              rdfGraph.add("<"+"_:b"+flight+">\t<"+base+"to"+">\t<"+city2+">  .");
              rdfGraph.add("<"+"_:b"+flight+">\t<"+"http://www.w3.org/1999/02/22-rdf-syntax-ns#type"+">\t<"+base+transports[transportType]+">  .");
              //System.out.println("<"+city1+">\t<"+transports[transportType+2]+">\t<"+city2+">  .");
            }
        }
        System.out.println(rdfGraph.size());
                
    }

    public void writeRDFGraphToFile(Vector rdfG, String destination){
        if (Debug)  
            System.out.println("Writing to the file at..."+rdfG);
        File outputFile = new File(destination);
        FileWriter outFile;
        try{
              outFile   = new FileWriter(outputFile);
              String str;
              for(int i=0; i<rdfG.size(); i++){
                  str = (String)rdfG.get(i)+"\n";
                  outFile.write(str);
                  
             }
           outFile.close();
         } catch (IOException e) {if (Debug)  System.out.println(e); } 
    }

    
    public File openLocalFile(String URI, String destination, String predicate, Vector cities, Vector countries){
        if (Debug)  
            System.out.println("Loading the file at..."+URI);
        //URI = URI.substring(URI.indexOf(':')+1);
        File inputFile = new File(URI);
        FileReader inFile;
        File outputFile = new File(destination);
        FileWriter outFile;
        BufferedReader in;
        String str1="",str="";
        String city, country;
        try{
              inFile = new FileReader(inputFile);
              outFile   = new FileWriter(outputFile);
              in = new BufferedReader(inFile);    
              while((str=in.readLine()) !=null){
                  //System.out.println(str);
                  city = base+ str.substring(0, str.indexOf(','));
                  country = base+ str.substring(str.indexOf(',')+1);
                  //outFile.write(city+","+"France"+"\n");
                  outFile.write("<"+city+">\t"+"<"+base+predicate+">\t"+"<"+country+">  .\n");
                  //rdfGraph.add("<"+city+">\t"+"<"+base+predicate+">\t"+"<"+country+">  .");
                  cities.add(city); 
                  countries.add(country);
             }
           inFile.close();
           outFile.close();
         } catch (IOException e) {if (Debug)  System.out.println(e); } 
        //System.out.println(cities.size());
        return outputFile;
    }

    private boolean Debug; 
    private Vector citiesList, countriesList, citiesInFranceList;
    private Vector rdfGraph;
    private String base =  "http://www.inrialpes.fr/";
        
        
}

