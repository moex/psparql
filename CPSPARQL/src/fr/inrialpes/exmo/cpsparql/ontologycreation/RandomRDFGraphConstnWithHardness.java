/*
 * RandomRDFGraphConstn.java
 *
 * Created on September 27, 2007, 6:08 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.ontologycreation;

import java.util.Vector;
import java.io.*;
import java.util.Random;

/**
 * @author faisal alkhateeb
 */
public class RandomRDFGraphConstnWithHardness {
    
    /**
     * Creates a new instance of RandomRDFGraphConstn */
    public RandomRDFGraphConstnWithHardness() {//long seed, int graphSize, int inOutDist
       //start(inOutDist, graphSize, seed);
    }
    
     /**
     * Creates an RDF graph using the specified parameetres 
     * @param seed for the random graph generator 
     * @param graphSize is the number of the required size  
     * @param inOutDist the in- and out-degree for nodes
     * @param nodeSetting for determining the number of nodes
     */
    
    public void start(long seed, int graphSize, int inOutDist, int nodeSetting){
        int nodeSize = graphSize/inOutDist;
        if(nodeSetting ==1)
            citiesList = generateLabels(graphSize, "city");
        else
            citiesList = generateLabels(nodeSize, "city");
        /*if(edgeSetting ==1)
            edgeList = generateLabels(graphSize, "edge"); // case 1
        else*/
            edgeList = fixedLabels(); // case 2
        rdfGraph = new Vector();
        createRandomRDFGraph(citiesList, edgeList, inOutDist, graphSize, seed,nodeSetting);
        writeRDFGraphToFile(rdfGraph, "testRDFGraphc#"+graphSize+".ttl");
    }
    
    private void createRandomRDFGraph(Vector cities, Vector edgeLabels, int inOutDist, int nTriples, long extSeed, int nodeSetting){
        Random gen = new Random(extSeed);
        long seed = gen.nextInt();
        Random generator = new Random();
        generator.setSeed(seed);
        int index2, edgeIndex;
        String city1, city2;
        int maxNumOfNodes;
        if(nodeSetting ==1)
            maxNumOfNodes = nTriples;
        else
            maxNumOfNodes = nTriples/inOutDist;
        for(int j=0; j < (maxNumOfNodes); j++){//citiesInFrance.size()
            for(int i=0; i< (inOutDist); i++){
              
              //index1 = generator.nextInt(citiesInFrance.size());
                index2 = generator.nextInt(maxNumOfNodes);//citiesInFrance.size()
                while ((index2==j)&&(nTriples >2)){
                 //seed = gen.nextInt();
                 //generator.setSeed(seed);
                 index2 = generator.nextInt(maxNumOfNodes);//citiesInFrance.size()  
              }
              //System.out.println(j+"faisal \t"+index2);
              seed = gen.nextInt();
              generator.setSeed(seed);
              /*if(edgeSetting ==1)
                  edgeIndex = generator.nextInt(nTriples);  
              else*/
              edgeIndex = generator.nextInt(4);
              city1 = (String)cities.get(j);
              city2 =  (String)cities.get(index2);
              String edgeLabel = (String)edgeLabels.get(edgeIndex);
              //rdfGraph.add("<"+city1+">\t<"+base+transports[transportType]+">\t<"+city2+">  .");
              rdfGraph.add(city1+"\t"+edgeLabel+"\t"+city2+"  . \n");
              //System.out.println("<"+city1+">\t<"+transports[transportType]+">\t<"+city2+">  .");
            }
        }       
        //System.out.println(rdfGraph.size());
                
    }

    private Vector generateLabels(int size, String s){
        Vector labels = new Vector();
        String label;
        for(int i=0; i<size; i++){
            label = "<"+base+s+"-"+i+">";
            labels.add(label);           
        }
        //System.out.println(s+labels.size());
        return labels;
    }
    
     private Vector fixedLabels(){
        Vector labels = new Vector();
        String label;
        label = "<"+base+"bus"+">";
        labels.add(label); 
        label = "<"+base+"train"+">";
        labels.add(label); 
        label = "<"+base+"avion"+">";
        labels.add(label); 
        label = "<"+base+"taxi"+">";
        labels.add(label); 
        //System.out.println(s+labels.size());
        return labels;
    }
    
    private void writeRDFGraphToFile(Vector rdfG, String destination){
        outputFile = new File(destination);
        FileWriter outFile;
        try{
              outFile   = new FileWriter(outputFile);
              String str;
              for(int i=0; i<rdfG.size(); i++){
                  str = (String)rdfG.get(i)+"\n";
                  outFile.write(str);
                  
             }
           outFile.close();
         } catch (IOException e) {System.out.println(e); } 
    }

    
    /**
     * retruns a node at a specified index
     * @param index the index of the node to be returned 
     */
    public String getNodeAt(int index){
        return (String)citiesList.get(index);
    }
    /**
     * retruns the constructed RDF graph in a turtle file
     */
    public File getRDFFile(){
        return outputFile;
    }
    /**
     * retruns an edge at a specified index
     * @param index the index of the edge to be returned 
     */
    
    public String getEdgeAt(int index){
        return (String)edgeList.get(index);
    }
    
    private Vector citiesList;
    private Vector edgeList;
    private Vector rdfGraph;
    private File outputFile;
    private String base =  "http://www.inrialpes.fr/";
        
        
}

