/*
 * Candidates.java
 *
 * Created on February 1, 2006, 12:42 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.util.Vector;
/**
 *
 * @author faisal
 */
public class Candidates {
    
    /** Creates a new instance of Candidates */
    public Candidates() {
        nextCandIndex=0;
        String concept=new String("");
        conceptCands= new Vector();
        lambda= new Vector();
    }
    
    public Candidates(Candidates c) {
        this.nextCandIndex=c.nextCandIndex;
        this.concept=new String(c.concept);
        this.conceptCands= new Vector(c.conceptCands);
        this.lambda =new Vector(c.lambda);
    }
    
    public void addCands(TransCandidate tc){
        conceptCands.add(tc);        
    }
   
    public void addLambda(Vector l){
        lambda = new Vector(l);
    }
    
    public Vector getLambda(){
        return lambda;
    }
   
    public void addCands(int i,TransCandidate tc){
        conceptCands.add(i,tc);
    }
   
     public Vector getConceptCands(){
        return this.conceptCands;
    }
     
     public boolean isEmpty(){
         return (nextCandIndex==conceptCands.size());
     }
   
    public String getConcept(){
        return concept;
    }
    
    public void setConcept(String c){
        concept =c;
    }
    
    public TransCandidate getTransCands(){
        nextCandIndex++;
        return (TransCandidate)(conceptCands.get(nextCandIndex-1));
    }
   
     public TransCandidate getTransCands(int i){
        return (TransCandidate)(conceptCands.get(i));
    }
   
     public void printConceptCands(){
        System.out.println("_________________________________________");
        System.out.println("Candidates("+concept+")=");
        System.out.println("    nextCandIndex="+nextCandIndex);
        TransCandidate tc = new TransCandidate();           
        for(int i=0;i<conceptCands.size();i++){
            tc.prindTCand((TransCandidate)(conceptCands.get(i))); 
        }
        System.out.println("_________________________________________");        
    }
    
    public void reset(){
        nextCandIndex=0;
    }
    
    public void printConceptCands(Candidates c){
        System.out.println("_________________________________________");
        System.out.println("Candidates(+"+concept+")=");
        System.out.println("    nextCandIndex="+nextCandIndex);
        TransCandidate tc = new TransCandidate();           
        for(int i=0;i<c.conceptCands.size();i++){
            tc.prindTCand((TransCandidate)(c.conceptCands.get(i))); 
        }
        System.out.println("_________________________________________");        
    }
    
    private int nextCandIndex;
    private String concept;
    private Vector conceptCands;
    private Vector lambda;
}
