/*
 * Graph.java
 *
 * Created on January 3, 2006, 3:20 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.io.*;
import java.util.Vector;

/**
 *
 * @author faisal
 */
public class Graph {
    
    /** Creates a new instance of Graph */
    public Graph() {
        triples =new Vector();    
        optionalTriples =new Vector();    
        constraint =new String [maxTriples];
        constraints =new Vector ();
        optConstraints =new Vector ();
        optConstraint =new String [maxTriples];
        optConstraintsNom=0;
        constraintsNom=0;
        triplesNom=0;
        optTriplesNom=0;
        textGraph=new String("");    
    }
    
    
    public Graph(Graph g) {
        this.triples = g.triples;
        this.triplesNom = g.triplesNom;
        this.optionalTriples =g.optionalTriples;
        this.optTriplesNom= g.optTriplesNom;
        this.constraint=g.constraint;
        this.constraintsNom=g.constraintsNom;
        this.optConstraint=g.optConstraint;
        this.optConstraintsNom=g.optConstraintsNom;
        
    }
    
    public void setTriple(Triple t){
         triples.add(new Triple(t));
    }
    
    public void setOptionalTriple(Triple t){
        optionalTriples.add(new Triple(t));
    }
    
    public boolean atomic(int i, int op){
        if(op==0){
            Triple t;
            t = (Triple)triples.get(i);
            return (t.gettype()==0);
        }
        else{
            Triple t;
            t = (Triple)optionalTriples.get(i);
            return (t.gettype()==0);
        }
    }
    
    public Triple TripleAt(int counter, int op){
       if(op==0){
           //System.out.println("not optional"+op);
            Triple t;
            t = (Triple)triples.get(counter);
            return t;
        }
        else{
           //System.out.println("optional");
            Triple t;
            t = (Triple)optionalTriples.get(counter);
            return t;
        }
    }
    
    public Triple optionalTripleAt(int counter){
        Triple t;
        t = (Triple)optionalTriples.get(counter);
        return t;
    }
    
    
    public int getTriplesNom(){
        return triples.size();        
        //return triplesNom;        
    }
   
    public String getTextGraph(){
        return textGraph;
    }
    
    public int getOptionalTriplesNom(){
        return optionalTriples.size();
        //return optTriplesNom;
    }

    public int size(){
        return (triples.size()+optionalTriples.size());
    }
    
    public void setConstraint(String c){
        constraint[constraintsNom++]= new String(c);
    }
    
    
    public void addConstraint(Vector c){
        constraints.add(c);
    }
    
    public Vector getConstraints(){
        return constraints;
    }
    
    public void addOptionalConstraint(Vector c){
        optConstraints.add(c);
    }
    
    public void setOptionalConstraint(String c){
        optConstraint[optConstraintsNom++]= new String(c);
    }
    
    
    public void print(String s){     
        textGraph = new String("");            
        //System.out.println("___________________________________________________________");
        //System.out.println("Graph "+s);        
        textGraph+="Graph "+s+"\n";
        Triple t;
        for(int j=0;j< triples.size();j++){
            t = (Triple)triples.get(j);
            //System.out.println("Triple["+j+"]= {"+t.getSubject()+","+t.getPredicate()+","+t.getobject()+","+t.getSubjectImage()+","+t.getPredicateImage()+","+t.getobjectImage()+"}");
            textGraph+="Triple["+j+"]= {"+t.gettype()+","+t.getSubject()+","+t.getPredicate()+","+t.getobject()+"}\n";
        }
        //System.out.println("The  Optional Triples are: ");
        textGraph+="The  Optional Triples are: \n";
        for(int j=0;j< optionalTriples.size();j++){
            t = (Triple)optionalTriples.get(j);
            //System.out.println("Optional Triple["+j+"]= {"+t.getSubject()+","+t.getPredicate()+","+t.getobject()+","+t.getSubjectImage()+","+t.getPredicateImage()+","+t.getobjectImage()+"}");
            textGraph+="Optional Triple["+j+"]= {"+t.gettype()+","+t.getSubject()+","+t.getPredicate()+","+t.getobject()+"}\n";
        }
        for(int j=0;j< constraintsNom;j++){
             //System.out.println("Constraint ="+constraint[j]);
             textGraph+="Constraint ="+constraint[j]+"\n";
        }
        for(int j=0;j< optConstraintsNom;j++){
             //System.out.println("Optional Constraint ="+optConstraint[j]);
             textGraph+="Optional Constraint ="+optConstraint[j]+"\n";
        }
        //System.out.println("___________________________________________________________");
        textGraph+="__________________________________________________________________________________________________________________"+"\n";

    }
    

    private int triplesNom=0;
    private int maxTriples =100;
    private int optTriplesNom =0;
    private int constraintsNom =0;
    private int optConstraintsNom =0;
    private String constraint[];
    private Vector constraints;
    private Vector optConstraints;
    private String optConstraint[];
    private Vector triples ;
    private Vector optionalTriples;    
    private String textGraph;    
}
