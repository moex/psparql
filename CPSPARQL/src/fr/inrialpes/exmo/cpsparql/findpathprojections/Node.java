/*
 * Node.java
 *
 * Created on March 8, 2006, 3:45 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.util.Vector;
/**
 *
 * @author faisal
 */
public class Node {
    
   
    /** Creates a new instance of Node */
    public Node(String s) {
        id =-1;
        label= new String(s);
        edges = new Vector();
    }
    
     /** Creates a new instance of Node */
    public Node(String s, boolean inverseOperator) {
        id =-1;
        label= new String(s);
        edges = new Vector();
        if (inverseOperator)
            InEdges = new Vector();
        //mark=-1;
        //R= new Vector();
    }
    
    /** Creates a new instance of Node */
    public Node(int i,String l,Vector eds,Vector InEds,int m, Vector r) {
        id =i;
        label= new String(l);
        edges = new Vector(eds);
        InEdges = new Vector(InEds);
        mark=m;
        R = new Vector(r);
    }
    
    /** copy constructor */
    public Node(Node n) {
        this.id =n.id;
        this.label= new String(n.label);
        this.edges = new Vector(n.edges);
        this.InEdges = new Vector(n.InEdges);
        this.mark =n.mark;
        this.R = n.R;
    }
    
    public void setId(int i){
        id=i;
    }
    
    public int getId(){
        return id;
    }
    
    public void setLabel(String l){
        label = new String(l);
    }
    
    public String getLabel(){
        return label;
    }
        
    public void addEdge(Edge e){
        edges.add(e);
    }
    
    public void addInEdge(Edge e){
        InEdges.add(e);
    }
    
    public Vector getEdges(){
        return edges;
    }
    
    public Vector getInEdges(){
        return InEdges;
    }
    public void setMark(int m){
        mark =m;
    }
    
    public int getMark(){
        return mark;
    }
    
    public Vector getR(){
        return R;
    }
    
    public Vector getR(int index){
        return (Vector)R.get(index);
    }
    
    
    public void setR(int index, Vector r){
        R.set(index,r);
    }
    
    public void setR(Vector R){
        this.R = new Vector(R);
    }
    
    private int id;
    private String label;
    private Vector edges;
    private Vector InEdges;
    private int mark;
    private Vector R;
}
