/*
 * Edge.java
 *
 * Created on March 8, 2006, 3:52 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.util.Vector;

/**
 *
 * @author faisal
 */
public class Edge {
    
    /** Creates a new instance of Edge */
    public Edge(String e) {
        id =-1;
        label= new String(e);
        nodes = new Vector();
    }
    
    /** Creates a new instance of Node */
    public Edge(int i,String l,Vector ns) {
        id =i;
        label= new String(l);
        nodes = new Vector(ns);
    }
    
    /** copy constructor */
    public Edge(Edge e) {
        this.id =e.id;
        this.label= new String(e.label);
        this.nodes = new Vector(e.nodes);
    }
    
    public void setId(int i){
        id=i;
    }
    
    public int getId(){
        return id;
    }
    
    public void setLabel(String l){
        label = new String(l);
    }
    
    public String getLabel(){
        return label;
    }
    
    
    public void addNode(Node n){
        nodes.add(n);
    }
    
    public Vector getNodes(){
        return nodes;
    }
    
    public Node getNode(int i){
        return (Node)nodes.get(i);
    }
    
    private int id;
    private String label;
    private Vector nodes;
}
