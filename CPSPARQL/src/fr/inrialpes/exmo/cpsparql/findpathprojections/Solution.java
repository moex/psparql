/*
 * Solution.java
 *
 * Created on February 9, 2006, 9:26 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.util.Vector;
/**
 *
 * @author faisal
 */
public class Solution {
    
    /** Creates a new instance of Solution */
    public Solution(Vector cs, Vector ims) {
        concepts=new Vector(cs);
        images = new Vector(ims);
       
    }
    
    /** Creates a new instance of Solution */
    public Solution() {
        concepts=new Vector();
        images = new Vector();       
    }    
    
    /** copy constructor */
    public Solution(Solution sol) {
        this.concepts=new Vector(sol.concepts);
        this.images = new Vector(sol.images);
    }
        
    public Vector getConcepts(){
        return concepts;
    }
    
    public void setConcepts(Vector cs){
        concepts=new Vector(cs);
    }
    
    public void addConcept(String s){
        concepts.add(s);
    }
    
    public void addImage(String image){
        images.add(image);
    }
    
    
    public void setImages(Vector ims){
        //System.out.println("setImage "+(System)(ims.get(0)));
        images= new Vector(ims);
    }
    
    public Vector getImages(){
        return images;
    }
   
    public String print(){
        String s =new String("[");
        for(int i=0;i<concepts.size() ;i++){
            if (i<concepts.size()-1)
                s+="("+(String)(concepts.get(i))+","+(String)(images.get(i))+")"+",";
            else
                s+="("+(String)(concepts.get(i))+","+(String)(images.get(i))+")";
                
        }
        s+="]";
        System.out.println(s);            
        return s;
    }
   
    private Vector concepts;    
    private Vector images;
    
}
