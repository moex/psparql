/*
 * TransCandidate.java
 *
 * Created on February 1, 2006, 11:46 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.util.Vector;

/**
 *
 * @author faisal
 */
public class TransCandidate {
    
    /**
     * Creates a new instance of TransCandidate 
     */
    public TransCandidate() {
        transitionIndex=-1;
        candidate=new String("");
        label=new String("");
        variable=new String("");
    }
   
    /**
     * Creates a new instance of TransCandidate 
     */
    public TransCandidate(String c,int index,String l, String var) {
        transitionIndex=index;
        candidate=new String(c);
        label=new String(l);
        variable =new String(var);
    }
   
     /** copy constructor */
    public TransCandidate(TransCandidate tc) {
        this.transitionIndex=tc.transitionIndex;
        this.candidate=tc.candidate;
        this.label=tc.label;
        this.variable =tc.variable;
    }
   
    
    public String getLabel(){ 
        return label;
    }
    
    public String getVar(){ 
        return variable;
    }
    
    public String getCand(){ 
        return candidate;
    }
    
    public int getTransition(){
        return transitionIndex;
    }
    
    
    public void setTransitionIndex(int i){
        transitionIndex=i;
    }
    
    
    public void prindTCand(){
        System.out.println(" transitionIndex"+transitionIndex+"    candidate="+candidate+"  label="+label+" Var="+variable);
    }
    
     public void prindTCand(TransCandidate tc){
        System.out.println(" transitionIndex"+tc.transitionIndex+"    candidate="+tc.candidate+"  label="+label+" Var="+variable);
    }
    private int transitionIndex;
    private  String candidate;
    private String label;
    private String variable;
}
