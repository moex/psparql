/* * FindPathProjections.java
 *
 * Created on February 1, 2006, 11:31 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import fr.inrialpes.exmo.cpsparql.automate.SolvUniversalRPQ;
import fr.inrialpes.exmo.cpsparql.automate.PathSolution;
import java.util.Vector;
import java.io.*;
import java.util.Hashtable;

/**
 *
 * @author faisal
 */
public class FindPathProjections {
    
    /**
     * Creates a new instance of FindPathProjections 
     */
    public FindPathProjections(Graph H1,MultiGraph graphG, Vector cREMappings) {
        //System.out.println(" we are at FindPathProjections"+cREMappings.size());
        initialization(H1, graphG,cREMappings);
        this.cREMappings = new Vector(cREMappings); //these are mappings for constraints in regular expressions
        if (H1.size()!=0){
            //System.out.println(" Enter....");
            startFindPaths();
        }        
        //startFindPaths();
    }
    
     public FindPathProjections(Graph H1,MultiGraph graphG) {
        //System.out.println(" we are at FindPathProjections");
        Vector cREMaps = new Vector();
        initialization(H1, graphG,cREMaps);
        if (H1.size()!=0){
            //System.out.println(" Enter....");
            startFindPaths();
        }        
        //startFindPaths();
    }
    
    public FindPathProjections(Graph H1,MultiGraph graphG, Vector partialMap, Vector cREMappings) {
        // System.out.println(" we are at FindPathProjections"+cREMappings.size());
        initialization(H1, graphG,cREMappings);
        this.cREMappings = new Vector(cREMappings);
        lambda = new Vector(partialMap);        
        if (H1.size()!=0){
            //System.out.println(" Enter....");
            startFindPaths();
        }        
        //startFindPaths();
    }
    
    public void initialization(Graph H1, MultiGraph graphG,Vector cREMaps){ 
        startTime = System.currentTimeMillis();
        simplePaths = false;
        NS=0;
        H = new Vector();
        /*GraphG= copy(graphG, 0,graphG.length());
        readGraph(GraphG);      //readGraphG();        */
        G=graphG;
        Hg =H1;                  //readGraphH();
        //G.print();
        Hg.print("H");
        order(Hg);
        calcPrevU(Hg);
        //printH();
        currentConcept = new String("");
        solutions = new Vector();      
        lambda = new Vector();
        GlobalIndex=2;   
        
        int size = cREMaps.size();
        //System.out.println(size);
        alreadyConstructedHashT = new boolean [size];
        hashTableForMaps = new Hashtable[size];
        for(int i=0; i<size; i++){
            //System.out.println(size);
            alreadyConstructedHashT[i]=false;
            hashTableForMaps[i] = new Hashtable();
            //System.out.println(i+"\t"+alreadyConstructedHashT[i]);
        }
       //if (size==1) System.out.println(alreadyConstructedHashT[0]); 
    }
    
    public String copy(String graphG, int from, int to){
        String str= new String("");
        from =graphG.indexOf('<')+1;
        to =graphG.indexOf('>');
        for(int i=from;i<to;i++)
            str+=graphG.charAt(i);
        return str;
    }
    
    public int positionInH(String concept,Vector H){
        Concepts c = new Concepts();
        int i =0;
        boolean found=false;
        int location =-1;
        while((!found) && (i<H.size())){
            c =(Concepts)H.get(i);
            if (concept.equals(c.getConcept())){
                location =i;
                found =true;
            }
            i++;
        }
        return location;
    }
    
    public void testAndAdd(String concept, Vector H){
        int location;
        Concepts c;
        Vector v = new Vector();
        location =positionInH(concept, H);
            if (location <0){
                c= new Concepts(concept,v, " ", v, v);
                H.add(c);
            }                
            
    }
    
    
    public void readGraph(String GraphG){
        G= new MultiGraph();        
        try {
            BufferedReader in = new BufferedReader(new FileReader(GraphG));
            String subject=new String(""),object=new String(""),predicate=new String("");
            char c;
            int value;
            int index =1;
            while ((value=in.read())!=-1) { 
                c= (char)(value);
                switch (index){
                    case 1:{
                        if (c!=',')
                            subject+=c;
                        else index=2;
                        break;
                    }
                    case 2:{
                        if (c!=',')
                            predicate+=c;
                        else index=3;
                        break;
                    }
                    case 3:{
                        if(c!='}')
                            object+=c;
                        else {
                            //G.setTriple(new Triple(0, subject, predicate,object, " ",  " ", ""));
                            G.setTriple(subject, predicate,object);
                            String s=in.readLine();
                            index =1;        
                            subject="";
                            object="";                  
                            predicate="";                        
                        }                   
                        break;
                    }
                }
            }
            in.close();
        } catch (IOException e) {
        }

    }
    
    
    
    public void order(Graph Hg){
        //System.out.println("Order");
        //Hg.print("Hg");
        Concepts c;
        Vector v = new Vector();
        c= new Concepts(" ",v, " ", v, v);   
        H.add(0,c);
        String sub,obj = new String("");
        for(int i=0;i<Hg.getTriplesNom();i++){   //may be Hg.size()
            sub = Hg.TripleAt(i,0).getSubject();
            obj = Hg.TripleAt(i,0).getobject();
            if (obj.startsWith("?") || obj.startsWith("$")){
               testAndAdd(sub, H);
               testAndAdd(obj, H); 
            }
            else {
                testAndAdd(obj, H);
                testAndAdd(sub, H); 
            }
            
            /*if (Hg.atomic(i, 0)){
                node = Hg.TripleAt(i,0).getPredicate();
                testAndAdd(node, H);
            } */               
            
        }
    }
    
    
    public void calcPrevU(Graph Hg){
        Vector prevu;
        String object= new String(""),subject = new String("");
        int posObject,posSubject,pos;
        Concepts c =new Concepts();
        for(int i=0;i<Hg.getTriplesNom();i++){    // may be Hg.size()
            object = Hg.TripleAt(i,0).getobject();
            subject = Hg.TripleAt(i,0).getSubject();
            posObject =positionInH(object,H);
            posSubject =positionInH(subject,H);
            if(posObject > posSubject)
                pos=posObject;
            else
                pos =posSubject;
            c = (Concepts)(H.get(pos));  
            c.getPrevU().add(i);
            c.getPointoTriples().add(Hg.TripleAt(i,0));
        }
    }
    
    
    public void printH(){
        System.out.println("____________________________________________________");        
        String s = new String("");
        String s1 = new String("");
        Concepts con = new Concepts();
        for(int i=0;i<H.size();i++){
             con = (Concepts)(H.get(i));
             s1=con.printPrevU(); 
             if (i<=level-1)
                   s += "("+con.getConcept()+","+con.getImage()+","+s1+")"+",\n";
               else
                   s +=  "("+con.getConcept()+","+con.getImage()+","+s1+")\n";        
        }        
        System.out.println("H= {"+s+"}");
        System.out.println("____________________________________________________");
        VarImage vi;
        s="";
        /*for(int i=0;i<lambda.size();i++){
            vi = (VarImage)lambda.get(i);
            s+="("+(String)vi.getVar()+","+(String)vi.getImage()+"),  ";
        }
        System.out.println("lambda=    "   +s+"\n");*/
    }
    
    public Solution getSolutionFromH(){
        Solution sol=new Solution();
        Vector concepts = new Vector();
        Vector images = new Vector();
        Concepts c = new Concepts();
        for(int i=1;i<H.size();i++){
            c = (Concepts)(H.get(i));
            concepts.add(c.getConcept());
            images.add(c.getImage());
            //System.out.println("get "+c.getConcept()+c.getImage());
        }
        sol.setConcepts(concepts);
        sol.setImages(images);
        return sol;
    }
   
    public Solution getSolutionFromLambda(){
        Solution sol=new Solution();
        Vector concepts = new Vector();
        Vector images = new Vector();
        VarImage vi;
        for(int i=0;i<lambda.size();i++){
            vi=(VarImage)lambda.get(i);        
            concepts.add(vi.getVar());
            images.add(vi.getImage());
        }
        sol.setConcepts(concepts);
        sol.setImages(images);
        return sol;
    }
   
    public boolean equal(Solution sol1,Solution sol2){
        boolean eq=false;
        //if((sol1.getConcepts().containsAll(sol2.getConcepts())) 
        //&& (sol1.getImages().containsAll(sol2.getImages())))
        if((sol1.equals(sol2)))
            eq=true;
        return eq;
    }
    
    public boolean InSolution(Vector solutions, Solution solution){
        boolean found =false;
        int i=0;
        Solution sol;
        while((i<solutions.size())&&(!found)){
            sol=(Solution)solutions.get(i);
            if (equal(sol,solution))
                found =true;
            i++;
        }
        return found;
    }
    
    
    public boolean InSolutions(Vector solutions, Solution solution){
        boolean found =false;
        int i=0;
        Solution sol;
        Vector images;
        Vector concepts;
        while((i<solutions.size())&&(!found)){
            sol=(Solution)solutions.get(i);
            if ((sol.getConcepts().equals(solution.getConcepts()))
            && (sol.getImages().equals(solution.getImages())))
                found =true;
            i++;
        }
        return found;
    }
   
    public long getElapsedTimeMsFirstS(){
        return elapsedTimeMsFirstS;
    }
    
    public void startFindPaths(){
        level=1;
        Concepts c= new Concepts();
        c =(Concepts)(H.get(level));
        c.setLambda(lambda);
        currentConcept=c.getConcept();
        BackTrack=false;
        Solution solution=new Solution();
        if(H.size()==0) 
            /*   foundedSolution(A)   */
            solutions.add(solution);
        else{
            while(level!=0){
                //printH();
                 if(level==(H.size())){
                     //System.out.println("founded solution..............................");
                     if (FirstSolution){
                         elapsedTimeMsFirstS = System.currentTimeMillis()-startTime;
                         //System.out.println("elapsed TimeMs First Solution = "+ elapsedTimeMsFirstS);
                     }
                     FirstSolution =false;
                   // printH();
                    /*   foundedSolution(A)  */ 
                    //Hg.print("Hg solution");       
                    solution=getSolutionFromLambda();  //solution=getSolutionFromH();
                    //if (! InSolutions(solutions,solution)){   //InSolution(solutions,solution)  !!!!!!
                      /*  VarImage vi;
                        String s = new String("lambda= [");
                        for(int i=0;i<lambda.size();i++){
                            vi=(VarImage)lambda.get(i);
                            s+="("+vi.getVar()+","+vi.getImage()+"),";
                        }
                        s+="]";
                        System.out.println(s);*/
                         solutions.add(solution);
                    //}
                    solution = new Solution();
                    BackTrack=true;
                    level=PreviousLevel(level);
                }
                else{
                    c=(Concepts)(H.get(level));
                    currentConcept=c.getConcept();
                }
                if(BackTrack)
                    if(AnotherCandidates(currentConcept)){
                    //System.out.println("true another  "+currentConcept);
                    BackTrack=false;
                    level=NextLevel(level);
                    }
                    else{
                    level=PreviousLevel(level);
                    }
                else
                    if(SearchCandidates(currentConcept)){
                     //System.out.println("true search "+currentConcept);
                     level=NextLevel(level);
                    }
                    else{
                     //System.out.println("false search"+currentConcept);
                     BackTrack=true;
                     level=PreviousLevel(level);
                    }                    
            }
        }           
       // printSolutions(solutions);
    }
    
    public int NextLevel(int level){
        Concepts c= new Concepts();        
        ++level;
        if(level<H.size())
            c= (Concepts)(H.get(level));
        //c.printConcept();
        c.setLambda(lambda);
        currentConcept=c.getConcept();     
        return level;
    }
    public int PreviousLevel(int level){
        Concepts c= new Concepts(); 
       if(level<H.size()){
            c= (Concepts)(H.get(level));
            c.setImage(" ");
        }
        --level;
        lambda = c.getLambda();
        c= (Concepts)(H.get(level));
        currentConcept=c.getConcept();     
        GlobalIndex=c.getGlobalIndex();
        //System.out.println(" previous  "+currentConcept);
        return level;
    }
    
    
    public int hasImage(Vector v, String var){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if (var.equals(vi.getVar())){
                found =true;
                location=i;
            }
            i++;
        }
        return location;
    }
    
    public boolean InVarImage(Vector v, String var, String str){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if ((var.equals(vi.getVar())) &&(str.equals(vi.getImage()))){
                found =true;
                location=i;
            }
            i++;
        }
        return found;
    }
    
    public boolean preorder(String c1, String c2){
        if(((c2.charAt(0)=='?') || (c2.charAt(0)=='$') ||(c2.charAt(0)=='_'))){
            VarImage vi; 
            int location =hasImage(lambda,c2);
            if (location < 0){                
                /*if (!InVarImage(tempLambda, c2,c1)){
                    vi= new VarImage(c2, c1); 
                    tempLambda.add(vi);
                }*/
                return true;
            }
            else {
                vi=(VarImage)(lambda.get(location));
                if (vi.getImage().equals(c1))
                    return true;
                else return false;
            }
        }
        
        else if(c2.charAt(0)=='!'){
            String s2=new String("");
            s2 =copy(c2,1);
            if(!s2.equals(c1))
                return true;
            else 
                return false;
        }
        else if(c1.equals(c2))
            return true;
        else 
            return false;
    }
    
    public String copy(String s1,int i){
        String s = new String("");
        for(int j=i;j<s1.length();j++)
            s+=s1.charAt(j);
        return s;
    }
    
    
    
    public void printSolutions(Vector solutions){
        System.out.println("____________________________________________________");
        System.out.println("Solutions= {");
        Solution sol;
        for(int i=0;i< solutions.size();i++){
            sol = (Solution)(solutions.get(i));
        }
        System.out.println("}-end-solutions");
        System.out.println("#Solutions= "+NS);
        System.out.println("____________________________________________________");

    }
    
    public boolean Consistent(Vector lambda1, Vector lambda2){
        boolean consistent=true;
        int i=0;
        int location;
        while((consistent) &&(i<lambda1.size())){
            VarImage vi;
            vi = (VarImage)lambda1.get(i);
            location = hasImage(lambda2,vi.getVar());
            if(location>=0){
                VarImage vi2;
                vi2=(VarImage)lambda2.get(location);
                if(!vi2.getImage().equals(vi.getImage()))
                    consistent=false;
            }
            i++;
        }
        return consistent;
    }
    
    public Vector merge(Vector lambda1,Vector lambda2){
        for(int i=0;i<lambda2.size();i++)
            if(!lambda1.contains(lambda2.get(i)))
                lambda1.add(lambda2.get(i));
        return lambda1;
    }
    
    public boolean contains(Vector solutions, PathSolution solution,int index,int index_i){
        int point1,point2;
        switch (index){ 
            case 3:{
                //System.out.println("faisal");
                point1=solution.getStartPoint();
                break;
            }
            default:{
                point1=solution.getEndPoint();
                break;
            }
                
        }
        
        int i=0;
        boolean found=false;
        PathSolution ps = new PathSolution();
        //System.out.println("solutions.size()="+solutions.size());
        while((i<solutions.size()) && (!found)){
            ps =(PathSolution)(solutions.get(i));
            //System.out.println("i="+i+" ps.endPoint="+ps.getEndPoint());
            switch (index_i){ 
                case 3:{
                    point2=ps.getStartPoint();
                    break;
                }
                default:{
                    //System.out.println("ebtesam");
                    point2=ps.getEndPoint();
                    break;
                }
            }
            //System.out.println("point1="+point1+"   point2="+point2);
            if((point1==point2) 
            //&& (ps.getStartPoint().equals(solution.getStartPoint())) 
            && (Consistent(ps.getLambda(),solution.getLambda()))
            ){
                    //(ps.getLambda().containsAll(solution.getLambda()))
                ps.setLambda(merge(ps.getLambda(),solution.getLambda()));
                found=true;
            }
            i++;
        }
        //System.out.println(found);
        return found;
    }
    
    public Vector filter(Vector toBeFiltered, Vector usedToFilter,int index,int index_i){
        int point1,point2;
        PathSolution ps = new PathSolution();
        Vector tobefiltered = new Vector();
        int s=0;
        int size = toBeFiltered.size();
        //System.out.println("size before"+toBeFiltered.size()+"  "+usedToFilter);
        while(s<toBeFiltered.size()){
            //System.out.println("s="+s);
            ps =(PathSolution)(toBeFiltered.get(s));
            //System.out.println("ps.getStartPoint()="+ps.getStartPoint()+ps.getEndPoint());
            if(!contains(usedToFilter, ps,index,index_i)){
                //toBeFiltered.remove(s);
                //System.out.println("DELETED");
            }
            else{
                //System.out.println("Found");
                boolean found =false;
                PathSolution paths;
                int k=0;
                switch (index){ 
                    case 3:{
                        point1=ps.getStartPoint(); 
                        break;
                    }
                    default:{
                        point1=ps.getEndPoint(); 
                        break;
                    }

                }

                while((k<usedToFilter.size())){
                    //System.out.println("k="+k+"   s="+s);
                    paths = new PathSolution((PathSolution)usedToFilter.get(k));
                    switch (index_i){ 
                        case 3:{
                            point2=paths.getStartPoint();
                            break;
                        }
                        default:{
                            point2=paths.getEndPoint();
                            break;
                        }

                    }                    
                    if((point1==point2) 
                    //&& (paths.getStartPoint().equals(ps.getStartPoint())) 
                    //&& (paths.getLambda().containsAll(ps.getLambda()))
                    && Consistent(paths.getLambda(), ps.getLambda())){
                        found =true;
                         ps.setLambda(merge(ps.getLambda(),paths.getLambda()));
                    }
                    k++;
                if(!found);
                    //toBeFiltered.remove(s);
                else 
                    tobefiltered.add(ps);
                }
            }
            s++;
            //size=toBeFiltered.size();
            
        }
        toBeFiltered= tobefiltered;
        //System.out.println("size after="+toBeFiltered.size());
        return tobefiltered;
    }
    
    
    public Vector DemandeCandidates(String concept){
        GlobalIndex =2;
        Vector  v = new Vector();
        Vector  tempV= new Vector();
        Node node = new Node(concept);
        String subject=new String("");
        String predicate=new String("");               
        String object=new String(""); 
        PathSolution psol = new PathSolution();
        if (!variable(concept)){  // !!!! When it is constant, it is the only candidate for itself
            int index = G.contain(concept); // changed in 9-5-2008, it was (node)
            if (index >=0){
                psol.setStartPoint(index);
                psol.setEndtPoint(index);
                psol.setLambda(lambda);
                v.add(psol);
            }
        }
        else{
            for(int i=0;i<G.getNodes().size();i++){
                //System.out.println(node.getLabel());
                node = G.getNode(i);
                if( preorder(node.getLabel(), concept)){
                    psol = new PathSolution();
                    psol.setStartPoint(i);
                    psol.setEndtPoint(i);
                    psol.setLambda(lambda);
                    v.add(psol); 
                }
            }                
        }            
        return v;
    }

        
    public Vector calculateCands(String concept){
        tempLambda= new Vector(lambda);
        //System.out.println(concept);
        Vector  v = new Vector();
        Vector  tempV= new Vector();
        Concepts con;
        con = (Concepts)(H.get(level));
        Triple tripleOfG,tripleOfH;
        Vector edges = new Vector();
        edges =con.getPointoTriples();                    
        String subject=new String("");
        String predicate=new String("");               
        String object=new String(""); 
        SolvUniversalRPQ fp;// = new SolvUniversalRPQ();
        //System.out.println(alreadyConstructedHashT[0]);
        if (con.getPrevU().size()==0)
            v=DemandeCandidates(concept);
        else{
                    tripleOfH = (Triple)(edges.get(0));
                    Vector pathsolutions = new Vector();
                    //System.out.println(tripleOfH.getSubject()+tripleOfH.getPredicate());
                    boolean sub=(tripleOfH.getSubject().equals(concept));
                    boolean obj=(tripleOfH.getobject().equals(concept));
                    int index,index_i;
                    if(obj && sub){                        
                        index=1;
                        GlobalIndex=index;                        
                        fp= new SolvUniversalRPQ(tripleOfH.getSubject(), -1,tripleOfH.getPredicate(),tripleOfH.getobject(),lambda,G,simplePaths,cREMappings); 
                        pathsolutions= fp.getSolutions();
                    }
                    else if(obj){
                        index=2;
                        GlobalIndex=index;                        
                        fp= new SolvUniversalRPQ(tripleOfH.getSubject(),tripleOfH.getSubjectImage(),tripleOfH.getPredicate(),tripleOfH.getobject(),lambda, G,simplePaths,cREMappings); 
                        pathsolutions= fp.getSolutions();
                    }
                    else {
                        index=3;
                        GlobalIndex=index;
                        fp= new SolvUniversalRPQ(tripleOfH.getSubject(), -1,tripleOfH.getPredicate(),G.getNode(tripleOfH.getobjectImage()).getLabel(),lambda, G, simplePaths,cREMappings); 
                        //System.out.println("index"+index);
                        pathsolutions= fp.getSolutions();
                        //System.out.println("pathsolutions.size()="+pathsolutions.size());                            
                        //PathSolution ps = (PathSolution)pathsolutions.get(0);
                        //System.out.println(ps.getStartPoint());
                    }
                    Vector pathsolutions_i = new Vector();
                    for(int count=1;count<con.getPrevU().size();count++){
                        tripleOfH = (Triple)(edges.get(count));
                        //System.out.println(tripleOfH.getSubjectImage()+tripleOfH.getPredicate());
                        boolean sub_i=(tripleOfH.getSubject().equals(concept));
                        boolean obj_i=(tripleOfH.getobject().equals(concept));
                        if(obj_i && sub_i){                        
                            index_i=1;
                            fp= new SolvUniversalRPQ(tripleOfH.getSubject(), tripleOfH.getSubjectImage(),tripleOfH.getPredicate(),tripleOfH.getobject(),lambda,G,simplePaths,cREMappings); 
                            pathsolutions_i= fp.getSolutions();
                        }
                        else if(obj_i){
                            index_i=2;
                            fp= new SolvUniversalRPQ(tripleOfH.getSubject(),tripleOfH.getSubjectImage(),tripleOfH.getPredicate(),tripleOfH.getobject(),lambda, G, simplePaths,cREMappings); 
                            //System.out.println("index_i="+index_i);
                            pathsolutions_i= fp.getSolutions();
                            //System.out.println("pathsolutions_i.size()="+pathsolutions_i.size());                            
                        }
                        else {
                            index_i=3;
                            fp= new SolvUniversalRPQ(tripleOfH.getSubject(), -1,tripleOfH.getPredicate(),G.getNode(tripleOfH.getobjectImage()).getLabel(),lambda, G,simplePaths,cREMappings); 
                            pathsolutions_i= fp.getSolutions();
                        }                    
                        pathsolutions=filter(pathsolutions,pathsolutions_i,index,index_i);
                        

                    }
                    v=pathsolutions;
        } 
        /*if(this.cREMappings.size()>1){
        hashTableForMaps[0] = fp.getHashTablesForMaps()[0];
        alreadyConstructedHashT[0] = fp.getAlreadyConstructedHashT()[0];
        //System.out.println(alreadyConstructedHashT[0]);
        }*/
        return v;
    }
    
    public void putCandInImage(String concept,String image,int level,int index){
        //System.out.println(concept+image);
        Triple triple;
        Concepts con;
        con = (Concepts)(H.get(level));
        con.setImage(image);
        con.setIndex(index);
        for(int i=0;i<Hg.getTriplesNom();i++){  // may be Hg.size()
            triple = Hg.TripleAt(i, 0);
            if(triple.getSubject().equals(concept))
                triple.setSubjectImage(index);
            if(triple.getPredicate().equals(concept))
                triple.setPredicateImage(index);
            if(triple.getobject().equals(concept))
                triple.setObjectImage(index);
        }
    }
    
    public boolean variable(String var){
        if ((var.charAt(0)=='?') ||(var.charAt(0)=='$') ||(var.charAt(0)=='_'))
            return true;
        else return false;
    }
    
    public boolean SearchCandidates(String concept){
        int point1;
        boolean foundCand =false;
        Vector cands = new Vector();
        Concepts con;
        con= (Concepts)(H.get(level));
        cands = calculateCands(concept);
        con.setCandidates(cands);
        con.setNextCand();
        foundCand=!con.emptyCandidates();
        if(foundCand){
            PathSolution ps= new PathSolution();
            VarImage vi;
            ps =con.getNextCand();
            con.setGlobalIndex(GlobalIndex); 
            switch (GlobalIndex){ 
                case 3:{
                    point1=ps.getStartPoint();
                    break;
                }
                default:{
                    point1=ps.getEndPoint();
                    break;
                }

            }
        
            
            lambda=ps.getLambda();
            Node node = (Node)G.getNodes().get(point1);                                
            if (variable(concept)){
                if(hasImage(lambda,concept)<0){
                    vi=new VarImage(concept, node.getLabel(), point1);  //ps.getEndPoint()
                    lambda.add(vi);
                }
            }
            putCandInImage(concept, node.getLabel(),level,point1);  //ps.getEndPoint()
            int location=-10;
            for(int j=0;j<lambda.size();j++){
                vi = new VarImage((VarImage)(lambda.get(j)));
                location = InH(vi.getVar());
                if (location>=0)
                    putCandInImage(vi.getVar(),vi.getImage(),location,vi.getIndex());
            }
        }
        return (foundCand);
    }
    
    
    public boolean AnotherCandidates(String concept){
        int point1;
        boolean foundCand =false;
        Concepts con;
        con = (Concepts)(H.get(level));
        foundCand=!con.emptyCandidates();
        if(foundCand){
            PathSolution ps= new PathSolution();
            ps =con.getNextCand();
            con.setGlobalIndex(GlobalIndex); 
            switch (GlobalIndex){ 
                case 3:{
                    point1=ps.getStartPoint();
                    break;
                }
                default:{
                    point1=ps.getEndPoint();
                    break;
                }

            }
            //ps.print();
            lambda=ps.getLambda();
            VarImage vi;
            Node node = (Node)G.getNodes().get(point1);                    
            if (variable(concept)){
                if(hasImage(lambda,concept)<0){
                    vi=new VarImage(concept, node.getLabel(), point1);  //ps.getEndPoint()
                    lambda.add(vi);
                }
            }
            putCandInImage(concept, node.getLabel(),level,point1);   //ps.getEndPoint()
            int location=-10;
            for(int j=0;j<lambda.size();j++){
                vi = new VarImage((VarImage)(lambda.get(j)));
                location = InH(vi.getVar());
                if (location>=0)
                    putCandInImage(vi.getVar(),vi.getImage(),location,vi.getIndex());
            }
        }
        return (foundCand);
    }
    
    
    public int InH(String var){
        boolean found=false;
        int location=-10;
        int counter=0;
        while((counter<H.size()) && (!found)){
            Concepts con=(Concepts)(H.get(counter));
            if(con.getConcept().equals(var)){ 
                found=true;
                location=counter;
            }
            counter++;
        }
        return location;
    }
    
    
    public MultiGraph getGraphG(){
        return G;
    }
    
    
    public Vector Solutions(){
        return solutions;
    }
    
    public String getSolutions(){
        String str = new String("");
        str+="#Solutions= "+NS+"\n";
        str+="____________________________________________________\n";
        str+="Solutions= {\n";
        Solution sol;
        for(int i=0;i< solutions.size();i++){
            sol = (Solution)(solutions.get(i));
            str+=sol.print()+"\n";
        }
        str+="}-end-solutions\n";
        str+="____________________________________________________\n";
        return str;
    }
            
            
    private Vector H;
    private Graph Hg;
    private MultiGraph G;
    private int level=1;
    private boolean BackTrack;
    private String currentConcept;
    private Vector solutions;
    private Vector lambda;
    private Vector tempLambda,cREMappings;
    private int NS;
    private int GlobalIndex;
    private String GraphG;
    private boolean simplePaths;
    private long startTime;
    private long elapsedTimeMsFirstS;
    private boolean FirstSolution = true;
    private Hashtable [] hashTableForMaps;
    private boolean [] alreadyConstructedHashT;
}
