/*
 * MultiGraph.java
 *
 * Created on March 8, 2006, 3:56 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.util.Vector;
import java.util.Hashtable;

/**
 *
 * @author faisal
 */
public class MultiGraph {
    
    /** Creates a new instance of MultiGraph */
    public MultiGraph() {
        edges =new Vector();
        InEdges =new Vector();
        nodes =new Vector();
        hashTOfNodes =new Hashtable();
    }
    /** Creates a new instance of MultiGraph */
    public MultiGraph(boolean qContainInverseOpertor) {
        edges =new Vector();
        InEdges =new Vector();
        nodes =new Vector();
        hashTOfNodes =new Hashtable();
        this.qContainInverseOperator = qContainInverseOpertor;
    }
    
    
    /** copy constructor */
    public MultiGraph(MultiGraph MG) {
        this.edges =new Vector(MG.edges);
        this.InEdges =new Vector(MG.InEdges);
        this.nodes = new Vector(MG.nodes);
        this.hashTOfNodes =new Hashtable(MG.hashTOfNodes);
        this.qContainInverseOperator = MG.qContainInverseOperator;
    }
    
    public void setNodes(Vector ns){
        nodes =new Vector(ns);
    }
    
    
    public Vector getNodes(){
        return nodes;
    }
    
    
    public void setEdges(Vector es){
        edges =new Vector(es);
    }
    public Vector getEdges(){
        return edges;
    }
    
    public void setInEdges(Vector es){
        InEdges =new Vector(es);
    }
    public Vector getInEdges(){
        return InEdges;
    }
    
    public Node getNode(int i){
        return (Node)nodes.get(i);
    }
    
    public Edge getEdge(int i){
        return (Edge)edges.get(i);
    }
    
    public Edge getInEdge(int i){
        return (Edge)InEdges.get(i);
    }
    public void setHashTOfNodes(Hashtable ht){
        hashTOfNodes = new Hashtable(ht);
    }
    public Hashtable getHashTOfNodes(){
        return hashTOfNodes;
    }

    public int containOld(Node n){
        boolean found =false;
        int location=-10,i=0;
        String lab;
        Node n2;
        while((!found)&&(i<nodes.size())){
            n2 = (Node)nodes.get(i);
            lab =n2.getLabel();
            if (lab.equals(n.getLabel())){
                found =true;
                location=i;
            }
            i++;
        }        
        return location;
    }
    public int contain(String key){
        //if (hashTOfNodes.containsKey(key))
          //  System.out.println("we are at contain()"+key+" "+hashTOfNodes.get(key)); 
        if (hashTOfNodes.get(key)==null)        
            return -10;
        else return (Integer)hashTOfNodes.get(key);
    }

    public void setTriple(String subject,String predicate,String object){
        //System.out.println("we are at setTriple()"); 
        Node s =new Node(subject,qContainInverseOperator); 
        Node o =new Node(object,qContainInverseOperator);
        Edge e =new Edge(predicate); 
        Edge InEdge =new Edge(predicate); 
        int subIndex = contain(subject);
        int objIndex = contain(object);
        //System.out.println("we exit contain()"); 
        int nextId;
        if (objIndex <0){
            nextId = nodes.size();
            o.setId(nextId);
            nodes.add(o);
            hashTOfNodes.put(object,nextId);
        }
        else 
            o=(Node)nodes.get(objIndex);
        e.addNode(s); 
        e.addNode(o);
        e.setId(edges.size());
        edges.add(e);
        edges.add(e);
        if (subIndex <0){
            nextId = nodes.size();
            s.addEdge(e);  
            s.setId(nextId);
            nodes.add(s);
            hashTOfNodes.put(subject,nextId);
        }
        else {
            s=(Node)nodes.get(subIndex);
            s.addEdge(e);
        }
        if(qContainInverseOperator){
            //System.out.println("we add an inverse edge");
            InEdge.addNode(o);
            InEdge.addNode(s);
            InEdge.setId(InEdges.size());
            o.addInEdge(InEdge);
        } 
        //System.out.println("we exit at setTriple()"); 
    }
    
    public void setMark(int index,int m){
        Node node =(Node)nodes.get(index);
        node.setMark(m);
    }
    
    public void unMark(int index){
        Node node =(Node)nodes.get(index);
        node.setMark(-1);
    }
    
    public int getMark(int index){
        Node node =(Node)nodes.get(index);
        return node.getMark();
    }
        
    public void print(){
        Node n,n2;
        Vector es;
        Edge e;
        for(int i=0;i< nodes.size();i++){
             n = (Node)nodes.get(i);
             es=n.getEdges();
             for(int j=0;j< es.size();j++){
               e =(Edge)es.get(j);  
               n2 =(Node)e.getNodes().get(1);
               System.out.println(n.getLabel()+"  "+e.getLabel()+"  "+n2.getLabel());
             }
         }
       
    }
    
    private Vector nodes;
    private Hashtable hashTOfNodes;
    private Vector edges;
    private Vector InEdges;
    private boolean qContainInverseOperator;
}
