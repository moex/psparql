/*
 * Triple.java
 *
 * Created on January 3, 2006, 2:52 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

/**
 *
 * @author faisal
 */
public class Triple {
    
    /** Creates a new instance of Triple */
    public Triple(int t, String s, String p, String o, int si, int pi, int oi) {
        type=t;
        this.subject= new String(s);
        this.predicate=new String(p);
        this.object= new String(o);
        this.subjectImage=si;
        this.predicateImage=pi;
        this.objectImage=oi;
    }
    
    public Triple(Triple t) {
        this.type=t.type;
        this.subject= new String(t.subject);
        this.predicate=new String(t.predicate);
        this.object=new String(t.object);
        this.subjectImage= t.subjectImage;
        this.predicateImage=t.predicateImage;
        this.objectImage=t.objectImage;
    }
    
    public int getSubjectImage(){
        return subjectImage;
    }
    public String getSubject(){
        return subject;
    }
    
    public int getPredicateImage(){
        return predicateImage;
    }
   
    public void setSubjectImage(int si){
        subjectImage=si;
    }
    
    public void setObjectImage(int oi){
        objectImage=oi;
    }
    
    public void setPredicateImage(int pi){
        predicateImage=pi;
    }
    
    public String getPredicate(){
        return predicate;
    }
    
    public String getobject(){
        return object;
    }
    
    public int gettype(){
        return type;
    }
    
    public int getobjectImage(){
        return objectImage;
    }

    private int type;
    private String subject;
    private String predicate;
    private String object;
    private int subjectImage;
    private int predicateImage;
    private int objectImage;
}
