/*
 * Dataset.java
 *
 * Created on February 6, 2007, 9:57 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import java.util.Vector;

/**
 *
 * @author faisal
 */
public class Dataset {
    
    /** Creates a new instance of Dataset */
    public Dataset() {
        defaultGraph = new MultiGraph();
        namedGraphs = new Vector();
        namedGraphIris = new Vector();
    }
    
    public void setDefaultGraph(MultiGraph G){
       // System.out.println("set default graph ...");
        defaultGraph = new MultiGraph(G);
    }

    public MultiGraph getDefaultGraph(){
        return defaultGraph;
    }
    
    public void addNamedGraph(MultiGraph G,String iri){
        namedGraphs.add(G);
        namedGraphIris.add(iri);
    }

    public MultiGraph getNamedGraph(int i){
        return (MultiGraph)namedGraphs.get(i);
    }
    
    public MultiGraph getNamedGraph(String iri){
        int i = namedGraphIris.indexOf(iri);
        if (i>=0)
            return (MultiGraph)namedGraphs.get(i);
        else{
            MultiGraph G = new MultiGraph();
            return G;
        }
    }

    
    MultiGraph defaultGraph;
    Vector namedGraphs;
    Vector namedGraphIris;
}
