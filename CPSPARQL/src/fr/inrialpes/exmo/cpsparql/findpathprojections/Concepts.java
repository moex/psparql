/*
 * Concepts.java
 *
 * Created on February 1, 2006, 11:20 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

import fr.inrialpes.exmo.cpsparql.automate.PathSolution;
import java.util.Vector;
import java.lang.Integer;
/**
 *
 * @author faisal
 */
public class Concepts {
    
    /** Creates a new instance of Concepts   */
    public Concepts(String c, Vector v, String im, Vector cands, Vector pTT) {
        lambda = new Vector();
        concept=c;
        PrevU = new Vector(v);
        image = new String(im);
        candidates = new Vector(cands);
        pointToTriples = new Vector(pTT);
        nextCand= -1;
        GlobalIndex=2;
    }
    
    /*** Creates a new instance of Concepts    */
    public Concepts() {
        GlobalIndex=2;
        lambda = new Vector();
        concept=new String(" ");
        image = new String(" ");
        PrevU = new Vector();
        candidates = new Vector();
        pointToTriples = new Vector();
        nextCand =-1;
    }
    
    /*** copy constructor   */
    public Concepts(Concepts c) {
        this.GlobalIndex=c.GlobalIndex;
        this.lambda = new Vector(c.lambda);
        this.concept=new String(c.concept);
        this.image = new String(c.image);
        this.PrevU = new Vector(c.PrevU);        
        this.candidates = new Vector(c.candidates);
        this.pointToTriples = new Vector(c.pointToTriples);
        this.nextCand =c.nextCand;
    }
    
    void printConcept(){
        System.out.println(concept);
    }
    
     public String printPrevU(){
        //System.out.println("____________________________________________________");
        PathSolution cand =new PathSolution();
        String s1 = new String();
        String s = new String("[");        
        Integer value;
        for(int i =0;i<PrevU.size();i++){
            value=(Integer)PrevU.get(i);
            s+=value.toString()+"  ";
        }
        s+="],";
        s1+=s+"[";
        for(int i=0;i<candidates.size() ;i++){
            cand=(PathSolution)(candidates.get(i));
            s1+=cand.print();                
        }        
        s1+="]";
        return s1;
    }
   
   public int getIndex(){
       return index;
   }
   
   public void setIndex(int i){
       index=i;
   }
   
   
    public String getConcept(){
        return concept;
    }
    
    public PathSolution getNextCand(){
        nextCand++;
        return (PathSolution)(candidates.get(nextCand));
    }
  
    
    
    public void setConcept(String c){
        concept=c;
    }
    
    public void setCandidates(Vector cands){
        candidates = new Vector(cands);
    }
    
    public Vector getCandidates(){
        return candidates;
    }
    
    public void setImage(String im){
        image= new String(im);
    }
    
    public String getImage(){
        return image;
    }
   
    public Vector getPrevU(){
        return PrevU;
    }
    
    public Vector getPointoTriples(){
        return pointToTriples;
    }
    
    public void setPrevU(Vector v){
        PrevU = new Vector(v);
    }
    
    public void setPointToTriples(Vector v){
        pointToTriples = new Vector(v);
    }
    
    public boolean emptyCandidates(){
        return (((nextCand+1)==candidates.size()) ||(candidates.size()==0));
    }
    
    public void setNextCand(){
        nextCand=-1;
    }
    
    public void setLambda(Vector lam){
        lambda = new Vector();
    }
    
    public Vector getLambda(){
        return lambda;
    }
    
    public int getGlobalIndex(){
        return GlobalIndex;
    }
    
    public void setGlobalIndex(int index){
        GlobalIndex =index;
    }
    
    private Vector lambda;
    private String concept;    
    private String image;
    private Vector PrevU;
    private Vector pointToTriples;
    private Vector candidates;    
    private int nextCand;
    private int GlobalIndex;
    private int index;
}
