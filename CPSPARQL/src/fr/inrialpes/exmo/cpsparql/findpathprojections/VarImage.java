/*
 * VarImage.java
 *
 * Created on February 8, 2006, 1:43 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;

/**
 *
 * @author faisal
 */
public class VarImage {
    
    /** Creates a new instance of VarImage */
    public VarImage() {
        var = new String("");
        image = new String("");
        index=-1;
    }
    
    
    /** Creates a new instance of VarImage */
    public VarImage(String v,String im,int i) {
        var = new String(v);
        image = new String(im);
        index=i;
    }
    
    /** copy constructor */
    public VarImage(VarImage vi) {
        this.var = new String(vi.var);
        image = new String(vi.image);
        this.index =vi.index;
    }
    
    public String getVar(){
        return var;
    }
    
    public String getImage(){
        return image;
    }
    
    public void setIndex(int i){
        index=i;
    }
    
    public int getIndex(){
        return index;
    }
    
    private String var;
    private String image;
    private int index;
}
