/*
 * StateCands.java
 *
 * Created on February 1, 2006, 1:17 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.findpathprojections;


import java.util.Vector;
/**
 *
 * @author faisal
 */
public class StateCands {
    
    /** Creates a new instance of StateCands */
    public StateCands() {
        //System.out.println("we are StateCands constructor");
        firstConceptIndex=-1;
        stateCands= new Vector();
    }
    
    public StateCands(StateCands sc) {
        this.firstConceptIndex=sc.firstConceptIndex;
        this.stateCands= new Vector(sc.stateCands);
    }
    
    
    public void addConceptCands(Candidates c){
        stateCands.add(c);
    }
   
    public void setfirstConceptIndex(){
        firstConceptIndex=-1;
    }
   
    
    public int getSize(){
        //System.out.println("we are StateCands.getSize()");
        return stateCands.size();
    }
    public Candidates getConceptCands(){
        ++firstConceptIndex;
        return (Candidates)(stateCands.get(firstConceptIndex-1));
    }
   
     public Vector getStateCands(){
        return stateCands;
    }
   
    public void printStateCand(){
        System.out.println("firstConceptIndex="+firstConceptIndex);
        Candidates c = new Candidates(); 
        for(int i=0;i<stateCands.size();i++){
            c=new Candidates((Candidates)(stateCands.get(i)));
            c.printConceptCands(); 
        }
    }
    
    private int firstConceptIndex;
    private Vector stateCands;    
}
