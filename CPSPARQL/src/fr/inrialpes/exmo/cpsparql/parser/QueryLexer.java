/*
 * parser1.java
 *
 * Created on December 25, 2005, 8:05 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.parser;

import java.lang.String.*;
/**
 *
 * @author faisal
 */
public class QueryLexer {    
      private TokenBuffer bufferToken1=new TokenBuffer(0,0);      
      
    /** Creates a new instance of parser1 */
    public QueryLexer(String query) {
        startLexing(query+'\0');
        /*try{
             bufferToken1.getFileWriter().close();                
        } catch (Exception e) { System.out.println(" Exception: "+e);};*/
    } /* end class lexer1 */
    
    public TokenBuffer getTokenBuffer(){
        return bufferToken1;
    }

    public boolean reservedCharacter(char c){
        if((c =='(') || (c==')') || (c==';')|| (c==',') || (c=='[') || (c==']')
            || (c=='{') || (c=='}')  )
            return true;
        else 
            return false;
    }
    
 public TokenAddress getNextString(boolean wsAllowed, char nextchar, int index1,String query){
     TokenAddress tokenaddress= new TokenAddress();
     int i=index1;
     if(!wsAllowed){
         while ((!WS(query.charAt(i)))&& (query.charAt(i)!='\0') && (!reservedCharacter(query.charAt(i))))
             tokenaddress.text+=query.charAt(i++);
         tokenaddress.address=i;
     }
     else{
          tokenaddress.text+=query.charAt(i++);
          while ((query.charAt(i)!=nextchar)&& WS(query.charAt(i))&& (query.charAt(i)!='\0'))
             tokenaddress.text+=query.charAt(i++);
          if (query.charAt(i)==nextchar){
             tokenaddress.text+=query.charAt(i);
             /*System.out.println(i+"we are in getNextString:+true");*/
              tokenaddress.address=i+1;    
          }
          else {
              tokenaddress.address=index1+1;    /* !  to be revised  */
             /* System.out.println(i+"we are in getNextString:+true");*/
          }
     }
     
     /*System.out.println(tokenaddress.text+":"+index1);*/
     return tokenaddress;
 }   
    
    public void  startLexing(String query){
        /*System.out.println("Query:"+query);*/
        int index=0;
        Token token=new Token("", "", -100, -1);
        TokenAddress tokenaddress= new TokenAddress();
        char choice;
        while( (query.charAt(index)!='\0')){
            if (query.charAt(index)=='_')
                choice= query.charAt(index);
            else if (NCCHAR1(query.charAt(index)))
                choice='a';
            else if (DIGIT(query.charAt(index)))
                choice='0';
            else choice= query.charAt(index);
            /*if (RDFLiteral1("'TruE'^^dsfjl:\0")) System.out.println("'TruE'^^dsfjl: RDFLiteral1");*/
            switch (choice){
                case '0':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    if (INTEGER(tokenaddress.text+'\0')){
                        token.type="INTEGER";
                        token.text=tokenaddress.text;
                        //token.text='"'+token.text+'"'+"^^";  
                        //token.text+="<http://www.w3.org/2001/XMLSchema#integer>";
                        token.typeIndex=107;
                    }
                    else if (DECIMAL(tokenaddress.text+'\0')){
                        token.type="DECIMAL";
                        token.text=tokenaddress.text;
                        //token.text='"'+token.text+'"'+"^^";
                        //token.text+="<http://www.w3.org/2001/XMLSchema#decimal>";
                        token.typeIndex=108;
                    }
                    else if (DOUBLE(tokenaddress.text+'\0')){
                        token.type="DOUBLE";
                        token.text=tokenaddress.text;
                        //token.text='"'+token.text+'"'+"^^";
                        //token.text+="<http://www.w3.org/2001/XMLSchema#double>";
                        token.typeIndex=109;
                    }
                    else if (VARNAME(tokenaddress.text+'\0')){
                        token.type="VARNAME";
                        token.text=tokenaddress.text;
                        token.typeIndex=121;
                    }
                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-94;                
                    }
                    index =tokenaddress.address;
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                case '_':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    /*System.out.println("we are here in _");*/
                    if (BLANK_NODE_LABEL(tokenaddress.text+'\0')){
                        token.type="BLANK_NODE_LABEL";
                        token.text=tokenaddress.text;
                        token.typeIndex=103;
                    }
                    else if (NCNAME(tokenaddress.text+'\0')){
                        token.type="NCNAME";
                        token.text=tokenaddress.text;
                        token.typeIndex=124;
                    }
                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-98;                
                    }
                    index =tokenaddress.address;
                    /*tokenBuffer5[currentToken++]=token;*/
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    break;
                }
                case '?':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    /*System.out.println("we are here in ?");
                    System.out.println(tokenaddress.text);*/
                    if (VAR3(tokenaddress.text+'\0')){
                        token.type="VAR3";
                        token.text=tokenaddress.text;
                        token.typeIndex=1013;
                        //System.out.println("we are here in ?:  "+token.text);
                    }
                    else if (VAR1(tokenaddress.text+'\0')){
                        token.type="VAR1";
                        token.text=tokenaddress.text;
                        token.typeIndex=104;
                    }
                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-97;                
                    }
                    index =tokenaddress.address;
                    /*System.out.println("token.text="+token.text);*/
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '$':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    if (VAR2(tokenaddress.text+'\0')){
                        token.type="VAR2";
                        token.text=tokenaddress.text;
                        token.typeIndex=105;
                    }
                    
                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-96;                
                    }
                    index =tokenaddress.address;
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '@':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    if (LANGTAG(tokenaddress.text+'\0')){
                        token.type="LANGTAG";
                        token.text=tokenaddress.text;
                        token.typeIndex=106;
                    }
                    
                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-95;                
                    }
                    index =tokenaddress.address;
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '.':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    if (DECIMAL(tokenaddress.text+'\0')){
                        token.type="DECIMAL";
                        token.text=tokenaddress.text;
                        token.typeIndex=108;
                    }
                    else if (DOUBLE(tokenaddress.text+'\0')){
                        token.type="DOUBLE";
                        token.text=tokenaddress.text;
                        token.typeIndex=109;
                    }
                    else if ((tokenaddress.text.equals("."))){
                        token.type="DOT";
                        token.text=tokenaddress.text;
                        token.typeIndex=28;
                    }
                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-93;                
                    }
                    index =tokenaddress.address;
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '(':{
                    int j=index;
                    tokenaddress=getNextString(true, ')', j,query);
                    /*String text=tokenaddress.text.toUpperCase();*/
                    if (NIL(tokenaddress.text+'\0')){
                        token.type="NIL";
                        token.text=tokenaddress.text;
                        token.typeIndex=116;
                        index =tokenaddress.address;   
                    }
                    else{/* if (WS(tokenaddress.text.charAt(j+1))){*/
                        token.type="LPAREN";
                        token.text=tokenaddress.text;
                        token.typeIndex=18;
                        index++;
                    }
                    /*else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-92;                
                    } */                   
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '[':{
                    int j=index;
                    tokenaddress=getNextString(true, ']', j,query);
                    /*String text=tokenaddress.text.toUpperCase();*/
                    /*System.out.println(" tokenaddress.text:"+tokenaddress.text);*/
                    if (ANON(tokenaddress.text+'\0')){
                        token.type="ANON";
                        token.text=tokenaddress.text;
                        token.typeIndex=118;
                        index =tokenaddress.address;   
                    }
                    else {/*if (WS(tokenaddress.text.charAt(j+1))){*/
                        token.type="LBRACKET";
                        token.text="["; //tokenaddress.text;
                        token.typeIndex=23;
                        index++;
                    }
                    /*else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-92;                
                    } */                   
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case ']':{
                    int j=index;
                    /*tokenaddress=getNextString(false, ' ', j,query);*/
                    /*if (WS(tokenaddress.text.charAt(j+1))){*/
                        token.type="RBRACKET";
                        token.text="]";
                        token.typeIndex=24;
                        index++;
                  /*  }  
                    else { 
                        token.type="RBRACKET";
                        token.text=tokenaddress.text;
                        token.typeIndex=24;
                        index++;
                    }       */            
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case ';':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    /*if (WS(tokenaddress.text.charAt(j+1))){
                        token.type="SEMICOLON";
                        token.text=tokenaddress.text;
                        token.typeIndex=26;
                        index++;
                    }  
                    else { */
                        token.type="SEMICOLON";
                        token.text=tokenaddress.text;
                        token.typeIndex=26;
                        index++;
                    /*}        */            
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case ',':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                   /* if (WS(tokenaddress.text.charAt(j+1))){
                        token.type="COMMA";
                        token.text=tokenaddress.text;
                        token.typeIndex=27;
                        index++;
                    }  
                    else { */
                        token.type="COMMA";
                        token.text=tokenaddress.text;
                        token.typeIndex=27;
                        index++;
                    /*}        */            
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                case '>':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    /*System.out.println(tokenaddress.text);*/
                    if (query.charAt(j+1)=='='){ 
                        token.type="GE";
                        token.text=tokenaddress.text;
                        token.typeIndex=6;
                        index+=2;
                    } 
                    else if (query.charAt(j+1)=='&'){ 
                        token.type="END_RE_CONSTRAINT";
                        token.text="&>";
                        token.typeIndex=1001;
                        index+=2;
                    }
                    else /*if (WS(tokenaddress.text.charAt(j+1)))*/{
                        token.type="GT";
                        token.text=tokenaddress.text;
                        token.typeIndex=3;
                        index++;
                    }                      
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '=':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    if (query.charAt(j+1)=='>'){ 
                        token.type="GE";
                        token.text="=>";
                        token.typeIndex=3;
                        index+=2;
                    } 
                    else if (query.charAt(j+1)=='<'){ 
                        token.type="LE";
                        token.text="=<";
                        token.typeIndex=4;
                        index+=2;
                    }
                    
                    else /*if (WS(tokenaddress.text.charAt(j+1)))*/{
                        token.type="EQ";
                        token.text="=";
                        token.typeIndex=1;
                        index++;
                    }  
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                case '&':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    if (query.charAt(j+1)=='&'){ 
                        token.type="SC_AND";
                        token.text="&&";
                        token.typeIndex=11;
                        index+=2;
                    }
                    else if (query.charAt(j+1)=='<'){ 
                        token.type="BEGIN_RE_CONSTRAINT";
                        token.text="&<";
                        token.typeIndex=1000;
                        index+=2;
                    }
                    else if (query.charAt(j+1)=='>'){ 
                        token.type="END_RE_CONSTRAINT";
                        token.text="&>";
                        token.typeIndex=1001;
                        index+=2;
                    }
                    else /*if (WS(tokenaddress.text.charAt(j+1)))*/{
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-86;                
                    
                    /*    token.type="AMP";
                        token.text=tokenaddress.text;
                        token.typeIndex=18;
                        index++;*/
                    }  
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                case '~':{
                        token.type="TILDE";
                        token.text="~";
                        token.typeIndex=8;
                        index++;
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                
                case ':':{
                        
                        int j=index;
                        tokenaddress=getNextString(false, ' ', j,query);
                        String text=tokenaddress.text.toUpperCase();
                        if (tokenaddress.text.equals(":")) {
                        token.type="COLON";
                        token.text=":";
                        token.typeIndex=9;
                        index++;
                        }
                        else if (QNAME_NS(tokenaddress.text+'\0')){
                            token.type="QNAME_NS";
                            token.text=tokenaddress.text;
                            token.typeIndex=103;
                            index =tokenaddress.address;
                        }
                        else if (QNAME(tokenaddress.text+'\0')){
                            token.type="QNAME_NS";
                            token.text=tokenaddress.text;
                            token.typeIndex=103;
                            index =tokenaddress.address;
                        }
                        token.setIndex(index);                    
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                
                case '+':{
                        token.type="PLUS";
                        token.text="+";
                        token.typeIndex=12;
                        index++;
                        token.setIndex(index);                    
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                
                case '#':{
                        token.type="WILDCARD";
                        token.text="#";
                        token.typeIndex=999;
                        index++;
                        token.setIndex(index);                    
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                
                
                case '-':{
                        token.type="MINUS";
                        token.text="-";
                        token.typeIndex=13;
                        index++;
                        token.setIndex(index);                    
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                case '*':{
                        token.type="STAR";
                        token.text="*";
                        token.typeIndex=14;
                        index++;
                        token.setIndex(index);                    
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                case '/':{
                        token.type="SLASH";
                        token.text="/";
                        token.typeIndex=15;
                        index++;
                        token.setIndex(index);                    
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                
                
                case '!':{
                    int j=index;
                    /*tokenaddress=getNextString(false, ' ', j,query);*/
                    if (query.charAt(j+1)=='='){ 
                        token.type="NE";
                        token.text="!=";
                        token.typeIndex=2;
                        index+=2;
                    }  
                    else /*if (WS(tokenaddress.text.charAt(j+1)))*/{
                        token.type="BANG";
                        token.text="!";
                        token.typeIndex=7;
                        index++;
                    }
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                
                 case '|':{
                    int j=index;
                    /*tokenaddress=getNextString(false, ' ', j,query);*/
                    if (query.charAt(j+1)=='|'){ 
                        token.type="SC_OR";
                        token.text="||";
                        token.typeIndex=10;
                        index+=2;
                    } 
                    else if (query.charAt(j+1)==' '){ 
                        token.type="OR";
                        token.text="|";
                        token.typeIndex=80;
                        index++;
                    }
                    else /*if (WS(tokenaddress.text.charAt(j+1)))*/{
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-87;                
                    }
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
               
                case '^':{
                    int j=index;
                    /*tokenaddress=getNextString(false, ' ', j,query);*/
                    if (query.charAt(j+1)=='^'){ 
                        token.type="DTYPE";
                        token.text="^^";
                        token.typeIndex=16;
                        index+=2;
                    }  
                    else /*if (WS(tokenaddress.text.charAt(j+1)))*/{
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-85;                
                    }
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                case '{':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    /*if (WS(tokenaddress.text.charAt(j+1))){
                        token.type="LBRACE";
                        token.text=tokenaddress.text;
                        token.typeIndex=21;
                        index++;
                    }  
                    else { */
                        token.type="LBRACE";
                        token.text="{";
                        token.typeIndex=21;
                        index++;
                    /*}*/                    
                    token.setIndex(index);                        
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '}':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    /*if (WS(tokenaddress.text.charAt(j+1))){
                        token.type="RBRACE";
                        token.text=tokenaddress.text;
                        token.typeIndex=22;
                        index++;
                    }  
                    else { */
                        token.type="RBRACE";
                        token.text="}";
                        token.typeIndex=22;
                        index++;
                    /*}        */            
                    token.setIndex(index);                        
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                case ')':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    /*if (WS(tokenaddress.text.charAt(j+1))){
                        token.type="RPAREN";
                        token.text=tokenaddress.text;
                        token.typeIndex=19;
                        index++;
                    }  
                    else { */
                        token.type="RPAREN";
                        token.text=")";
                        token.typeIndex=19;
                        index++;
                    /*}        */            
                    token.setIndex(index);                        
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                
                case '%':{
                        token.type="PERCENTAGE";
                        token.text="%";
                        token.typeIndex=1030;
                        index++;
                        token.setIndex(index);                    
                        bufferToken1.setToken(token);
                        /*tokenBuffer5[currentToken++]=token;*/
                        break;
                }
                case 'a':{
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    if (text.equals("A")){
                        token.type="rdf:type";
                        token.text="a";
                        token.typeIndex=70;                                    
                    }
                    
                    else if (text.equals("CONSTRAINT")){
                        token.type="CONSTRAINT";
                        token.text=tokenaddress.text;
                        token.typeIndex=1030;                                    
                    }
                   else if (text.equals("SELECT")){
                        token.type="SELECT";
                        token.text=tokenaddress.text;
                        token.typeIndex=32;                                    
                    }
                    else  if (text.equals("BASE")){
                        token.type="BASE";
                        token.text=tokenaddress.text;
                        token.typeIndex=30;                                    
                    }
                    else  if (text.equals("ALL")){
                        token.type="ALL";
                        token.text=tokenaddress.text;
                        token.typeIndex=1002;                                    
                    }
                    else  if (text.equals("EXISTS")){
                        token.type="EXISTS";
                        token.text=tokenaddress.text;
                        token.typeIndex=1003;                                    
                    }
                    else if (text.equals("EVEN")){
                        token.type="EVEN";
                        token.text=tokenaddress.text;
                        token.typeIndex=1005;                                    
                    }
                    else if (text.equals("ODD")){
                        token.type="ODD";
                        token.text=tokenaddress.text;
                        token.typeIndex=1006;                                    
                    }
                    else  if (text.equals("LENGTH")){
                        token.type="LENGTH";
                        token.text=tokenaddress.text;
                        token.typeIndex=1007;                                    
                    }
                    else  if (text.equals("NLENGTH")){
                        token.type="NLENGTH";
                        token.text=tokenaddress.text;
                        token.typeIndex=1008;                                    
                    }
                    else  if (text.equals("SUM")){
                        token.type="SUM";
                        token.text=tokenaddress.text;
                        token.typeIndex=1009;                                    
                    }
                    else  if (text.equals("AVG")){
                        token.type="AVG";
                        token.text=tokenaddress.text;
                        token.typeIndex=1010;                                    
                    }
                    else  if (text.equals("COUNT")){
                        token.type="COUNT";
                        token.text=tokenaddress.text;
                        token.typeIndex=1011;                                    
                    }
                    else  if (text.equals("DEFINED")){
                        token.type="DEFINED";
                        token.text=tokenaddress.text;
                        token.typeIndex=1012;                                    
                    }
                    else  if (text.equals("EDGE")){
                        token.type="EDGE";
                        token.text=tokenaddress.text;
                        token.typeIndex=1013;                                    
                    }
                    else  if (text.equals("PREFIX")){
                        token.type="PREFIX";
                        token.text=tokenaddress.text;
                        token.typeIndex=31;                                    
                    }
                    else  if (text.equals("DISTINCT")){
                        token.type="DISTINCT";
                        token.text=tokenaddress.text;
                        token.typeIndex=33;                                    
                    }
                    else  if (text.equals("DESCRIBE")){
                        token.type="DESCRIBE";
                        token.text=tokenaddress.text;
                        token.typeIndex=34;                                    
                    }
                    else  if (text.equals("CONSTRUCT")){
                        token.type="CONSTRUCT";
                        token.text=tokenaddress.text;
                        token.typeIndex=35;                                    
                    }
                    else  if (text.equals("ASK")){
                        token.type="ASK";
                        token.text=tokenaddress.text;
                        token.typeIndex=36;                                    
                    }
                    else  if (text.equals("LIMIT")){
                        token.type="LIMIT";
                        token.text=tokenaddress.text;
                        token.typeIndex=37;                                    
                    }
                    else  if (text.equals("OFFSET")){
                        token.type="OFFSET";
                        token.text=tokenaddress.text;
                        token.typeIndex=38;                                    
                    }
                    else  if (text.equals("ORDER")){
                        token.type="ORDER";
                        token.text=tokenaddress.text;
                        token.typeIndex=39;                                    
                    }
                    else  if (text.equals("BY")){
                        token.type="BY";
                        token.text=tokenaddress.text;
                        token.typeIndex=40;                                    
                    }
                    else  if (text.equals("ASC")){
                        token.type="ASC";
                        token.text=tokenaddress.text;
                        token.typeIndex=41;                                    
                    }
                    else  if (text.equals("DESC")){
                        token.type="DESC";
                        token.text=tokenaddress.text;
                        token.typeIndex=42;                                    
                    }
                    else  if (text.equals("NAMED")){
                        token.type="NAMED";
                        token.text=tokenaddress.text;
                        token.typeIndex=43;                                    
                    }
                    else  if (text.equals("FROM")){
                        token.type="FROM";
                        token.text=tokenaddress.text;
                        token.typeIndex=44;                                    
                    }
                    else  if (text.equals("WHERE")){
                        token.type="WHERE";
                        token.text=tokenaddress.text;
                        token.typeIndex=45;                                    
                    }
                    else  if (text.equals("AND")){
                        token.type="AND";
                        token.text=tokenaddress.text;
                        token.typeIndex=46;                                    
                    }
                    else  if (text.equals("GRAPH")){
                        token.type="GRAPH";
                        token.text=tokenaddress.text;
                        token.typeIndex=47;                                    
                    }
                    else  if (text.equals("OPTIONAL")){
                        token.type="OPTIONAL";
                        token.text=tokenaddress.text;
                        token.typeIndex=48;                                    
                    }
                    else  if (text.equals("UNION")){
                        token.type="UNION";
                        token.text=tokenaddress.text;
                        token.typeIndex=49;                                    
                    }
                    else  if (text.equals("FILTER")){
                        token.type="FILTER";
                        token.text=tokenaddress.text;
                        token.typeIndex=50;                                    
                    }
                    else  if (text.equals("BOUND")){
                        token.type="BOUND";
                        token.text=tokenaddress.text;
                        token.typeIndex=51;                                    
                    }
                    else  if (text.equals("STR")){
                        token.type="STR";
                        token.text=tokenaddress.text;
                        token.typeIndex=52;                                    
                    }
                    else  if (text.equals("DATATYPE")){
                        token.type="DATATYPE";
                        token.text=tokenaddress.text;
                        token.typeIndex=53;                                    
                    }
                    else  if (text.equals("LANG")){
                        token.type="LANG";
                        token.text=tokenaddress.text;
                        token.typeIndex=54;                                    
                    }
                    else  if (text.equals("LANGMATCHES")){
                        token.type="LANGMATCHES";
                        token.text=tokenaddress.text;
                        token.typeIndex=55;                                    
                    }
                    else  if (text.equals("ISURI")){
                        token.type="ISURI";
                        token.text=tokenaddress.text;
                        token.typeIndex=56;                                    
                    }
                    else  if (text.equals("ISIRI")){
                        token.type="ISIRI";
                        token.text=tokenaddress.text;
                        token.typeIndex=57;                                    
                    }
                    else  if (text.equals("ISBLANK")){
                        token.type="ISBLANK";
                        token.text=tokenaddress.text;
                        token.typeIndex=58;                                    
                    }
                    else  if (text.equals("ISLITERAL")){
                        token.type="ISLITERAL";
                        token.text=tokenaddress.text;
                        token.typeIndex=59;                                    
                    }
                    else  if (text.equals("REGEX")){
                        token.type="REGEX";
                        token.text=tokenaddress.text;
                        token.typeIndex=60;                                    
                    }
                    else  if (text.equals("TRUE")){
                        token.type="TRUE";
                        token.text=tokenaddress.text;
                        token.typeIndex=61;                                    
                    }
                    else  if (text.equals("FALSE")){
                        token.type="FALSE";
                        token.text=tokenaddress.text;
                        token.typeIndex=62;                                    
                    }
                   
                    else if (NCNAME_PREFIX(tokenaddress.text+'\0')){
                        token.type="NCNAME_PREFIX";
                        token.text=tokenaddress.text;
                        token.typeIndex=123;                
                    }
                    else  if (NCNAME(tokenaddress.text+'\0')){
                        token.type="NCNAME";
                        token.text=tokenaddress.text;
                        token.typeIndex=124;
                    }
                    else if (QNAME_NS(tokenaddress.text+'\0')){
                        token.type="QNAME_NS";
                        token.text=tokenaddress.text;
                        token.typeIndex=101;                
                    }
                    else if (QNAME(tokenaddress.text+'\0')){
                        token.type="QNAME";
                        token.text=tokenaddress.text;
                        token.typeIndex=102;                
                    }
                    else  if (VARNAME(tokenaddress.text+'\0')){
                        token.type="VARNAME";
                        token.text=tokenaddress.text;
                        token.typeIndex=121;                                    
                    }
                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-100;                
                    }
                    index =tokenaddress.address;
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;      
                }
                case '<':{
                    //System.out.println(tokenaddress.text+"  Faisal");
                    int j=index;
                    tokenaddress=getNextString(false, ' ', j,query);
                    String text=tokenaddress.text.toUpperCase();
                    if (Q_IRI_REF(tokenaddress.text+'\0')){
                        token.type="Q_IRI_REF";
                        token.text=tokenaddress.text;
                        token.typeIndex=100;
                    }
                    else if (tokenaddress.text.equals("<")){
                        token.type="LT";
                        token.text=tokenaddress.text;
                        token.typeIndex=4;
                    }
                    else if (tokenaddress.text.equals("<=")){
                        token.type="LE";
                        token.text=tokenaddress.text;
                        token.typeIndex=5;
                    }

                    else {
                        System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                        token.type="ERROR";
                        token.text=tokenaddress.text;
                        token.typeIndex=-101;                
                    }
                    index =tokenaddress.address;
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }

                case '\t':{
                    index++;
                    token.setIndex(index);                    
                    break;      
                }
                case '\n':{
                    index++;
                    token.setIndex(index);                    
                    break;      
                }
                
                case '\r':{
                    index++;
                    token.setIndex(index);                    
                    break;      
                }

                case ' ':{
                    index++;
                    token.setIndex(index);                    
                    break;      
                }
                case '\\':{
                    index++;
                    token.setIndex(index);                    
                    break;      
                }
                case '"':{/* ! revise this case*/
                    String s="";
                    for(int j=index;query.charAt(j)!='\0';j++)
                        s+=query.charAt(j);
                    int pos1 =-20;
                    int pos2=-30;
                    int end1=index;
                    
                    pos1=query.indexOf("\"\"\"",index);
                    //last modified 11/10/2006
                    if (pos1>=0){
                        end1=query.indexOf("\\\"\"\"", index+3);
                        int end2=-1;
                        boolean last =false;
                        while ((end1>=0)&& (!last)){
                            end2=query.indexOf("\\\"\"\"", end1+4);
                            //System.out.println("we are at longString(): end1="+end1+"  end2="+end2);
                            if (end2 <0) 
                                last =true;
                            else end1=end2;
                        }
                        //System.out.println("we are at longString(): pos1="+pos1);
                        
                    }
                    //last modified 11/10/2006
                    if (end1 <0) end1=index;
                    pos2 =query.indexOf("\"\"\"", 3+end1);
                    
                    //pos2=query.indexOf("\"\"\"",pos1+3);
                    
                    /*while ((query.indexOf(pos2-1)=='\\')||(pos2<0))
                        pos2=query.indexOf("\"\"\"",pos2+1);*/
                    
                    if (pos1>=0){
                        if(pos2>=0){
                            
                            String text="\"\"\"";
                            for(int j=index+3;j<= (pos2-1);j++){
                                if (query.charAt(j)=='"')
                                    text+='\'';
                                else
                                    text+=query.charAt(j);
                            }
                            text+="\"\"\"";
                            //System.out.println("STRING_LITERAL_LONG2"+text);
                        
                          if (STRING_LITERAL_LONG2(text+'\0')){
                              token.type="STRING_LITERAL_LONG2";
                              token.text=text;
                              token.typeIndex=115;
                              index=pos2+3;
                          }
                          else {
                                System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                                token.type="ERROR";
                                token.text=tokenaddress.text;
                                token.typeIndex=-80;                
                          }
                        }
                        else{   
                             System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                             token.type="ERROR";
                             token.text=tokenaddress.text;
                             token.typeIndex=-78;                      
                                
                        }
                    }
                    else {        
                        
                        
                        
                        
                        
                        
                                   int pos4=query.indexOf('"',index);
                                   int pos3=query.indexOf('"', pos4+1); 
                                   
                                   end1=index;
                                    //last modified 11/10/2006
                                    if (pos4>=0){
                                        end1=query.indexOf("\\\"", index+1);
                                        int end2=-1;
                                        boolean last =false;
                                        while ((end1>=0)&& (!last)){
                                            end2=query.indexOf("\\\"", end1+2);
                                            //System.out.println("we are at string(): end1="+end1+"  end2="+end2);
                                            if (end2 <0) 
                                                last =true;
                                            else end1=end2;
                                        }
                                        //System.out.println("we are at longString(): pos1="+pos1);

                                    }
                                    //last modified 11/10/2006
                                    if (end1 <0){
                                       end1=index;
                                       pos3 =query.indexOf('"', 1+end1);
                                    }
                                    else pos3 =query.indexOf('"', 2+end1);
                                    

                                   
                                   if(pos3>=0){
                                        
                            
                                        String text="\"";
                                        for(int j=index+1;j<= (pos3-1);j++){
                                            if (query.charAt(j)=='"')
                                                text+='\'';
                                            else
                                                text+=query.charAt(j);
                                        }
                                        text+="\"";
                                        
                                      /*for(int j=index;j<= pos3;j++)
                                           text+=query.charAt(j);*/
                                      //System.out.println(text);
                        
                                   
                                  
                                      if (STRING_LITERAL2(text+'\0')){
                                          token.type="STRING_LITERAL2";
                                          token.text=text;
                                          token.typeIndex=113;
                                          index=pos3+1;     
                                       /*   System.out.println(index+" "+"we are in deema");*/
                                      }
                                      else{
                                            System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                                            token.type="ERROR";
                                            token.text=tokenaddress.text;
                                            token.typeIndex=-78;                      
                                      }
                                  }
                                  else{
                                            System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                                            token.type="ERROR";
                                            token.text=tokenaddress.text;
                                            token.typeIndex=-78;                      
                                      }
                    }                        
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                case '\'':{/* ! revise this case */
                   String s="";
                    for(int j=index;query.charAt(j)!='\0';j++)
                        s+=query.charAt(j);
                    int pos1 =-20;
                    int pos2=-30;
                    int end1=index;
                    
                    pos1=query.indexOf("'''",index);
                    //last modified 11/10/2006
                    if (pos1>=0){
                        end1=query.indexOf("\\'''", index+3);
                        int end2=-1;
                        boolean last =false;
                        while ((end1>=0)&& (!last)){
                            end2=query.indexOf("\\'''", end1+4);
                            //System.out.println("we are at longString(): end1="+end1+"  end2="+end2);
                            if (end2 <0) 
                                last =true;
                            else end1=end2;
                        }
                        //System.out.println("we are at longString(): pos1="+pos1);
                        
                    }
                    //last modified 11/10/2006
                    if (end1 <0) end1=index;
                    pos2 =query.indexOf("'''", 3+end1);
                    
                    
                    
                    
                  /*   pos1=query.indexOf("'''",index);
                    pos2=query.indexOf("'''",pos1+3);*/
                    
                   /* while ((query.indexOf(pos2-1)=='\\')||(pos2<0))
                        pos2=query.indexOf("'''",pos2+1);*/
                    
                    if (pos1>=0){
                        if(pos2>=0){
                          String text="";
                          for(int j=index;j<= (pos2+2);j++)
                               text+=query.charAt(j);
                          if (STRING_LITERAL_LONG1(text+'\0')){
                              token.type="STRING_LITERAL_LONG1";
                              token.text=text;
                              token.typeIndex=114;
                              index=pos2+3;
                          }
                          else {
                                System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                                token.type="ERROR";
                                token.text=tokenaddress.text;
                                token.typeIndex=-77;                
                          }
                        }
                        else{   
                             System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                             token.type="ERROR";
                             token.text=tokenaddress.text;
                             token.typeIndex=-77;                      
                                
                        }
                    }
                    else {         int pos4=query.indexOf('\'',index);
                                   int pos3=query.indexOf('\'', pos4+1); 
                                   
                                   end1=index;
                                    //last modified 11/10/2006
                                    if (pos4>=0){
                                        end1=query.indexOf("\\'", index+1);
                                        int end2=-1;
                                        boolean last =false;
                                        while ((end1>=0)&& (!last)){
                                            end2=query.indexOf("\\'", end1+2);
                                            //System.out.println("we are at string(): end1="+end1+"  end2="+end2);
                                            if (end2 <0) 
                                                last =true;
                                            else end1=end2;
                                        }
                                        //System.out.println("we are at longString(): pos1="+pos1);

                                    }
                                    //last modified 11/10/2006
                                    if (end1 <0){
                                       end1=index;
                                       pos3 =query.indexOf('\'', 1+end1);
                                    }
                                    else pos3 =query.indexOf('\'', 2+end1);
                                    

                                   
                                   if(pos3>=0){
                                        
                            
                                        String text="'";
                                        for(int j=index+1;j<= (pos3-1);j++){
                                            if (query.charAt(j)=='\'')
                                                text+='"';
                                            else
                                                text+=query.charAt(j);
                                        }
                                        text+="'";
                                        
                                      /*for(int j=index;j<= pos3;j++)
                                           text+=query.charAt(j);*/
                                      //System.out.println(text);
                                      if (STRING_LITERAL1(text+'\0')){
                                          token.type="STRING_LITERAL1";
                                          token.text=text;
                                          token.typeIndex=112;
                                          index=pos3+1;     
                                      }
                                      else{
                                            System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                                            token.type="ERROR";
                                            token.text=tokenaddress.text;
                                            token.typeIndex=-77;                      
                                      }
                                  }
                                  else{
                                            System.out.println(tokenaddress.text+"ERROR TYPE AT:"+index);
                                            token.type="ERROR";
                                            token.text=tokenaddress.text;
                                            token.typeIndex=-77;                      
                                      }
                    }                        
                    token.setIndex(index);                    
                    bufferToken1.setToken(token);
                    /*tokenBuffer5[currentToken++]=token;*/
                    break;
                }
                default:{
                     System.out.println(token+":error");                
                }
            }/* end switch */

        }/* end while*/
       /* bufferToken1.print();*/
        token.text="EOF";
        token.type="EOF";
        token.typeIndex=1000;
        bufferToken1.setToken(token);       
    }/* end startParsing*/
    public void printbuffer(Token tokenBuffer[],int current ){
        System.out.println("                   Token                     "+"              Token Type          "+"                  Token Type Index   ");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------   ");
        for(int j=0;j<current;j++){
            System.out.println("                     "+tokenBuffer[j].text+"                                    "+tokenBuffer[j].type+"                                 "+tokenBuffer[j].typeIndex+"   ");
        }
    }
    
    
   public boolean DIGIT(char c){
        if((c >= '0') && (c<='9'))
            return true;
        else return false;     
    } /*end DIGIT*/
    
   public boolean TRUE(String token){
        token=token.toLowerCase();
        System.out.println(token);       
        if(token.equals("true"))
            return true;
        else return false;
    } /*end TRUE*/
   
   public boolean FALSE(String token){
        token=token.toLowerCase();
       /* System.out.println(token);*/
        if(token.equals("false"))
            return true;
        else return false;
    } /*end FALSE*/
   
   public boolean LETTER(char c){
        boolean letter;
        if((c >= 'a') && (c<='z'))
            letter=true;
        else if((c >= 'A') && (c<='Z')) 
                  letter=true;
        else letter= false;     
        return letter;
    } /*end LETTER*/
   
   
   
   /* rule [60] */
   public boolean RDFLiteral1(String token){
       /*System.out.println("we are in RDFLiteral1:"+token);*/
       boolean rdfliteral1;
       int pos=-20;
       pos=token.indexOf('@');
       if (pos < 0)
               rdfliteral1=false;
       else{
                String token1="",token2="";
                for(int j=0;token.charAt(j)!='\0';j++)
                    if (j<pos)
                         token1+=token.charAt(j);
                    else if (j>=pos)
                            token2+=token.charAt(j);
                    /*else : nothing*/;
                  /*System.out.println(token1+"we are in VAR1"+token2);  */
                 token1+='\0';
                 token2+='\0';
                 /*System.out.println("we :"+token2);
                 System.out.println(LANGTAG(token2));*/
        
                 if((string(token1)) && LANGTAG(token2))
                      rdfliteral1=true;
                 else rdfliteral1=false;
           }
       if (!rdfliteral1){
                   pos=token.indexOf('^');
                   if (pos < 0)
                           rdfliteral1=false;
                   else if (token.charAt(pos+1)=='^'){
                            String token1="",token2="";
                            for(int j=0;token.charAt(j)!='\0';j++)
                                if (j<pos)
                                     token1+=token.charAt(j);
                                else if (j>(pos+1))
                                        token2+=token.charAt(j);
                                /*else : nothing*/;
                              /*System.out.println(token1+"we are in VAR1"+token2);  */
                             token1+='\0';
                             token2+='\0';
                             /*System.out.println(token1+"$we :"+token2);
                             System.out.println(IRIref(token2));*/

                             if((string(token1)) && IRIref(token2))
                                  rdfliteral1=true;
                             else rdfliteral1=false;
                       }
       }
       return rdfliteral1;   
   }  /* end RDFLiteral1 */
   
   /* rule [62] */
    public boolean string(String token){
        /*System.out.println("we are in string:"+token);
        System.out.println(STRING_LITERAL1(token));*/
        if(STRING_LITERAL1(token))
            return true;
        else if(STRING_LITERAL2(token)) 
                  return true;
        else if(STRING_LITERAL_LONG1(token)) 
                  return true;
        else if(STRING_LITERAL_LONG2(token)) 
                  return true;
        else return false;  
     } /*end string*/
  
  /* rule [63] */
    public boolean IRIref(String token){
        /*System.out.println("we are in string:"+token);
        System.out.println(STRING_LITERAL1(token));*/
        if(Q_IRI_REF(token))
            return true;
        else if(QNAME_NS(token)) 
                  return true;
        else return false;  
     } /*end IRIref*/
  
    
   /* rule [66] */
   public boolean Q_IRI_REF(String token){
       boolean qiriref;
       int l=token.length();
       if (l >=2){
           String token1="", token2="",token3="";
           token1+=token.charAt(0);
           token2+=token.charAt(l-2);
           for(int j=1; j<l-2;j++)
               token3+=token.charAt(j);
           /* System.out.println(token1+"$"+token2+"$"+token3+"$length("+token+")"+l);*/
            token3+='\0';
           if (token1.equals("<") && token2.equals(">")){
                /*System.out.println("we are here:::");*/
                   boolean star=true;
                   int i=0,j=0;
                   while((star) && (token3.charAt(i++)!='\0')){
                       boolean s;
                       if (!((token.charAt(i)>='\u0000') && (token.charAt(i)<='\u0020')))
                             s=true;
                       else s=false;
                       star=((token.charAt(i)!='>')&&(token.charAt(i)!='<')&&(token.charAt(i)!='{')
                             &&(token.charAt(i)!='}') &&(token.charAt(i)!='\'') &&(token.charAt(i)!='|')
                             &&(token.charAt(i)!='^') &&(token.charAt(i)!='`') && s);
                       j=i;
                   }
                   if (star)
                        qiriref=true;
                   else qiriref=false;
           }
           else qiriref=false;
       }
       else qiriref=false;       
       return qiriref;
   }  /* end Q_IRI_REF */
   
   
   /* rule [67] */
   public boolean QNAME_NS(String token){
       /*System.out.println("we are in QNAME_NS");*/
       boolean qnamens;
       if (token.equals(":"))
           qnamens=true;
       else {
           int pos=-10;
           pos = token.indexOf(':');
          /* System.out.println(token+" we are in QNAME"+pos);*/
           if (pos<0)
               qnamens=false;
           else{
                String token1="",token2="";
                for(int j=0;token.charAt(j)!='\0';j++)
                    if (j<pos)
                         token1+=token.charAt(j);
                    else if (j>pos)
                            token2+=token.charAt(j);
                    /*else : nothing*/;
                  /*System.out.println(token1+"we are in VAR1"+token2);  */
                 token1+='\0';
                 token2+='\0';
                 if(((token1.equals("\0"))||(NCNAME_PREFIX(token1)))
                      && (token2.equals("\0")))
                      qnamens=true;
                 else qnamens=false;
           }
       }
       return qnamens;   
   }  /* end QNAME_NS */
   
   
   /* rule [68] */
   public boolean QNAME(String token){
       /*System.out.println("we are in VAR1");*/
       boolean qname;
       if (token.equals(":"))
           qname=true;
       else {
           int pos=-10;
           pos = token.indexOf(':');
          /* System.out.println(token+" we are in QNAME"+pos);*/
           if (pos<0)
               qname=false;
           else{
                String token1="",token2="";
                for(int j=0;token.charAt(j)!='\0';j++)
                    if (j<pos)
                         token1+=token.charAt(j);
                    else if (j>pos)
                            token2+=token.charAt(j);
                    /*else : nothing*/;
                  /*System.out.println(token1+"we are in VAR1"+token2);  */
                 token1+='\0';
                 token2+='\0';
                 if(((token1.equals("\0"))||(NCNAME_PREFIX(token1)))
                      && ((token2.equals("\0"))||(NCNAME(token2))))
                      qname=true;
                 else qname=false;
           }
       }
       return qname;   
   }  /* end QNAME */
   
   /* rule [69] */
   public boolean BLANK_NODE_LABEL(String token){
       /*System.out.println("we are in VAR1");*/
       boolean blanknodelabel;
       int i=0;
       if (token.charAt(i)=='_') 
        if(token.charAt(i+1)==':'){
           String token1="";
           for(int j=2;token.charAt(j)!='\0';j++)
               token1+=token.charAt(j);
          /* System.out.println("we are in second"+token1);*/
           token1+='\0';
          if (NCNAME(token1))
              blanknodelabel=true;
          else blanknodelabel=false;
       }
       else blanknodelabel=false;
        else blanknodelabel=false;
       return blanknodelabel;   
   }  /* end BLANK_NODE_LABEL */
   
   
   /* rule [70] */
   public boolean VAR1(String token){
       /*System.out.println("we are in VAR1");*/
       boolean var1;
       int i=0;
       if (token.charAt(i)=='?'){
           String token1="";
           for(int j=1;token.charAt(j)!='\0';j++)
               token1+=token.charAt(j);
          /* System.out.println("we are in second"+token1);*/
           token1+='\0';
          if (VARNAME(token1))
              var1=true;
          else var1=false;
       }
       else var1=false;
       return var1;   
   }  /* end VAR1 */
   
   /* rule [70.1] */
   public boolean VAR3(String token){
       //System.out.println("we are in VAR3");
       boolean var3;
       int i=0;
       if ((token.charAt(i)=='?')&&((token.charAt(1)=='?'))){
           String token1="";
           for(int j=2;token.charAt(j)!='\0';j++)
               token1+=token.charAt(j);
           //System.out.println("we are in second"+token1);
           token1+='\0';
          if (VARNAME(token1))
              var3=true;              
          else var3=false;
       }
       else var3 = false;
       //System.out.println("we are in second"+var3);
       return var3;   
   }  /* end VAR3 */
   
    /* rule [71] */
   public boolean VAR2(String token){
       boolean var2;
       int i=0;
       if (token.charAt(i)=='$'){
           String token1="";
           for(int j=1;token.charAt(j)!='\0';j++)
               token1+=token.charAt(j);
           token1+='\0';
          if (VARNAME(token1))
              var2=true;
          else var2=false;
       }
       else var2=false;
       return var2;   
   }  /* end VAR2 */
   
  /* rule [73] */
   public boolean INTEGER(String token){
        int i;
        if ((token.charAt(0)=='-') || (token.charAt(0)=='+'))
            i=1;
        else i=0;
        boolean plus=true;
        plus =DIGIT(token.charAt(i++));
        while ((plus) && (token.charAt(i)!='\0'))
            plus = DIGIT(token.charAt(i++));
        return plus;
    }/* end INTEGER*/
    
  /* rule [74] */
   public boolean DECIMAL(String token){
       /*System.out.println(" we are at Decimal:"+token);*/
        boolean decimal1, firstchoice=false,secondchoice=false;
        int i=0;
        if ((token.charAt(0)=='-') || (token.charAt(0)=='+'))
            i=1;
        else i=0;
        if (token.charAt(i)=='.'){ 
            boolean plus=true;
            plus =DIGIT(token.charAt(++i));
            while ((plus) && (token.charAt(++i)!='\0'))
                plus = DIGIT(token.charAt(i));
            if (plus)
                decimal1=true;
            else decimal1= false;
            firstchoice=decimal1;
          /*  System.out.println(firstchoice);*/
        }    
        else if (DIGIT(token.charAt(i))){
                   /* System.out.println(" ???????");*/
                    int pos = token.indexOf('.');
                    if (pos >0){
                        String token1="", token2="";
                        for(int j=0;token.charAt(j)!='\0';j++)
                            if (j<pos)
                                token1+=token.charAt(j);
                            else if (j>pos)
                                token2+=token.charAt(j);
                        token1+='\0';
                        token2+='\0';
                         if ((INTEGER(token1)) && (INTEGER(token2) || token2.equals("\0")))
                            secondchoice=true;
                        else secondchoice=false;
                        }
        }
        else decimal1=false;
        if (firstchoice || secondchoice)
            decimal1=true;
        else 
            decimal1=false;
        return decimal1;
    }  /* end DECIMAL*/
    
     /* rule [76] */
   public boolean EXPONENT(String token){
        boolean exponent;
        int i=0;
        if ((token.charAt(0)=='e') || (token.charAt(0)=='E')){
            i++;
             if ((token.charAt(i)=='-') || (token.charAt(i)=='+'))
                 i++;
            String token1="";
            for(int j=i; (token.charAt(j)!='\0'); j++)
                token1+=token.charAt(j);
            token1+='\0';
            if(INTEGER(token1))
                exponent=true;
            else exponent=false;
        }
        else exponent=false;
        return exponent;     
    }/* end EXPONENT*/

  /*rule [75] */
   public boolean DOUBLE(String token){
       /*System.out.println("we are in DOUBLE");*/
        boolean double1;
        String token1="",token2="";
        int pos=-1;
        pos = token.indexOf('e');
        if (pos<0)
            pos = token.indexOf('E');
        if (pos<0)
            double1=false;
        else{
            for(int j=0;  (token.charAt(j)!='\0'); j++)
                if (j <pos)
                    token1+=token.charAt(j);
                else token2+=token.charAt(j);
            token1+='\0';
            token2+='\0';
            if(DECIMAL(token1))
                if (EXPONENT(token2))
                    double1=true;
                else double1=false;
            else if(INTEGER(token1))
                    if (EXPONENT(token2))
                        double1=true;
                    else double1=false;
            else double1=false;
        }
        return double1;        
    }/* end DOUBLE*/
   
    /*rule [72] */
   public boolean LANGTAG(String token){
     /*  System.out.println(token+":we are in LANGTAG");*/
        boolean langtag;
        int i=0;
        if (token.charAt(i++)=='@'){
            boolean plus = LETTER(token.charAt(i));
            while((plus) && (token.charAt(++i)!='\0'))
                plus = LETTER(token.charAt(i));
            if((plus) || ((token.charAt(i)=='-')&& (i!=1))){
                boolean star=true;
                while((star) && (token.charAt(i)!='\0')){
                    ++i;
                    boolean plus1=(LETTER(token.charAt(i))|| DIGIT(token.charAt(i)));
                    while((plus1) && (token.charAt(++i)!='\0'))
                         plus1=(LETTER(token.charAt(i))|| DIGIT(token.charAt(i)));
                    if ((plus1) || ((token.charAt(i)=='-')&& (token.charAt(i-1)!='-')))
                        star=true;
                    else star=false;
                }
                if (star)
                    langtag=true;
                else langtag=false;
            }
            else langtag=false;
        }
        else langtag=false;
            
        return langtag;        
    }/* end LANGTAG*/
   
   /* rule [77] */
   public boolean ECHAR(char token){
       boolean echar;
       if (token==('\\'))
           echar=true;
       else if (token==('\t'))
                  echar=true;
       else if (token==('\b'))
                  echar=true;
       else if (token==('\n'))
                  echar=true;
       else if (token==('\r'))
                  echar=true;
       else if (token=='\f')
                  echar=true;
       else if (token=='\"')
                  echar=true;
       else if (token=='\'')
                  echar=true;
       else echar = false;
       return echar;
   }  /* end ECHAR */
   
   /* rule [78] */
   public boolean STRING_LITERAL1(String token){
       boolean stringliteral1;
       int j=0, i=0;
       if (token.charAt(i)=='\''){
           boolean star=true;
           while((star) && (token.charAt(++i)!='\0')){
               if (ECHAR(token.charAt(i)))
                   star = true;
               else if((token.charAt(i)!='\r') &&(token.charAt(i)!='\n')&&
                       (token.charAt(i)!='\\') && (token.charAt(i)!='\''))
                       star = true;
               else star=false;
               j=i;
           }
           if((token.charAt(j)=='\'') && (token.charAt(j+1)=='\0'))
               stringliteral1 = true;
           else stringliteral1 =false;                   
       }
       else stringliteral1 =false;
       return stringliteral1;
   }  /* end STRING_LITERAL1 */
   
   /* rule [78] */
   public boolean STRING_LITERAL2(String token){
       boolean stringliteral2;
       int j=0, i=0;
       if (token.charAt(i)=='\"'){
           boolean star=true;
           while((star) && (token.charAt(++i)!='\0')){
               if (ECHAR(token.charAt(i)))
                   star = true;
               else if((token.charAt(i)!='\r') &&(token.charAt(i)!='\n')&&
                       (token.charAt(i)!='\\') && (token.charAt(i)!='\"'))
                       star = true;
               else star=false;
               j=i;
           }
           if((token.charAt(j)=='\"') && (token.charAt(j+1)=='\0'))
               stringliteral2 = true;
           else stringliteral2 =false;                   
       }
       else stringliteral2 =false;
       return stringliteral2;
   }  /* end STRING_LITERAL2 */
   
   /* rule [80] */
   public boolean STRING_LITERAL_LONG1(String token){
       boolean stringliterallong1;
       int l=token.length();
       if (l >=6){
           String token1="", token2="",token3="";
           for(int j=0;j<3;j++)
               token1+=token.charAt(j);
           for(int j=l-4;j<l-1;j++)
               token2+=token.charAt(j);
           for(int j=3; j<l-4;j++)
               token3+=token.charAt(j);
           /* System.out.println(token1+"$"+token2+"$"+token3+"$length("+token+")"+l);*/
            token3+='\0';
           if (token1.equals("'''") && token2.equals("'''")){
                //System.out.println("we are here:::::");
                boolean star=true;
                   int i=0,j=0;
                   while((star) && (token3.charAt(i++)!='\0')){
                       if (ECHAR(token3.charAt(i)))
                           star = true;
                       else if((token3.charAt(i)!='\\')&&  (token3.charAt(i)!='\''))
                           star=true;
                       else if((token3.charAt(i)=='\'')&&  (token3.charAt(i+1)!='\''))
                           star=true;
                       else if((token3.charAt(i)=='\'') &&(token3.charAt(i+1)=='\'') /* !: see this option */
                                     &&  (token3.charAt(i+2)!='\''))
                           star=true;
                       else star=false;
                       j=i;
                   }
                   if (star)
                        stringliterallong1=true;
                   else stringliterallong1=false;
           }
           else stringliterallong1=false;
       }
       else stringliterallong1=false;       
       return stringliterallong1;
   }  /* end STRING_LITERAL_LONG1 */
   
   
   /* rule [81] */
   public boolean STRING_LITERAL_LONG2(String token){
       boolean stringliterallong2;
       int l=token.length();
       if (l >=6){
           String token1="", token2="",token3="";
           for(int j=0;j<3;j++)
               token1+=token.charAt(j);
           for(int j=l-4;j<l-1;j++)
               token2+=token.charAt(j);
           for(int j=3; j<l-4;j++)
               token3+=token.charAt(j);
            /*System.out.println(token1+"$"+token2+"$"+token3+"$length("+token+")"+l);*/
            token3+='\0';
           if (token1.equals("\"\"\"") && token2.equals("\"\"\"")){
                   boolean star=true;
                   int i=0,j=0;
                   while((star) && (token3.charAt(i++)!='\0')){
                       if (ECHAR(token3.charAt(i)))
                           star = true;
                       else if((token3.charAt(i)!='\\')&&  (token3.charAt(i)!='\"'))
                           star=true;
                       else if((token3.charAt(i)=='\"')&&  (token3.charAt(i+1)!='\"'))
                           star=true;
                       else if((token3.charAt(i)=='\"') &&(token3.charAt(i+1)=='\"') /* !: see this option */
                                     &&  (token3.charAt(i+2)!='\"'))
                           star=true;
                       else star=false;
                       j=i;
                   }
                   if (star)
                        stringliterallong2=true;
                   else stringliterallong2=false;
           }
           else stringliterallong2=false;
       }
       else stringliterallong2=false;       
       return stringliterallong2;
   }  /* end STRING_LITERAL_LONG2 */
   
   
    /* rule [83] */
   public boolean WS(char token){
       boolean ws;
       if ((token==' ') || (token=='\t') ||(token=='\r')||(token=='\n'))
           ws=true;
      /* else if ((token=='\u0020') ||(token=='\u0009') 
                ||(token=='\u000D') || (token=='\u000A'))
                  ws=true;*/
       else ws = false;
       return ws;
   }  /* end WS */
   
   /* rule [82] */
   public boolean NIL(String token){
       boolean nil;
       int j=0, i=0;
       if (token.charAt(i)=='('){
           boolean star=true;
           while((star) && (token.charAt(++i)!='\0')){
               star=WS(token.charAt(i));
               j=i;
           }
           if((token.charAt(j)==')')&& (token.charAt(j+1)=='\0'))
               nil = true;
           else nil =false;                   
       }
       else nil =false;
       return nil;
   }  /* end NIL */
   
   /* rule [84] */
   public boolean ANON(String token){
       boolean anon;
       int j=0, i=0;
       if (token.charAt(i)=='['){
           boolean star=true;
           while((star) && (token.charAt(++i)!='\0')){
               star=WS(token.charAt(i));
               j=i;
           }
           if((token.charAt(j)==']') && (token.charAt(j+1)=='\0'))
               anon = true;
           else anon =false;                   
       }
       else anon =false;
       return anon;
   }  /* end ANON */
   
   /* rule [85] */
   public boolean NCCHAR1p(char token){
       /*System.out.println("we are in NCCHAR1p");*/
       if (LETTER(token))
           return true;
       else if ((token >= '\u00C0') && (token <='\u00D6'))
           return true;
       else if ((token >= '\u00D8') && (token <='\u00F6'))
           return true;       
       else if ((token >= '\u00F8') && (token <='\u02FF'))
           return true;
       else if ((token >= '\u0370') && (token <='\u037D'))
           return true;       
       else if ((token >= '\u037F') && (token <='\u1FFF'))
           return true;
       else if ((token >= '\u200C') && (token <='\u200D'))
           return true;
       else if ((token >= '\u2070') && (token <='\u218F'))
           return true;
       else if ((token >= '\u2C00') && (token <='\u2FEF'))
           return true;
       else if ((token >= '\u3001') && (token <='\uD7FF'))
           return true;
       else if ((token >= '\uF900') && (token <='\uFDCF'))
           return true;
       else if ((token >= '\uFDF0') && (token <='\uFFFD'))
           return true;
    /*   else if ((token >= '\u10000') && (token <='\uEFFFF'))
           return true;*/
       else return false;
   }  /* end NCCHAR1 */
   
   /* rule [86] */
   public boolean NCCHAR1(char token){
       if (NCCHAR1p(token)) 
           return true;
       else if (token == '_')
           return true;
       else return false;
   }  /* end NCCHAR1 */
   
   /* rule [87] */
   public boolean VARNAME(String token){
       /*System.out.println("we are in VARNAME");*/
       boolean varname;
       int i=0;
       if ((NCCHAR1(token.charAt(i))) ||(DIGIT(token.charAt(i)))){
           boolean star=true;
           while((star) && (token.charAt(++i)!='\0')){
               if (NCCHAR1(token.charAt(i)))
                   star= true;
               else if (DIGIT(token.charAt(i)))
                   star= true;
               else if (token.charAt(i)=='\u00B7')
                   star= true;
               else if ((token.charAt(i) >= '\u0300')&&(token.charAt(i) <='\u036F'))
                   star= true;
               else if ((token.charAt(i) >= '\u203F')&&(token.charAt(i) <='\u2040'))
                   star= true;
               else
                   star= true;
           }
           if (star)
               varname=true;
           else 
               varname=false;
       }
       else 
           varname = false;
       return varname;
   }  /* end VARNAME */
   
   /* rule [88] */
   public boolean NCCHAR(char token){
       if (token=='-')
           return true;
       else if (DIGIT(token))
           return true;
       else if (token=='\u00B7')
           return true;
       else if ((token >= '\u0300')&&(token <='\u036F'))
           return true;
       else if ((token >= '\u203F')&&(token <='\u2040'))
           return true;
       else if (NCCHAR1(token))
           return true;
       else return false;
   }  /* end NCCHAR */
   
   /* rule [89] */
   public boolean NCNAME_PREFIX(String token){
       boolean ncnameprefix;
       int i=0;
       if (NCCHAR1p(token.charAt(i))){
          if (token.charAt(i+1)=='\0')
              ncnameprefix=true;
          else {
           boolean star=true;
           i++;
           while((star) && (token.charAt(i)!='\0')){
               star= ((NCCHAR(token.charAt(i)))|| (token.charAt(i)=='.'));
               i++;
           }
            if((star) && (NCCHAR(token.charAt(i-1))))
                ncnameprefix=true;
            else ncnameprefix=false;
          }
       }
       else ncnameprefix=false;
       return ncnameprefix;   
   }  /* end NCNAME_PREFIX */
   
   /* rule [90] */
   public boolean NCNAME(String token){
       boolean ncname;
       int i=0;
       if (NCCHAR1(token.charAt(i))){
          if (token.charAt(i+1)=='\0')
              ncname=true;
          else {
           boolean star=true;
           i++;
           while((star) && (token.charAt(i)!='\0')){
               star= ((NCCHAR(token.charAt(i)))|| (token.charAt(i)=='.'));
               i++;
           }
            if((star) && (NCCHAR(token.charAt(i-1))))
                ncname=true;
            else ncname=false;
          }
       }
       else ncname=false;
       return ncname;   
   }  /* end NCNAME */
   
   
}