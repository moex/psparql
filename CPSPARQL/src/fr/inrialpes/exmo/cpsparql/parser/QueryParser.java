/*
 * parser1.java
 *
 * Created on December 31, 2005, 10:57 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.parser;

import java.util.Vector;
import java.io.*;
import java.lang.String.*;
import fr.inrialpes.exmo.cpsparql.findpathprojections.*;
import fr.inrialpes.exmo.cpsparql.query.*;
import fr.inrialpes.exmo.cpsparql.turtleParser.NumericType;
import fr.inrialpes.exmo.cpsparql.maininterface.LoadDataset;
import fr.inrialpes.exmo.cpsparql.maininterface.Join;
import fr.inrialpes.exmo.cpsparql.maininterface.ApplyConstraint;

/**
 *
 * @author faisal
 */
public class QueryParser {
    private boolean optional=false;      /* this field is used to decide the type of the triples: optional or basic */
    private boolean defaultGraphIndicator = true;  /* this fiield is used to decide the type of the graph: default or named graph */
    private String predicate= new String("");
    private String subject= new String("");
    private String object= new String("");
    private Triple myTriple;
    private String parameter;
    private int maxNom=500;
    private int previousIndex []=new int[maxNom];        
    private Graph template = new Graph();
    private Graph myGraph = new Graph();
    private Graph rEConstraintGraph = new Graph();
    private boolean rEConstraintIndicator = false;
    private Graph myNamedGraph = new Graph();
    private GraphPattern myGraphPattern=new GraphPattern();
    private GraphSet myGraphSet;
    private Query query= new Query();    
    private File outputErrorFile;
    private FileWriter out;
    private FileWriter outIndex;
    private File outputErrorIndexFile;
    private BufferedReader in ;
    private int nomError=0;
    private Token tokenBuffer []= new Token [500];
    private Token tokenBuffer1 []= new Token [500];  /* this will contain the contents of tokenBuffer1    after resolving macros */
    private boolean Debug;      
    private boolean parseResult;      
    private int currentToken=-1;
    private int currentToken1=-1;
    private static int bNom=1000;
    private int lastSubject;  /* to keep last common subject  */
    private int lastStartPredicate=-1;  /* to keep last common predicate  */
    private int lastEndPredicate=-1;  /* to keep last common predicate  */
    private String lastBlankNode=new String("");  /* to keep last BlankNode  */
    private String lastBlankNodefromCollection;
    private Vector errorsExceptions=new Vector();
    private Dataset dataset = new Dataset();
    private Vector mappings = new Vector();
    private Vector tempMappings = new Vector();
    private Vector cREMappings = new Vector();
    private FindPathProjections findMappings;
    private int graphType;
    private Vector pathVarExpPairs = new Vector();
    private Vector pathConstraints = new Vector();
    private PathConstraint pathC;
    private boolean transformQuery; 
    private String transformedQuery="";
    private boolean queryContainInverse;

    
    private long elapsedTimeMillis;
    private long startTime;
    private long startLoadTimeS;
    private long elapsedLoadTimeS;
    private long elapsedTimeMSFirstS;
    /** Creates a new instance of parser1 */
    
    public QueryParser(Token tokenB [], boolean D, boolean pR, boolean transformQuery, boolean queryContainInverse) {
        Debug=D;
        parseResult=pR;
        this.queryContainInverse = queryContainInverse;
        tokenBuffer=tokenB;
        this.transformQuery = transformQuery;
        /*tokenBuffer1=tokenB;*/
        startParsing();
    }
    
    public Query getQuery(){
        return query;
    }
    
    
    public int integerVal(Token t){
        if ((t.text !=null) &&(t.type.equals("INTEGER")))
            return Integer.parseInt(t.text);        
        else return -10;
    }
    
    public Token LA(int i){
        return tokenBuffer[currentToken+i];
    }/* end LA */
    
    public Token LT(int i){
        return tokenBuffer[currentToken+i];
    }/* end LT */
    
    public boolean match(String token){
        
        if(Debug)  System.out.println(tokenBuffer[currentToken+1].getIndex()+"  "+(currentToken+1)+" we are at match:   "+"tokenBuffer[currentToken+1].type="+tokenBuffer[currentToken+1].type+" token="+token);
        previousIndex[currentToken+1]=tokenBuffer[currentToken+1].getIndex();                        
        if (tokenBuffer[currentToken+1].type.equals(token)){
            ++ currentToken1;
            ++ currentToken;
            
            Token t = new Token(tokenBuffer[currentToken]); /* copy the content of tokenBuffer */
            tokenBuffer1[currentToken1]= new Token(t);           /*  into tokenBuffer1  */
            
            return true;
        }
        else {
            nomError++;
            errorsExceptions.add(" ERROR TYPE AT TOKEN:"+(currentToken+1+1)+"   Index:"+ tokenBuffer[currentToken].getIndex()+". TOKEN MUST BE:"+token+"\n");
            //System.exit(0);
            /*try{
                outIndex.write(previousIndex[currentToken]+"\n");
                outIndex.write(tokenBuffer[currentToken+1].getIndex()+"\n");
                out.write(" ERROR TYPE AT TOKEN:"+(currentToken+1+1)+"   Index:"+ tokenBuffer[currentToken].getIndex()+". TOKEN MUST BE:"+token+"\n");
                
            }
            catch (IOException e) {
                System.out.println(" Exception: "+e);
                
            };*/
            return false;
        }
    }/* end match */
    
    public Vector getErrorsExceptions(){
        return errorsExceptions;
    }
    
    public int getErrorNom(){
        return nomError;
    }
    
    public void printErrorFile(){
            String str;
            /*try{
                in = new BufferedReader(new FileReader(outputErrorFile));
                while ((str = in.readLine()) != null){
                    System.out.println(str);
             }
            in.close();
            }
            catch (IOException e){
                System.out.println(e);
            }*/
            
    }/* end printErrorFile */
    
     public String toString(){
        String str=new String("");
        for(int j=0;j<=currentToken1;j++)
            str+=(tokenBuffer1[j].text+" ");
        return str;
    }  
   
    
     public String getTextGraphSet(){
        return query.getTextGraphSet();
    }
    
    public void printBuffer(Token tokenBuffer[],int current ){
        /*System.out.println(" printBuffer");*/
        System.out.println("                   Token                     "+"              Token Type          "+"                  Token Type Index   ");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------   ");
        for(int j=0;j<=current;j++){
            System.out.println(j+"                     "+tokenBuffer[j].text+"                                    "+tokenBuffer[j].type+"                                 "+tokenBuffer[j].typeIndex+"   ");
        }
    }
    
    
    static int create(){
        return ++bNom;
    } 
    
    public String createBlankNode(){
        int i=0;
        String str=new String("_:bbbb"+create());
        i++;
        lastBlankNode=str;
        return str;
    }
    
    public Token [] getTokenBuffer(){
        return tokenBuffer1;
    }
    
    public void startParsing(){
        nomError=0;
       /* outputErrorFile = new File("errors.txt");
        outputErrorIndexFile = new File("index.txt");
        try{
             out = new FileWriter(outputErrorFile);
             outIndex=new FileWriter(outputErrorIndexFile);
        } catch (IOException e)
         {
                   System.out.println(e);
              }*/
        
        Query();
        match("EOF");
        
       /* try{
                out.write("EOF\n");
                if( nomError <=0){
                         out.write(" \n BUILD SUCCESSFUL");
                }
                outIndex.close();
                out.close();
            }
        catch (Exception e) { System.out.println(" Exception: "+e);};*/
        if( nomError <=0){
            System.out.println("\n Parsing Correctly");
        }
        else {
                printErrorFile();
            }
        if (Debug || parseResult){
            query.print();                
          /*  myGraph.print();*/
        }
        if (Debug)
            System.out.println(transformedQuery);                
  }
   
    /* [1]  */
    public void Query(){
        if(Debug)  System.out.println(" we are at Query");
        Prolog();
        if(LA(1).type.equals("SELECT"))
            SelectQuery();
        else if(LA(1).type.equals("CONSTRUCT"))
            ConstructQuery();
        else if(LA(1).type.equals("DESCRIBE"))
            DescribeQuery();
        else if (LA(1).type.equals("ASK"))
            AskQuery();
        else {
            nomError++;
            try{
                out.write("EOF\n");
                out.write("\n ERROR AT TOKEN: "+(currentToken+1+1));
                outIndex.write(currentToken+"\n");
                outIndex.close();
                out.close();
            }
            catch (Exception e) { System.out.println(" Exception: "+e);};
        }
        /*else out.write(" ERROR QUERY AT:"+(currentToken+1)); */
         if(Debug) printBuffer(tokenBuffer1,currentToken1);
    }/* end Query */
    
    /* [2]  */
    public void Prolog(){
        if(Debug)  System.out.println(" we are at Prolog");
        if(LA(1).type.equals("BASE"))
            BaseDec1();
        while(LA(1).type.equals("PREFIX"))
            PrefixDec1();
    }/* end Prolog */
    
    /* [3]  */
    public void BaseDec1(){
        if(Debug)  System.out.println(" we are at BaseDec1");
        transformedQuery+="\n"+ LA(1).text+" ";
        match("BASE");
        if (LA(1).type.equals("Q_IRI_REF"))
            query.setBase(LA(1).text);        
        transformedQuery+= LA(1).text+" ";
        match("Q_IRI_REF");
    }/* end BaseDec1 */
    
    /* [4]  */
    public void PrefixDec1(){
       if(Debug)  System.out.println(" we are at PrefixDec1");
       transformedQuery+=""+  LA(1).text+" ";
       match("PREFIX");
       if ((LA(1).type.equals("NCNAME_PREFIX")) || LA(1).type.equals("COLON")){      
           if (LA(1).type.equals("NCNAME_PREFIX")){
               transformedQuery+= LA(1).text+" ";
               match("NCNAME_PREFIX");
               query.setPrefix(LA(3).text, (LA(1).text+LA(2).text));   /* this the prefix and the prefixName  */           
           }
           else query.setPrefix(LA(2).text, LA(1).text);   /* this the prefix and the prefixName  */           
           transformedQuery+= LA(1).text+" ";
           match("COLON");           
       }
       else {
           query.setPrefix(LA(2).text, LA(1).text);  /* this the prefix and the prefixName  */ 
           transformedQuery+= LA(1).text+" ";
           match("QNAME_NS");
       }        
       transformedQuery+= LA(1).text+" \n";
       match("Q_IRI_REF");
    }/* end PrefixDec1 */
    
    
    /* [5]  */
    public void SelectQuery(){
        if(Debug)  System.out.println(" we are at SelectQuery");
        transformedQuery+= LA(1).text+" ";
        match("SELECT");
        query.setQueryType(1);
        if(LA(1).type.equals("DISTINCT")){
            query.setDistinct(true);
            transformedQuery+= LA(1).text+" ";
            match("DISTINCT");
        }
        if(LA(1).type.equals("STAR")){
            transformedQuery+= LA(1).text+" ";
            match("STAR");
        }
        else {
            query.setSelectIndexStar(0); 
            query.setVariables(LA(1).text);
            Var();
            while ((LA(1).type.equals("VAR1")) || (LA(1).type.equals("VAR2"))) {
                query.setVariables(LA(1).text);
                transformedQuery+= LA(1).text+" ";
                Var();
            }
        }        
        while(LA(1).type.equals("FROM"))
            DatasetClause();
        WhereClause();
        SolutionModifier();
    }/* end SelectQuery */
    
    /* [6]  */
    public void ConstructQuery(){
        if(Debug)  System.out.println(" we are at ConstructQuery");
        transformedQuery+= LA(1).text+" ";
        match("CONSTRUCT");
        query.setQueryType(2);
        if (LA(1).type.equals("LBRACE"))
            ConstructTemplate();
        else 
            query.setConstructTemplateUri(SourceSelector());            
        while(LA(1).type.equals("FROM"))
            DatasetClause();
        WhereClause();
        SolutionModifier();
    }/* end ConstructQuery */
    
    /* [7]  */
    public void DescribeQuery(){
        if(Debug)  System.out.println(" we are at DescribeQuery");
        transformedQuery+= LA(1).text+" ";
        match("DESCRIBE");
        query.setQueryType(4);
        if(LA(1).type.equals("STAR")){
            transformedQuery+= LA(1).text+" ";
            match("STAR");
        }
        else {
            transformedQuery+= LA(1).text+" ";
            VarOrIRIref();
            while ((LA(1).type.equals("VAR1")) || (LA(1).type.equals("VAR2")) 
                   || (LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))){
                transformedQuery+= LA(1).text+" ";
                VarOrIRIref();
            }
        }        
        while(LA(1).type.equals("FROM"))
            DatasetClause();
        if ((LA(1).type.equals("WHERE")) || (LA(1).type.equals("LBRACE")))
            WhereClause();
        SolutionModifier();
    }/* end DescribeQuery */
    
    /* [8]  */
    public void AskQuery(){
        if(Debug)  System.out.println(" we are at AskQuery");
        transformedQuery+= LA(1).text+" ";
        match("ASK");
        query.setQueryType(3);
        while(LA(1).type.equals("FROM"))
            DatasetClause();
        WhereClause();
    }/* end AskQuery */
    
    /* [9]  */
    public void DatasetClause(){
        if(Debug)  System.out.println(" we are at DatasetClause");
        transformedQuery+="\n"+  LA(1).text+" ";
        match("FROM");
        if (LA(1).type.equals("NAMED"))
            NamedGraphClause();
        else DefaultGraphClause();
    }/* end DatasetClause */
    
    /* [10]  */
    public void DefaultGraphClause(){
        if(Debug)  System.out.println(" we are at DefaultGraphClause");
      
        /* resolve macros base IRI   */
        String base1=new String("");        
        /*if (!(query.base.equals(""))){  /* !!!!!!!!! i don't know if we apply BASE to this  
                String s1=LA(1).text;
                for(int j=0;query.base.charAt(j)!='>';j++)
                     base1+=query.base.charAt(j);
                for(int j=1;s1.charAt(j)!='>';j++)
                     base1+=s1.charAt(j);
                base1+='>';
                String s2= LA(0).type;
                int ty = LA(0).typeIndex;
                Token t = new Token(base1,s2, ty);
                tokenBuffer1[currentToken1]= new Token(t);
                tokenBuffer[currentToken]= new Token(t);
            } /* end resolve macros: base */
        /*else base1 =new String(LA(1).text);*/
        base1 =LA(1).text;
        query.setFromIRI(base1);  /* if do not want here or is not applyied the base to the from IRI,*/
        transformedQuery+= LA(1).text;
        SourceSelector();          /*    we delete the previous block   */
    }/* end DefaultGraphClause */
    
    /* [11]  */
    public void NamedGraphClause(){
        if(Debug)  System.out.println(" we are at NamedGraphClause");
        transformedQuery+= LA(1).text+" ";
        match("NAMED");
          /* resolve macros base IRI   */
        String base1=new String("");        
        /*if (!(query.base.equals(""))){    /* !!!!!!!!! i don't know if we apply BASE to this  
                String s1=LA(1).text;
                for(int j=0;query.base.charAt(j)!='>';j++)
                     base1+=query.base.charAt(j);
                for(int j=1;s1.charAt(j)!='>';j++)
                     base1+=s1.charAt(j);
                base1+='>';
                String s2= LA(0).type;
                int ty = LA(0).typeIndex;
                Token t = new Token(base1,s2, ty);
                tokenBuffer1[currentToken1]= new Token(t);
                tokenBuffer[currentToken]= new Token(t);
            } /* end resolve macros: base */
       /* else base1 =new String(LA(1).text);*/
        base1 =LA(1).text;
        query.setFromNamedIRI(base1);  /* if do not want here or is not applyied the base to the from IRI,*/
        transformedQuery+= LA(1).text;
        SourceSelector();          /*    we delete the previous block   */        
    }/* end NamedGraphClause */
    
    /* [12]  */
    public String SourceSelector(){
        if(Debug)  System.out.println(" we are at SourceSelector");
      return IRIref();
    }/* end SourceSelector */
    
    /* [13]  */
    public Vector WhereClause(){
        
        /*here we start loading dataset, i.e., the default and named graphes*/
        
        // start Loading Time
        startLoadTimeS = System.currentTimeMillis()/1000;
        if(!transformQuery){       
            LoadDataset loadDataset = new LoadDataset(query.getFromIRIs(),query.getFromNamedIRIs(),queryContainInverse);
            dataset = loadDataset.getDataset();
        }
        
        elapsedLoadTimeS = System.currentTimeMillis()/1000-startLoadTimeS;
        //System.out.println("Elapsed Load Time Seconds = "+elapsedLoadTimeS);
        /* end loading dataset*/
        
        startTime = System.currentTimeMillis();

        myGraphSet= new GraphSet(); //modified at 30/07/2007
        if(Debug)  System.out.println(" we are at WhereClause");
        if (LA(1).type.equals("WHERE")){
            transformedQuery+= "\n"+ LA(1).text+" ";
            match("WHERE");
        }
        boolean namedGraph = false; Vector tempMappings = new Vector(); String namedGraphIndex="";
        mappings = new Vector(GroupGraphPattern(tempMappings,namedGraph,namedGraphIndex));
        myGraphSet.setGraphPattern(myGraphPattern); //modified at 30/07/2007               
        query.setGraphSet(myGraphSet); //modified at 30/07/2007
        return mappings;
    }/* end WhereClause */
    
    /* [14]  */
    public void SolutionModifier(){
        if(Debug)  System.out.println(" we are at SolutionModifier");
        if (LA(1).type.equals("ORDER"))
            OrderClause();
        if (LA(1).type.equals("LIMIT"))
            LimitClause();
        if (LA(1).type.equals("OFFSET"))
            OffesetClause();
        
        elapsedTimeMillis = System.currentTimeMillis()-startTime;
        //System.out.println("Elapsed Time Milli Seconds = "+elapsedTimeMillis);

    }/* end SolutionModifier */
    
    public long getElapsedTimeMillis(){
        return elapsedTimeMillis;
    }
    
     public long getElapsedLoadTimeS(){
        return elapsedLoadTimeS;
    }
    
    /* [15]  */
    public void OrderClause(){
        if(Debug)  System.out.println(" we are at OrderClause");
        query.setOrder();
        transformedQuery+= LA(1).text+" ";
        match("ORDER");
        transformedQuery+= LA(1).text+" ";
        match("BY");
        int startOrderCondition =currentToken+1;        
        OrderCondition();
        while ((LA(1).type.equals("ASC")) || (LA(1).type.equals("DESC")) 
               ||(LA(1).type.equals("LPAREN")) || (LA(1).type.equals("VAR1")) || (LA(1).type.equals("VAR2"))
               || (LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS")))
            OrderCondition();
        /* we will capture the order condition   */
        int endOrderCondition=currentToken;
        String orderCondition=new String("");
        for(int j=startOrderCondition;j<=endOrderCondition;j++)
            orderCondition+=tokenBuffer[j].text;      //+"  ";        
        query.setOrderClause(orderCondition);        
    }/* end OrderClause */
    
    /* [16]  */
    public void OrderCondition(){
        if(Debug)  System.out.println(" we are at OrderCondition");
        int startOrderC,endOrderC,dir=1,plusOne=0;
        String type;
        if ((LA(1).type.equals("ASC")) || (LA(1).type.equals("DESC"))){
            if (LA(1).type.equals("DESC"))
                dir=0;
            transformedQuery+= LA(1).text+" ";
            match(LA(1).type);
            startOrderC =currentToken+2;        
            BrackettedExpression();
            type ="BracketedExpresssion";
        }
        else if(LA(1).type.equals("LPAREN")){ 
            startOrderC =currentToken+2;        
            BrackettedExpression();
            type ="BracketedExpresssion";
        }
        else if ((LA(1).type.equals("VAR1")) || (LA(1).type.equals("VAR2"))){            
            startOrderC =currentToken+1;
            Var();
            type ="var";
        }
        else{
            startOrderC =currentToken+1;
            FunctionCall();
            type ="FunctionCall";
        }  
        if(type.equals("var"))
            endOrderC=currentToken;
        else {endOrderC=currentToken-1;plusOne=1;}
        /* we will capture the order condition   */
        int endOrderCondition=currentToken;
        String orderCondition=new String("");
        transformedQuery+= tokenBuffer[startOrderC-1].text;
        for(int j=startOrderC;j<=endOrderC;j++)
            orderCondition+=tokenBuffer[j].text;      //+"  ";        
        transformedQuery+= orderCondition+" ";
        if (plusOne==1)
            transformedQuery+= tokenBuffer[endOrderC+plusOne].text;
        OrderCondition oc = new OrderCondition(dir, type, orderCondition); 
        query.setOrderCondition(oc);        
    }/* end OrderCondition */
    
        
    /* [17]  */
    public void LimitClause(){
        if(Debug)  System.out.println(" we are at LimitClause");
        transformedQuery+= LA(1).text+" ";
        match("LIMIT");
        transformedQuery+= LA(1).text+" ";
        match("INTEGER");
        query.setLimitClause(integerVal(LA(0)));        
    }/* end LimitClause */
    
    /* [18]  */
    public void OffesetClause(){
        if(Debug)  System.out.println(" we are at OffsetClause");
        transformedQuery+= LA(1).text+" ";
        match("OFFSET");
        transformedQuery+= LA(1).text+" ";
        match("INTEGER");
        query.setOffsetClause(integerVal(LA(0)));        
    }/* end OffesetClause */
    
    /* [19]  */
    public Vector GroupGraphPattern(Vector tempMappings,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at GroupGraphPattern");
        transformedQuery+= LA(1).text+" ";
        match("LBRACE");
        tempMappings = new Vector(GraphPattern(tempMappings,namedGraph,namedGraphIndex));
        transformedQuery+= LA(1).text+" ";
        match("RBRACE");  
        //if (defaultGraphIndicator) myGraphPattern.setDefaultGraph(myGraph);       
        //else myGraphPattern.setNamedGraph(myNamedGraph,parameter);
        return tempMappings;
    }/* end GroupGraphPattern */
    
    /* [20]  */
    public Vector GraphPattern(Vector tempMappings,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at GraphPattern");
        boolean emptyGraph = true;
        if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1"))
            || LA(1).type.equals("CONSTRAINT")
            ||(LA(1).type.equals("COLON"))
            ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
           || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
           || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
           || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
            (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
             (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
            ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET"))|| (LA(1).type.equals("DEFINED")))){
            
            tempMappings = FilteredBasicGraphPattern(tempMappings,namedGraph,namedGraphIndex);
            emptyGraph = false;
            //Graph pattern = WhereTriples();
            
            /* finding PRDF projections */
            //findMappings = new FindPathProjections(pattern,dataset.getDefaultGraph());  
            //eval(dataset,pattern);
            
        }
        if  ((LA(1).type.equals("OPTIONAL")) ||(LA(1).type.equals("LBRACE")) 
                  || (LA(1).type.equals("GRAPH"))){
            Join join = new Join();                
            //if (LA(1).type.equals("DEFINED"))                                
              //  GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex);
            //else
                if (LA(1).type.equals("OPTIONAL"))                                
                if (!emptyGraph)
                    tempMappings= join.outerJoin(tempMappings, GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex)); 
                else {
                emptyGraph = false;
                tempMappings = GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex);
                }
            else// if ((LA(1).type.equals("LBRACE"))|| (LA(1).type.equals("GRAPH")))
                if (!emptyGraph)
                    tempMappings= join.join(tempMappings, GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex)); 
                else{
                emptyGraph = false;
                tempMappings = GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex);
                }
            if(LA(1).type.equals("DOT"))
                match("DOT");                
            
            //tempMappings =GraphPattern(tempMappings,namedGraph,namedGraphIndex);                         
            if (LA(1).type.equals("OPTIONAL"))                
                if (!emptyGraph)
                    tempMappings= join.outerJoin(tempMappings, GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex)); 
                else tempMappings = GraphPattern(tempMappings,namedGraph,namedGraphIndex);
            else// if ((LA(1).type.equals("LBRACE"))|| (LA(1).type.equals("GRAPH")))
                if (!emptyGraph)
                    tempMappings= join.join(tempMappings, GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex)); 
                else tempMappings = GraphPattern(tempMappings,namedGraph,namedGraphIndex);
             
        }         
        return tempMappings;
    }/* end GraphPattern */
    
    public void eval(Dataset dataset,Graph pattern){
        Join join = new Join();                
        switch (graphType){
            case 0: // AND of graphs
                mappings = join.join(mappings, findMappings.Solutions());
                break;
            case 1: // union of graphs
                mappings.addAll(findMappings.Solutions());
                break;
            case 2://optional graph
                mappings = join.outerJoin(mappings, findMappings.Solutions());
                break;
            case 3:// Named Graph 
                break;
            case 4: // constraint
                break;
            default:
                
        }
    }
    
    /* [21]  */
    public Vector FilteredBasicGraphPattern(Vector tempMappings,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at FilteredBasicGraphPattern");
        //System.out.println(transformQuery);
        Vector prdfProjections = new Vector();            
        if (LA(1).type.equals("CONSTRAINT"))                                
            //GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex);
            PathConstraint(namedGraph,namedGraphIndex);
        if (LA(1).type.equals("DEFINED"))                                
            //GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex);
            DefinedPathVar(namedGraph,namedGraphIndex);
        if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) 
            ||(LA(1).type.equals("COLON"))
            ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
           || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
           || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
           || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
            (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
             (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
            ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET")))){
            
            if (rEConstraintIndicator)
                rEConstraintGraph = new Graph();
            else
                myGraph = new Graph();
            Graph pattern = BlockOfTriples(namedGraph,namedGraphIndex);
            
            /* finding PRDF projections */
            if(!namedGraph){
                //System.out.println("default graph ...");
                Join join = new Join();
                if (!transformQuery){
                    findMappings = new FindPathProjections(pattern,dataset.getDefaultGraph(), cREMappings);  
                    if(tempMappings.size()==0)
                        prdfProjections =  findMappings.Solutions();
                    else
                        prdfProjections =  join.join(tempMappings,findMappings.Solutions());
                }
                tempMappings = prdfProjections;
                //System.out.println(prdfProjections.size());
            }
            else if ((namedGraphIndex.charAt(0)!='?')&&(namedGraphIndex.charAt(0)!='#')&&(namedGraphIndex.charAt(0)!='_')) {
                //System.out.println("named graph ...");
                int index = query.getFromNamedIRIs().indexOf(namedGraphIndex);
                if (index >=0){
                    if(!transformQuery){
                        findMappings = new FindPathProjections(pattern,dataset.getNamedGraph(namedGraphIndex), cREMappings);   
                        prdfProjections =  findMappings.Solutions();
                    }
                }
            }
            else {
                for (int i=0;i<query.getFromNamedIRIs().size();i++){
                    //System.out.println("named graph ...");
                    Vector partialMap = new Vector();                
                    VarImage vi = new VarImage(namedGraphIndex, (String)query.getFromNamedIRIs().get(i),-1);
                    partialMap.add(vi);                
                    if(!transformQuery){                        
                        findMappings = new FindPathProjections(pattern,dataset.getNamedGraph((String)query.getFromNamedIRIs().get(i)),partialMap, cREMappings);       
                        prdfProjections.addAll(findMappings.Solutions());                    
                    }
                }                
                
            }
        }
        if  (LA(1).type.equals("FILTER") || (LA(1).type.equals("DEFINED"))){
            if (LA(1).type.equals("DEFINED"))                                
                //GraphPatternNotTriples(tempMappings,namedGraph,namedGraphIndex);
                DefinedPathVar(namedGraph,namedGraphIndex);
            else{
                Constraint();
                 if (rEConstraintIndicator)
                     prdfProjections = new Vector(applyingConstraints(prdfProjections,rEConstraintGraph.getConstraints()));            
                 
                 else{
                     prdfProjections=new Vector(applyingConstraints(prdfProjections,myGraph.getConstraints()));            
                     //System.out.println(prdfProjections.size());
                 }
            }
        }
                if(LA(1).type.equals("DOT"))
                    match("DOT");   
                if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) 
                    ||(LA(1).type.equals("COLON"))
                    ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
                   || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
                   || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
                   || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
                    (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
                     (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                        (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
                    ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("FILTER")) ||(LA(1).type.equals("LBRACKET"))|| (LA(1).type.equals("DEFINED"))
                     || (LA(1).type.equals("CONSTRAINT")))){
                    //Join join = new Join();
                    //prdfProjections = join.join(prdfProjections,FilteredBasicGraphPattern(tempMappings, namedGraph,namedGraphIndex));
                    FilteredBasicGraphPattern(tempMappings, namedGraph,namedGraphIndex);
                }
        if (findMappings != null)
            elapsedTimeMSFirstS = findMappings.getElapsedTimeMsFirstS();
        if(Debug)  System.out.println(" we exit FilteredBasicGraphPattern");
        return prdfProjections;
    }/* end FilteredBasicGraphPattern */
    
    public long getElapsedTimeMSFirstS(){
        return elapsedTimeMSFirstS;
    }
    
    public Vector applyingConstraints(Vector sols,Vector cons){
        Vector solutionsAfter;
        ApplyConstraint applyConstraints = new ApplyConstraint(sols, cons);
        solutionsAfter = applyConstraints.getSolutionsAfter(); 
        //System.out.println(solutionsAfter.size());
        return solutionsAfter;
    }
    
    /* [22]  */
    public Graph BlockOfTriples(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at BlockOfTriples()");
        //myGraph = new Graph();  //modified at 30/7/2007
        PathTriplesSameSubject(namedGraph,namedGraphIndex);
        
        
        while (LA(1).type.equals("DOT")) {
            match("DOT");
            if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) || (LA(1).type.equals("COLON"))                
            ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
               || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
               || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
               || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
                (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
                 (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                    (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
                ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET"))))
            PathTriplesSameSubject(namedGraph,namedGraphIndex);            
        }   
        if(Debug)  System.out.println(" we exit BlockOfTriples()");
        if (rEConstraintIndicator)
            return rEConstraintGraph;
        else
            return myGraph;
    }/* end BlockOfTriples() */
    
    /* [23]  */
    public Vector GraphPatternNotTriples(Vector tempMappings,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at GraphPatternNotTriples");
        if (LA(1).type.equals("OPTIONAL"))
            return new Vector(OptionalGraphPattern(tempMappings,namedGraph,namedGraphIndex));
        else if (LA(1).type.equals("LBRACE")){
            //myGraphSet.setGraphPattern(myGraphPattern);                
            return new Vector(GroupOrUnionGraphPattern(tempMappings,namedGraph,namedGraphIndex));            
        }
        else if (LA(1).type.equals("GRAPH"))
            return new Vector(GraphGraphPattern(tempMappings,namedGraph,namedGraphIndex));         
       /* else if (LA(1).type.equals("DEFINED")){
            DefinedPathVar(namedGraph,namedGraphIndex);
            return tempMappings;
        }*/
        else return tempMappings;
        /*else if(LA(1).type.equals("FILTER"))
            Constraint();*/
    }/* end GraphPatternsNotTriples */
   
    
    /* [24]  */
    public Vector OptionalGraphPattern(Vector tempMappings,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at OptionalGraphPattern");
        transformedQuery+= LA(1).text+" ";
        match("OPTIONAL");
        //optional=true;       /* the following are optional Triples  */
        return new Vector(GroupGraphPattern(tempMappings,namedGraph,namedGraphIndex));  
        //optional =false;         /* end of the optional triples  */
    }/* end OptionalGraphPattern */
    
    /* [25]  */
    public Vector GraphGraphPattern(Vector tempMappings,boolean namedGraph, String namedGraphIndex){
        if(Debug)  System.out.println(" we are at GraphGraphPattern");        
        //myNamedGraph=new Graph();
        transformedQuery+= LA(1).text+" ";
        match("GRAPH");
        /* here increment the number of named graphs by 1 */
        //defaultGraphIndicator=false;              
        parameter= LA(1).text;
        namedGraph = true;
        transformedQuery+= LA(1).text+" ";
        VarOrBlankNodeOrIRIref();
        return new Vector(GroupGraphPattern(tempMappings,namedGraph,parameter));  
        //defaultGraphIndicator=true;              
    }/* end ConstructTemplate */
    
    /* [26]  */
    public Vector GroupOrUnionGraphPattern(Vector tempMappings,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at GroupOrUnionGraphPattern");
        tempMappings = new Vector(GroupGraphPattern(tempMappings,namedGraph, namedGraphIndex));
        while (LA(1).type.equals("UNION")){
            transformedQuery+= LA(1).text+" ";
            match("UNION");
            //myGraphSet.setGraphPattern(myGraphPattern);                
            /* query.getGraphSet().incrementGraphPatternNom();   /* increment the number of graph patterns by 1 */
            //myGraph = new Graph();
            //myGraphPattern=new GraphPattern();
            tempMappings.addAll(GroupGraphPattern(tempMappings,namedGraph, namedGraphIndex)); 
        }
        return tempMappings;
    }/* end GroupOrUnionGraphPattern */
    
    public void addConstraintToGraph(Vector c){
        if (rEConstraintIndicator)
            rEConstraintGraph.addConstraint(c);
        else
            myGraph.addConstraint(c);        
    }/* end addConstraintToGraph   */
    
            /* [26.1]  */
    public void DefinedPathVar(boolean namedGraph,String namedGraphIndex){
       if(Debug)  System.out.println(" we are at DefinedPathVar()");
       transformedQuery+= LA(1).text+" ";
       match("DEFINED");
       transformedQuery+= LA(1).text+" ";
       match("BY");
       // ex: Defined By ??var1 ex:Amman +(ex:train) ?Y
       String pathVar = LA(1).text;
       match("VAR3");//PathVar(); 
       String s = VarOrTerm();
       String pathExp = PathVerb(namedGraph,namedGraphIndex);
       String o = VarOrTerm();
       transformedQuery+= pathVar+" "+s+" "+pathExp+" "+o+" ";
       PathVarExpPair pvep = new PathVarExpPair(pathVar,s, pathExp,o);
       if (!existPathVar(pathVar))
           pathVarExpPairs.add(pvep);
       else {
           //System.out.println(" mutiplied definition: path variable already defined");
           //do something
           errorsExceptions.add(" Mutiplied definition: path variable already defined:"+(currentToken+1+1));
           //System.exit(0);
       }
    }/* end DefinedPathVar */
    
    public boolean existPathVar(String var){
        boolean found = false;
        String pathExp = "+(#)"; //"+(  -ex:from  .  ex:to  )"; 
        int index = pathVarExpPairs.size()-1;
        PathVarExpPair pvep;
        while((!found) && (index >=0)){
            pvep = (PathVarExpPair) pathVarExpPairs.get(index);
            String pVar = pvep.getPathVar();
            if (pVar.equals(var))
                found = true;
            index--;
        }
        return found;
    }
    
    /*public void addConstraintToGraph(Vector c){
        if(defaultGraphIndicator)
          if(optional)
            myGraph.addOptionalConstraint(c);
          else
             myGraph.addConstraint(c);
        else
             if(optional)
                myNamedGraph.addOptionalConstraint(c);
             else
                 myNamedGraph.addConstraint(c);
    } end addConstraintToGraph   */
    
    /* [27]  */
    public void Constraint(){
        if(Debug)  System.out.println(" we are at Constraint");
        transformedQuery+= LA(1).text+" ";
        match("FILTER");
        int beginConstraint=0, endConstraint=0;
        beginConstraint=currentToken+1;  /*  here is the begining of the constrait  */
        if ((LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS")))
            FunctionCall();
        else if (LA(1).type.equals("LPAREN"))
            BrackettedExpression();
        else if (LA(1).type.equals("STR") ||LA(1).type.equals("LANG")||
                LA(1).type.equals("LANGMATCHES") ||LA(1).type.equals("DATATYPE")||
                LA(1).type.equals("BOUND") ||LA(1).type.equals("isIRI")||
                LA(1).type.equals("isURI") ||LA(1).type.equals("isBLANK")||
                LA(1).type.equals("isLITERAL") ||LA(1).type.equals("REGEX")) 
            BuiltInCall();
        else match("FILTER");
        endConstraint=currentToken;  /*  here is the end of the constraint  */
        Vector constraint=new Vector();
        for (int j=beginConstraint;j<=endConstraint;j++){
            constraint.add(tokenBuffer[j]);        
            transformedQuery+= tokenBuffer[j].text+" ";
        }
        Token token =new Token("EOF","EOF",1000, 1001);
        constraint.add(token);
        addConstraintToGraph(constraint);
    }/* Constraint */
   
     
    /* [27.1]  */
    public void PathConstraint(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathConstraint");
        transformedQuery+= "\n"+ LA(1).text+" ";
        int beginConstraint=0, endConstraint=0;
        match("CONSTRAINT");
        transformedQuery+=LA(1).text+" ";
        String cName =  ConstraintName();
        char cNum = (char)cREMappings.size();
        String type = constraintType(LA(2).type, LA(2).text);
        String cRegExpDelTypeVar = LA(1).text+type+LA(3).text+LA(4).text;
        //System.out.println(" we are at PathConstraint:"+cRegExpDelTypeVar);
        PathConstraint pathC = new PathConstraint(cNum,cName,cRegExpDelTypeVar);
        pathConstraints.add(pathC);
        beginConstraint=currentToken+1;  /*  here is the begining of the constrait  */
        for (int j=beginConstraint;j<=beginConstraint+4;j++)
            transformedQuery+= tokenBuffer[j].text+" ";
        cREMappings.add(RegularExpressionConstraint(namedGraph,namedGraphIndex));
        //endConstraint=currentToken;  /*  here is the end of the constraint  */
        
    }/* PathConstraint */
    
    /* [27.2]  */
    public String ConstraintName(){
        if(Debug)  System.out.println(" we are at ConstraintName");
        //match("CONSTRAINT");
        if (LA(1).type.equals("NCNAME") || LA(1).type.equals("NCNAME_PREFIX"))
            match(LA(1).type);
        return LA(0).text;
    }/* PathConstraint */
    
    
      /* [28]  */
    public void FunctionCall(){
        if(Debug)  System.out.println(" we are at FunctionCall");
        IRIref();
        ArgList();
    }/* end FunctionCall */
   
    /* [29]  */
    public void ArgList(){
        if(Debug)  System.out.println(" we are at ArgList");
        if (LA(1).type.equals("NIL")){
            match("NIL");
           
            /* resolve macros NIL collection */
            /*String s1="<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>";    /*"rdf:nil";*/
            /*subject=s1;
            String s2= LA(0).type;
            int ty = LA(0).typeIndex;
            Token t = new Token(s1,s2, ty);
            tokenBuffer1[currentToken1]= new Token(t);
            tokenBuffer[currentToken]= new Token(t);*/
            
        }
        else {
            match("LPAREN");
            Expression();
            while (LA(1).type.equals("COMMA")){
                match("COMMA");
                Expression();
            }
            match("RPAREN");
        }
    }/* end ArgList */
   
    
    /* [30]  */
    public void ConstructTemplate(){
        if(Debug)  System.out.println(" we are at ConstructTemplate");
        match("LBRACE");
        int startTemplateIndex = currentToken;
        ConstructTriples();
        match("RBRACE");
        int endTemplateIndex = currentToken;
        String constructTemplate = "";
        for(int j=startTemplateIndex;j<=endTemplateIndex;j++)
            constructTemplate+=tokenBuffer[j].text; 
        transformedQuery+= constructTemplate+" ";
        
    }/* end ConstructTemplate */
    
    public void addTripleToTemplate(String subject, String predicate,String object){
        /* here we add the triple to the graph Template   */        
        myTriple = new Triple(0,subject,predicate,object, -1, -1, -1);
        template.setTriple(myTriple);
        /* end adding the triple to the graph  Template*/
        
    }
    
    
       
    /* [31]  */
    public void ConstructTriples(){
        if(Debug)  System.out.println(" we are at ConstructTriples");
        if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) || (LA(1).type.equals("COLON"))
            ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
           || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
           || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
           || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
            (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
             (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
            ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET")))){
            
            TriplesSameSubject();
            
            
            if (LA(1).type.equals("DOT")){
                match("DOT");
                ConstructTriples();
            }
        }
        query.setTemplate(template);        
    }/* end ConstructTriples */
    
    
    /* [32]  */
    public void TriplesSameSubject(){
        if(Debug)  System.out.println(" we are at Triples1");
        if ((LA(1).type.equals("LPAREN"))||(LA(1).type.equals("LBRACKET"))) {
            String s=TriplesNode();
            PropertyList(s);
        }
        else{
            lastSubject=currentToken+1;     /* this for resolving macros: common subjects  */
            VarOrTerm();
            if (!(tokenBuffer[currentToken].type.equals("NIL")))
                subject = tokenBuffer[currentToken].text;
            else subject ="<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>";
            PropertyListNotEmpty(subject);             
            
        }
    }/* end TriplesSameSubject */
    
    /* [32.1]  */
    public void PathTriplesSameSubject(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathTriplesSameSubject");
        if ((LA(1).type.equals("LPAREN"))||(LA(1).type.equals("LBRACKET"))) {
            String n=PathTriplesNode(namedGraph,namedGraphIndex);
            PathPropertyList(n,namedGraph,namedGraphIndex);
        
        }
        else{
            lastSubject=currentToken+1;     /* this for resolving macros: common subjects  */
            VarOrTerm();
            if (!(tokenBuffer[currentToken].type.equals("NIL")))
                subject = tokenBuffer[currentToken].text;
            else subject ="<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>";
            PathPropertyListNotEmpty(subject,namedGraph,namedGraphIndex);                         
            
        }
    }/* end PathTriplesSameSubject */
    
        
   /* [33]  */
    public void PropertyList(String s){
        if(Debug)  System.out.println(" we are at PropertyList");
        if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) 
                ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
                || (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) ||
                  (LA(1).type.equals("COLON")) || ((LA(1).text.equals("a")) )){
            PropertyListNotEmpty(s);
           }
            
    }/* end PropertyList */
   
    
    /* [33.1]  */
    public void PathPropertyList(String s,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathPropertyList");
        if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) 
                ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
                || (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) ||
                ((LA(1).text.equals("a"))) || ((LA(1).text.equals("("))) || ((LA(1).text.equals("!")))
                || (LA(1).type.equals("COLON"))
                || ((LA(1).text.equals("+")))|| ((LA(1).text.equals("*")))){
            PathPropertyListNotEmpty(s,namedGraph,namedGraphIndex);            
        }
    }/* end PathPropertyList */
   
    
    /* [34]  */
    public void PropertyListNotEmpty(String s){
        if(Debug)  System.out.println(" we are at PropertyListNotEmpty");
        String p="";
        p =Verb();
        //System.out.println("p="+p);                
        ObjectList(s,p);
        if (LA(1).type.equals("SEMICOLON")){
            match("SEMICOLON");
            PropertyList(s);
        }
    }/* end PropertyListNotEmpty */
    
    
    /* [34.1]  */
    public void PathPropertyListNotEmpty(String s,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathPropertyListNotEmpty");
        String predicate="";
        predicate =PathVerb(namedGraph,namedGraphIndex);
        PathObjectList(s,predicate,namedGraph,namedGraphIndex);
        
        if (LA(1).type.equals("SEMICOLON")){
            match("SEMICOLON");    
            PathPropertyList(s,namedGraph,namedGraphIndex);
        }
    }/* end PathPropertyListNotEmpty */
    
    
    
    /* [35]  */
    public void ObjectList(String s, String p){
        if(Debug)  System.out.println(" we are at ObjectList");
        String object="";
        object=GraphNode();
        /* here we add the triple to the graph   */
        addTripleToTemplate(s,p,object);
        /* end adding the triple to the graph */
        if (LA(1).type.equals("COMMA")){
            match("COMMA");            
            ObjectList(s, p);
        }
    }/* end ObjectList */
    
   
    /* [35.1]  */
    public void PathObjectList(String s, String p,boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathObjectList");
        String object="";
        object=PathGraphNode(namedGraph,namedGraphIndex);
        /* here we add the triple to the graph   */
        addTripleToGraph(s,p,object);        
        /* end adding the triple to the graph */
        
        if (LA(1).type.equals("COMMA")){
            match("COMMA");
            PathObjectList(s,p,namedGraph,namedGraphIndex);
        }
    }/* end PathObjectList */
    
    
     /* [28]  */
    /*public void Triples(){
        if(Debug)  System.out.println(" we are at Triples");
        TriplesSameSubject();
        while (LA(1).type.equals("DOT")) {
            match("DOT");
            if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) || (LA(1).type.equals("COLON"))
                ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
               || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
               || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
               || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
                (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
                 (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                    (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
                ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET"))))
            TriplesSameSubject();
        }
    }/* end Triples */
   
    /*public void addTripleToGraph(String subject, String predicate,String object){
        if(Debug)  System.out.println(" we are at addTripleToGraph");
        int type =0;
        if (lastStartPredicate==lastEndPredicate)
            type=0;
        else type =1;        
        myTriple = new Triple(type,subject,predicate,object, -1, -1, -1);
        if(defaultGraphIndicator)
          if(optional)
            myGraph.setOptionalTriple(myTriple);
          else
             myGraph.setTriple(myTriple);
        else
             if(optional)
                myNamedGraph.setOptionalTriple(myTriple);
             else
                 myNamedGraph.setTriple(myTriple);
    }  end addTripleToGraph   */
    
    public String getPathExp(String var, String subject, String object){
        boolean found = false;
        String pathExp = "+(#)"; //"+(  -ex:from  .  ex:to  )"; 
        int index = pathVarExpPairs.size()-1;
        PathVarExpPair pvep;
        while((!found) && (index >=0)){
            pvep = (PathVarExpPair) pathVarExpPairs.get(index);
            String pVar = pvep.getPathVar();
            String s = pvep.getSNode();
            String o = pvep.getONode();
            if (pVar.equals(var)){
                if (s.equals(subject)&&(o.equals(object))){
                    pathExp = pvep.getPathExp();
                    found = true;
                }
                else {
                    errorsExceptions.add(" Subject node or/and Object node of the path variable '"+var +"' do not match the subject or/and object node in the triple where the path variable is used");
                    //System.exit(0);
                }
                    
            }
            index--;
        }
        return pathExp;
    }
    
    public void addTripleToGraph(String subject, String predicate,String object){
        if(Debug)  System.out.println(" we are at addTripleToGraph");
        int type =0;
        if (lastStartPredicate==lastEndPredicate)
            type=0;
        else type =1;        
        if ((predicate.charAt(0)=='?')&&(predicate.charAt(1)=='?')){
            predicate += " "+ getPathExp(predicate,subject,object);
        }
        //System.out.println("predicate"+predicate);
        myTriple = new Triple(type,subject,predicate,object, -1, -1, -1);
        //predicate = testThePredicate(predicate);
        transformedQuery+= subject+" "+predicate+" "+ object+" . \n";
        if (rEConstraintIndicator)
            rEConstraintGraph.setTriple(myTriple);
        else  myGraph.setTriple(myTriple);
      } /* end addTripleToGraph   */
    
    public String testThePredicate(String predicate, boolean transitive){
       if(predicate.charAt(0)=='?' || predicate.charAt(0)=='$')
            return predicate;
        else if(predicate.equals("<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>")&&
                !transitive)
            return "*<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>";
        else if(predicate.equals("<http://www.w3.org/2000/01/rdf-schema#subClassOf>")&&
               !transitive)
            return "*<http://www.w3.org/2000/01/rdf-schema#subClassOf>";
        else if(predicate.equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"))
            return predicate+ " . +<http://www.w3.org/2000/01/rdf-schema#subClassOf>";
        else if((!predicate.contains("http://www.w3.org/2000/01/rdf-schema#"))
                 && (!predicate.contains("http://www.w3.org/2002/07/owl#"))
                 && (!predicate.contains("http://www.w3.org/1999/02/22-rdf-syntax-ns#")))
            return "(# % [EDGE ?P]: { ?P *<http://www.w3.org/2000/01/rdf-schema#subPropertyOf> " + predicate+" . } % ) ";
        else return predicate;
    }
    
    /* [28.1]  */
    /*public Graph PathTriples(){
        if(Debug)  System.out.println(" we are at PathTriples");
        PathTriplesSameSubject();
        
        
        while (LA(1).type.equals("DOT")) {
            match("DOT");
            if ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) || (LA(1).type.equals("COLON"))                
            ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
               || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
               || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
               || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
                (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
                 (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                    (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
                ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET"))))
            PathTriplesSameSubject();            
        }        
        return myGraph;
    } end PathTriples */
   
    
    /* [36]  */
    public String Verb(){
        if(Debug)  System.out.println(" we are at Verb");
        if (LA(1).text.equals("a")){  /* .type == rdf:type ?   */
           match("rdf:type");
           /* resolve macros a */
           String s1="<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";    /*"rdf:type";*/
           predicate=s1;
           String s2= LA(0).type;
           int ty = LA(0).typeIndex;
           Token t = new Token(s1,s2, ty, 0);
           tokenBuffer1[currentToken1]= new Token(t);
            
        }
        else {
            lastStartPredicate=currentToken+1;  /* this is for keeping common predicate (solving macros)*/
            VarOrBlankNodeOrIRIref();
            lastEndPredicate=currentToken;/* this is for keeping common predicate (solving macros)  */
            predicate=LA(0).text;           
        }   
        return predicate;
    }/* end Verb */
        
    
    /* [36.1]  */
    public String PathVerb(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathVerb");
        //lastStartPredicate=currentToken+1;  /* this is for keeping common predicate (solving macros)*/
        //System.out.println(" we are at PathVerb()"+ LA(1).text);
        if (LA(1).type.equals("VAR3")){
            match(LA(1).type);
            return LA(0).text;
        }
        else if ((LA(1).type.equals("LBRACE")))
                return ConstrainedRegularExpression(namedGraph,namedGraphIndex);
        else return RegularExpression(namedGraph,namedGraphIndex);
        //lastEndPredicate=currentToken;/* this is for keeping common predicate (solving macros)  */
        /* this is for taking the predicate   */
        //predicate ="";
        //for(int j=lastStartPredicate;j<=lastEndPredicate;j++)
          //      predicate+=tokenBuffer[j].text;
        //return predicate;
    }/* end PathVerb */
    
    
    /* [37]  */
    public String TriplesNode(){
        if(Debug)  System.out.println(" we are at TriplesNode");
        String node="";
        if (LA(1).type.equals("LPAREN"))
            node=Collection();
        else node=BlankNodePropertyList();            
        return node;
    }/* end TriplesNode */
    
    
    /* [37.1]  */
    public String PathTriplesNode(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathTriplesNode");
        String node="";
        if (LA(1).type.equals("LPAREN"))
            node=PathCollection(namedGraph,namedGraphIndex);
        else node=PathBlankNodePropertyList(namedGraph,namedGraphIndex);            
        return node;
    }/* end PathTriplesNode */
    
    /* [38]  */
    public String BlankNodePropertyList(){
        if(Debug)  System.out.println(" we are at BlankNodePropertyList");
        match("LBRACKET");
        /* resolve macros BlankNode */
        String s= createBlankNode();
        PropertyListNotEmpty(s);
        match("RBRACKET");
        return s;
    }/* end BlankNodePropertyList */
   
     
    /* [38.1]  */
    public String PathBlankNodePropertyList(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathBlankNodePropertyList");
        match("LBRACKET");
        /* resolve macros BlankNode */
        String s= createBlankNode();
        PathPropertyListNotEmpty(s,namedGraph,namedGraphIndex);
        match("RBRACKET");
        return s;
    }/* end PathBlankNodePropertyList */
    
   
    /* [39]  */
    public String Collection(){
        if(Debug)  System.out.println(" we are at Collection");
        match("LPAREN");
        /* resolve macros Collection */
        String head= createBlankNode();
        String node=new String(head);
        
        String n="";
        n=GraphNode();
        addTripleToTemplate(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>",n);
        String newNode="";
        while ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) 
            ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
           || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
           || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
           || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
            (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
             (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
            ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET")))){
            
            n=GraphNode();
            newNode=createBlankNode();
            addTripleToTemplate(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>",newNode);                      
            addTripleToTemplate(newNode,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>",n);                                      
            node=newNode;
        }
        addTripleToTemplate(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>","<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>");                      
        match("RPAREN");        
        return head;
    }/* end Collection */
    
    
    /* [39.1]  */
    public String PathCollection(boolean namedGraph,String namedGraphIndex){
        
        if(Debug)  System.out.println(" we are at PathCollection");
        match("LPAREN");
        /* resolve macros Collection */
        String head= createBlankNode();
        String node=new String(head);
        String n="";
        n=PathGraphNode(namedGraph,namedGraphIndex);
        addTripleToGraph(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>",n);
        String newNode="";
        while ((LA(1).type.equals("VAR2")) ||(LA(1).type.equals("VAR1")) 
            ||(LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))
           || (LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2")) 
           || (LA(1).type.equals("STRING_LITERAL_LONG1")) ||(LA(1).type.equals("STRING_LITERAL_LONG2"))
           || (LA(1).type.equals("PLUS")) ||(LA(1).type.equals("MINUS")) ||(LA(1).type.equals("INTEGER")) ||
            (LA(1).type.equals("DECIMAL")) ||(LA(1).type.equals("DOUBLE")) ||(LA(1).type.equals("TRUE")) ||
             (LA(1).type.equals("FALSE")) ||(LA(1).type.equals("Q_IRI_REF")) ||   
                (LA(1).type.equals("BLANK_NODE_LABEL")) || (LA(1).type.equals("ANON")) || (LA(1).type.equals("NIL")) ||
            ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET")))){
            
            n=PathGraphNode(namedGraph,namedGraphIndex);
            newNode=createBlankNode();
            addTripleToGraph(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>",newNode);                      
            addTripleToGraph(newNode,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>",n);                                      
            node=newNode;
        }
        addTripleToGraph(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>","<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>");                      
        match("RPAREN");        
        return head;
    }/* end PathCollection */
    
    
    /* [40]  */
    public String GraphNode(){
        if(Debug)  System.out.println(" we are at GraphNode");
        String node="";
        if ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET")))
            node=TriplesNode();
        else{
            //int start,end;
            //start=currentToken+1;   /* this field is used for pointing to the begining of the object  */
            node=VarOrTerm();
            //end=currentToken;     /*  this field is used for pointing to the end of the object  */
            /*for(int j=start; j<=end;j++)
                node +=tokenBuffer[j].text;            */
        }
        return node;
    }/* end GraphNode */
    
    
    /* [40.1]  */
    public String PathGraphNode(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at PathGraphNode");
        String node="";
        if ((LA(1).type.equals("LPAREN")) || (LA(1).type.equals("LBRACKET")))
            node=PathTriplesNode(namedGraph,namedGraphIndex);
        else{
            //int start,end;
            //start=currentToken+1;   /* this field is used for pointing to the begining of the object  */
            node=VarOrTerm();
            //end=currentToken;     /*  this field is used for pointing to the end of the object  */
            //for(int j=start; j<=end;j++)
              //  node +=tokenBuffer[j].text;            
        }
        return node;
    }/* end PathGraphNode */
    
        
        
    /* [41]  */
    public String VarOrTerm(){
        if(Debug)  System.out.println(" we are at VarOrTerm");
        if ((LA(1).type.equals("VAR1")) || (LA(1).type.equals("VAR2")))
            return Var();
        else
            return GraphTerm();
        // return LA(0).text;
    }/* end VarOrTerm */
    
    
    /* [42]  */
    public String VarOrIRIref(){
        if(Debug)  System.out.println(" we are at VarOrIRIref");
        if ((LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS")))
            return IRIref();
        else
            return Var();
    }/* end VarOrIRIref */
    
    /* [41.1]  */
    public String RegularExpression(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at RegularExpression");
        String regExp = new String("");
        regExp += Rexp(namedGraph,namedGraphIndex);
        /*System.out.println(" ............ Rex() ");*/
       while ((LA(1).type.equals("DOT")) || (LA(1).type.equals("OR"))){
            regExp +=LA(1).text;
            match(LA(1).type);
            regExp += Rexp(namedGraph,namedGraphIndex);            
       }
        //System.out.println(" ............ RegularExpression() :"+regExp);
        return regExp;
    }  /* end RegularExpression */
        
    
    public boolean findConstraint(Vector pathCons, String name){
        //PathConstraint pathC;
        boolean found = false;
        int index =0;
        while ((index < pathCons.size()) && !found){
            pathC = (PathConstraint)pathCons.get(index);
            if(pathC.getCName().equals(name))
                found = true;
            index++;
        }
        return found;
    }
    
        /* [41.11]  */
    public String ConstrainedRegularExpression(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at ConstrainedRegularExpression");
        String cRegExp =new String(""),transformedcRegExp="";
        cRegExp += LA(1).text+'.';
        transformedcRegExp += LA(1).text;
        match("LBRACE");
        String exp1= Rexp(namedGraph,namedGraphIndex);
        cRegExp += exp1;
        transformedcRegExp += exp1;
        /*System.out.println(" ............ Rex() ");*/
       
        while ((LA(1).type.equals("DOT")) || (LA(1).type.equals("OR"))){
            cRegExp +=LA(1).text;
            transformedcRegExp += LA(1).text;
            match(LA(1).type);
            String exp2 = Rexp(namedGraph,namedGraphIndex);            
            cRegExp += exp2;
            transformedcRegExp += exp2;
       }
        cRegExp += '.'+LA(1).text;
        transformedcRegExp += LA(1).text;
        match("RBRACE");
        int beginConstraintedRE=currentToken+1;  /*  here is the begining of the constraited R. E.  */
        if (LA(1).type.equals("PERCENTAGE")){//BEGIN_RE_CONSTRAINT
            char c = (char)cREMappings.size();
            //System.out.println(" ............ Rex() "+cREMappings.size()+":"+c+":");
            //cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
            boolean foundConstraint = true;        
            match("PERCENTAGE");//BEGIN_RE_CONSTRAINT
            if (LA(1).type.equals("NCNAME") || LA(1).type.equals("NCNAME_PREFIX")){
                String name = LA(1).text;
                match(LA(1).type);
                if (findConstraint(pathConstraints,name)){
                   cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                   cRegExp += pathC.getCNum()+pathC.getCDelVarType(); 
                }
                else {
                    cRegExp += "&<empty&>";
                    foundConstraint =false;
                }
                //String type = constraintType(LA(2).type, LA(2).text);
                //cRegExp +=LA(1).text+type+LA(3).text+LA(4).text;
            }
            else if (LA(1).type.equals("DISTINCT")){
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c+LA(1).text;
                match("DISTINCT");
            }
            else if (LA(1).type.equals("LENGTH")){
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c+LA(1).text;
                match("LENGTH");
                cRegExp +=LA(1).text;
                match("INTEGER");
            }
            else if (LA(1).type.equals("NLENGTH")){
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c+LA(1).text;
                match("NLENGTH");
                cRegExp +=LA(1).text;
                match("INTEGER");
            }
            else{
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c;
                if (LA(1).type.equals("SUM")){
                    cRegExp +="sum";
                    match("SUM");
                    cRegExp +="(";
                    match("LPAREN");
                    cRegExp +=LA(1).text+" ";
                    Var();
                    if (LA(1).type.equals("COMMA")){
                        cRegExp +=",";
                        match("COMMA");
                        cRegExp +=LA(1).text;
                        NumericLiteral();
                    }
                    cRegExp +=")";
                    match("RPAREN");
                }
                else if (LA(1).type.equals("AVG")){
                    cRegExp +="avg";
                    match("AVG");
                    cRegExp +="(";
                    match("LPAREN");
                    cRegExp +=LA(1).text+" ";
                    Var();
                    if (LA(1).type.equals("COMMA")){
                        cRegExp +=",";
                        match("COMMA");
                        if (LA(1).type.equals("INTEGER")){
                            cRegExp +=LA(1).text;
                            match("INTEGER");
                        }
                        else if (LA(1).type.equals("DECIMAL")){
                            cRegExp +=LA(1).text;
                            match("DECIMAL");
                        }
                        else {
                            cRegExp +=LA(1).text;
                            match("DOUBLE");
                        }
                    }
                    cRegExp +=")";
                    match("RPAREN");
                }
                else if (LA(1).type.equals("COUNT")){
                    cRegExp +="count";
                    match("COUNT");
                    cRegExp +="(";
                    match("LPAREN");
                    cRegExp +=LA(1).text+" ";
                    Var();
                    if (LA(1).type.equals("COMMA")){
                        cRegExp +=",";
                        match("COMMA");
                        cRegExp +=LA(1).text;
                        match("INTEGER");
                    }
                    /*if (LA(1).type.equals("INTEGER")){
                        cRegExp +=LA(1).text;
                        match("INTEGER");
                    }*/
                    cRegExp +=")";
                    match("RPAREN");
                }
                String type = constraintType(LA(2).type, LA(2).text);
                cRegExp +=LA(1).text+type+LA(3).text+LA(4).text;
                cREMappings.add(RegularExpressionConstraint(namedGraph,namedGraphIndex));
            }
            if (foundConstraint)
                cRegExp +="&>";//LA(1).text;
            match("PERCENTAGE");//END_RE_CONSTRAINT
            int endConstraintedRE=currentToken;  /*  here is the begining of the constraited R. E.  */
            for (int j=beginConstraintedRE;j<=endConstraintedRE;j++)
                transformedcRegExp+= tokenBuffer[j].text+" ";
        
        }
        while ((LA(1).type.equals("DOT")) || (LA(1).type.equals("OR"))){
            cRegExp +=LA(1).text;
            transformedcRegExp+= LA(1).text;
            match(LA(1).type);
            String exp3 = Rexp(namedGraph,namedGraphIndex);            
            cRegExp += exp3;
            transformedcRegExp += exp3;
       }
        //System.out.println(" ............ ConstrainedRegularExpression() :"+cRegExp+" \n"+ cREMappings.size());
        if (transformQuery)
            return transformedcRegExp;
        else return cRegExp;
    }/* end ConstrainedRegularExpression */
   
    public String constraintType(String type,String text){
        //System.out.println(" ............ constraintType() :"+type+"  "+text);
        String returnedText = new String("");
        if (type.equals("INTEGER")){
                Integer value = new Integer(-1); value = value.valueOf(text);
                int v = value;
                char ch =(char) v;
                //System.out.println(" ............ ConstrainedRegularExpression() :"+ch+"  "+value+"  "+LA(2).text);
                returnedText += ch;
                return returnedText;
            }
        else if (type.equals("EVEN"))
            return "V";
        else if (type.equals("ODD"))
                return "ODD";
        else if (type.equals("ALL"))
                return "ALL";
        else if (type.equals("EXISTS"))
                return "EXISTS";
        else 
            return  text;
           
            
        
            /*if (!LA(4).type.equals("COLON"))
                cRegExp +=LA(4).text;
            else cRegExp +="ALL";
            cRegExp +=LA(2).text+LA(3).text;
            if (!LA(4).type.equals("COLON"))
                cRegExp +=LA(4).text;*/
            
    }
    
     /* [41.12]  */
    public Vector RegularExpressionConstraint(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at RegularExpressionConstraint");
        /*System.out.println(" ............  ");*/
        //transformedQuery+= LA(1).text+" ";
        if (LA(1).type.equals("LBRACKET"))
                match("LBRACKET");
        else match("RBRACKET");
       //transformedQuery+= LA(1).text+" ";
       if ((LA(1).type.equals("ALL")) || (LA(1).type.equals("EXISTS"))|| (LA(1).type.equals("EDGE")))
                match(LA(1).type);
       else if (LA(1).type.equals("INTEGER"))
           match("INTEGER");
       else if (LA(1).type.equals("EVEN"))
           match("EVEN");
       else 
           match("ODD");
        //transformedQuery+= LA(1).text+" ";
        Var();
        //transformedQuery+= LA(1).text+" ";
        if (LA(1).type.equals("LBRACKET"))
                match("LBRACKET");
        else match("RBRACKET");
        //transformedQuery+= LA(1).text+" ";
        match("COLON");
        Vector tempMappings = new Vector();
        rEConstraintGraph = new Graph();
        rEConstraintIndicator = true;
        Vector constraintMappings = new Vector(GraphPattern(tempMappings,namedGraph,namedGraphIndex));
        rEConstraintIndicator = false;
        if (Debug) 
            System.out.println(" we are at RegularExpressionConstraint"+constraintMappings.size());
        return constraintMappings;
    }/* end RegularExpressionConstraint */
    
     /* [41.2]  */
    public String Rexp(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at Rexp");
        String rexp = new String("");
        if ((LA(1).type.equals("STAR")) || (LA(1).type.equals("PLUS"))){
            rexp = LA(1).text;
            match(LA(1).type);
        }
        rexp +=Atom(namedGraph,namedGraphIndex);
        return rexp;
    }/* end Rexp */
   
        /* [41.11']  */
    public String ConstrainedRegularExpressionWithoutBrace(boolean namedGraph,String namedGraphIndex){
        if(Debug)  
            System.out.println(" we are at ConstrainedRegularExpressionWithoutBrace");
        String cRegExp =new String(""),transformedcRegExp="";
        int beginConstraintedRE=currentToken+1;  /*  here is the begining of the constraited R. E.  */
        if (LA(1).type.equals("PERCENTAGE")){//BEGIN_RE_CONSTRAINT
            char c = (char)cREMappings.size();
            //cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
            boolean foundConstraint = true;        
            match("PERCENTAGE");//BEGIN_RE_CONSTRAINT
            if (LA(1).type.equals("NCNAME") || LA(1).type.equals("NCNAME_PREFIX")){
                String name = LA(1).text;
                match(LA(1).type);
                if (findConstraint(pathConstraints,name)){
                   cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                   cRegExp += pathC.getCNum()+pathC.getCDelVarType(); 
                }
                else {
                    cRegExp += "&<empty&>";
                    foundConstraint =false;
                }
                //String type = constraintType(LA(2).type, LA(2).text);
                //cRegExp +=LA(1).text+type+LA(3).text+LA(4).text;
            }
            else if (LA(1).type.equals("DISTINCT")){
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c+LA(1).text;
                match("DISTINCT");
            }
            else if (LA(1).type.equals("LENGTH")){
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c+LA(1).text;
                match("LENGTH");
                cRegExp +=LA(1).text;
                match("INTEGER");
            }
            else if (LA(1).type.equals("NLENGTH")){
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c+LA(1).text;
                match("NLENGTH");
                cRegExp +=LA(1).text;
                match("INTEGER");
            }
            else{
                cRegExp +="&<"; //LA(1).text; // FAISAL: see this statement
                cRegExp +=c;
                if (LA(1).type.equals("SUM")){
                    cRegExp +="sum";
                    match("SUM");
                    cRegExp +="(";
                    match("LPAREN");
                    cRegExp +=LA(1).text+" ";
                    Var();
                    if (LA(1).type.equals("COMMA")){
                        cRegExp +=",";
                        match("COMMA");
                        cRegExp +=LA(1).text;
                        NumericLiteral();
                    }
                    cRegExp +=")";
                    match("RPAREN");
                }
                else if (LA(1).type.equals("AVG")){
                    cRegExp +="avg";
                    match("AVG");
                    cRegExp +="(";
                    match("LPAREN");
                    cRegExp +=LA(1).text+" ";
                    Var();
                    if (LA(1).type.equals("COMMA")){
                        cRegExp +=",";
                        match("COMMA");
                        if (LA(1).type.equals("INTEGER")){
                            cRegExp +=LA(1).text;
                            match("INTEGER");
                        }
                        else if (LA(1).type.equals("DECIMAL")){
                            cRegExp +=LA(1).text;
                            match("DECIMAL");
                        }
                        else {
                            cRegExp +=LA(1).text;
                            match("DOUBLE");
                        }
                    }
                    cRegExp +=")";
                    match("RPAREN");
                }
                else if (LA(1).type.equals("COUNT")){
                    cRegExp +="count";
                    match("COUNT");
                    cRegExp +="(";
                    match("LPAREN");
                    cRegExp +=LA(1).text+" ";
                    Var();
                    if (LA(1).type.equals("COMMA")){
                        cRegExp +=",";
                        match("COMMA");
                        cRegExp +=LA(1).text;
                        match("INTEGER");
                    }
                    /*if (LA(1).type.equals("INTEGER")){
                        cRegExp +=LA(1).text;
                        match("INTEGER");
                    }*/
                    cRegExp +=")";
                    match("RPAREN");
                }
                String type = constraintType(LA(2).type, LA(2).text);
                cRegExp +=LA(1).text+type+LA(3).text+LA(4).text;
                //System.out.println(" calculating results");
                cREMappings.add(RegularExpressionConstraint(namedGraph,namedGraphIndex));
            }
            if (foundConstraint)
                cRegExp +="&>";//LA(1).text;
            match("PERCENTAGE");//END_RE_CONSTRAINT
            int endConstraintedRE=currentToken;  /*  here is the begining of the constraited R. E.  */
            for (int j=beginConstraintedRE;j<=endConstraintedRE;j++)
                transformedcRegExp+= tokenBuffer[j].text+" ";
        
        }
        if (transformQuery)
            return transformedcRegExp;
        else return cRegExp;
    }/* end ConstrainedRegularExpressionWithoutBrace */
   
    
     /* [41.3]  */
    public String Atom(boolean namedGraph,String namedGraphIndex){
        if(Debug)  System.out.println(" we are at Atom");
        String atom =  new String("");
        if ((LA(1).type.equals("LPAREN"))){
            atom+= LA(1).text;
            match(LA(1).type);
            if ((LA(1).type.equals("LBRACE")))
                atom +=ConstrainedRegularExpression(namedGraph,namedGraphIndex);
            else atom +=RegularExpression(namedGraph,namedGraphIndex);
            atom+= LA(1).text;
            match("RPAREN");
       }
        else if ((LA(1).type.equals("LBRACE"))){
            //atom+= LA(1).text;
            //match(LA(1).type);
            atom +=ConstrainedRegularExpression(namedGraph,namedGraphIndex);
            //atom+= LA(1).text;
           // match("RPAREN");
       }
        else if ((LA(1).type.equals("BANG"))){
            atom+= LA(1).text;
            match("BANG");
            atom +=IRIref();
            if (transformQuery)
                atom = "(# % [EDGE ?P]: { ?P *( ! <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ) " + predicate+" . } % ) ";
       
        }
        else if ((LA(1).type.equals("MINUS"))){
            atom+= LA(1).text;
            match("MINUS"); 
            queryContainInverse = true;
            //System.out.println("we are at atom"+ queryContainInverse);
            atom +=IRIref();
        }
        else  if (LA(1).text.equals("epsilon")){ 
            atom = LA(1).text;
            match("epsilon");
        }
        else if ((LA(1).type.equals("WILDCARD"))){
            atom = LA(1).text;
            match("WILDCARD");
            if (LA(1).type.equals("PERCENTAGE"))
                atom +=ConstrainedRegularExpressionWithoutBrace(namedGraph,namedGraphIndex);
        }
        
        else  if (LA(1).text.equals("a")){  /* .type == rdf:type ?   */
            match("rdf:type");
            /* resolve macros a */
            String s1="<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";    /*"rdf:type";*/
            atom = s1;
            String s2= LA(0).type;
            int ty = LA(0).typeIndex;
            Token t = new Token(s1,s2, ty,0);
            tokenBuffer1[currentToken1]= new Token(t);
            tokenBuffer[currentToken]= new Token(t);
            if (transformQuery){
                boolean transitive = false;
                atom = testThePredicate(atom,transitive);
            }
        
        }
        else {
            atom += VarOrBlankNodeOrIRIref(); 
             if (transformQuery){
                boolean transitive = false;
                if  (LA(-1).type.equals("STAR")|| LA(-1).type.equals("PLUS"))
                    transitive = true;
                 atom = testThePredicate(atom,transitive);
             }
        }
        return atom;
    }/* end Atom */
    
   
    
   
    /* [43]  */
    public String VarOrBlankNodeOrIRIref(){
        if(Debug)  System.out.println(" we are at VarORBlankNodeOrIRIref");
        if ((LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))|| (LA(1).type.equals("COLON")))
            return IRIref();
        else if (LA(1).type.equals("BLANK_NODE_LABEL") || (LA(1).type.equals("ANON")))
            return BlankNode();
        else
            return Var();
    }/* end VarOrBlankNodeOrIRIref */
    
    
    /* [44]  */
    public String Var(){
        if(Debug)  System.out.println(" we are at Var");
        if (LA(1).type.equals("VAR1")) 
            match("VAR1");
        else match("VAR2");
        return LA(0).text;
    }/* end Var */
    
        
    
    /* [45]  */
    public String GraphTerm(){
        if(Debug)  System.out.println(" we are at GraphTerm");
        String base1=new String("");
        if ((LA(1).type.equals("Q_IRI_REF")) || (LA(1).type.equals("QNAME")) || (LA(1).type.equals("QNAME_NS"))||(LA(1).type.equals("COLON")))
            return IRIref();
        else if ((LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2"))
                  || (LA(1).type.equals("STRING_LITERAL_LONG1")) || (LA(1).type.equals("STRING_LITERAL_LONG2")))
            return RDFLiteral();
        else if (LA(1).type.equals("BLANK_NODE_LABEL") || (LA(1).type.equals("ANON")))
            return BlankNode();
        else if (LA(1).type.equals("TRUE")|| (LA(1).type.equals("FALSE")))
            return BooleanLiteral();
       
        else if (LA(1).type.equals("NIL")){
            match("NIL");
            /* resolve macros NIL collection */
            String s1="<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>";    /*"rdf:nil";*/
            subject=s1;
            String s2= LA(0).type;
            int ty = LA(0).typeIndex;
            Token t = new Token(s1,s2, ty,0);
            tokenBuffer[currentToken]= new Token(t);
            
        }
        
        else if(LA(1).type.equals("PLUS") || (LA(1).type.equals("MINUS")) 
                || (LA(1).type.equals("INTEGER"))|| (LA(1).type.equals("DECIMAL"))
                || (LA(1).type.equals("DOUBLE"))) {
            String op="";
            if (LA(1).type.equals("PLUS") || (LA(1).type.equals("MINUS"))){
                  match(LA(1).type);
                  op=LA(0).text;
            }
             return "\"\"\""+op+NumericLiteral();
        }
        else match("GraphTerm");
         return LA(0).text;
    }/* end GraphTerm */
    
    
    /* [46]  */
    public void Expression(){
        if(Debug)  System.out.println(" we are at Expression");
        ConditionalOrExpression();
    }/* end Expression */
    
    
    /* [47]  */
    public void ConditionalOrExpression(){
        if(Debug)  System.out.println(" we are at CondionalOrEXpression");
        ConditionalAndExpression();
        while (LA(1).type.equals("SC_OR")){
            match("SC_OR");
            ConditionalAndExpression();
        }
    }/* end ConditionalOrExpression */
    
    /* [48]  */
    public void ConditionalAndExpression(){
        if(Debug)  System.out.println(" we are at ConditionalAndExpression");
        ValueLogical();
        while (LA(1).type.equals("SC_AND")){
            match("SC_AND");
            ValueLogical();
        }
    }/* end ConditionalAndExpression */
    
    /* [49]  */
    public void ValueLogical(){
        if(Debug)  System.out.println(" we are at ValueLogical");
        RelationalExpression();
    }/* end ValueLogical */
    
    
    /* [50]  */
    public void RelationalExpression(){
        if(Debug)  System.out.println(" we are at RelationalExpression");
        NumericExpression();
        if ((LA(1).type.equals("EQ"))||(LA(1).type.equals("NE"))||(LA(1).type.equals("GT"))
               ||(LA(1).type.equals("LT")) ||(LA(1).type.equals("LE")) ||(LA(1).type.equals("GE"))){
            match(LA(1).type);
            NumericExpression();
        }
    }/* end RelationalExpression */
    
    /* [51]  */
    public void NumericExpression(){
        if(Debug)  System.out.println(" we are at NumericExpression");
        AdditiveExpression();
    }/* end NumericExpression */
    
    /* [52]  */
    public void AdditiveExpression(){
        if(Debug)  System.out.println(" we are at AdditiveExpression");
        MultiplicativeExpression();
        while ((LA(1).type.equals("MINUS"))||(LA(1).type.equals("PLUS"))){
            match(LA(1).type);
            MultiplicativeExpression();
        }
    }/* end AdditiveExpression */
    
    
    /* [53]  */
    public void MultiplicativeExpression(){
        if(Debug)  System.out.println(" we are at MultiplicativeExpression");
        UnaryExpression();
        while ((LA(1).type.equals("SLASH"))||(LA(1).type.equals("STAR"))){
            match(LA(1).type);
            UnaryExpression();
        }
    }/* end MultiplicativeExpression */
    
    /* [54]  */
    public void UnaryExpression(){
        if(Debug)  System.out.println(" we are at UnaryExpression");
        if (LA(1).type.equals("BANG")){
            match("BANG");
            PrimaryExpression();
        }
        else if (LA(1).type.equals("PLUS")){
            match("PLUS");
            PrimaryExpression();
        }
        else if (LA(1).type.equals("MINUS")){
            match("MINUS");
            PrimaryExpression();
        }
        else PrimaryExpression();
    }/* end UnaryExpression */
    
    /* [55]  */
    public void PrimaryExpression(){
        if(Debug)  System.out.println(" we are at PrimaryExpression");
        if (LA(1).type.equals("LPAREN"))
            BrackettedExpression();
        else if ((LA(1).type.equals("STR")) || (LA(1).type.equals("LANG")) || (LA(1).type.equals("LANGMATCHES"))
              || (LA(1).type.equals("DATATYPE")) || (LA(1).type.equals("BOUND"))|| (LA(1).type.equals("ISIRI"))
              || (LA(1).type.equals("ISURI")) || (LA(1).type.equals("ISBLANK")) || (LA(1).type.equals("ISLITERAL"))
              || (LA(1).type.equals("REGEX"))|| (LA(1).type.equals("SUM"))|| (LA(1).type.equals("AVG"))
              || (LA(1).type.equals("COUNT")))
            BuiltInCall();
        else if (LA(1).type.equals("VAR1")|| (LA(1).type.equals("VAR2")))
            Var();
        else if (LA(1).type.equals("TRUE")|| (LA(1).type.equals("FALSE")))
            BooleanLiteral();
        else if (LA(1).type.equals("BLANK_NODE_LABEL") || (LA(1).type.equals("ANON")))
            BlankNode();
        else if (LA(1).type.equals("INTEGER") || (LA(1).type.equals("DECIMAL")) || (LA(1).type.equals("DOUBLE")))
            NumericLiteral();
        else if ((LA(1).type.equals("STRING_LITERAL1")) || (LA(1).type.equals("STRING_LITERAL2"))
                  || (LA(1).type.equals("STRING_LITERAL_LONG1")) || (LA(1).type.equals("STRING_LITERAL_LONG2")))
            RDFLiteral();
        else IRIrefOrFunction();
    }/* end PrimaryExpression */
    
    
    /* [56]  */
    public void BrackettedExpression(){
        if(Debug)  System.out.println(" we are at BrackettedExpression");
        match("LPAREN");
        Expression();
        match("RPAREN");
    }/* end BrackettedExpressuion */
    
           
    /* [57]  */
    public void BuiltInCall(){
        if(Debug)  System.out.println(" we are at BuiltInCall");
        if (LA(1).type.equals("STR")){
            match("STR");
            match("LPAREN");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("LANG")){
            match("LANG");
            match("LPAREN");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("LANGMATCHES")){
            match("LANGMATCHES");
            match("LPAREN");
            Expression();
            match("COMMA");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("DATATYPE")){
            match("DATATYPE");
            match("LPAREN");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("BOUND")){
            match("BOUND");
            match("LPAREN");
            Var();
            match("RPAREN");
        }
        else if(LA(1).type.equals("ISURI")){
            match("ISURI");
            match("LPAREN");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("ISIRI")){
            match("ISIRI");
            match("LPAREN");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("ISBLANK")){
            match("ISBLANK");
            match("LPAREN");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("ISLITERAL")){
            match("ISLITERAL");
            match("LPAREN");
            Expression();
            match("RPAREN");
        }
        else if(LA(1).type.equals("SUM")){
            match("SUM");
            match("LPAREN");
            Var();
            match("RPAREN");
        }
        else if(LA(1).type.equals("AVG")){
            match("AVG");
            match("LPAREN");
            Var();
            match("RPAREN");
        }
        else if(LA(1).type.equals("COUNT")){
            match("COUNT");
            match("LPAREN");
            Var();
            match("RPAREN");
        }
        else RegexExpression();
    }/* end BuiltInCall */
    
    
    
    /* [58]  */
    public void RegexExpression(){
        if(Debug)  System.out.println(" we are at RegexExpression");
        match("REGEX");
        match("LPAREN");
        Expression();
        match("COMMA");
        Expression();
        if (LA(1).type.equals("COMMA")){
            match("COMMA");
            Expression();
        }
        match("RPAREN");
    }/* end RegexExpression */
    
    
    
    
    /* [59]  */
    public void IRIrefOrFunction(){
        if(Debug)  System.out.println(" we are at IRIrefOrFunction");
        IRIref();
        if ((LA(1).type.equals("NIL")) || (LA(1).type.equals("LPAREN")))
            ArgList();
    }/* end IRIrefOrFunction */
   
    
    
    /* [60]  */
    public String RDFLiteral(){
        if(Debug)  System.out.println(" we are at RDFLiteral");
        String l;        
        l=string();
        int i=1;
        if (LA(1).type.equals("LANGTAG")){
            l+=LA(1).text;
            match("LANGTAG");
            i=2;
        }
        else if (LA(1).type.equals("DTYPE")){
            match("DTYPE");
            i=3;
            l+='^';
            l+='^';
            l+=IRIref();
        }
        if ((i==1)){// && (!l.contains("^^"))){
            l+="^^";
            //l+=type(l.substring(3, l.indexOf("\"\"\"", 3)));
            l+="<http://www.w3.org/2001/XMLSchema#string>"; //last modified 10/10/2006 ==> l+=type(l.substring(3, l.indexOf("\"\"\"", 3)));
        }
        if(Debug) System.out.println(" we are at RDFLiteral:"+l);
         return l;
    }/* end RDFLiteral */
    
    
    public String type(String str){
        if (Debug) System.out.println("we exit at type():"+str);
        String t="";
        NumericType nt=new NumericType(str);
        if (str.equals("true")||
                    str.equals("false"))
                t="<http://www.w3.org/2001/XMLSchema#boolean>";
        else if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#integer>")){ 
            t="<http://www.w3.org/2001/XMLSchema#integer>";
        }
        else if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#decimal>")){
            t="<http://www.w3.org/2001/XMLSchema#decimal>";
        }
        else if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#double>")){
            t="<http://www.w3.org/2001/XMLSchema#double>";
        }
        else
            t="<http://www.w3.org/2001/XMLSchema#string>";
        return t;
    }

        /* [61]  */
    public String NumericLiteral(){
        if(Debug)  System.out.println(" we are at NumericLiteral");
        String type=new String("");
        if (LA(1).type.equals("INTEGER")){
            match("INTEGER");
            type="^^<http://www.w3.org/2001/XMLSchema#integer>";
        }
        else if (LA(1).type.equals("DECIMAL")){
            match("DECIMAL");
            type="^^<http://www.w3.org/2001/XMLSchema#decimal>";
        }
        else if (LA(1).type.equals("DOUBLE")){
            match("DOUBLE");
            type="^^<http://www.w3.org/2001/XMLSchema#double>";
        }
        /*else match("Numericliteral");*/
         return LA(0).text+"\"\"\""+type;
    }/* end NumericLiteral */
    

    /* [62]  */
    public String BooleanLiteral(){
        if(Debug)  System.out.println(" we are at BooleanLiteral");
        if (LA(1).type.equals("TRUE"))
            match("TRUE");
        else if (LA(1).type.equals("FALSE"))
            match("FALSE");
        
        String s2= "\"\"\""+(LA(0).text.toLowerCase()+"\"\"\""+"^^"+"<http://www.w3.org/2001/XMLSchema#boolean>");
        int ty = LA(0).typeIndex;Token t = new Token(s2,LA(0).type, ty,0);
        tokenBuffer[currentToken]= new Token(t);
        
         return s2;
    }/* end BooleanLiteral */
    
    /* [63]  */
    public String string(){
        if(Debug)  System.out.println(" we are at string");
        String s=LA(1).text;
        if (LA(1).type.equals("STRING_LITERAL1")){
            match("STRING_LITERAL1");
            s=s.substring(1,s.length()-1);
            s="\"\"\""+s+"\"\"\"";        
        }
        else if (LA(1).type.equals("STRING_LITERAL2")){
            match("STRING_LITERAL2");
             s=s.substring(1,s.length()-1);
            s="\"\"\""+s+"\"\"\"";        
        }
        else if (LA(1).type.equals("STRING_LITERAL_LONG1")){
            match("STRING_LITERAL_LONG1");
             s=s.substring(3,s.length()-3);
            s="\"\"\""+s+"\"\"\"";        
        }
        else if (LA(1).type.equals("STRING_LITERAL_LONG2")){
            match("STRING_LITERAL_LONG2");
            s=LA(0).text;
        }
         return s;
    }/* end string */
    
    /* [64]  */
    public String IRIref(){
        if(Debug)  System.out.println(" we are at IRIref");
        String base1=new String("");                
        if (LA(1).type.equals("Q_IRI_REF")){
            match("Q_IRI_REF");
            
            /* resolve macros base IRI   */
            if (!(query.base.equals(""))){
                String s1=LA(0).text;
                for(int j=0;query.base.charAt(j)!='>';j++)
                     base1+=query.base.charAt(j);
                for(int j=1;s1.charAt(j)!='>';j++)
                     base1+=s1.charAt(j);
                base1+='>';
                String s2= LA(0).type;
                int ty = LA(0).typeIndex;
                Token t = new Token(base1,s2, ty,0);
                tokenBuffer1[currentToken1]= new Token(t);
                tokenBuffer[currentToken]= new Token(t);
            }
            else base1=LA(0).text;
        } 
        else return QName();
        //System.out.println(base1);
        return base1;
    }/* end IRIref */
    
    public int searchPrefixIRI(String ncnameprefix){
        //System.out.println(" we are at searchPrefixIRI:"+ncnameprefix+ query.prefixesNom);
        boolean found=false;
        int prefixesCounter=0;
        int location=-5;
        while((!found) && prefixesCounter<=query.prefixesNom){
            if (query.prefixesNames[prefixesCounter].equals(ncnameprefix)){
                found=true;
                location=prefixesCounter;
            }
            ++prefixesCounter;
        }
        //System.out.println(" we are at saerchPrefixIRI:"+found);
        return location;
    }
    
    /* [65]  */
    public String QName(){
        if(Debug)  System.out.println(" we are at QName");
        String base1=new String("");
        if (LA(1).type.equals("QNAME")){
            match("QNAME");
            
            /* resolve macros prefixes IRIs   */
                String s1=LA(0).text;
                s1+='\0';
                String ncnameprefix =new String("");
                String ncname =new String("");
                int pos =s1.indexOf(":");
                for(int j=0;s1.charAt(j)!=':';j++){
                      ncnameprefix += s1.charAt(j);
                }                
                ncnameprefix +=':';
                for(int j=pos+1;s1.charAt(j)!='\0';j++){
                      ncname += s1.charAt(j);
                }
                ncname +='\0';
                int location =searchPrefixIRI(ncnameprefix);
                /*System.out.println("ncname="+ncname);*/
                if (location>=0){
                     /*System.out.println("location="+location); */
                    int j=0;
                    for(j=0;query.prefixes[location].charAt(j)!='>';j++)
                         base1+=query.prefixes[location].charAt(j);
                     if ((query.prefixes[location].charAt(j-1)!='/') &&
                            ((query.prefixes[location].charAt(j-1)!='#') ))
                         base1+='/';
                    /*System.out.println("BASE="+base1);*/
                    for(j=0;ncname.charAt(j)!='\0';j++)
                         base1+=ncname.charAt(j);
                    base1+='>';
                    String s2= LA(0).type;
                    int ty = LA(0).typeIndex;
                    Token t = new Token(base1,s2, ty,0);
                    tokenBuffer1[currentToken1]= new Token(t);
                    tokenBuffer[currentToken]= new Token(t);                   
                }
                else {
                    nomError++;
                    try{
                        out.write(" ERROR TYPE AT TOKEN:"+(currentToken)+". NOT FOUND PREFIX:"+"\n");
                    }
                    catch (IOException e) { System.out.println(" Exception: "+e);}
                }
        }        
        else if (LA(1).type.equals("QNAME_NS")){
            match("QNAME_NS");
            /* resolve macros prefixes IRIs   */
                String s1=LA(0).text;
                s1+='\0';
                String ncnameprefix =new String("");
                String ncname =new String("");
                int pos =s1.indexOf(":");
                for(int j=0;s1.charAt(j)!=':';j++){
                      ncnameprefix += s1.charAt(j);
                }                
                ncnameprefix +=':';
                for(int j=pos+1;s1.charAt(j)!='\0';j++){
                      ncname += s1.charAt(j);
                }
                ncname +='\0';
                int location =searchPrefixIRI(ncnameprefix);
                /*System.out.println("ncname="+ncname);*/
                if (location>=0){
                     /*System.out.println("location="+location); */
                    int j=0;
                    for(j=0;query.prefixes[location].charAt(j)!='>';j++)
                         base1+=query.prefixes[location].charAt(j);
                     if ((query.prefixes[location].charAt(j-1)!='/') &&
                            ((query.prefixes[location].charAt(j-1)!='#') ))
                         base1+='/';
                    /*System.out.println("BASE="+base1);*/
                    for(j=0;ncname.charAt(j)!='\0';j++)
                         base1+=ncname.charAt(j);
                    base1+='>';
                    String s2= LA(0).type;
                    int ty = LA(0).typeIndex;
                    Token t = new Token(base1,s2, ty,0);
                    tokenBuffer1[currentToken1]= new Token(t);
                    tokenBuffer[currentToken]= new Token(t);                   
                }
                else {
                    nomError++;
                    try{
                        out.write(" ERROR TYPE AT TOKEN:"+(currentToken)+". NOT FOUND PREFIX:"+"\n");
                    }
                    catch (IOException e) { System.out.println(" Exception: "+e);}
                }
        }
        else {
            if (LA(1).type.equals("NCNAME_PREFIX"))
               match("NCNAME_PREFIX");
            match("COLON");
            
            /* resolve macros prefixes IRIs   */
                String s1=LA(0).text;
                s1+='\0';
                String ncnameprefix =new String("");
                String ncname =new String("");
                int pos =s1.indexOf(":");
                for(int j=0;j<pos;j++){  //s1.charAt(j)!=':'
                      ncnameprefix += s1.charAt(j);
                }                
                ncnameprefix +=':';
                for(int j=pos+1;s1.charAt(j)!='\0';j++){
                      ncname += s1.charAt(j);
                }
                ncname +='\0';
                int location =searchPrefixIRI(ncnameprefix);
                /*System.out.println("ncname="+ncname);*/
                if (location>=0){
                     /*System.out.println("location="+location); */
                    int j=0;
                    for(j=0;query.prefixes[location].charAt(j)!='>';j++)
                         base1+=query.prefixes[location].charAt(j);
                     if ((query.prefixes[location].charAt(j-1)!='/') &&
                            ((query.prefixes[location].charAt(j-1)!='#') ))
                         base1+='/';
                        /*System.out.println("BASE="+base1);*/
                        for(j=0;ncname.charAt(j)!='\0';j++)
                             base1+=ncname.charAt(j);
                        base1+='>';
                        String s2= LA(0).type;
                        int ty = LA(0).typeIndex;
                        Token t = new Token(base1,s2, ty,0);
                        tokenBuffer1[currentToken1]= new Token(t);
                        tokenBuffer[currentToken]= new Token(t);                   
                    }
                    else {
                        nomError++;
                        try{
                            out.write(" ERROR TYPE AT TOKEN:"+(currentToken)+". NOT FOUND PREFIX:"+"\n");
                        }
                        catch (IOException e) { System.out.println(" Exception: "+e);}
                    }
                    //end resolve macros
            
            
            if (LA(1).type.equals("NCNAME"))
                match("NCNAME");
        }
        //System.out.println(base1);
        return base1;
    }/* end QName */
    
    /* [66]  */
    public String BlankNode(){
        if(Debug)  System.out.println(" we are at BlankNode");
        String s1;
        if (LA(1).type.equals("BLANK_NODE_LABEL")){
            s1=LA(1).text;
            match("BLANK_NODE_LABEL");
        }
        
        else {
            match("ANON");
            /* resolve macros BlankNode */
            s1=createBlankNode();
            String s2= LA(0).type;
            int ty = LA(0).typeIndex;
            Token t = new Token(s1,s2, ty,0);
            tokenBuffer1[currentToken1]= new Token(t);
            tokenBuffer[currentToken]= new Token(t);
            /*finalTokenBuffer.setToken(t,currentToken);*/
        }
         return s1;
    }/* end BlankNode */
    

    public Vector getMappings(){
        return mappings;
    }
    
    public String getTransformedQuery(){
        return transformedQuery;
    }
     public boolean getQueryContainInverseFlag(){
        return queryContainInverse;
    }
}

