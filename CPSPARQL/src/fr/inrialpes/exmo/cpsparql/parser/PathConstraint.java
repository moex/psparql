/*
 * PathConstraints.java
 *
 * Created on 18 decembre 2007, 16:00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package fr.inrialpes.exmo.cpsparql.parser;

/**
 *
 * @author faisal
 */
public class PathConstraint {
    
    /** Creates a new instance of PathConstraints */
    public PathConstraint(char num, String name, String delTypeVar) {
        cNum = num;
        cName = name;
        //cType = type;
        cDelVarType = delTypeVar;
    }
    
     public char getCNum(){
        return cNum;          
    }
    
    public String getCName(){
        return cName;          
    }
    
    public String getCType(){
        return cType;          
    }
      
    public String getCDelVarType() {
        return cDelVarType;
    }

    private char cNum;
    private String cName;
    private String cType;
    private String cDelVarType;
}
