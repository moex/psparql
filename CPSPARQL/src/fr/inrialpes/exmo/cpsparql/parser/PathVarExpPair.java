/*
 * PathVarExpPair.java
 *
 * Created on August 7, 2007, 3:58 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.parser;

/**
 *
 * @author faisal
 */
public class PathVarExpPair {
    
    /** Creates a new instance of PathVarExpPair */
    public PathVarExpPair(String pathVar, String sNode, String pathExp, String oNode){
        this.pathVar = pathVar;
        this.sNode   = sNode;
        this.pathExp = pathExp;
        this.oNode   = oNode;
    }
    
    public String getPathVar(){
        return pathVar;
    }
 
    public String getSNode(){
        return sNode;
    }
    
    public String getPathExp(){
        return pathExp;
    }
    public String getONode(){
        return oNode;
    }
    
    private String pathVar;
    private String sNode;
    private String pathExp;
    private String oNode;
}
