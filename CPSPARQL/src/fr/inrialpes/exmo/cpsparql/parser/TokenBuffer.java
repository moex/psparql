/*
 * TokenBuffer.java
 *
 * Created on December 28, 2005, 2:23 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.parser;

import java.io.*;
/**
 *
 * @author faisal
 */
public class TokenBuffer {
    File lexerFile;
    FileWriter outlexer;
    
    /** Creates a new instance of TokenBuffer */
    public TokenBuffer(int i) {
        currentIndex=0;
        /*lexerFile = new File("lexicalFile.txt");
        try{
             outlexer = new FileWriter(lexerFile);
        } catch (IOException e)
         {
                   System.out.println(e);
              }*/
        
    }
    public TokenBuffer(int i, int j) {
        currentIndex=0;
        /*lexerFile = new File("lexicalFile.txt");
        try{
             outlexer = new FileWriter(lexerFile);             
             outlexer.write("      Token                               "+"                    Token Type          "+"                                 Token Type Index            Token Index \n");
             outlexer.write("------------------------------------------------------------------------------------------------------------------------------------------------------------------  \n");
             
        } catch (IOException e){System.out.println(e); }*/
        
    }
    /*public  FileWriter getFileWriter(){
        return outlexer;
    }*/
    
    public  Token getToken(){
        return tokenBuffer[currentIndex];
    }
    
    public  Token getToken(int i){
        return tokenBuffer[i];
    }
    
    public  void setToken(Token token){
        /*try{ 
            outlexer.write("  "+token.text+ space(100-token.text.length())+token.type+space(100-token.type.length())+token.typeIndex+"         "+token.getIndex()+"\n");
            //outlexer.write(token.text+"\n");
            //outlexer.write(token.type+"\n");
            //outlexer.write(token.typeIndex+"\n");
         
          } catch (IOException e) { System.out.println(" Exception: "+e);};*/
        tokenBuffer[currentIndex]=new Token(token);
        currentIndex++;         
    }
        
    
    public  void setToken(Token token,int i){
         tokenBuffer[i]= new Token(token);
    }
    public String space(int size){
        String s=new String("");
        for(int i=0;i<=size;i++)
            s+=" ";
        if (size <= 0) s="  ";
        return s;
    }
    
    public void print( ){
        /*System.out.println("length = " + tokenBuffer.length);*/
        System.out.println("      Token                     "+"                    Token Type          "+"                       Token Type Index    Token Index ");
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------   ");
        for(int j=0;j<currentIndex;j++){
            System.out.println(j+"  "+tokenBuffer[j].text+ space(50-tokenBuffer[j].text.length())+tokenBuffer[j].type+space(50-tokenBuffer[j].type.length())+tokenBuffer[j].typeIndex+"         "+tokenBuffer[j].getIndex()+"   ");
        }
    }
    
    private int currentIndex=0;
    private int maxNumber=1000;
    public Token [] tokenBuffer= new Token [maxNumber];
    /*private int typeIndexarray [] = new int [maxNumber];*/
}
