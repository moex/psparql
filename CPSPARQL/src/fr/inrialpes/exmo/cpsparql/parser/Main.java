/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.inrialpes.exmo.cpsparql.parser;

import java.lang.String.*;
import fr.inrialpes.exmo.cpsparql.maininterface.MainFrame;
import fr.inrialpes.exmo.cpsparql.maininterface.PSPARQLEngine; 
import fr.inrialpes.exmo.cpsparql.ontologycreation.RandomRDFGraphConstnWithHardness;
import java.util.Random;

/**
 *
 * @author Faisal
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MainFrame mainframe= new MainFrame();
        //testSweto();
    }

    
    private static void testSweto(){
       System.out.println(" Running ... ");
       long elapsedTimeMillis;
       PSPARQLEngine mainClass= new PSPARQLEngine();
       int sizeMax = 2;
       
       // array for elapsed time in milli seconds at each repeated time 
       long [] elapsedTimes = new long [sizeMax];
       long [] elapsedLoadTimesS = new long [sizeMax];
       long [] elapsedTimesMSFirstS = new long [sizeMax];
       long [] solutionSize = new long [sizeMax];
       for (int c=0; c<sizeMax;c++){
           elapsedTimes[c]=0; 
           elapsedLoadTimesS[c]=0;
           elapsedTimesMSFirstS[c]=0;
           solutionSize[c]=0;
       }
           for (int c=0;c < sizeMax; c++){
              
          //     mainClass = new PSPARQLEngine(" prefix ex: <http://www.inrialpes.fr/>" +
            //           " prefix foaf: <http://xmlns.com/foaf/0.1/> " +
              //         " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                      // "construct {?pub <http://lsdis.cs.uga.edu/projects/semdis/opus#year>  \"2004^^http://www.w3.org/2001/XMLSchema#gYear\" .} " +
                //       " select * " +
                  //     " from <swetodblp/swetodblp-part-1-1.ttl>  " +
                    //   " from <swetodblp/swetodblp-part-1-2.ttl>  " +
                      // " from <swetodblp/swetodblp-part-1-3.ttl>  " +
                      // " from <swetodblp/swetodblp-part-1-4.ttl>  " +
                      // " from <swetodblp/swetodblp-part-1-5.ttl>  " +
                      // " FROM   <testRDFGraphc#1000.ttl> \n" +
                      // " FROM   <testRDFGraphc#50000.ttl> \n" +
                      // " WHERE { " +
                     //  sourceNode + " " + edgeLabel + "  ?o . " +
                       //" CONSTRAINT const1 ] all ?flightType ] : { ?flightType rdf:type ex:train . } " +
                        // "?pub <http://lsdis.cs.uga.edu/projects/semdis/opus#year>  \"2004^^http://www.w3.org/2001/XMLSchema#gYear\" . " +
                       //" ?X  +(  {-ex:from }% const1  %   .  ex:to  )  ?Y   .  " + 
                     //   " <http://dblp.uni-trier.de/rec/bibtex/books/cs/Ullman89> ??path  ?o ."+
                       //" <http://dblp.uni-trier.de/rec/bibtex/books/cs/Ullman89> +(<http://lsdis.cs.uga.edu/projects/semdis/opus#cites> | <http://lsdis.cs.uga.edu/projects/semdis/opus#isIncludedIn>) ?o ."+
                      // " <http://dblp.uni-trier.de/rec/bibtex/books/cs/Ullman89>   +(<http://lsdis.cs.uga.edu/projects/semdis/opus#cites> | <http://lsdis.cs.uga.edu/projects/semdis/opus#isIncludedIn>)| <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> . *<http://www.w3.org/2000/01/rdf-schema#subClassOf> ?o ."+
                     //   " from <rdfsSchemas/univ-bench-300.ttl> " +
                      // "from <TestBeds/University0_0.owl-10000.ttl> " +
                       //" where { " +
                       //" <http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#AssociateProfessor> <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?o ." +
                      // " <http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#headOf> <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?o ." +
                       //" {<http://www.Department0.University0.edu/FullProfessor7> (<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> . *<httpwww.w3.org200001rdf-schema#subClassOf>) ?o  } " +
                     // "}",false);
           elapsedTimes[c] = elapsedTimeMillis = mainClass.getElapsedTimeMillis();
           elapsedTimesMSFirstS[c] = mainClass.getElapsedTimeMSFirstS();
           elapsedLoadTimesS[c] = mainClass.getElapsedLoadTimeS();
           //System.out.println("End running: "+c+"\t"+elapsedTimes[c]+'\t'+elapsedTimesMSFirstS[c]);
           //System.out.println(mainClass.getResult());
            solutionSize[c] = mainClass.getMappings().size();
           }
           //System.out.println(mainClass.getResult());
           // calculating the average elapsed time time
           float averageElapsedTimeMs =0,averageSolutionSize=0,averageElapsedLoadTimeS=0,averageElapsedTimeMsFirstS =0;
           for (int c=0;c<sizeMax;c++){
               averageElapsedTimeMs += elapsedTimes[c];
               averageElapsedLoadTimeS += elapsedLoadTimesS[c];
               averageElapsedTimeMsFirstS += elapsedTimesMSFirstS[c];
               averageSolutionSize+=solutionSize[c];
           }
           averageElapsedTimeMs/=sizeMax;
           averageElapsedLoadTimeS/=sizeMax;
           averageElapsedTimeMsFirstS /= sizeMax;
           averageSolutionSize/=sizeMax;
           System.out.println(averageElapsedTimeMs+"\t"+averageSolutionSize);
       
    }
    
    private static void test(){
       System.out.println(" Running ... ");
       int graphSize=1000, inOutD=1;
       long elapsedTimeMillis;
       PSPARQLEngine mainClass= new PSPARQLEngine();
       int sizeMax = 20;
       
       // array for elapsed time in milli seconds at each repeated time 
       long [] elapsedTimes = new long [sizeMax];
       long [] elapsedLoadTimesS = new long [sizeMax];
       long [] elapsedTimesMSFirstS = new long [sizeMax];
       long [] solutionSize = new long [sizeMax];
       for (int c=0; c<sizeMax;c++){
           elapsedTimes[c]=0; 
           elapsedLoadTimesS[c]=0;
           elapsedTimesMSFirstS[c]=0;
           solutionSize[c]=0;
       }
       //end initialization
       //example.org/ex1
       int nodeSize = 0, index ; String sourceNode, edgeLabel;
       int setting = 1; // this decides the number of nodes
                        // 1: node size fixed
                        // 2: node size in function of in-out distributions
        int edgeSetting = 1; // this decides the labeling for edges 1 or 2
                        // 1: from edge label list with size equal to graph size
                        // 2: edges will be labeled from 4 distict edge labels
       for (int k=1; k<=7; k++){
            for (int in=0; in<sizeMax;in++){
               elapsedTimes[in]=0; 
               elapsedLoadTimesS[in]=0;
               elapsedTimesMSFirstS[in]=0;
               solutionSize[in]=0;
           }
           inOutD *= 2; nodeSize = graphSize/inOutD;
           Random gen = new Random(1545443);
           long seed = gen.nextInt();
           Random generator = new Random();
           generator.setSeed(seed);
           for (int c=0;c < sizeMax; c++){
               seed = gen.nextInt();
               generator = new Random();
               generator.setSeed(seed);
               seed = generator.nextInt();
               RandomRDFGraphConstnWithHardness rRGC = new  RandomRDFGraphConstnWithHardness();
               rRGC.start(seed, graphSize, inOutD,setting);//, edgeSetting
               if (setting==1)
                   index = generator.nextInt(graphSize);
               else
                   index = generator.nextInt(nodeSize);
               sourceNode = rRGC.getNodeAt(index);
               if (edgeSetting==1)
                   edgeLabel = rRGC.getEdgeAt(index);
               else 
                   index = generator.nextInt(4);
               edgeLabel = rRGC.getEdgeAt(index);

           //    mainClass = new PSPARQLEngine(" prefix ex: <http://www.inrialpes.fr/>" +
             //          " prefix foaf: <http://xmlns.com/foaf/0.1/> " +
               //        " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                      // "construct {?pub <http://lsdis.cs.uga.edu/projects/semdis/opus#year>  \"2004^^http://www.w3.org/2001/XMLSchema#gYear\" .} " +
                 //      " select * " +
                       //" from <swetodblp/swetodblp-part-1-1.ttl>  " +
                      // " from <swetodblp/swetodblp-part-1-2.ttl>  " +
                       //" from <swetodblp/swetodblp-part-1-3.ttl>  " +
                       //" from <swetodblp/swetodblp-part-1-4.ttl>  " +
                      // " FROM   <testRDFGraphc#1000.ttl> \n" +
                      // " FROM   <testRDFGraphc#50000.ttl> \n" +
             //          " WHERE { " +
                     //  sourceNode + " " + edgeLabel + "  ?o . " +
                       //" CONSTRAINT const1 ] all ?flightType ] : { ?flightType rdf:type ex:train . } " +
                        // "?pub <http://lsdis.cs.uga.edu/projects/semdis/opus#year>  \"2004^^http://www.w3.org/2001/XMLSchema#gYear\" . " +
                       //" ?X  +(  {-ex:from }% const1  %   .  ex:to  )  ?Y   .  " + 
                      // " <http://dblp.uni-trier.de/rec/bibtex/conf/adbis/MatskinH95> +(<http://lsdis.cs.uga.edu/projects/semdis/opus#cites> | <http://lsdis.cs.uga.edu/projects/semdis/opus#isIncludedIn>) ?o ."+
                      // " <http://dblp.uni-trier.de/rec/bibtex/conf/adbis/MatskinH95> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> . *<http://www.w3.org/2000/01/rdf-schema#subClassOf> ?o ."+
                     //   " from <rdfsSchemas/univ-bench-300.ttl> " +
                      // "from <TestBeds/University0_0.owl-10000.ttl> " +
                       //" where { " +
                       //" <http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#AssociateProfessor> <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?o ." +
                      // " <http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#headOf> <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?o ." +
                       //" {<http://www.Department0.University0.edu/FullProfessor7> (<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> . *<httpwww.w3.org200001rdf-schema#subClassOf>) ?o  } " +
               //       "}",false);
           elapsedTimes[c] = elapsedTimeMillis = mainClass.getElapsedTimeMillis();
           elapsedTimesMSFirstS[c] = mainClass.getElapsedTimeMSFirstS();
           elapsedLoadTimesS[c] = mainClass.getElapsedLoadTimeS();
           //System.out.println("End running: "+c+"\t"+elapsedTimes[c]+'\t'+elapsedTimesMSFirstS[c]);
           //System.out.println(mainClass.getResult());
            solutionSize[c] = mainClass.getMappings().size();
           }
           //System.out.println(mainClass.getResult());
           // calculating the average elapsed time time
           float averageElapsedTimeMs =0,averageSolutionSize=0,averageElapsedLoadTimeS=0,averageElapsedTimeMsFirstS =0;
           for (int c=0;c<sizeMax;c++){
               averageElapsedTimeMs += elapsedTimes[c];
               averageElapsedLoadTimeS += elapsedLoadTimesS[c];
               averageElapsedTimeMsFirstS += elapsedTimesMSFirstS[c];
               averageSolutionSize+=solutionSize[c];
           }
           averageElapsedTimeMs/=sizeMax;
           averageElapsedLoadTimeS/=sizeMax;
           averageElapsedTimeMsFirstS /= sizeMax;
           averageSolutionSize/=sizeMax;
           System.out.println(inOutD+"\t"+averageElapsedTimeMs+"\t"+averageSolutionSize);
       }// end calculation
       
    }
}