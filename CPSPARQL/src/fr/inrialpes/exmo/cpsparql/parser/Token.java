/*
 * Token.java
 *
 * Created on December 25, 2005, 8:10 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.parser;
import java.lang.String.*;
/**
 *
 * @author faisal
 */
public class Token {
    
    /** Creates a new instance of Token */
    public Token(String s, String t, int tI,  int ind) {
        text =  s;
        type=t;
        typeIndex=tI;
        index=ind;
    }
    
    /** Creates a new instance of Token */
    public Token() {
        text =  "";
        type="";
        typeIndex=-1;
        index=-1;
    }
    
    
    public Token(Token t) {
        text = new String(t.text);
        type= new String(t.type);
        typeIndex=t.typeIndex;
        index=t.index;
    }
    
    public void setIndex(int i){
        index=i;
    }
    public int getIndex(){
        return index;
    }
    
    public String text;
    public String type;
    public int typeIndex;
    private int index;
    
}
