/*
 * Triple.java
 *
 * Created on January 3, 2006, 2:52 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;

import java.util.Vector;

/**
 *
 * @author faisal
 */
public class Triple {
    
    
    /** Creates a new instance of Triple */
    public Triple(int s, int p, Vector o,Vector vNodes, Vector constraintDelimiters, String path) {
        this.subject= s;
        this.predicate=p;
        this.object= new Vector(o);
        this.visitedNodes= new Vector(vNodes);
        this.constraintDelimiters = new Vector(constraintDelimiters);
        this.path = new String(path);
    }
    
    
    
    public int getSubject(){
        return subject;
    }
    
    
    
    public int getPredicate(){
        return predicate;
    }
    
    public Vector getobject(){
        return object;
    }
    
    public Vector getVisitedNodes(){
        return visitedNodes;
    }
    
    public Vector getConstraintDelimiters(){
        return constraintDelimiters;
    }
    
    public String getPath(){
        return path;
    }
    

    private int subject;
    private int predicate;
    private Vector object;
    private Vector visitedNodes;
    private Vector constraintDelimiters;
    private String path;
}
