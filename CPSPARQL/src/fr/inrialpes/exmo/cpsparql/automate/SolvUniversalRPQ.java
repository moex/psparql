/*
 * SolvUniversalRPQ.java
 *
 * Created on April 29, 2006, 12:48 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;


import java.util.Hashtable;
import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.*;
import java.io.*;

/**
 *
 * @author faisal
 */
public class SolvUniversalRPQ {
    
    /** Creates a new instance of SolvUniversalRPQ */
    public SolvUniversalRPQ(String X,int node, String E, String Y, Vector lambda, MultiGraph G1,boolean simplePaths,Vector cREMappings) {
        //System.out.println("we are SolvUniveralRPQ():"+X+"  "+"  "+E+"  "+Y+ "   node="+node);
        //System.out.println(E);
        //if(cREMappings.size()==1) System.out.println(currentalreadyConstructedHashT[0]);
        String pathVar = new String("  ");
        if ((E.charAt(0)=='?')&&(E.charAt(1)=='?')){
            pathVar = E.substring(0, E.indexOf(' '));
            E = E.substring(E.indexOf(' '));
        }
        //System.out.println("we are SolvUniveralRPQ():"+X+"  "+"  "+E+"  "+Y+ "   pathVar="+pathVar);
        initialization(E,G1,cREMappings);
            if (node >=0)
            startFindPaths2(node,E,Y,lambda,simplePaths,pathVar);
        else
            startFindPaths(node,X,E,Y,lambda,simplePaths,pathVar);
    }
    
        
    public void startFindPaths(int n,String X,String E, String Y,Vector globalLambda,boolean simplePaths, String pathVar){
        solutions = new Vector();
        pathS = new PathSolution();
        int s0= A.getStartState();
        Node node;
        Vector visited = initializeVisited();
        if ((X.charAt(0)=='?')|| (X.charAt(0)=='$') || (X.charAt(0)=='_')){
               VarImage vi; 
               int location =hasImage(globalLambda,X);
               if (location >= 0){                  
                   vi=(VarImage)globalLambda.get(location);
                   if (simplePaths)
                       findSimplePaths(vi.getIndex(), vi.getIndex(), s0, globalLambda,Y,visited,pathVar);                   
                   else
                       find(vi.getIndex(), vi.getIndex(), s0, globalLambda,Y,pathVar);                   
               }
               else{    
                   for(int k=0;k< G.getNodes().size();k++){
                        Vector tlambda = new Vector(globalLambda);
                        node= G.getNode(k);
                        vi = new VarImage(X,node.getLabel(),k);
                        tlambda.add(vi);
                        //System.out.println(k+"$"+node.getLabel());
                        visited = initializeVisited();;
                        if (simplePaths)
                            findSimplePaths(k, k, s0, tlambda,Y,visited,pathVar);                   
                        else
                            find(k, k, s0, tlambda,Y,pathVar);
                    }

               }
        }
        else{
            //Node node1 = new Node(X);
            int index=G.contain(X); // changed in 9-5-2008, it was (node1)
            visited = initializeVisited();;
            if(simplePaths)
                findSimplePaths(index,index, s0, globalLambda,Y,visited,pathVar);
            else
                find(index,index, s0, globalLambda,Y,pathVar);
        }
            
    }
   
    public void startFindPaths2(int node,String E, String Y,Vector globalLambda,boolean simplePaths, String pathVar){
        solutions = new Vector();
        pathS = new PathSolution();        
        int s0= A.getStartState();
        Vector visited = initializeVisited();;
        if(simplePaths)
            findSimplePaths(node,node, s0, globalLambda,Y,visited,pathVar);
        else
            find(node,node, s0, globalLambda,Y,pathVar);
    }
   
    public void find(int v0,int v, int s0, Vector gLambda, String Y , String pathVar){
        R = new Vector();         //initialize reach set
        W = new Vector();         //initialize work list
        String constraint = new String("");
        Vector visitedNodes = new Vector();
        Vector constraintDelimiters = new Vector();
        String path = new String();
        
        // for each triple <v0,el,v> in G
        Node node =G.getNode(v0);
        //System.out.println("1        node.getLabel()="+node.getLabel());
        Edge edge;
        Transition tran = new Transition();
        Vector edges;
        int transIndex;
        
        //visited.add(v,"true");
        /*char c = (char)0;
        String h = new String(""); h+= 0;
        int k = h.charAt(0);
        System.out.println(k);*/
        //for each triple <s0,tl,s> in P
        Vector tempLambda = new Vector(gLambda);        
        for(int i=0;i<AftU[s0].size();i++){
            visitedNodes = new Vector();
            constraintDelimiters = new Vector();
            transIndex=AU[s0][i];
            tran = A.TransitionAt(transIndex);
            boolean inverse = false;
            if (tran.getSymbolState().charAt(0)=='-'){
                 edges = node.getInEdges();
                 inverse = true;
            }
            else edges = node.getEdges();
            //System.out.println(edges.size());
            if (tran.getSymbolState().charAt(0)=='{'){
                //System.out.println(tran.getSymbolState());
                visitedNodes.add(-1);  // -1 ----> {
                visitedNodes.add(v);
                constraintDelimiters.add(visitedNodes.size()-1);
                W.add(new Triple(v, tran.gettoState(),tempLambda, visitedNodes,constraintDelimiters,path));
            }
            else if (tran.getSymbolState().charAt(0)=='}'){
                System.out.println(tran.getSymbolState().charAt(3));
                visitedNodes.add(-2);  // -2 ----> }
                constraintDelimiters.add(visitedNodes.size()-1);
                constraint = tran.getSymbolState().substring(1);
                /*int constraintIndex = tran.getSymbolState().charAt(3);
                    Vector cMappings = new Vector();
                    if (constraintIndex >=0)
                        cMappings = (Vector)(cREMappings.get(constraintIndex));*/
                if (satisfiedConstraint(v,constraint,visitedNodes, cREMappings,constraintDelimiters,tempLambda,lastTraversedEdgeLabel)) 
                    W.add(new Triple(v, tran.gettoState(),tempLambda,visitedNodes,constraintDelimiters, path));
            }
            else {
                for(int counter=0;counter<(edges.size());counter++){
                    edge =(Edge)edges.get(counter);                    
                    v=edge.getNode(1).getId();
                    //System.out.println(edge.getNode(1).getLabel());        
                     //for each theta in match(tl,el)
                    tempLambda = new Vector(gLambda);        
                    visitedNodes = new Vector();        
                    constraintDelimiters = new Vector();
                    path = new String();
                    //System.out.println(tran.getSymbolState()+'\t'+ edge.getLabel());
                    visitedNodes.add(v);
                    lastTraversedEdgeLabel = edge.getLabel();
                    if (matchAndMerge(tran.getSymbolState(), edge.getLabel(), tempLambda,v,constraint)){
                        if (inverse)
                            path += "-->"+G.getNode(v).getLabel()+","+edge.getLabel()+","+G.getNode(v0).getLabel();
                        else 
                            path += "-->"+G.getNode(v0).getLabel()+","+edge.getLabel()+","+G.getNode(v).getLabel();
                        W.add(new Triple(edge.getNode(1).getId(), tran.gettoState(),tempLambda, visitedNodes, constraintDelimiters,path));
                    }
                    /*if (A.finalState(s0))                  
                        W.add(new Triple(v0, tran.gettoState(),tempLambda));*/
                }
            }
        }
        
        E = new Vector();
        int first =0;
        Triple triple;
        int s,s1,v1;
        Vector theta,theta1,theta2;
        while (exists(W, first)){   //initialize query result
            //R.add(W.get(first));    //R = R U {<v,s,theta>}
            
            triple = (Triple)W.get(first);
            v = triple.getSubject();
            
            s = triple.getPredicate();
            theta = triple.getobject();
            
            //G.getNode(v).setR(R); //added in 27/11/2007
            RNew[v][s].add(triple);//added in 27/11/2007
            
            
            theta2 =theta;

            // for each triple <v,el,v1> in G
            //System.out.println(s);
            node =G.getNode(v);
            //edges = node.getEdges();
            //System.out.println(s+"  "+node.getLabel());
            
            //for each triple <s,tl,s1> in P
            for(int i=0;i<AftU[s].size();i++){
                visitedNodes =  new Vector(triple.getVisitedNodes());
                constraintDelimiters = new Vector(triple.getConstraintDelimiters());
                path = new String(triple.getPath());
                transIndex=AU[s][i];
                tran = A.TransitionAt(transIndex);
                s1=tran.gettoState();
                boolean inverse = false;
                if (tran.getSymbolState().charAt(0)=='-'){
                     edges = node.getInEdges();
                     inverse = true;
                }
                else edges = node.getEdges();
                if (tran.getSymbolState().charAt(0)=='{'){
                    visitedNodes.add(-1);  // -1 ----> {
                    constraintDelimiters.add(visitedNodes.size()-1);
                    W.add(new Triple(v, tran.gettoState(),tempLambda, visitedNodes, constraintDelimiters, path));
                }
                else if (tran.getSymbolState().charAt(0)=='}'){
                    //System.out.println(tran.getSymbolState().charAt(3));
                    visitedNodes.add(-2);  // -2 ----> }
                    constraintDelimiters.add(visitedNodes.size()-1);
                    constraint = tran.getSymbolState().substring(1);
                    /*int constraintIndex = tran.getSymbolState().charAt(3);
                    Vector cMappings = new Vector();
                    if (constraintIndex >=0)
                        cMappings = (Vector)(cREMappings.get(constraintIndex));*/
                    if (satisfiedConstraint(v,constraint,visitedNodes, cREMappings, constraintDelimiters, tempLambda,lastTraversedEdgeLabel)) 
                        W.add(new Triple(v, tran.gettoState(),tempLambda,visitedNodes, constraintDelimiters,path));
                            
                }
                else {

                    for(int counter=0;counter<(edges.size());counter++){
                        edge =(Edge)edges.get(counter);                    
                        v1=edge.getNode(1).getId();
                        theta2=new Vector(theta);

                        //for each theta1 in match(tl,el)
                        //System.out.println(tran.getSymbolState()+"$"+ edge.getLabel()+"$"+G.getNode(v1).getLabel()+G.getNode(v).getLabel());                        
                        //System.out.println("Theta:");
                        //print(theta2);
                        visitedNodes =  new Vector(triple.getVisitedNodes());
                        constraintDelimiters = new Vector(triple.getConstraintDelimiters());
                        visitedNodes.add(v1);
                        path = new String(triple.getPath());
                        //System.out.println(visitedNodes.size());
                        lastTraversedEdgeLabel = edge.getLabel();
                        if (matchAndMerge(tran.getSymbolState(), edge.getLabel(), theta2,v1,constraint)){
                            String tempPath= new String(path);
                            if (inverse)
                                tempPath += "-->"+G.getNode(v1).getLabel()+","+edge.getLabel()+","+G.getNode(v).getLabel();
                            else 
                                tempPath += "-->"+G.getNode(v).getLabel()+","+edge.getLabel()+","+G.getNode(v1).getLabel();
                            if (!contain(v1, s1,theta2 )) // in R, now RNew
                                W.add(new Triple(v1, s1,theta2,visitedNodes, constraintDelimiters, tempPath));
                        }
                    }
                }
            }
            //System.out.println("Here we print Y="+Y);
           
            if (pathVar.charAt(0)=='?'){
                //path = (String)triple.getPath();
                VarImage vi = new VarImage(pathVar, path, v);
                theta2.add(vi);
            }
            
            //if (isCF)
              //  theta2.add(vICF);
            
            if (Y.length()==0) ; //do nothing
            else if ((Y.charAt(0)=='?')|| (Y.charAt(0)=='$') || (Y.charAt(0)=='_')){
                VarImage vi;             
                int location =hasImage(theta2,Y);                //theta2=globalLambda
                if (location >= 0){
                    vi=(VarImage)theta2.get(location);
                    if ((A.finalState(s)) && ((vi.getImage().equals(G.getNode(v).getLabel())))){
                        pathS.setStartPoint(v0);
                        pathS.setEndtPoint(v);
                        pathS.setLambda(theta2);
                        if(! contains(solutions,pathS))
                            solutions.add(pathS);
                        pathS = new PathSolution();
                    }
                }
                else{
                    //System.out.println("no: "+s);
                    //vi=new VarImage(Y, v);
                    if (A.finalState(s)){
                        //System.out.println("yes");
                       // lambda.add(vi);   
                        pathS.setStartPoint(v0);
                        pathS.setEndtPoint(v);
                        pathS.setLambda(theta2);
                        if(! contains(solutions,pathS))
                            solutions.add(pathS);
                        pathS = new PathSolution();
                    }
                }


        }
        else if ((A.finalState(s))&&(Y.equals(G.getNode(v).getLabel()))){
              //System.out.println("Y not variables");
              //pathS = new PathSolution();
              pathS.setStartPoint(v0);
              pathS.setEndtPoint(v);
              pathS.setLambda(theta2);
              if(! contains(solutions,pathS))
                  solutions.add(pathS);
              pathS = new PathSolution();
            }
            
            first++;                //W = W - {<v,s,theta>}
        }// endWhile               
        
        
        //the following condition is for *, and can be deleted with the function addToSolution()
        if (A.finalState(s0)){                  
                    tempLambda = new Vector(gLambda);
                    addToSolution(v0, tempLambda,Y);
        }
        
    }  // end find
    
    public boolean satisfiedConstraint(int currentNode,String constraint,Vector visitedNodes,Vector cREMappings, Vector constraintDelimiters, Vector lambda,String edge){
        //Note: these steps can be done one time
        //System.out.println(constraint);
        if(constraint.startsWith("&<empty&>")){
            return true;
        }
        //System.out.println("not empty");
        int constraintIndex = -1;
        String constraintFunction = new String(" ");
        boolean noComparedValue = true;
        Double comparedValue = 0.0; count = 0; valueOfConstraintFunction = 0; isCF = false;
        if ((constraint.charAt(3)=='s')|| (constraint.charAt(3)=='a')){
            int e = constraint.indexOf(')');
            constraintFunction = constraint.substring(3, e+1);
            //System.out.println(constraintFunction);
            if (constraintFunction.indexOf(',')>=0){
                String v = constraintFunction.substring(constraintFunction.indexOf(',')+1, constraintFunction.indexOf(')'));
                comparedValue = Double.valueOf(v);
                noComparedValue = false;
            }
            //System.out.println(noComparedValue);
            String c = constraint.substring(0, 3)+constraint.substring(e+1);
            constraint = c;
        }
        else if ((constraint.charAt(3)=='c')|| (constraint.charAt(3)=='C')){
             int e = constraint.indexOf(')');
             constraintFunction = constraint.substring(3, e+1);
            if (constraintFunction.indexOf(',')>=0){
                String v = constraintFunction.substring(constraintFunction.indexOf(',')+1, constraintFunction.indexOf(')'));
                comparedValue = Double.valueOf(v);
                noComparedValue = false;
            }
             String c = constraint.substring(0, 3)+constraint.substring(e+1);
             constraint = c;
        }
        //System.out.println(constraint);
        boolean satisfied = false;
        char beginSetDel , endSetDel;
        beginSetDel = constraint.charAt(3);
        endSetDel = constraint.charAt(constraint.length()-3);
        //System.out.println(beginSetDel+"  "+endSetDel);
        Integer endSetIndex=0, beginSetIndex=0;
        if (constraintDelimiters.size()!=0){
            endSetIndex = (Integer)constraintDelimiters.get(constraintDelimiters.size()-1)-1;
            beginSetIndex = 0;

            // this loop for finding {, i.e., the begining of the node set to be tested
            boolean stop = false;
            int index = constraintDelimiters.size()-2;
            int countDel =0; Integer del;
            while ((!stop) && (index>=0)){
                beginSetIndex = (Integer)constraintDelimiters.get(index);
                del = (Integer)visitedNodes.get(index);
                if ( del == -2)
                    countDel++;
                if ( del == -1)
                    countDel--;
                if (countDel==0) 
                    stop = true;
                index--;
            }// the end of the loop for finding {, i.e., the begining of the node set to be tested

            if (beginSetDel == ']')
                beginSetIndex++;
            if (endSetDel == '[')
                endSetIndex--;
        }

        
        if ((constraint.charAt(3)=='D') || (constraint.charAt(3)=='d')){
            //checkSimplePath;
            //System.out.println("checkSimplePaths (distinctNodes) ");
            int begin = beginSetIndex; int end = endSetIndex;
            satisfied = true;
            long [] orderNodes = new long [G.getNodes().size()];
            for (int i=0; i < G.getNodes().size();i++)
                orderNodes[i] = -1;
            while ((satisfied)&& (begin <= end)){
                Integer nodeID = (Integer)visitedNodes.get(begin);
                long checkVisited = orderNodes[nodeID];
                if (checkVisited >= 0)
                     satisfied = false;
                else
                    orderNodes[nodeID]= nodeID;
                begin++;
            }//end while
        }
        else if ((constraint.charAt(3)=='l') || (constraint.charAt(3)=='L')){
            //checkSimplePath;
            //System.out.println("check length of Paths....");
            String lengthInString = constraint.substring(9, constraint.indexOf('&', 9));
            //System.out.println(lengthInString);
            Integer length = Integer.valueOf(lengthInString);
            //System.out.println(length);
            if ((endSetIndex - beginSetIndex-2)<= length)
                satisfied = true;
            else 
                satisfied = false;
        }
        else if ((constraint.charAt(3)=='n') || (constraint.charAt(3)=='N')){
            //checkSimplePath;
            //System.out.println("check length of Paths....");
            String lengthInString = constraint.substring(10, constraint.indexOf('&', 10));
            //System.out.println(lengthInString);
            Integer length = Integer.valueOf(lengthInString);
            //System.out.println(length);
            if ((endSetIndex - beginSetIndex-1)<= length)
                satisfied = true;
            else 
                satisfied = false;
        }
        else {  
            int varFromIndex = constraint.indexOf('?', 5);
            String var = constraint.substring(varFromIndex, constraint.length()-3);
            //System.out.println(constraint);
            
            constraintIndex = constraint.charAt(2);
            //System.out.println(constraintIndex);
            Vector cMappings = new Vector();
            if (constraintIndex >=0)
                cMappings = (Vector)(cREMappings.get(constraintIndex));
            
            //===========================EDGE CONSTRAINT=================================
            if (constraint.contains("EDGE") ||constraint.contains("edge")){
                //System.out.println("EDGE");
                satisfied = true;
                int begin = beginSetIndex; int end = endSetIndex;String s ="true";
                //while ((!satisfied)&& (begin <= end)){
                    if (hashTableForSatisfiedEdges[constraintIndex].containsKey(lastTraversedEdgeLabel)){
                        //System.out.println("found  "+nodeID); 
                        s = (String)hashTableForSatisfiedEdges[constraintIndex].get(lastTraversedEdgeLabel);
                         if (s.equals("true")) 
                             satisfied = true;
                         else satisfied = false;
                    }
                    else if (!edgeSatisfiesConstraint(var,lastTraversedEdgeLabel,cMappings,constraintFunction,constraintIndex)){// node in the mappings cMappings
                        satisfied = false;
                        s = "false";
                        //System.out.println("added  "+lastTraversedEdgeLabel);
                        //hashTableForSatisfiedNodes[constraintIndex].put(nodeID, s);
                    }
                    else {
                        //System.out.println("added  "+nodeID);
                        hashTableForSatisfiedEdges[constraintIndex].put(lastTraversedEdgeLabel, s);
                    }
                  //  begin++;
                //}
                //System.out.println("satisfied="+satisfied);
            }
             //===========================END EDGE CONSTRAINT=================================
            else if ((constraint.charAt(4) =='E') ||(constraint.charAt(4) =='e')){
                //System.out.println("EXISTS");
                int begin = beginSetIndex; int end = endSetIndex;String s ="true";
                //System.out.println(begin+"   "+end+satisfied);
                while ((!satisfied)&& (begin <= end)){
                    Integer nodeID = (Integer)visitedNodes.get(begin);
                    if (nodeID >= 0){
                        if (hashTableForSatisfiedNodes[constraintIndex].containsKey(nodeID)){
                            //System.out.println("found  "+nodeID); 
                            s = (String)hashTableForSatisfiedNodes[constraintIndex].get(nodeID);
                             if (s.equals("true")) 
                                 satisfied = true;
                             else satisfied = false;
                        }
                        else if (!nodeSatisfiesConstraint(var,nodeID,cMappings,constraintFunction,constraintIndex)){// node in the mappings cMappings
                            satisfied = false;
                            s = "false";
                            //System.out.println("added  "+nodeID);
                            //hashTableForSatisfiedNodes[constraintIndex].put(nodeID, s);
                        }
                        else {
                            //System.out.println("added  "+nodeID);
                            hashTableForSatisfiedNodes[constraintIndex].put(nodeID, s);
                        }
                    }
                    begin++;
                }
                //System.out.println(satisfied);
            }
            else if ((constraint.charAt(4) =='A') ||(constraint.charAt(4) =='a')){
                //System.out.println("ALL");
                int begin = beginSetIndex; int end = endSetIndex;
                satisfied = true; String s ="true"; 
                while ((satisfied)&& (begin <= end)){
                    Integer nodeID = (Integer)visitedNodes.get(begin);
                    if (nodeID >= 0){
                        if (hashTableForSatisfiedNodes[constraintIndex].containsKey(nodeID)){
                            //System.out.println("found  "+nodeID); 
                            s = (String)hashTableForSatisfiedNodes[constraintIndex].get(nodeID);
                             if (s.equals("true")) 
                                 satisfied = true;
                             else satisfied = false;
                        }
                        else if (!nodeSatisfiesConstraint(var,nodeID,cMappings,constraintFunction,constraintIndex)){// node in the mappings cMappings
                            satisfied = false;
                            s = "false";
                            //System.out.println("added  "+nodeID);
                            //hashTableForSatisfiedNodes[constraintIndex].put(nodeID, s);
                        }
                        else {
                            //System.out.println("added  "+nodeID);
                            hashTableForSatisfiedNodes[constraintIndex].put(nodeID, s);
                        }
                    }
                    begin++;
                }
            }
            else if ((constraint.charAt(4) =='V') ||(constraint.charAt(4) =='v')){
                //System.out.println("EVEN");
                int begin = beginSetIndex+1; int end = endSetIndex;
                satisfied = true;
                while ((satisfied)&& (begin <= end)){
                    Integer nodeID = (Integer)visitedNodes.get(begin);
                    if (nodeID >= 0)
                        if (!nodeSatisfiesConstraint(var,nodeID,cMappings,constraintFunction,constraintIndex))// node in the mappings cMappings
                            satisfied = false;
                    begin+=2;
                }
            }
            else if ((constraint.charAt(4) =='O') ||(constraint.charAt(4) =='o')){
                //System.out.println("ODD");
                int begin = beginSetIndex; int end = endSetIndex;
                satisfied = true;
                while ((satisfied)&& (begin <= end)){
                    Integer nodeID = (Integer)visitedNodes.get(begin);
                    if (nodeID >= 0)
                        if (!nodeSatisfiesConstraint(var,nodeID,cMappings,constraintFunction,constraintIndex))// node in the mappings cMappings
                            satisfied = false;
                    begin+=2;
                }
            }
            else {
                int nodeIndex = constraint.charAt(4);
                //System.out.println("INTEGER");
                int begin = beginSetIndex; int end = endSetIndex;
                //System.out.println(begin+"   "+end+ "  "+nodeIndex);
                if (nodeIndex < 0)
                    satisfied = false;
                else if ((begin+nodeIndex-1)<=end){
                    Integer nodeID; 
                    nodeID = (Integer)visitedNodes.get(begin+nodeIndex-1);
                    if (nodeID >= 0)
                        if (nodeSatisfiesConstraint(var,nodeID,cMappings,constraintFunction,constraintIndex))// node in the mappings cMappings
                            satisfied = true;
                    begin++;
                }
            }
        }

        //System.out.println(valueOfConstraintFunction+" "+count);
        //VarImage vi;
        if (constraintFunction.charAt(0)=='s'){
            isCF = true;
            String image ="\"\"\""+ valueOfConstraintFunction+"\"\"\""+"DECIMAL";//+"<http://www.w3.org/2001/XMLSchema#decimal>";
            vICF = new VarImage(varConstraintFunction+"SUM", image, currentNode);
            addToMapping(lambda,0,vICF);  //lambda.add(0,vICF);
            if (noComparedValue)
                ;
            else if (valueOfConstraintFunction <= comparedValue)
                satisfied = true;
            else satisfied = false;
        }
        else if (constraintFunction.charAt(0)=='a'){
            isCF = true;
            double valueOFAV = valueOfConstraintFunction/count;
            String image ="\"\"\""+ valueOFAV +"\"\"\""+"DECIMAL";
            //String image =""+ valueOfConstraintFunction/count;
            vICF = new VarImage(varConstraintFunction+"AVG", image, currentNode);
            addToMapping(lambda,0,vICF); //lambda.add(0,vICF);
            //System.out.println(valueOfConstraintFunction/count);
            if (noComparedValue)
                ;
            else if ((valueOfConstraintFunction/count) <= comparedValue)
                satisfied = true;
            else satisfied = false;
        }
        else if (constraintFunction.charAt(0)=='c'){
            isCF = true;
            //String image =""+ count;
            String image ="\"\"\""+ count +"\"\"\""+"DECIMAL";
            vICF = new VarImage(varConstraintFunction+"COUNT", image, currentNode);
            addToMapping(lambda,0,vICF); //lambda.add(0,vICF);
            if (noComparedValue)
                ;
            else if (count <= comparedValue)
                satisfied = true;
            else satisfied = false;
        }
        return satisfied;
    }
    
    public void addToMapping(Vector lambda,int index ,VarImage vICF){
        VarImage vi = (VarImage)lambda.get(index);
        if (vi.getVar().equals(vICF.getVar())){
            lambda.set(0, vICF);
        }
        else lambda.add(index, vICF);
    }
    
    public boolean search(Solution s1,String var, String label){
        //s1.print();
        //System.out.println(var+"  "+label);
        int loc=-1,i=0;
        boolean  found=false;
        String concept,image;
        while ((!found) &&(i<s1.getConcepts().size())){
            concept =(String)s1.getConcepts().get(i);
            if (concept.equals(var)){
                image = (String)s1.getImages().get(i);
                //System.out.println(image);
                if (label.equals("-1")){
                    found = true;
                    String s = image.substring(3, image.indexOf("\"\"\"", 3));
                    //System.out.println(s);
                    Integer value = Integer.valueOf(s);
                    valueOfConstraintFunction += value;
                    count++;
                }
                else if (image.equals(label))
                    found=true;
                //loc=i;
            }
            i++;
        }
        //System.out.println(var+"  "+label+"  "+found);
        return found;
        //return loc;        
    }
    
    public boolean nodeSatisfiesConstraintOld(String var, Integer nodeID,Vector cMappings,String constraintFunction,int constindex){
        //System.out.println(var+"  "+nodeID+"  "+constraintFunction+"  ");
        String varCF = constraintFunction.substring(constraintFunction.indexOf('(')+1, constraintFunction.indexOf(' '));
        //System.out.println(varCF);
        varConstraintFunction = varCF;
        boolean satisfied = false;
        int index =0; int location =-1;
        Node node = G.getNode(nodeID);
        String nodeLabel = node.getLabel();
        Solution solution;
        while ((!satisfied) && (index <cMappings.size())){
            //System.out.println(var+"  "+nodeID+"  "+nodeLabel+"  "+satisfied);
            solution = (Solution)cMappings.get(index);
            if (search(solution,var, nodeLabel)){
                satisfied = true;            
                if ((constraintFunction.charAt(0)=='s') 
                || (constraintFunction.charAt(0)=='a') ||(constraintFunction.charAt(0)=='c'))
                    if (search(solution,varCF, "-1"))
                        satisfied = true;            
            }
            
            index++;
        }
        //System.out.println(var+"  "+nodeID+"  "+nodeLabel+"  "+satisfied+"  "+cMappings.size());
        return satisfied;
    }
    
    public boolean nodeSatisfiesConstraint(String var, Integer nodeID,Vector cMappings,String constraintFunction,int constIndex){
        //System.out.println("we are at nodeSatisfiesConstraint()"+constIndex);
        String varCF = constraintFunction.substring(constraintFunction.indexOf('(')+1, constraintFunction.indexOf(' '));
        //System.out.println(alreadyConstructedHashT[constIndex]);
        varConstraintFunction = varCF;
        boolean satisfied = false;
        int index =0; int location =-1;
        Node node = G.getNode(nodeID);
        String nodeLabel = node.getLabel();
        Solution solution;
        //System.out.println(cREMappings.size());
        if(!alreadyConstructedHashT[constIndex]){
            //System.out.println(constIndex+"\t"+alreadyConstructedHashT[constIndex]);
            alreadyConstructedHashT[constIndex] = true;
            //System.out.println(constIndex+"\t"+alreadyConstructedHashT[constIndex]);
            constructHashTableForMaps(cMappings,var,varCF,constIndex);
            
        }        
        if(hashTableForMaps[constIndex].containsKey(nodeLabel)){
            satisfied = true;                        
            if ((constraintFunction.charAt(0)=='s') || (constraintFunction.charAt(0)=='a')
            ||(constraintFunction.charAt(0)=='c')){
                String image = (String)hashTableForMaps[constIndex].get(nodeLabel);
                String s = image.substring(3, image.indexOf("\"\"\"", 3));
                //System.out.println(s);
                Integer value = Integer.valueOf(s);
                valueOfConstraintFunction += value;
                count++;
            }
        }
        return satisfied;
    }//nodeSatisfiesConstraint

    
    public boolean edgeSatisfiesConstraint(String var, String edgeLabel,Vector cMappings,String constraintFunction,int constIndex){
        //System.out.println("we are at edgeSatisfiesConstraint():"+edgeLabel);
        String varCF = constraintFunction.substring(constraintFunction.indexOf('(')+1, constraintFunction.indexOf(' '));
        //System.out.println(alreadyConstructedHashT[constIndex]);
        varConstraintFunction = varCF;
        boolean satisfied = false;
        int index =0; int location =-1;
        Solution solution;
        //System.out.println(cREMappings.size());
        if(!alreadyConstructedHashT[constIndex]){
            //System.out.println(constIndex+"\t"+alreadyConstructedHashT[constIndex]);
            alreadyConstructedHashT[constIndex] = true;
            //System.out.println(constIndex+"\t"+alreadyConstructedHashT[constIndex]);
            constructHashTableForMaps(cMappings,var,varCF,constIndex);
            
        }       
        if(hashTableForMaps[constIndex].containsKey(edgeLabel)){
            satisfied = true;                        
            if ((constraintFunction.charAt(0)=='s') || (constraintFunction.charAt(0)=='a')
            ||(constraintFunction.charAt(0)=='c')){
                String image = (String)hashTableForMaps[constIndex].get(edgeLabel);
                String s = image.substring(3, image.indexOf("\"\"\"", 3));
                Integer value = Integer.valueOf(s);
                valueOfConstraintFunction += value;
                count++;
            }
        }
        return satisfied;
    }//edgeSatisfiesConstraint

    
    public void constructHashTableForMaps(Vector cMappings,String var,String varCF,int constIndex){
        //System.out.println("we are at constructHashTableForMaps()");
        Solution solution;
        String concept="", imageKey="", imagevarCF="\"\"\"0\"\"\"";
        int i;
        for(int index=0; index <cMappings.size();index++){
            solution = (Solution)cMappings.get(index);
            i=0;
            while (i < solution.getConcepts().size()){
                concept =(String)solution.getConcepts().get(i);
                if (concept.equals(var))
                    imageKey = (String)solution.getImages().get(i);
                //System.out.println(image);
                if (concept.equals(varCF))
                    imagevarCF = (String)solution.getImages().get(i);
                i++;
            }//end-while
            hashTableForMaps[constIndex].put(imageKey,imagevarCF);
        }//en-for        
    }
    
    
    public void findSimplePaths(int v0,int v, int s0, Vector gLambda, String Y,Vector visited, String pathVar){
        String constraint = new String("");
        if (A.finalState(s0)){
            if (Y.length()==0) ; //do nothing
            else if ((Y.charAt(0)=='?')|| (Y.charAt(0)=='$') || (Y.charAt(0)=='_')){
                VarImage vi;             
                int location =hasImage(gLambda,Y);                
                if (location >= 0){
                    vi=(VarImage)gLambda.get(location);
                    if (vi.getImage().equals(G.getNode(v).getLabel())){
                        pathS.setStartPoint(v0);
                        pathS.setEndtPoint(v);
                        pathS.setLambda(gLambda);
                        if(! contains(solutions,pathS))
                            solutions.add(pathS);
                        pathS = new PathSolution();
                    }
                }
                else{
                   // System.out.println("no");
                    //vi=new VarImage(Y, v);
                    //System.out.println("yes");
                    // lambda.add(vi);                
                    pathS.setStartPoint(v0);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(gLambda);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
                }
            }
            else if (Y.equals(G.getNode(v).getLabel())){
                //System.out.println("Y not variables");
                //pathS = new PathSolution();
                pathS.setStartPoint(v0);
                pathS.setEndtPoint(v);
                pathS.setLambda(gLambda);
                if(! contains(solutions,pathS))
                    solutions.add(pathS);
                pathS = new PathSolution();
            }            
        }
        
        // for each triple <v0,el,v> in G
        Node node =G.getNode(v);
        //System.out.println("1        node.getLabel()="+node.getLabel());
        Edge edge;
        Transition tran = new Transition();
        Vector edges;
        int transIndex;
        //visited.add(v,"true");
        //System.out.println(node.getLabel());
        //for each triple <s0,tl,s> in P
        for(int i=0;i<AftU[s0].size();i++){
            transIndex=AU[s0][i];
            tran = A.TransitionAt(transIndex);
            if (tran.getSymbolState().charAt(0)=='-')
                 edges = node.getInEdges();
            else edges = node.getEdges();
            for(int counter=0;counter<(edges.size());counter++){
                edge =(Edge)edges.get(counter);                    
                v=edge.getNode(1).getId();
            
                 //for each theta in match(tl,el)
                Vector tempLambda = new Vector(gLambda);        
                if (matchAndMerge(tran.getSymbolState(), edge.getLabel(), tempLambda,v,constraint)){
                    visitedNode = (String)visited.get(edge.getNode(1).getId());
                    //System.out.println("2        "+edge.getNode(1).getLabel());
                    if (!visitedNode.equals("true")){
                        //Vector visited1 = new Vector(visited);
                        visited.set(edge.getNode(1).getId(),"true");
                        findSimplePaths(v0,edge.getNode(1).getId(), tran.gettoState(),tempLambda,Y,visited,pathVar);
                    }
                }
                 //   W.add(new Triple(edge.getNode(1).getId(), tran.gettoState(),tempLambda));
                
            }
        }
        
    }  // end findSimplePaths
   
    
    public void addToSolution(int v,Vector theta2,String Y){        
        
        if ((Y.charAt(0)=='?')|| (Y.charAt(0)=='$') || (Y.charAt(0)=='_')){
            VarImage vi;             
            int location =hasImage(theta2,Y);                //theta2=globalLambda
            if (location >= 0){
                vi=(VarImage)theta2.get(location);
                if (vi.getImage().equals(G.getNode(v).getLabel())){
                    pathS.setStartPoint(v);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(theta2);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
                }
            }
            else{            
                    pathS.setStartPoint(v);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(theta2);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
            }
        }
        else if (Y.equals(G.getNode(v).getLabel())){
              pathS.setStartPoint(v);
              pathS.setEndtPoint(v);
              pathS.setLambda(theta2);
              if(! contains(solutions,pathS))
                  solutions.add(pathS);
              pathS = new PathSolution();
            }
    }
    
    public boolean contains(Vector solutions, PathSolution solution){
        int i=0;
        boolean found=false;
        PathSolution ps = new PathSolution();
        while((i<solutions.size()) && (!found)){
            ps =(PathSolution)(solutions.get(i));
            if((ps.getEndPoint()==solution.getEndPoint()) 
            && (ps.getStartPoint()==solution.getStartPoint()) 
            && (ps.getLambda().containsAll(solution.getLambda()))
            )
                found=true;
            i++;
        }
        return found;
    }
    
    public boolean contain(int s,int p,Vector theta){
        //System.out.println("enter");
        boolean found =false;
        int i=0;
        //Vector R = G.getNode(s).getR();
        Vector R = RNew[s][p];
        //System.out.println(R.size());
        Triple triple;
        while ((!found) && (i < R.size())){
            triple = (Triple)(R.get(i));
            if ((triple.getSubject()==s) &&(triple.getPredicate()==p) 
            &&(triple.getobject().equals(theta)))
                found=true;
            i++;
        }
        return found;
    }
    
        public boolean containOld(Vector R, int s,int p,Vector theta){
        boolean found =false;
        int i=0;
        Triple triple;
        while ((!found) && (i<R.size())){
            triple = (Triple)(R.get(i));
            if ((triple.getSubject()==s) &&(triple.getPredicate()==p) 
            &&(triple.getobject().equals(theta)))
                found=true;
            i++;
        }
        return found;
    }

    
    public boolean exists(Vector v,int first){
        boolean isNotEmpty =true;
        int end= v.size();
        if ((end-first)>0)
            isNotEmpty= true;
        else isNotEmpty =false;
        return isNotEmpty;
    }
    
    public boolean variable(String str){
        //System.out.println("we are at variable():"+str);
        if ((str.charAt(0)=='?')|| (str.charAt(0)=='$') || (str.charAt(0)=='_'))
            return true;
        else return false;
    }
    
    public void print(Vector theta){
        VarImage vi;
        for(int i=0;i<theta.size();i++){
            vi=(VarImage)theta.get(i);
            System.out.println("("+vi.getVar()+","+vi.getImage()+"),");
        }
    }
    
    public boolean matchAndMerge(String symbol,String predicate, Vector gLambda,int v,String constraint){
        //System.out.println("$"+symbol+"$    predicate=$"+predicate+"$");
        //print(gLambda);
        boolean matchResult =false;
        VarImage vi; 
        int location =hasImage(gLambda,symbol);
        //System.out.println("$"+symbol+"$    predicate=$"+predicate+"$");
        if (variable(symbol)){
            if (location < 0){
                vi= new VarImage(symbol, predicate, v);//verify that v is the choice, may be v0 instead
                gLambda.add(vi);           
                matchResult=true;
             }
            else if(InVarImage(gLambda, symbol, predicate)){                            
                matchResult = true;            
            }
        }
        else if(symbol.charAt(0)=='!') {
                        String  str=new String("");
                        str =A.copy(symbol, 1);
                        if(!str.equals(predicate))
                            matchResult = true;                                    
        }
        else if(symbol.charAt(0)=='-') {
                        String  str=new String("");
                        str =symbol.substring(1);
                        //System.out.println("$"+str+"$    predicate=$"+predicate+"$");
                        if(str.equals(predicate)){
                            //System.out.println("matched");
                            matchResult = true;   
                        }
        }
        else if (symbol.charAt(0)=='#') {
            if ((symbol.length()>1)&& (symbol.charAt(1)=='&')){
                if (satisfiedConstraint(v,symbol.substring(1),new Vector(), cREMappings,new Vector(),tempLambda,predicate))
                    matchResult = true;
                else matchResult = false;
            }
            else matchResult = true;                    
        }
        else if (predicate.equals(symbol))
            matchResult=true;
        else matchResult=false;        
        return matchResult;
    }
    
    
    public boolean InVarImage(Vector v, String var, String str){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if ((var.equals(vi.getVar())) &&(str.equals(vi.getImage()))){
                found =true;
                location=i;
            }
            i++;
        }
        return found;
    }
    
     public int hasImage(Vector v, String var){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if (var.equals(vi.getVar())){
                found =true;
                location=i;
            }
            i++;
        }
        return location;
    }
   
    public Vector initializeVisited(){
        Vector visited = new Vector();
        //System.out.println(G.getNodes().size());
        for(int i=0;i<=G.getNodes().size();i++)
            visited.add(i,"false");
        return visited;
    }
     
     public void initialization(String E,MultiGraph G1, Vector cREMappings){        
            //System.out.println("we are at initialization()"+cREMappings.size());
            A= new Automate(E);
            maxStates = A.getMaxStates();         
            maxConcepts = A.getMaxConcepts();         
            G = G1;
            AftU=new Vector[maxStates];
            initialize(AftU);
            AftU =A.getAftU();
            copyto();
            markVector=new Vector();
            tempLambda = new Vector();        
            this.cREMappings = new Vector(cREMappings);
            int sizeOfG = G.getNodes().size();
            //System.out.println(G.getNodes().size());
            RNew = new Vector [sizeOfG][maxStates];
            for (int n=0; n<sizeOfG;n++)
                for (int m=0; m<maxStates;m++)
                    RNew [n][m] = new Vector();
            //System.out.println(G.getNodes().size());
            
            int size = cREMappings.size();
            alreadyConstructedHashT = new boolean [size];
            hashTableForMaps = new Hashtable[size];
            hashTableForSatisfiedNodes = new Hashtable[size];
            hashTableForSatisfiedEdges = new Hashtable[size];
            for(int i=0; i<size; i++){
                //System.out.println(alreadyConstructedHashTs[i]);
                this.alreadyConstructedHashT[i]=false;
                hashTableForMaps[i] =  new Hashtable();
                hashTableForSatisfiedNodes[i] = new Hashtable(G.getNodes().size()+2);
                hashTableForSatisfiedEdges[i] = new Hashtable(G.getNodes().size()+2);
            }
               
    }
     
     public void initialize(Vector[] v){
        for(int i=0;i<maxStates;i++)
            v[i] = new Vector();
    }
    
      public void copyto(){
        AU = new Integer[maxStates][maxConcepts];
        for(int i=0;i< maxStates;i++)
            AftU[i].copyInto(AU[i]);
        //printAftU(AU);
    }
      
      
    public Vector getSolutions(){
        return solutions;
    }
      
    public Hashtable [] getHashTablesForMaps(){
        return hashTableForMaps;
    }
    
    public boolean [] getAlreadyConstructedHashT(){
//        System.out.println(alreadyConstructedHashT[0]);        
        return alreadyConstructedHashT;
    }
      private Vector [][] RNew;
      private Vector R;
      private Vector W;
      private Vector E;
      private Automate A;
      private int maxStates;
      private int maxConcepts;
      private MultiGraph G;
      private Vector[] AftU;
      private Vector solutions;
      private Integer [][] AU; 
      private Vector markVector;        
      private Vector tempLambda, cREMappings;
      private PathSolution pathS;
      private String GraphG;
      private int finalState;    
      private String visitedNode;
      private double valueOfConstraintFunction = 0;
      private int count = 0;
      private String varConstraintFunction;
      private VarImage vICF;
      private boolean isCF;
      private Hashtable [] hashTableForMaps;
      private boolean [] alreadyConstructedHashT;
      private Hashtable [] hashTableForSatisfiedNodes;
      private Hashtable [] hashTableForSatisfiedEdges;
      private String lastTraversedEdgeLabel;
}
