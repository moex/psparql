/*
 * Automatee.java
 *
 * Created on February 1, 2006, 9:49 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;
import java.util.Vector;


/**
 *
 * @author faisal alkhateeb
 */
public class Automate {  
    
    
    
    /** Creates a new instance of Automatee */
    public Automate(String E) {
        maxStates=100;
        currentstate=1;
        startState=0;
	finalStates = new Vector();
        finalStates.add(1);
        transitions = new Vector();
        startState=0;
        transformedExp = new String("");
        transformIndex =  new Vector();
        transformedExp = transform(E+'\0');
        construct(transformedExp,0,currentstate);
        AftU = new Vector[maxStates];
        AftU2 = new Vector[maxStates];
        EReachable = new Vector[maxStates];
        visit = new Vector();
        initializeAftU();   
        calculateAftU(AftU);
        //printAftU(AftU);
        //printAutomate(transitions);  //with epsilons            
        if (closure){
            initializeER();             
            RemoveEpsilons();
            //printAutomate(transitions);   // without epsilons
            calculateAftU(AftU2);
            //printAftU(AftU2);
        }       
        else AftU2=AftU;
    }
    
    public void printAftU(Vector [] after){
        String s;
        for (int i=0;i<= currentstate;i++){
            s="";
            for (int j=0;j<after[i].size();j++)
                s+=(Integer)after[i].get(j)+"    ";
            System.out.println("AftU2["+i+"]= "+s);
        }
        s="";
       for (int i=0;i< finalStates.size();i++)
           s+=(Integer)finalStates.get(i)+"    ";
        System.out.println("finalStates= ["+s+"]");
    }
    
    public String  copy(String stringfrom){
        String string2 = new String("");
        int found=0;
        int j=0;
        inputcursor=-100;
        int k=1;
        while ((k<stringfrom.length()) && (found==0)){
            if (stringfrom.charAt(k)==')')   
                j--;
            if (stringfrom.charAt(k)=='(')
                j++;
            if (j<=0)
                found=1;
            string2+=stringfrom.charAt(k);
            k++;
        }
        string2+='\0';
        inputcursor=k;
        return string2;
    }
    
    public String copy(String stringfrom, int cursor){
        int i=cursor;
        String string2 = new String("");
        while (i<stringfrom.length())
            string2+=stringfrom.charAt(i++);
        //string2+='$';
        return string2;
    }
	
    
    public int contains(Vector v , String str){
        boolean found=false;
        int index =-1;
        int i=-1;
        while((++i<v.size()) && (index ==-1))
            if(str.equals((String)v.get(i)))
                index=i;
        return index;
    }
    
    
    public String transform(String E){
        //System.out.println(" ............ transform() :"+E);
        int n=0,m=-1;
        String string1=new String("");
        int index;
        boolean added=false;
        while (++m<(E.length()-1)){
            string1="";
            added=false;
            if ((E.charAt(m) !='(') && (E.charAt(m) !=')') && (E.charAt(m) !='+') && (E.charAt(m) !='*') 
            && (E.charAt(m) !='.') && (E.charAt(m) !='|')  && (E.charAt(m) !=' ')){
                switch (E.charAt(m)){
                    case '<':
                    case '!':{
                        while ((E.charAt(m) !='>')){
                                          string1+=E.charAt(m++);
                                          added=true;
                        }
                        string1+=E.charAt(m++);
                        break;                        
                    }
                    case '-':{
                        while ((E.charAt(m) !='>')){
                                          string1+=E.charAt(m++);
                                          added=true;
                        }
                        string1+=E.charAt(m++);
                        break;                        
                    }
                    case '{':{
                        string1+=E.charAt(m++);
                        added=true;
                        break;                        
                    }
                    case '}':{
                        while ((E.charAt(m) !='>')){
                                          string1+=E.charAt(m++);
                                          added=true;
                        }
                        string1+=E.charAt(m++);
                        break;                        
                    }
                    case '#':{
                        if (E.charAt(m+1) !='&'){
                            string1+=E.charAt(m++);
                            added=true;
                        }
                        else{ while ((E.charAt(m) !='>')){
                             string1+=E.charAt(m++);
                             added=true;
                        }
                        string1+=E.charAt(m++);
                        }
                        break;                        
                    }
                    default:{
                         while ((E.charAt(m) !=' ') && (E.charAt(m) !='(') && (E.charAt(m) !=')') 
                            && (E.charAt(m) !='+') && (E.charAt(m) !='*') && (E.charAt(m) !='.') 
                            && (E.charAt(m) !='|') && (E.charAt(m) !='\0')){
                                              string1+=E.charAt(m++);
                                              added=true;
                            }


                    }
                }
                if (added)
                    m--;
                //System.out.println('$'+string1+'$');        
                //string1+='$';
                index= contains(transformIndex,string1);
                if (index<0){   // !transformIndex.contains(string1)
                    n=transformIndex.size();
                    transformIndex.add(string1);
                    transformedExp+= (char)(n+97);
                }	     
                else
                    transformedExp+=(char)(index+97);  //ex+97
            }
            else if (E.charAt(m)!=' '){
                //System.out.println(E.charAt(m));        
                transformedExp+=E.charAt(m);
            }
        }
        transformedExp+='\0';
        return transformedExp;
 }
    
    public void construct (String E, int i, int f){
        switch (E.charAt(0)){
            case ')':  break;  //cout<<"i="<<i<<"f="<<f;cout<<"case ')'";
            case '(':{ 	       
                //cout<<"i="<<i<<"f="<<f;
                String E3=new String("");
                int k=1;
                int found=0;
                int j=1;
                inputcursor=-100;
                while  ((k<E.length()) &&(found==0)){
                    if (E.charAt(k)==')'){
                        j--;
                        if (j<1){
                            found=1; 
                            E3+='\0';
                        }
                        else {
                            E3+=E.charAt(k);
                        }
                        }
                    else { 
                        E3+=E.charAt(k);
                        if (E.charAt(k)=='(')
                            j++;
                        }
                    k++;
                    }
                String E4=new String("");
                E4=copy(E,k);   //
     		//cout<<"\n case (: #E=";print(E);cout<<"  #E1=";print(E3);cout<<" 
                String E5,E6;
                if (E4.charAt(0)=='.') {
                    int cs=currentstate;
                    construct(E4,cs,f);
                    construct(E3,i,cs+1);
                    //cout<<"case ( .: #E=";print(E);cout<<"  #E1=";print(E3);
                }
                else if (E4.charAt(0)=='|') {
                    construct(E4,i,f);
                    construct(E3,i,f);
                    //cout<<"case ( |: #E=";print(E);cout<<"  #E1=";print(E5);
                }
                else  {
                    construct(E3,i,f);
                }
                break;                
            }
            
               case '+':{
                   closure =true;
                   String E5=new String(""),E6=new String("");
                   if ((E.charAt(1)>='a') && (E.charAt(1)<='z')) {
                       E6=copy(E,2);   //copy(E,E6,2);
                       E5+=E.charAt(1);
                       E5+='\0';
                   }
                   else {
                       E5=copy(E);
                       E6=copy(E,inputcursor);
                   }
                   int c=currentstate;
                   construct(E6,i,f);
                   currentstate++;
                   int cs=currentstate;
                   if (E6.charAt(0)=='.') {
                       transitions.add(new Transition(currentstate, "epsilon",++c));   
                       cs=currentstate;
                       construct(E5,i,cs);
                       construct(E5,cs,cs);
                   }
                   else {
                       transitions.add(new Transition(currentstate, "epsilon",f));                   
                       construct(E5,i,cs);
                       construct(E5,cs,cs);
                   }
                   break;
               }
               case '*':{
                   closure =true;
                   String E5=new String(""),E6=new String("");
                   if ((E.charAt(1)>='a') && (E.charAt(1)<='z')) {
                       E6=copy(E,2);
                       E5+=E.charAt(1);
                       E5+='\0';
                   }
                   else {
                       E5=copy(E);
                       E6=copy(E,inputcursor);
   		   }
                   String E3;
                   int c=currentstate;
                   construct(E6,i,f);
                   currentstate++;
                   transitions.add(new Transition(i, "epsilon",currentstate));                   
                   int cs=currentstate;
                   if (E6.charAt(0)=='.') {
                       transitions.add(new Transition(currentstate, "epsilon",++c));                   
                       cs=currentstate;
                       construct(E5,cs,cs);
                   }
                   else {
                       //T[i][column-1]=++c;					
                       transitions.add(new Transition(currentstate, "epsilon",f));
                       construct(E5,cs,cs);
                   }
                   break;
               }
               case '.':{
                   //cout<<"i="<<i<<"f="<<f;
                   currentstate++;
		   String E3= new String("");			
                   E3=copy(E,1);
                   //cout<<"case .: #E=";print(E);cout<<"  #E3=";print(E3);cout<<'\n';
                   construct(E3,currentstate,f);
                   break;
               }
               case '|':{ 
                   //cout<<"i="<<i<<"f="<<f;
                   String E3= new String("");			
                   E3=copy(E,1);
                   //cout<<"case .: #E=";print(E);cout<<"  #E3=";print(E3);cout<<'\n';
                   construct(E3,i,f);
                   break;
               }
            case '\0':{
                //cout<<"i="<<i<<"f="<<f;cout<<"case $";print(E);cout<<'\n';
                break;
            }
            case ' ':{
                //cout<<"case space \n";
                String E5= new String("");			
                E5=copy(E,1);
                construct(E5,i,f);
                break;
            }
            default:{   
                //cout<<"i="<<i<<"f="<<f;
                String E5= new String("");			
                int cs=currentstate;
                //cout<<"case default: #E=";print(E);cout<<'\n';
                E5=copy(E,1);
                construct(E5,i,f);
                if (E5.charAt(0)=='.') {
                    //T[i][int(E[0]-97)]=cs+1;
                    Integer value= (Integer)(E.charAt(0)-97);
                    transitions.add(new Transition(i,(String)(transformIndex.get(value)) ,cs+1));
                }
                else {
                    //T[i][int(E[0]-97)]=f;
                    Integer value= (Integer)(E.charAt(0)-97);
                    transitions.add(new Transition(i,(String)(transformIndex.get(value)) ,f));
                }
                break;
            }
        }
    }

    
    public void initializeAftU(){
        for(int i=0;i<maxStates;i++){
            AftU[i] = new Vector();
            AftU2[i] = new Vector();
        }
      
    }
    
    public void initializeER(){
        for(int i=0;i<maxStates;i++){
            EReachable[i] = new Vector();
            visit.add(0);
        }
    }
    
    
    
    public void initialize(){
        for(int i=0;i<maxStates;i++)
            visit.set(i,0);
    }
    
    
    public void readAutomate(){
        transitions.add(new Transition(0, "e",1));
        transitions.add(new Transition(0, "?X",2));
        transitions.add(new Transition(1, "a",1));
        transitions.add(new Transition(1, "a",3));
        transitions.add(new Transition(2, "?X",4));
        transitions.add(new Transition(3, "epsilon",4));
    }
    
    public int size(){
        return transitions.size();
    }
    
    public int toState(int i){
        Transition t= (Transition)(transitions.get(i));
        return t.gettoState();        
    }
    
    public void calculateAftU(Vector [] AftU){
        Transition t= new Transition();
        for(int i=0;i<transitions.size();i++){
            t = (Transition)(transitions.get(i));
            //if(!AftU[t.getfromState()].contains(t.gettoState()))
                AftU[t.getfromState()].add(i);           //t.gettoState()
        }
        
    } 
    
    
    public Vector[] getAftU(){
        return AftU2;   // AftU
    }
    
    public int getMaxStates(){
        return maxStates;
    }
    public int getStartState(){
        return startState;
    }
    
    public Transition TransitionAt(int i){
        return ((Transition)(transitions.get(i)));
    }
    
    public Vector getfinalState(){
        return finalStates;
    }
    
    public int getMaxConcepts(){
        maxConcepts=0;
        for(int i=0;i<maxStates;i++)
            if(AftU[i].size() > maxConcepts)
                maxConcepts = AftU[i].size();
        return maxConcepts;
    }
    
    public void printAutomate(Vector transitions){
        Transition t= new Transition();
        System.out.println("____________________________________________________");
        System.out.println("Automate:");
        System.out.println("__________");        
        for(int i=0;i<transitions.size();i++){
             t= (Transition)(transitions.get(i));
             System.out.println(t.getfromState()+"  "+t.getSymbolState()+"  "+t.gettoState());
             if (maxStates <t.getfromState())
                maxStates= t.getfromState();
             if (maxStates < t.gettoState())
                maxStates = t.gettoState();
        
        }
        System.out.println("____________________________________________________");
    }
    
    
    public void RemoveEpsilons(){
        Vector transitions2 =new Vector();
        Transition t;
        int tostate;
        String symbol;
        
        // copy transitions that have entering symbol other epsilon
        for (int i=0; i<transitions.size();i++){
            t = new Transition(TransitionAt(i));            
            if (!t.getSymbolState().equals("epsilon"))
                transitions2.add(t);
        }
        //printAutomate(transitions2);
        for (int i=0;i<=currentstate;i++)
            for (int j=0;j<=currentstate;j++){
                initialize();
                if (i!=j)
                if (EpsilonReachable(i, j)){
                    //System.out.println(j+ "  epsilon reachable from  "+i);
                    EReachable[i].add(j);
                    if (finalStates.contains(j))
                        if (! finalStates.contains(i))
                            finalStates.add(i);
                }
            }
        
        Transition t1;
        Integer in;
        for (int i=0;i<=currentstate;i++)
            for (int k=0;k<EReachable[i].size();k++){
                in =(Integer)EReachable[i].get(k);
                for (int l=0;l<AftU[in].size();l++){
                    t =TransitionAt((Integer)AftU[in].get(l));                        
                    //System.out.println(t.getfromState()+t.getSymbolState()+t.gettoState()+"  "+i);
                    if ((!t.getSymbolState().equals("epsilon"))){
                        t1= new Transition(t);
                        t1.setFromState(i);
                        t1.setToState(t.gettoState());
                        if (! contain(transitions2,t1))
                            transitions2.add(t1);
                    }            
                }
            }
        transitions= transitions2;
    }
    
    public boolean contain(Vector trans,Transition t){
        Transition t1;
        boolean found=false;
        int i=0;
        while ((!found) && (i<trans.size())){
            t1 = (Transition)trans.get(i);
            if ((t1.getSymbolState().equals(t.getSymbolState()))
            && (t1.getfromState()==t.getfromState()) && (t1.gettoState()==t.gettoState()))
                found =true;
            i++;
        }
         return found;   
    }
    
    public boolean EpsilonReachable(int state1,int state2){
        boolean ereachable=false,found=false;
        int i=0,tostate;
        String symbol;
        Transition t;
        while ((!found) &&(i<AftU[state1].size())){
            t = TransitionAt((Integer)AftU[state1].get(i));
            symbol = t.getSymbolState();
            tostate =t.gettoState();           
            if(symbol.equals("epsilon")){   
                //EReachable[state1].add(tostate);
                if (tostate==state2){
                    ereachable =true;
                    found =true;
                }
                else if (!visited(tostate)){
                    visit.set(tostate,1);        
                    if (EpsilonReachable(tostate,state2)) 
                       ereachable =true;    // if (! EReachable[state1].contains(tostate))
                                            //   EReachable[state1].add(tostate);
                }
            }
            i++;
        }        
        return ereachable;
    }
    
    public boolean finalState(int s){
        if (finalStates.contains(s))
            return true;
        else return false;
    }
    
    public boolean visited(int s){
        if ((Integer)visit.get(s)==1)
            return true;
        else return false;
    }
    
    public int getCurrentState(){
        return currentstate;
    }
    
    
    public boolean getClosure(){
        return closure;
    }
    
    public Vector getTransitions(){
        return transitions;
    }
   
    private int startState;
    private Vector finalStates;
    private Vector transitions;
    private Vector[] AftU;
    private Vector[] AftU2;
    private int maxStates;
    private int maxConcepts;
    
    private String E1,E2;
    private int inputcursor;
    private int currentindex=-1;
    private int currentstate;
    private String transformedExp;
    private Vector transformIndex;
    private Vector[] EReachable;
    private Vector visit;
    private boolean closure;
}
