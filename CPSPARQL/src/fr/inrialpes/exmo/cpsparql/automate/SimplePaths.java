/*
 * SimplePaths.java
 *
 * Created on April 29, 2006, 12:48 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;


import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.*;
import java.io.*;

/**
 *
 * @author faisal
 */
public class SimplePaths {
    
    /** Creates a new instance of SimplePaths */
    public SimplePaths(String X,int node, String E, String Y, Vector lambda, MultiGraph G1) {
        //System.out.println("we are SolvUniveralRPQ():"+X+"  "+"  "+E+"  "+Y+ "   node="+node);
        initialization(E,G1);
            if (node >=0)
            startFindPaths2(node,E,Y,lambda);
        else
            startFindPaths(node,X,E,Y,lambda);
    }
    
    
    public void startFindPaths(int n,String X,String E, String Y,Vector globalLambda){
        solutions = new Vector();
        pathS = new PathSolution();
        int s0= A.getStartState();
        Node node;
        Vector visited;
        if ((X.charAt(0)=='?')|| (X.charAt(0)=='$') || (X.charAt(0)=='_')){
               VarImage vi; 
               int location =hasImage(globalLambda,X);
               if (location >= 0){                  
                   vi=(VarImage)globalLambda.get(location);
                   visited = new Vector();
                   find(vi.getIndex(), vi.getIndex(), s0, globalLambda,Y,visited);                   
               }
               else{    
                   for(int k=0;k< G.getNodes().size();k++){
                        Vector tlambda = new Vector(globalLambda);
                        node= G.getNode(k);
                        vi = new VarImage(X,node.getLabel(),k);
                        tlambda.add(vi);
                        //System.out.println(k+"$"+node.getLabel());
                        visited = new Vector();
                        find(k, k, s0, tlambda,Y,visited);
                    }

               }
        }
        else{
            //System.out.println("X is not a variable");
            Node node1 = new Node(X);
            int index=G.contain(node1.getLabel());
            visited = new Vector();
            find(index,index, s0, globalLambda,Y,visited);
        }
            
    }
   
    public void startFindPaths2(int node,String E, String Y,Vector globalLambda){
        System.out.println("X is not a variable");
        solutions = new Vector();
        pathS = new PathSolution();        
        int s0= A.getStartState();
        Vector visited = new Vector();
        find(node,node, s0, globalLambda,Y,visited);
    }
   
    public void find(int v0,int v, int s0, Vector gLambda, String Y,Vector visited){
        if (A.finalState(s0)){
            if (Y.length()==0) ; //do nothing
            else if ((Y.charAt(0)=='?')|| (Y.charAt(0)=='$') || (Y.charAt(0)=='_')){
                VarImage vi;             
                int location =hasImage(gLambda,Y);                
                if (location >= 0){
                    vi=(VarImage)gLambda.get(location);
                    if (vi.getImage().equals(G.getNode(v).getLabel())){
                        pathS.setStartPoint(v0);
                        pathS.setEndtPoint(v);
                        pathS.setLambda(gLambda);
                        if(! contains(solutions,pathS))
                            solutions.add(pathS);
                        pathS = new PathSolution();
                    }
                }
                else{
                   // System.out.println("no");
                    //vi=new VarImage(Y, v);
                    //System.out.println("yes");
                    // lambda.add(vi);                
                    pathS.setStartPoint(v0);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(gLambda);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
                }
            }
            else if (Y.equals(G.getNode(v).getLabel())){
                //System.out.println("Y not variables");
                //pathS = new PathSolution();
                pathS.setStartPoint(v0);
                pathS.setEndtPoint(v);
                pathS.setLambda(gLambda);
                if(! contains(solutions,pathS))
                    solutions.add(pathS);
                pathS = new PathSolution();
            }            
        }
        
       
        
        
        // for each triple <v0,el,v> in G
        Node node =G.getNode(v0);
        //System.out.println("1        node.getLabel()="+node.getLabel());
        Edge edge;
        Transition tran = new Transition();
        Vector edges;
        int transIndex;
        edges = node.getEdges();
        for(int counter=0;counter<(edges.size());counter++){
            edge =(Edge)edges.get(counter);                    
            v=edge.getNode(1).getId();
            //System.out.println("2        "+edge.getNode(1).getLabel());
            //for each triple <s0,tl,s> in P
            for(int i=0;i<AftU[s0].size();i++){
                transIndex=AU[s0][i];
                tran = A.TransitionAt(transIndex);
                
                 //for each theta in match(tl,el)
                Vector tempLambda = new Vector(gLambda);        
                if (matchAndMerge(tran.getSymbolState(), edge.getLabel(), tempLambda,v)){
                    visitedNode = (String)visited.get(edge.getNode(1).getId());
                    if (!visitedNode.equals("true")){
                        visited.add(edge.getNode(1).getId(),"true");
                        find(v0,edge.getNode(1).getId(), tran.gettoState(),tempLambda,Y,visited);
                    }
                }
                 //   W.add(new Triple(edge.getNode(1).getId(), tran.gettoState(),tempLambda));
                
            }
        }
        
    }  // end find
   
    public void find(int v0,int v, int s0, Vector gLambda, String Y ){
        R = new Vector();         //initialize reach set
        W = new Vector();         //initialize work list
        
        // for each triple <v0,el,v> in G
        Node node =G.getNode(v0);
        //System.out.println("1        node.getLabel()="+node.getLabel());
        Edge edge;
        Transition tran = new Transition();
        Vector edges;
        int transIndex;
        edges = node.getEdges();
        for(int counter=0;counter<(edges.size());counter++){
            edge =(Edge)edges.get(counter);                    
            v=edge.getNode(1).getId();
            //System.out.println("2        "+edge.getNode(1).getLabel());
            //for each triple <s0,tl,s> in P
            for(int i=0;i<AftU[s0].size();i++){
                transIndex=AU[s0][i];
                tran = A.TransitionAt(transIndex);
                
                 //for each theta in match(tl,el)
                Vector tempLambda = new Vector(gLambda);        
                if (matchAndMerge(tran.getSymbolState(), edge.getLabel(), tempLambda,v))
                    ;
                    //W.add(new Triple(edge.getNode(1).getId(), tran.gettoState(),tempLambda));
                /*if (A.finalState(s0))                  
                    W.add(new Triple(v0, tran.gettoState(),tempLambda));*/
            }
        }
        
        E = new Vector();
        int first =0;
        Triple triple;
        int s,s1,v1;
        Vector theta,theta1,theta2;
        while (exists(W, first)){   //initialize query result
            R.add(W.get(first));    //R = R U {<v,s,theta>}
            
            triple = (Triple)W.get(first);
            v = triple.getSubject();
            s = triple.getPredicate();
            theta=triple.getobject();
            theta2 =theta;
            
            // for each triple <v,el,v1> in G
            node =G.getNode(v);
            edges = node.getEdges();
            for(int counter=0;counter<(edges.size());counter++){
                edge =(Edge)edges.get(counter);                
                v1=edge.getNode(1).getId();
                    
                //for each triple <s,tl,s1> in P
                for(int i=0;i<AftU[s].size();i++){
                    transIndex=AU[s][i];
                    tran = A.TransitionAt(transIndex);
                    s1=tran.gettoState();
                    
                    theta2=new Vector(theta);
                   
                    //for each theta1 in match(tl,el)
                    //System.out.println(tran.getSymbolState()+"$"+ edge.getLabel()+"$"+G.getNode(v1).getLabel()+G.getNode(v).getLabel());                        
                    //System.out.println("Theta:");
                    //print(theta2);
                   
                    if (matchAndMerge(tran.getSymbolState(), edge.getLabel(), theta2,v1)){
                        if (!contain(R,v1, s1,theta2 ))
                            ;
                            //W.add(new Triple(v1, s1,theta2));
                    }
                    
                }
            }
            //System.out.println("Here we print Y="+Y);
           
            if (Y.length()==0) ; //do nothing
            else if ((Y.charAt(0)=='?')|| (Y.charAt(0)=='$') || (Y.charAt(0)=='_')){
                VarImage vi;             
                int location =hasImage(theta2,Y);                //theta2=globalLambda
                if (location >= 0){
                    vi=(VarImage)theta2.get(location);
                    if ((A.finalState(s)) && ((vi.getImage().equals(G.getNode(v).getLabel())))){
                        pathS.setStartPoint(v0);
                        pathS.setEndtPoint(v);
                        pathS.setLambda(theta2);
                        if(! contains(solutions,pathS))
                            solutions.add(pathS);
                        pathS = new PathSolution();
                    }
                }
                else{
                    //System.out.println("no");
                    //vi=new VarImage(Y, v);
                    if (A.finalState(s)){
                        //System.out.println("yes");
                       // lambda.add(vi);                
                        pathS.setStartPoint(v0);
                        pathS.setEndtPoint(v);
                        pathS.setLambda(theta2);
                        if(! contains(solutions,pathS))
                            solutions.add(pathS);
                        pathS = new PathSolution();
                    }
                }


        }
        else if ((A.finalState(s))&&(Y.equals(G.getNode(v).getLabel()))){
              //System.out.println("Y not variables");
              //pathS = new PathSolution();
              pathS.setStartPoint(v0);
              pathS.setEndtPoint(v);
              pathS.setLambda(theta2);
              if(! contains(solutions,pathS))
                  solutions.add(pathS);
              pathS = new PathSolution();
            }
            
            first++;                //W = W - {<v,s,theta>}
        }// endWhile               
        
        
        //the following condition is for *, and can be deleted with the function addToSolution()
        if (A.finalState(s0)){                  
                    Vector tempLambda = new Vector(gLambda);
                    addToSolution(v0, tempLambda,Y);
        }
        
    }  // end find
    
    
    public void addToSolution(int v,Vector theta2,String Y){        
        
        if ((Y.charAt(0)=='?')|| (Y.charAt(0)=='$') || (Y.charAt(0)=='_')){
            VarImage vi;             
            int location =hasImage(theta2,Y);                //theta2=globalLambda
            if (location >= 0){
                vi=(VarImage)theta2.get(location);
                if (vi.getImage().equals(G.getNode(v).getLabel())){
                    pathS.setStartPoint(v);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(theta2);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
                }
            }
            else{            
                    pathS.setStartPoint(v);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(theta2);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
            }
        }
        else if (Y.equals(G.getNode(v).getLabel())){
              pathS.setStartPoint(v);
              pathS.setEndtPoint(v);
              pathS.setLambda(theta2);
              if(! contains(solutions,pathS))
                  solutions.add(pathS);
              pathS = new PathSolution();
            }
    }
    
    public boolean contains(Vector solutions, PathSolution solution){
        int i=0;
        boolean found=false;
        PathSolution ps = new PathSolution();
        while((i<solutions.size()) && (!found)){
            ps =(PathSolution)(solutions.get(i));
            if((ps.getEndPoint()==solution.getEndPoint()) 
            && (ps.getStartPoint()==solution.getStartPoint()) 
            && (ps.getLambda().containsAll(solution.getLambda()))
            )
                found=true;
            i++;
        }
        return found;
    }
    
    public boolean contain(Vector R, int s,int p,Vector theta){
        boolean found =false;
        int i=0;
        Triple triple;
        while ((!found) && (i<R.size())){
            triple = (Triple)(R.get(i));
            if ((triple.getSubject()==s) &&(triple.getPredicate()==p) 
            &&(triple.getobject().equals(theta)))
                found=true;
            i++;
        }
        return found;
    }
    
    public boolean exists(Vector v,int first){
        boolean isNotEmpty =true;
        int end= v.size();
        if ((end-first)>0)
            isNotEmpty= true;
        else isNotEmpty =false;
        return isNotEmpty;
    }
    
    public boolean variable(String str){
        //System.out.println("we are at variable():"+str);
        if ((str.charAt(0)=='?')|| (str.charAt(0)=='$') || (str.charAt(0)=='_'))
            return true;
        else return false;
    }
    
    public void print(Vector theta){
        VarImage vi;
        for(int i=0;i<theta.size();i++){
            vi=(VarImage)theta.get(i);
            System.out.println("("+vi.getVar()+","+vi.getImage()+"),");
        }
    }
    
    public boolean matchAndMerge(String symbol,String predicate, Vector gLambda,int v){
        //System.out.println("$"+symbol+"$    predicate=$"+predicate+"$");
        //print(gLambda);
        boolean matchResult =false;
        VarImage vi; 
        int location =hasImage(gLambda,symbol);
        //System.out.println("$"+symbol+"$    predicate=$"+predicate+"$");
        if (variable(symbol)){
            if (location < 0){
                vi= new VarImage(symbol, predicate, v);//verify that v is the choice, may be v0 instead
                gLambda.add(vi);           
                matchResult=true;
             }
            else if(InVarImage(gLambda, symbol, predicate)){                            
                matchResult = true;            
            }
        }
        else if(symbol.charAt(0)=='!') {
                        String  str=new String("");
                        str =A.copy(symbol, 1);
                        if(!str.equals(predicate))
                            matchResult = true;                                    
        }
        else if(symbol.charAt(0)=='#') {
            matchResult = true;                    
        }
        else if (predicate.equals(symbol))
            matchResult=true;
        else matchResult=false;        
        return matchResult;
    }
    
    
    public boolean InVarImage(Vector v, String var, String str){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if ((var.equals(vi.getVar())) &&(str.equals(vi.getImage()))){
                found =true;
                location=i;
            }
            i++;
        }
        return found;
    }
    
     public int hasImage(Vector v, String var){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if (var.equals(vi.getVar())){
                found =true;
                location=i;
            }
            i++;
        }
        return location;
    }
   
    
     public void initialization(String E,MultiGraph G1){        
        A= new Automate(E);
        maxStates = A.getMaxStates();         
        maxConcepts = A.getMaxConcepts();         
        G = G1;
        AftU=new Vector[maxStates];
        initialize(AftU);
        AftU =A.getAftU();
        copyto();
        markVector=new Vector();
        tempLambda = new Vector();
    }
     
     public void initialize(Vector[] v){
        for(int i=0;i<maxStates;i++)
            v[i] = new Vector();
    }
    
      public void copyto(){
        AU = new Integer[maxStates][maxConcepts];
        for(int i=0;i< maxStates;i++)
            AftU[i].copyInto(AU[i]);
        //printAftU(AU);
    }
      
      
    public Vector getSolutions(){
        return solutions;
    }
      
      private Vector R;
      private Vector W;
      private Vector E;
      private Automate A;
      private int maxStates;
      private int maxConcepts;
      private MultiGraph G;
      private Vector[] AftU;
      private Vector solutions;
      private Integer [][] AU; 
      private Vector markVector;        
      private Vector tempLambda;
      private PathSolution pathS;
      private String GraphG;
      private int finalState;    
      private String visitedNode;
}
