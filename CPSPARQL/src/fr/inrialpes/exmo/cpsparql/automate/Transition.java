/*
 * Automate.java
 *
 * Created on February 1, 2006, 9:43 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;

/**
 *
 * @author faisal
 */
public class Transition {
    
    /** Creates a new instance of Automate */
    public Transition() {
        
    }
    
    /** Creates a new instance of Automate */
    public Transition(int i,String s,int j) {
        fromState=i;
        toState=j;
        symbol=s;
    }
   
     /** copy constructor */
    public Transition(Transition t) {
        this.fromState=t.fromState;
        this.toState=t.toState;
        this.symbol=new String(t.symbol);
    }
   
    int getfromState(){
        return fromState;        
    }
    int gettoState(){
        return toState;        
    }
    
    String getSymbolState(){
        return symbol;        
    }
    
    public void setFromState(int f){
        fromState=f;
    }
    
    
    public void setToState(int t){
        toState=t;
    }
    
    
    private int fromState;
    private String symbol;
    private int toState;
    
}
