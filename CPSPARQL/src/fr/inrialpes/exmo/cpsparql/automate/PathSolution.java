/*
 * PathSolution.java
 *
 * Created on February 12, 2006, 10:23 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;

import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.VarImage;

/**
 *
 * @author faisal
 */
public class PathSolution {
    
    /** Creates a new instance of PathSolution */
    public PathSolution(int sp, Vector lam, int ep) {
        lambda = new Vector(lam);
        startPoint = sp;
        endPoint= ep;
        
    }
    
     /** Creates a new instance of PathSolution */
    public PathSolution() {
        lambda = new Vector();
        startPoint = -1;
        endPoint= -1;
        
    }
   
    
     /** copy consctructor */
    public PathSolution(PathSolution ps) {
        lambda = new Vector(ps.lambda);
        startPoint = ps.startPoint;
        endPoint= ps.endPoint;
        
    }
   
    
    public void setLambda(Vector v){
        lambda= new Vector(v);
    }
    
    public void setStartPoint(int sp){
        startPoint = sp;
    }
    
    
    public void setEndtPoint(int ep){
        endPoint = ep;
    }
    
    public Vector getLambda(){
        return lambda;
    }
    
    public int getStartPoint(){
        return startPoint;
    }
    
    
    public int getEndPoint(){
        return endPoint;
    }
    
    public String print(){
        //System.out.println("____________________________________________________");        
        String s = new String("");
        String s1 = new String("");
        //System.out.println("("+startPoint+","+endPoint+")");
        s1=endPoint+" ";
       /* for(int i=0;i<lambda.size();i++){
            VarImage vi = new VarImage();
            vi = (VarImage)(lambda.get(i));
             if (i<lambda.size()-1)
                   s += "("+vi.getVar()+","+vi.getImage()+")"+",";
               else
                   s +=  "("+vi.getVar()+","+vi.getImage()+")";        
        }
        System.out.println("Lambda("+startPoint+","+endPoint+")="+"{"+s+"}");*/
       // System.out.println("____________________________________________________");
        return s1;

    }
    
    private Vector lambda;
    private int startPoint;
    private int endPoint;
    
}
