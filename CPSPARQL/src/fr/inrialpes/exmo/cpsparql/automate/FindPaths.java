/* * FindPaths.java
 *
 * Created on February 1, 2006, 11:31 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;

import java.util.Vector;
import fr.inrialpes.exmo.cpsparql.findpathprojections.*;
import java.io.*;

/**
 *
 * @author faisal
 */
public class FindPaths {
    
    /**
     * Creates a new instance of FindPaths 
     */
    public FindPaths(String X,int node, String E, String Y, Vector lambda, MultiGraph G1) {
        
        
        initialization(E,G1);
        if (node >=0)
            startFindPaths2(node,E,Y,lambda);
        else
            startFindPaths(node,X,E,Y,lambda);
    }

    
    public void find(int u,int v, int s, Vector lambda, String Y ){
        //System.out.println(u+"  "+s+" "+v+"  "+Y);                                          
        Node node;
        if ((Y.charAt(0)=='?')|| (Y.charAt(0)=='$') || (Y.charAt(0)=='_')){
            VarImage vi;             
            int location =hasImage(lambda,Y);                
            if (location >= 0){
                vi=(VarImage)lambda.get(location);
                if ((A.finalState(s)) && ((vi.getImage().equals(G.getNode(v).getLabel())))
                //|| (G.getNode(v).getLabel().charAt(0)=='_')
                ){
                    pathS.setStartPoint(u);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(lambda);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
                }
            }
            else{
                //System.out.println("no");
                //vi=new VarImage(Y, v);
                if (A.finalState(s)){
                    //System.out.println("yes");
                   // lambda.add(vi);                
                    pathS.setStartPoint(u);
                    pathS.setEndtPoint(v);
                    pathS.setLambda(lambda);
                    if(! contains(solutions,pathS))
                        solutions.add(pathS);
                    pathS = new PathSolution();
                }
            }
               
            
        }
        else if ((A.finalState(s))&&(Y.equals(G.getNode(v).getLabel()))){
              //System.out.println("solution "+u);
              //pathS = new PathSolution();
              pathS.setStartPoint(u);
              pathS.setEndtPoint(v);
              pathS.setLambda(lambda);
              if(! contains(solutions,pathS))
                  solutions.add(pathS);
              pathS = new PathSolution();
            }
        Transition tran = new Transition();
        setMark(v,s);                    
        for(int i=0;i<AftU[s].size();i++){
            int transIndex=AU[s][i];
            tran = A.TransitionAt(transIndex);
            if(tran.getSymbolState().equals("epsilon"))
                        //if( mark(v)!=s)
                            find(u, v,tran.gettoState(), lambda,Y);
            else{
                node =G.getNode(v);
                Vector edges;
                edges = node.getEdges();
                for(int counter=0;counter<(edges.size());counter++){
                    Edge edge =(Edge)edges.get(counter);                    
                    //TestAndAdd(u, edge.getNode(1).getId(), tran.gettoState(), lambda, Y);
                    if(tran.getSymbolState().equals(edge.getLabel())){
                        if (( mark(edge.getNode(1).getId())==-1) ||!closure) {
                            find(u, edge.getNode(1).getId(),tran.gettoState(), lambda,Y);
                        }
                    }
                    else if(tran.getSymbolState().charAt(0)=='!') {
                        String  str=new String("");
                        str =A.copy(tran.getSymbolState(), 1);
                        if(!str.equals(edge.getLabel()))
                            if (( mark(edge.getNode(1).getId())==-1) ||!closure)
                                find(u, edge.getNode(1).getId(),tran.gettoState(), lambda,Y);
                    }
                    else if(tran.getSymbolState().charAt(0)=='#') {
                        if (( mark(edge.getNode(1).getId())==-1) ||!closure)
                            find(u, edge.getNode(1).getId(),tran.gettoState(), lambda,Y);
                    }                                        
                    else if((tran.getSymbolState().charAt(0)=='?') || 
                            (tran.getSymbolState().charAt(0)=='$') ||
                            (tran.getSymbolState().charAt(0)=='_')){                        
                        Vector TempLambda = new Vector(lambda);
                        VarImage vi; 
                        int location =hasImage(lambda,tran.getSymbolState());
                            if (location < 0){
                                     if ((mark(edge.getNode(1).getId())==-1) ||!closure){
                                         vi= new VarImage(tran.getSymbolState(), edge.getLabel(), v); 
                                         TempLambda.add(vi);           
                                         find(u, edge.getNode(1).getId(),tran.gettoState(), TempLambda,Y);
                                     }
                             }
                            else if(InVarImage(lambda, tran.getSymbolState(), edge.getLabel())){                            
                                 if ((mark(edge.getNode(1).getId())==-1) ||!closure)
                                     find(u, edge.getNode(1).getId(),tran.gettoState(), TempLambda,Y);
                            }
                        }        
            }   
            }
        }
        unmark(v);
    }
    
    public String copy(String triple, char c, int position){
        String str= new String("");
        int i=position;
        while((i<triple.length()) && (triple.charAt(i)!=c)){
            str+=triple.charAt(i);
            i++;
        }
        return str;
    }
    
    public void startFindPaths(int n,String X,String E, String Y,Vector lambda){
        solutions = new Vector();
        pathS = new PathSolution();
        int s0= A.getStartState();
        Node node;
        if ((X.charAt(0)=='?')|| (X.charAt(0)=='$') || (X.charAt(0)=='_')){
               VarImage vi; 
               int location =hasImage(lambda,X);
               if (location >= 0){                  
                   vi=(VarImage)lambda.get(location);
                   find(vi.getIndex(), vi.getIndex(), s0, lambda,Y);                   
               }
               else{    
                   for(int k=0;k< G.getNodes().size();k++){
                        Vector tlambda = new Vector(lambda);
                        node= G.getNode(k);
                        vi = new VarImage(X,node.getLabel(),k);
                        tlambda.add(vi);
                        //System.out.println(k+"$"+node.getLabel());
                        find(k, k, s0, tlambda,Y);
                    }

               }
        }
        else{
            Node node1 = new Node(X);
            int index=G.contain(node1.getLabel());
            find(index,index, s0, lambda,Y);
        }
            
    }
   
    public void startFindPaths2(int node,String E, String Y,Vector lambda){
        solutions = new Vector();
        pathS = new PathSolution();        
        int s0= A.getStartState();
        find(node,node, s0, lambda,Y);
    }
   
    public int mark(int index){
        Node node =G.getNode(index);
        return node.getMark();
    }
    
    public void setMark(int index,int s){
        Node node =G.getNode(index);
        node.setMark(s);
    }
    
    
    public void unmark(int index){
        Node node =G.getNode(index);
        node.setMark(-1);
    }

    
    public Vector getSolutions(){
        return solutions;
    }
    
    public void printSolutions(){
        for(int i=0;i<solutions.size();i++){
            pathS = (PathSolution)(solutions.get(i));
            //pathS.print();
        }
    }
    
    public boolean contains(Vector solutions, PathSolution solution){
        int i=0;
        boolean found=false;
        PathSolution ps = new PathSolution();
        while((i<solutions.size()) && (!found)){
            ps =(PathSolution)(solutions.get(i));
            if((ps.getEndPoint()==solution.getEndPoint()) 
            && (ps.getStartPoint()==solution.getStartPoint()) 
            && (ps.getLambda().containsAll(solution.getLambda()))
            )
                found=true;
            i++;
        }
        return found;
    }
    
    public int hasImage(Vector v, String var){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if (var.equals(vi.getVar())){
                found =true;
                location=i;
            }
            i++;
        }
        return location;
    }
    
    public boolean InVarImage(Vector v, String var, String str){
        boolean found =false;
        int i=0;
        VarImage vi;
        int location =-1;
        while((i<v.size())&&(!found)){
            vi =(VarImage)(v.get(i));
            if ((var.equals(vi.getVar())) &&(str.equals(vi.getImage()))){
                found =true;
                location=i;
            }
            i++;
        }
        return found;
    }
    
    public void printVarImage(Vector v){
        VarImage vi = new VarImage();
        String s=new String("");
        System.out.println("____________________________________________________");        
        for(int i=0;i<v.size();i++){
            vi=(VarImage)(v.get(i));
             if (i<v.size()-1)
                   s += "("+vi.getVar()+","+vi.getImage()+")"+",";
               else
                   s +=  "("+vi.getVar()+","+vi.getImage()+")";        
        }
        System.out.println("VarImage= {"+s+"}");
        System.out.println("____________________________________________________");
        
    }
    
    
    
    
    public void initialization(String E,MultiGraph G1){        
        A= new Automate(E);
        maxStates = A.getMaxStates();         
        maxConcepts = A.getMaxConcepts();         
        G = G1;
        AftU=new Vector[maxStates];
        initialize(AftU);
        AftU =A.getAftU();
        copyto();
        markVector=new Vector();
        tempLambda = new Vector();
        closure = A.getClosure();
        //finalState=A.getfinalState();       
    }
    
    
    public void initialize(Vector[] v){
        for(int i=0;i<maxStates;i++)
            v[i] = new Vector();
    }
    
      public void copyto(){
        AU = new Integer[maxStates][maxConcepts];
        for(int i=0;i< maxStates;i++)
            AftU[i].copyInto(AU[i]);
        //printAftU(AU);
    }
  
      
      public void printAftU(Integer [][] AftU){
        System.out.println("____________________________________________________");
        String s =new String("");
        for(int i=0;i<= A.getCurrentState();i++){
            for(int j=0;j< A.getAftU()[i].size();j++){
               //System.out.println("AftU["+i+"]["+j+"]"+AftU[i][j]);
               if (j<A.getAftU()[i].size()-1)
                   s += AftU[i][j]+",";
               else
                   s += AftU[i][j];
            }
            System.out.println("AftU["+i+"]:{"+s+"}");
            s="";
        }
        System.out.println("____________________________________________________");

    }
    

      
   
    private Automate A;
    private int maxStates;
    private int maxConcepts;
    private MultiGraph G;
    private Vector[] AftU;
    private Vector solutions;
    private Integer [][] AU; 
    private Vector markVector;        
    private Vector tempLambda;
    private PathSolution pathS;
    private String GraphG;
    private int finalState;
    private boolean closure;
}
