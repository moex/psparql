/*
 * ConceptMark.java
 *
 * Created on February 2, 2006, 7:19 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;

import java.util.Vector;

/**
 *
 * @author faisal
 */
public class ConceptMark {
    
    /** Creates a new instance of ConceptMark */
    public ConceptMark() {
        concept = new String("");
        mark =-1;
    }
    
    public ConceptMark(String s,int i) {
        concept = new String(s);
        mark=i;
    }
    
    /** copy constructor */
    public ConceptMark(ConceptMark cm) {
        this.concept = new String(cm.concept);
        this.mark=cm.mark;
    }
    
    public String getConcept(){
        return concept;
    }
    
    public int getMark(){
        return mark;
    }

    /*public boolean InMark(int m){
        if(mark.contains(m))
            return true;
        else return false;
    }*/
    
    public void mark(){
            mark=1;
    }
    
    public void unmark(){
        mark=-1;
    }
    
    private String concept;
    private int mark;
}
