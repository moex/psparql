/*
 * TriplePath.java
 *
 * Created on March 20, 2006, 2:03 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.automate;

/**
 *
 * @author faisal
 */
public class TriplePath {
    
    /** Creates a new instance of TriplePath */
    public TriplePath(int s,String p,int o) {
        subject=s;
        predicate= new String(p);
        object=o;
    }
    
    /** copy constructor */
    public TriplePath(TriplePath tp) {
        this.subject=tp.subject;
        this.predicate= new String(tp.predicate);
        this.object=tp.object;
    }
    
    
    private int subject;
    private String predicate;
    private int object;
}
