/*
 * TurtleDocumentParser.java
 *
 * Created on March 29, 2006, 1:33 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.turtleParser;

import fr.inrialpes.exmo.cpsparql.findpathprojections.MultiGraph;
import java.net.URLConnection;
import java.util.Vector;
import java.io.*;
import java.net.URL;

/**
 *
 * @author faisal
 */
public class TurtleDocumentParser {
    
    /** Creates a new instance of TurtleDocumentParser */
    public TurtleDocumentParser(Vector RDFGraphsURIs, boolean qContainInverseOpertor) {
        isLocalFile = true;
        String GraphURI= new String();
        String stringGraph=new String("");
        RDFgraph = new MultiGraph(qContainInverseOpertor);
        nomOfErrors=0;       
        exceptions = new Vector();
        NumRDFGraphsURIs = RDFGraphsURIs.size();        
        for(int i=0;i<RDFGraphsURIs.size();i++){
            graphNum=(i+1);
            GraphURI = (String)RDFGraphsURIs.get(i);
            GraphURI = GraphURI.substring(GraphURI.indexOf('<')+1, GraphURI.indexOf('>'));            
            
            try {
                //System.out.println("faisal");
                
                // start Loading Time
                //stringGraph=openFile(GraphURI);//+'\n'; //here we do the merge of text RDF graphs, blanks are renamed during parsing
                startLoadTimeS = System.currentTimeMillis()/1000;
                if(!(GraphURI.contains("http://") || GraphURI.contains("ftp://")))
                    stringGraph = openLocalFile(GraphURI);
                else 
                    stringGraph = openWebFile(GraphURI);
                isLocalFile = true;
                elapsedLoadTimeS = System.currentTimeMillis()/1000-startLoadTimeS;
                //System.out.println("Elapsed Load Time Seconds = "+elapsedLoadTimeS);
                //end Loading Time
                
                initialize(stringGraph);
                start();
                if (Debug) 
                    RDFgraph.print();
            }
            catch(Exception e){
                exceptions.add(e);
            }
        }        
        if (Debug) 
            RDFgraph.print();
        if (RDFgraph.getHashTOfNodes().isEmpty())
            System.out.println("End file loading:\n num of triples="+numOfTriples);

    }
    
    
    public void initialize(String Graph){
        Debug = false;
        strsIndex =0;
        graph = Graph;//graph = (String)strs.get(strsIndex++);
        if (Debug)  
            System.out.println(graph);
        index=-1;
        prefixes=new Vector();
        prefixesNames=new Vector();
    }
        public Vector getExceptions(){
        return exceptions;
    }
    
    public MultiGraph getGraph() {
        return RDFgraph;
    }
    
    /**
     * return only the extension of the file (e.g., .html)
     * @param fileName the name of the File with the extension
     * @return only the extension of the file (e.g., .html)
     */
    private static String getFileExtension(String fileName) {
      int whereDot = fileName.lastIndexOf('.');
      if (0 < whereDot && whereDot <= fileName.length() - 2 ) {
        return fileName.substring(whereDot, fileName.length());
      }
      return "";
    }

    public String openLocalFile(String uri) {
        if (TurtleDocumentParser.getFileExtension(uri).equalsIgnoreCase(".ttl")){
            uri = uri.substring(uri.indexOf(':')+1);
       
            File turtleFile = new File(uri);
            if (!turtleFile.isAbsolute())
                turtleFile = new File(turtleFile.getAbsolutePath());
            StringBuffer s = new StringBuffer();

            InputStream fr = null;
            try{fr = new FileInputStream(turtleFile);
                fr = new BufferedInputStream(fr);
                try{                
                        long lg = turtleFile.length(), i = 0;

                        while(i++ < lg){
                            s.append((char) fr.read());
                        }
                        fr.close();               
                }catch(IOException e){System.out.println(e);isLocalFile = false;}
            }
            catch(FileNotFoundException e1){isLocalFile = false;}

            //System.out.println(s.toString());
            return s.toString();
        }
        else  if (TurtleDocumentParser.getFileExtension(uri).equalsIgnoreCase(".xhtml") || TurtleDocumentParser.getFileExtension(uri).equalsIgnoreCase(".xml")) {
            
            return TurtleDocumentParser.extractTriples(uri, true);
        }
        else {
             isLocalFile = false;
             return "";
        } 
            
    }

    public static String extractTriples(String uri, boolean online){
        if (online){
            String magicURI;
            magicURI = "http://www.w3.org/2007/08/pyRdfa/extract?format=nt&uri="+uri;
            StringBuffer s = new StringBuffer();
            try{
                URL url = new URL(magicURI);
                URLConnection connection = url.openConnection();
                InputStream urlData = connection.getInputStream();

                //String contentType = connection.getContentType();
                /*if (contentType == null || contentType.startsWith("text") == false)
                   throw new Exception("URL does not refer to a text file.");
                */
                while (true) {                
                    int data = urlData.read();
                    if (data < 0)
                        break;
                    s.append((char)data);
                }
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
            //System.out.println("extracting files...\n"+s.toString());
            return s.toString();      
        }
        else {
            //RDFa Extractor
            /*RDFaExtractorCore rdfx = new RDFaExtractorCore(false);
            try {
                URI u = new URI(uri);
                Vector inputDocURIList = new Vector();
		inputDocURIList.add(u);
		rdfx.processMultiple(inputDocURIList, "N-TRIPLE", true);
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
             */
            //SweetWiki
            /*String[] t = new String[1];
            t[0] = uri;
            RDFaParser.main(t);
             */
            return "offline test";
        }
             
    }
    
     public String openLocalFileOld(String URI){
        if (Debug)  
            System.out.println("Loading the file at..."+URI);
        URI = URI.substring(URI.indexOf(':')+1);
        File turtleFile = new File(URI);
        FileReader tFile;
        BufferedReader in;
        String str1="",str="";                                 
        strs = new Vector();
        try{
              tFile = new FileReader(turtleFile);
              in = new BufferedReader(tFile);    
              int counter = 0;
              while((str=in.readLine()) !=null){
                  str1+=str+'\n';
                  /*if (counter >= 5000){
                      System.out.println("yes");
                      strs.add(str1);
                      str1 = "";
                      counter = 0;
                  }
                  counter++;*/
                  //if (Debug) System.out.println(str);
                  
             }
           System.out.println("end reading the file");
           tFile.close();
         } catch (IOException e) {if (Debug)  System.out.println(e); } 
        return str1;
    }

    
    public String openWebFile(String URI) throws Exception{
        if (Debug)  
            System.out.println("Loading The File from the web...");
        //System.out.println("Loading The File...$"+URI+"$");
        //File turtleFile = new File(URI);
        //FileReader tFile;
       // BufferedReader in;
        //String str1="",str=""; 
       // System.out.println(URI);
        //System.out.println(TurtleDocumentParser.getFileExtension(URI));
        if (TurtleDocumentParser.getFileExtension(URI).equalsIgnoreCase(".ttl")){
                StringBuffer s = new StringBuffer();
                try{
                    URL url = new URL(URI);
                    URLConnection connection = url.openConnection();
                    InputStream urlData = connection.getInputStream();

                    String contentType = connection.getContentType();
                    if (contentType == null || contentType.startsWith("text") == false)
                       throw new Exception("URL does not refer to a text file.");

                    while (true) {                
                     int data = urlData.read();
                     if (data < 0)
                        break;
                      s.append((char)data);
                  }



                     /* tFile = new FileReader(turtleFile);
                      in = new BufferedReader(tFile);    
                      while((str=in.readLine()) !=null)
                          str1+=str;*/


                      if (Debug)    System.out.println(s);
                      //tFile.close();
                 } catch (IOException e) {if (Debug)  System.out.println(e); } 
                //System.out.println(str1);
                return s.toString();
        }
        else  if (TurtleDocumentParser.getFileExtension(URI).equalsIgnoreCase(".xhtml") || TurtleDocumentParser.getFileExtension(URI).equalsIgnoreCase(".xml")) {
            return TurtleDocumentParser.extractTriples(URI, true);
        }
        else return "";
        
    }
    
    static int create(){
        return ++bNom;
    } 
    
        
    public String createBlankNode(){
        if (Debug)  System.out.println("we are at createBlankNode()");
        int i=0;
        String str=new String("bbbb"+create());
        i++;
        return str;
    }
    
    public char LA(int i){
        if ((index+i) < graph.length())
            return graph.charAt(index+i);
        /*else if (strsIndex < strs.size()){
            index = -1;
            graph = (String)strs.get(strsIndex++);
            return graph.charAt(index+i);
        }*/
        else {
            index=graph.length()+i;   //before +1
            return ' ';
        }
    }/* end LA */
    
    
    public boolean match(char c){        
        if(Debug)  
            System.out.println(graph.charAt(index+1)+"  "+c+"   index="+(index+1)+"    graph.length()="+graph.length()+"  errorsNom="+nomOfErrors);
        if (graph.charAt(index+1)==c){
            index++;
            return true;
        }
        else {
            nomOfErrors++;
            index++;
            return false;
        }
    }/* end match */

    
    
        
    public void start(){
        if (Debug)  System.out.println("we are at start()");
        turtleDoc();
    }
    
    public boolean letter(char c){
        boolean letter;
        if((c >= 'a') && (c<='z'))
            letter=true;
        else if((c >= 'A') && (c<='Z')) 
                  letter=true;
        else letter= false;     
        return letter;
    } /*end letter*/
   
   public boolean digit(char c){
        if((c >= '\u0030') && (c<='\u0039'))
            return true;
        else return false;     
    } /*end letter*/
    
    public void wsStar(){
        while (ws(LA(1)) && (index<graph.length()))
            index++;
    }
    
    public void wsPlus(){
        if(ws(LA(1)))
            index++;
        else nomOfErrors++;
        while (ws(LA(1)) && (index<graph.length()))
            index++;
    }
    
    
    public String digitStar(){
        String number=new String("");
        while (digit(LA(1))){
            index++;
            number+=LA(1);
        }
        return number;
    }
    
    public String digitPlus(){
        String number=new String("");
        if (digit(LA(1))){
            index++;
            number+=LA(1);
        }
        else nomOfErrors++;
        while (digit(LA(1))){
            index++;
            number+=LA(1);
        }
        return number;
    }
    
    public String letterOrDigitPlus(){
        String number=new String("");
        if (digit(LA(1)) || letter(LA(1))){
            number+=LA(1);
            index++;            
        }
        else nomOfErrors++;
        while  (digit(LA(1)) || letter(LA(1))){
            number+=LA(1);
            index++;            
        }
        return number;
    }
    
    // [1]
    public void turtleDoc(){
        if (Debug)  System.out.println("we are at turtleDoc()");
        while (((index+1) < graph.length()))
            statement();
        if (nomOfErrors==0)
            if (Debug)  System.out.println("Graph Loaded Successfuly...");
        if (Debug)  System.out.println("we exit at turtleDoc()");
    }
    
    
    // [2]
    public void statement(){
        if (Debug)  System.out.println("we are at statement()");
        switch (LA(1)){
            case '#':
                comment();
                break;
            case '@':
                directive();
                wsStar();
                match('.');
                wsStar();
                break;
            case '\u0009':
            case '\u0020':
            case '\n':    
            case '\r':    
                index++;
                break;
                
            default:
                triples();
                wsStar();
                match('.');
                wsStar();
                break;                
        }   
        if (Debug)  System.out.println("we exit at statement()");
    }
    
    
    
    // [3]
    public void directive(){
        if (Debug)  System.out.println("we are at directive()");
        String pre=new String("");
        match('@'); match('p');match('r');match('e');match('f');match('i');match('x');//@prefix
        wsPlus();
        if ((LA(1)!='_')&&(nameStartChar(LA(1))))
                pre= prefixName();
        //System.out.println("we are at directive()"+pre);
        match(':');
        wsPlus();
        String uri=uriref();
        prefixesNames.add(pre);
        prefixes.add(uri);
        if (Debug)  System.out.println("we exit at directive()");
    }

    

    // [4]
    public void triples(){
        if (Debug)  System.out.println("we are at triples()");
        
        if ((LA(1)=='(')||(LA(1)=='[')){
            String n=blank();
            predicateList(n);  // new rule
        
        }
        else {
            String s=subject();
            wsPlus();
            predicateObjectList(s);
            if (Debug)  System.out.println("we exit at triples()");
        }
    }

        // [4.5]   added rule
    public void predicateList(String s){
        if (Debug)  System.out.println("we are at prdicateList()");
        wsStar();
        if (LA(1)!='.')
            predicateObjectList(s);
    }

    
    // [5]
    public void predicateObjectList(String s){
        if (Debug)  System.out.println("we are at predicateObjectList()");
        String p=verb();
        wsPlus();
        objectList(s,p);
        wsStar();
        while (LA(1)==';'){
            match(';');
            wsStar();
            if (LA(1)!='.'){   //I add this at 4/10/2006, since it is possible to have ?X ?Y ; .
                p=verb();
                wsPlus();
                objectList(s,p);
            }
            wsStar();
        }
        wsStar();
        if(LA(1)==';')
            match(';');
        if (Debug)  System.out.println("we exit at predicateObjectList()");
    }
    
    
    
    // [6]
    public void objectList(String s, String p){
        if (Debug)  System.out.println("we are at objectList()");
        String o=object();
        wsStar();
        numOfTriples++;
        RDFgraph.setTriple(s,p,o);
        while (LA(1)==','){
            match(',');
            wsStar();
            if (LA(1)!='.'){   //I add this at 4/10/2006, since it is possible to have ?X ?Y , .
                o=object();
                numOfTriples++;
                RDFgraph.setTriple(s,p,o);
            }
            wsStar();
        }
        if (Debug)  System.out.println("we exit at objectList()");
    }
    
    // [7]
    public String verb(){
        if (Debug)  System.out.println("we are at verb()");
        if ((LA(1)=='a') && (LA(2)==' ')){
            match('a');
            if (Debug)  System.out.println("we exit at verb()");        
            return ("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>");
        }
        else return predicate();        
    }
    
    // [8]
    public void comment(){
       if (Debug)  
           System.out.println("we are at comment()");
       String comment = "";
       match('#');
       boolean commentEnd=false;
       while  ((index<graph.length())&&(!commentEnd))
           if ((LA(1) =='\n') ||(LA(1) =='\r'))
               commentEnd=true;
           else {
               //System.out.println("we exit at comment():"+comment);
               comment+=LA(1);
               match(LA(1));
           }
       match(LA(1));
       if (Debug)  
           System.out.println("we exit at comment():"+comment);
    }
    
    // [9]
    public String subject(){
        if (Debug)  System.out.println("we are at subject()");
       String s;
       if ((LA(1)=='_')||(LA(1)=='-'))  //((LA(1)=='[')|| ((LA(1)=='_')))
           s=nodeID();
       else s=resource();       
       if (Debug)  System.out.println("we exit at subject()");
       return s;
    }
    
    // [10]
    public String predicate(){
        if (Debug)  System.out.println("we are at predicate()");
        String p;
        p=resource();
        if (Debug)  System.out.println("we exit at predicate()");
        return p;
    }
    

    // [11]
    public String object(){
       if (Debug)  System.out.println("we are at object()"); 
       
       String nLiteral=new String("");
        int i=index+1;
        while((i<graph.length()) && (graph.charAt(i)!=' ')){
            nLiteral+=graph.charAt(i);
            i++;
        }
        boolean bool=false;
        nLiteral= nLiteral.toLowerCase();
        if (nLiteral.equals("true") || nLiteral.equals("false")) 
            bool=true;
        
       String o; 
       if ((LA(1)=='[')|| ((LA(1)=='_')) || (LA(1)=='('))
           o=blank();
       else if ((LA(1)=='<')|| ((LA(1)=='_')) || (!(bool)&& (letter(LA(1)) || LA(1)==':')))
           o=resource();
       else o=literal();
       if (Debug)  System.out.println("we exit at object()"); 
       return o;       
    }
    
    

    // [12]
    public String literal(){
        if (Debug)  System.out.println("we are at literal()"); 
        String l=new String("");
        if (LA(1)=='\u0022')
            l=RDFLiteral();
        else if (letter(LA(1))){
            l="\"\"\""+boolean1()+"\"\"\"";
            l+="^^";
            l+="<http://www.w3.org/2001/XMLSchema#boolean>";
        }
        else  if (digit(LA(1))|| (LA(1)=='.') || (LA(1)=='+') || (LA(1)=='-'))
            l=NumericLiteral();
        if (Debug)  System.out.println("we exit at literal()"); 
        return l;
    }
    
    
    

    public String RDFLiteral(){
        if (Debug)  System.out.println("we are at RDFLiteral()"); 
        String l;
        l=quotedString();
        int i=1;
        if(LA(1)=='@'){
            i=2;
            match('@');
            l+='@';
            l+=language();
            //System.out.println("we are at RDFLiteral()"+l);            
        }
        else if(LA(1)=='^'){
            i=3;
            match('^');
            match('^');
            l+='^';
            l+='^';
            l+=resource();
        }
        //System.out.println("we are at RDFLiteral()"+l);            
        if ((i==1)){//&& (!l.contains("^^"))){
            l+="^^";
            l+="<http://www.w3.org/2001/XMLSchema#string>"; //last modified 10/10/2006 ==> l+=type(l.substring(3, l.indexOf("\"\"\"", 3)));
        }
        if (Debug)  System.out.println("we exit at RDFLiteral()"); 
        return l;
    }
       
    public String type(String str){
       if (Debug) System.out.println("we exit at type() from turtle:"+str); 
        String t="";
        NumericType nt=new NumericType(str);
        if (str.equals("true")||
                    str.equals("false"))
                t="<http://www.w3.org/2001/XMLSchema#boolean>";
        else if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#integer>")){ 
            t="<http://www.w3.org/2001/XMLSchema#integer>";
        }
        else if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#decimal>")){
            t="<http://www.w3.org/2001/XMLSchema#decimal>";
        }
        else if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#double>")){
            t="<http://www.w3.org/2001/XMLSchema#double>";
        }
        else
            t="<http://www.w3.org/2001/XMLSchema#string>";
        return t;
    }
     
    public String NumericLiteral(){
        if (Debug)  System.out.println("we are at NumericLiteral()"); 
        String nLiteral=new String("");
        //nLiteral+=graph.charAt(index+1);
        int i=index+1;
        while((i<graph.length()) && (graph.charAt(i)!=' ')){
            nLiteral+=graph.charAt(i);
            i++;
        }
        index=i;
        if (Debug)  System.out.println("we are at NumericLiteral(): nLiteral:"+nLiteral+"$"); 
        NumericType nt=new NumericType(nLiteral);
        if (Debug)  System.out.println("we are at NumericLiteral():"+nt.getType()); 
        if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#integer>")){
            nLiteral="\"\"\""+nLiteral+"\"\"\""+"^^";
            nLiteral+="<http://www.w3.org/2001/XMLSchema#integer>";
            return nLiteral;
        }
        else if(nt.getType().equals("<http://www.w3.org/2001/XMLSchema#decimal>")){
            nLiteral="\"\"\""+nLiteral+"\"\"\""+"^^";
            nLiteral+="<http://www.w3.org/2001/XMLSchema#decimal>";
            return nLiteral;
        }
        else  if (nt.getType().equals("<http://www.w3.org/2001/XMLSchema#double>")){
            nLiteral="\"\"\""+nLiteral+"\"\"\""+"^^";
            nLiteral+="<http://www.w3.org/2001/XMLSchema#double>";
            return nLiteral;
        }        
        else nomOfErrors++;
        if (Debug)  System.out.println("we exit at NumericLiteral()"); 
        return nLiteral;
    }
    

    // [13]
    public void dataTypeString(){
        quotedString();
        match('^');
        match('^');
        resource();
    }
    
    

    

    // [14]
    public boolean integer(String token){
       if (Debug)  System.out.println("we are at integer()"); 
       int i;
       boolean plus;
       if(token.length()>0){
        if ((token.charAt(0)=='-') || (token.charAt(0)=='+'))
            i=1;
        else i=0;
        plus=true;
        plus =digit(token.charAt(i++));
        while ((plus) && (i<token.length()))             //token.charAt(i) !='\0'
            plus = digit(token.charAt(i++));
       }
       else return false;
        if (Debug)  System.out.println("we exit at integer()"); 
        return plus;
    }
    
    

    // [15]
    public boolean double1(String token){
        if (Debug)  System.out.println("we are at double1()"); 
        boolean double1;
        String token1="",token2="";
        int pos=-1;
        pos = token.indexOf('e');
        if (pos<0)
            pos = token.indexOf('E');
        if (pos<0)
            double1=false;
        else{
            for(int j=0;  j<token.length(); j++)
                if (j <pos)
                    token1+=token.charAt(j);
                else token2+=token.charAt(j);
            token1+='\0';
            token2+='\0';
            if(decimal(token1))
                if (exponent(token2))
                    double1=true;
                else double1=false;
            else if(integer(token1))
                    if (exponent(token2))
                        double1=true;
                    else double1=false;
            else double1=false;
        }
        if (Debug)  System.out.println("we exit at double1()"); 
        return double1;        
    }
    
    
    

    // [16]
    public boolean decimal(String token){
        if (Debug)  System.out.println("we are at decimal()"); 
        boolean decimal1, firstchoice=false,secondchoice=false;
        int i=0;
        if ((token.charAt(0)=='-') || (token.charAt(0)=='+'))
            i=1;
        else i=0;
        if (token.charAt(i)=='.'){ 
            boolean plus=true;
            plus =digit(token.charAt(++i));
            while ((plus) && (token.charAt(++i)!='\0'))
                plus = digit(token.charAt(i));
            if (plus)
                decimal1=true;
            else decimal1= false;
            firstchoice=decimal1;
          /*  System.out.println(firstchoice);*/
        }    
        else if (digit(token.charAt(i))){
                   /* System.out.println(" ???????");*/
                    int pos = token.indexOf('.');
                    if (pos >0){
                        String token1="", token2="";
                        for(int j=0;j<token.length();j++)
                            if (j<pos)
                                token1+=token.charAt(j);
                            else if (j>pos)
                                token2+=token.charAt(j);
                        token1+='\0';
                        token2+='\0';
                         if ((integer(token1)) && (integer(token2) || token2.equals("\0")))
                            secondchoice=true;
                        else secondchoice=false;
                        }
        }
        else decimal1=false;
        if (firstchoice || secondchoice)
            decimal1=true;
        else 
            decimal1=false;
        if (Debug)  System.out.println("we exit at decimal()"); 
        return decimal1;
    }
    
    

    // [17]
    public boolean exponent(String token){
       if (Debug)  System.out.println("we are at exponent()"); 
       boolean exponent;
        int i=0;
        if ((token.charAt(0)=='e') || (token.charAt(0)=='E')){
            i++;
             if ((token.charAt(i)=='-') || (token.charAt(i)=='+'))
                 i++;
            String token1="";
            for(int j=i; j<token.length(); j++)  //(token.charAt(j)!='\0')
                token1+=token.charAt(j);
            token1+='\0';
            if(integer(token1))
                exponent=true;
            else exponent=false;
        }
        else exponent=false;
        if (Debug)  System.out.println("we are at exponent()"); 
        return exponent;     
    }
    
    

    // [18]
    public String boolean1(){
        if (Debug)  System.out.println("we are at boolean1()"); 
        
        String nLiteral=new String("");
        int i=index+1;
        while((i<graph.length()) && (graph.charAt(i)!=' ')){
            nLiteral+=graph.charAt(i);
            i++;
        }
        index=i;
        if (Debug) System.out.println("we exit at boolean1()"+nLiteral); 
        nLiteral= nLiteral.toLowerCase();
        if (Debug) System.out.println("we exit at boolean1()"+nLiteral); 
        if ((nLiteral.equals("true"))|| (nLiteral.equals("false")))
            return nLiteral;
        else nomOfErrors++;
        if (LA(1)=='t'){
            match('t');
            match('r');
            match('u');
            match('e');
            if (Debug)  System.out.println("we exit at boolean1()"); 
            return "true";
        }
        else {
            match('f');
            match('a');
            match('l');
            match('s');
            match('e');
            if (Debug)  System.out.println("we exit at boolean1()"); 
            return "false";
        }
    }
    
    
    
    // [19]
    public String blank(){
        if (Debug)  System.out.println("we are at blank()"); 
        String s="";
        if ((LA(1)=='_')){
            s=nodeID();
            //if (NumRDFGraphsURIs>1)
              //  s+=graphNum;
        }
        /* else if ((LA(1)=='[')&& (LA(2)==']')){
            s= createBlankNode();
            match('[');
            match(']');
            s+=graphNum;
        }
        else{
            match('[');
            s= createBlankNode();
            wsStar();
            s+=graphNum;
            predicateObjectList(s);
            wsStar();
            match(']');
        }*/
        else  if (LA(1)=='['){
            match('[');
            s= createBlankNode();
            wsStar();
            s+=graphNum;
            if (LA(1)!=']'){
                predicateObjectList(s);
                wsStar();
            }
            match(']');
            
        }
        else if (LA(1)=='(')
            s= collection();
        else nomOfErrors++;
        if (Debug)  System.out.println("we exit at blank()"); 
        return s;            
    }
    
    
    // [20]
    public String itemList(){
        if (Debug)  System.out.println("we are at itemList()"); 
        String o=object();
        /*wsStar();
        while (LA(1)!=')'){
            wsStar();
            object();
            wsPlus();
        }*/
        if (Debug)  System.out.println("we exit at itemList()"); 
        return o;
    }
    
    
    
    // [21]
    public String collection(){
        if (Debug)  System.out.println("we are at collection()"); 
        String head= createBlankNode();
        String node=new String(head);        
        String n="";
        match('(');
        wsStar();
        String newNode="";
        if (LA(1)!=')'){
            n=itemList();
            numOfTriples++;
            RDFgraph.setTriple(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>",n);
            if (ws(LA(1)))
                wsPlus();            
            else wsStar();
            while (LA(1)!=')'){
                n=itemList();
                newNode=createBlankNode();
                numOfTriples+=2;
                RDFgraph.setTriple(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>",newNode);                      
                RDFgraph.setTriple(newNode,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>",n);                                      
                node=newNode;
                wsPlus();
            }
            numOfTriples++;
            RDFgraph.setTriple(node,"<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>","<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>");                      
            match(')');
            if (Debug)  System.out.println("we exit at collection()"); 
            return head;            
        }
        else {
            if (Debug)  System.out.println("we exit at collection()"); 
            return "<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>";
        }
    }
    
    
    
    // [22]
    public boolean ws(char c){
        if (Debug)  System.out.println("we are at ws()"); 
        if ((c=='\u0009')|| (c=='\n') ||(c=='\r') || (c=='\u0020'))
            return true;
        else return false;
    }
    
    // [23]
    public String resource(){
        if (Debug)  System.out.println("we are at resource()"); 
        String res;
        if(LA(1)=='<'){
            res=uriref();
        }
        else {
            try{
                if ((LA(1)=='h')&&(LA(2)=='t')&&(LA(3)=='t')&&(LA(4)=='p'))
                    res=qnameOld();
                else res=qname();
            }catch(Exception e){System.out.println(e);res=qname();}
        }
        if (Debug)  System.out.println("we exit at resource()"); 
        return res;
    }
    
    
    // [24]
    public String nodeID(){
        if (Debug)  System.out.println("we are at nodeID()"); 
        String nodeId=new String("");
        if(LA(1)!='-'){
            match('_');
            nodeId+='_';
            match(':');
            nodeId+=':';
            nodeId+=name();
        }
        else {
            match('-');
            nodeId+='-';
            while(LA(1)!=' '){
                nodeId+=LA(1);
                match(LA(1));
            }   
        }
        if (NumRDFGraphsURIs>1)
            nodeId +=graphNum;
        if (Debug)  System.out.println("we exit at nodeID()"); 
        return nodeId;
    }
    
    
    public int searchPrefixIRI(String ncnameprefix){
        if (Debug)    System.out.println("we are at searchPrefixIRI()"); 
        boolean found=false;
        int prefixesCounter=0;
        int location=-5;
        while((!found) && prefixesCounter<prefixesNames.size()){
            if (((String)prefixesNames.get(prefixesCounter)).equals(ncnameprefix)){
                found=true;
                location=prefixesCounter;
            }
            ++prefixesCounter;
        }
        if (Debug) System.out.println(" we are at saerchPrefixIRI:"+found);
        if (Debug)
            System.out.println("we exit at searchPrefixIRI()"+ncnameprefix); 
        return location;
    }
    
    // [25']
    public String qnameOld(){
        if (Debug)  System.out.println("we are at qnameOld()"); 
        String uri=new String("");
        uri+='<';
        uri+=relativeURI();
        uri+='>';
        if (Debug)  System.out.println("we exit at qnameOld()"); 
        return uri;
    }
    
    // [25]
    public String qname(){
        if (Debug)  System.out.println("we are at qname()"); 
        String q=new String("");
        String base1=new String("");
        if ((LA(1)!='_')&&(nameStartChar(LA(1))))
                q=prefixName();
        
        
            /* resolve macros prefixes IRIs   */
                int location =searchPrefixIRI(q);
                /*System.out.println("ncname="+ncname);*/
                if (location>=0){
                     /*System.out.println("location="+location); */
                    int j=0;
                    for(j=0;((String)prefixes.get(location)).charAt(j)!='>';j++)
                         base1+=((String)prefixes.get(location)).charAt(j);
                    if ((((String)prefixes.get(location)).charAt(j-1)!='/')&&
                            (((String)prefixes.get(location)).charAt(j-1)!='#')) 
                        
                        base1+='/';                    
                }
        
      
        match(':');
        if (nameStartChar(LA(1))){
                String ncname=name();
                /*System.out.println("BASE="+base1);*/
                for(int j=0;j<ncname.length();j++)
                     base1+=ncname.charAt(j);
                base1+='>';
        }
        q=base1;
        if (Debug)  System.out.println("we exit at qname()"); 
        return q;
    }   
    
    
    // [26]
    public String uriref(){
        if (Debug)  System.out.println("we are at uriref()"); 
        String uri=new String("");
        uri+='<';
        match('<');
        uri+=relativeURI();
        uri+='>';
        match('>');
        if (Debug)  System.out.println("we exit at uriref()"); 
        return uri;
    }
    
    // [27]
    public String language(){
        if (Debug)  System.out.println("we are at language()"); 
        String lan=new String("");
        if (letter(LA(1))){
            lan+=LA(1);
            match(LA(1));
        }
        while (letter(LA(1))){
            lan+=LA(1);
            match(LA(1));
        }
        if (LA(1)=='-'){
            lan+=LA(1);
            match('-');
            lan+=letterOrDigitPlus();
        }
        if (Debug)  System.out.println("we exit at language()"); 
        return lan;
    }
    
    // [28]
    public boolean nameStartChar(char c){      
       if (Debug)  System.out.println("we are at nameStartChar()"); 
       if (letter(c))
           return true;
       else  if (c=='_')
           return true;
       else if ((c >= '\u00C0') && (c <='\u00D6'))
           return true;
       else if ((c >= '\u00D8') && (c <='\u00F6'))
           return true;       
       else if ((c >= '\u00F8') && (c <='\u02FF'))
           return true;
       else if ((c >= '\u0370') && (c <='\u037D'))
           return true;       
       else if ((c >= '\u037F') && (c <='\u1FFF'))
           return true;
       else if ((c >= '\u200C') && (c <='\u200D'))
           return true;
       else if ((c >= '\u2070') && (c <='\u218F'))
           return true;
       else if ((c >= '\u2C00') && (c <='\u2FEF'))
           return true;
       else if ((c >= '\u3001') && (c <='\uD7FF'))
           return true;
       else if ((c >= '\uF900') && (c <='\uFDCF'))
           return true;
       else if ((c >= '\uFDF0') && (c <='\uFFFD'))
           return true;
    /*   else if ((c >= '\u10000') && (c <='\uEFFFF'))
           return true;*/
       else return false;   
    }
    
    
    
    // [29]
    public boolean nameChar(char c){       
       if (Debug)  System.out.println("we are at nameChar()"); 
       if (c=='-')
           return true;
       else if (c=='_')  // this one is added in the data of the dawg test example rdfsemantics-var-type-vr
           return true;
       else if (digit(c))
           return true;
       else if (c=='\u00B7')
           return true;
       else if ((c >= '\u0300')&&(c <='\u036F'))
           return true;
       else if ((c >= '\u203F')&&(c <='\u2040'))
           return true;
       else if (nameStartChar(c))
           return true;
       else return false;
    }
    
    // [30]
    public String name(){
        if (Debug)  System.out.println("we are at name()"); 
        String n=new String("");
        if(nameStartChar(LA(1))){
            n+=LA(1);
            match(LA(1));
        }
        while (nameChar(LA(1))){
            n+=LA(1);
            match(LA(1));
        }
        if (Debug)  System.out.println("we exit at name()"); 
        return n;
    }
    
    
    
    // [31]
    public String prefixName(){
        if (Debug)  System.out.println("we are at prefixName()"); 
        String pre=new String("");
        if(nameStartChar(LA(1)) && (LA(1)!='_')){
            pre+=LA(1);
            match(LA(1));
        }
        while (nameChar(LA(1))){
            pre+=LA(1);
            match(LA(1));
        }
        if (Debug)  System.out.println("we exit at prefixName()"); 
        return pre;
    }
    
    
    // [32]
    public String relativeURI(){
        if (Debug)  System.out.println("we are at relativeURI()"); 
        String ruri=new String("");
        while (ucharacter(LA(1))){
            ruri+=LA(1);
            match(LA(1));
        }
        if (Debug)  System.out.println("we exit at relativeURI()"); 
        return ruri;
    }
    
      
    
    // [33]
    public String quotedString(){
        if (Debug) 
            System.out.println("we are at quotedString()"); 
        String quoted=new String("");
        if ((LA(1)=='\u0022') && (LA(2)=='\u0022') &&(LA(3)=='\u0022'))
            quoted=longString();
        else quoted=string();
        if (Debug)  System.out.println("we exit at quotedString()"); 
        return quoted;
    }
    
      
    
    // [34]
    public String string(){
        if (Debug)
            System.out.println("we are at string()"); 
        String str=new String("\"\"\"");
        match('\u0022');
        if (LA(1)!='\u0022')
            while ((scharacter(LA(1))) && ((LA(1)!='\u0022') ||((LA(1)=='\u0022')&&(LA(0)=='\\')))){
                if (LA(1)=='"'){
                    str+='\'';
                    match('\'');
                }
                else {
                    str+=LA(1);
                    match(LA(1));
                }
            }
        str+=new String("\"\"\"");
        match('\u0022');        
        if (Debug)  System.out.println("we exit at string()"); 
        return str;
    }
    
      
    // [35]
    public String longString(){
        if (Debug)
            System.out.println("we are at longString()"); 
        String longstr=new String("\"\"\"");
        match('\u0022');
        match('\u0022');
        match('\u0022');
        
        //last modified 11/10/2006
        int pos1=graph.indexOf("\\\"\"\"", index);
        int pos2=-1;
        boolean last =false;
        while ((pos1>=0)&& (!last)){
            pos2=graph.indexOf("\\\"\"\"", pos1+4);
            //System.out.println("we are at longString(): pos1="+pos1+"  pos2="+pos2);
            if (pos2 <0) 
                last =true;
            else pos1=pos2;
        }
        //System.out.println("we are at longString(): pos1="+pos1);
        if (pos1 <0) pos1=index;
        int loc =graph.indexOf("\"\"\"", pos1);
        //last modified 11/10/2006
        
        if (loc>index)
            while (character(LA(1))&& (index<loc-1)){//(LA(1)!='\u0022') && (LA(2)!='\u0022')&&(LA(3)!='\u0022')
                if (LA(1)=='"'){
                    longstr+='\'';
                    match('\'');
                }
                else {
                    //System.out.println(LA(1));
                    longstr+=LA(1);
                    match(LA(1));
                }
                /*longstr+=LA(1);
                match(LA(1));*/
            }
        longstr+="\"\"\"";
        match('\u0022');
        match('\u0022');
        match('\u0022');
        if (Debug)  
            System.out.println("we exit at longString()"+longstr+pos1+loc); 
        return longstr;
    }
    
      
    // [36]
    public boolean character(char c){
        if(c=='\\')
            return true;
        else  if(c=='_')
            return true;
        else if ((c>='\u0000')&&(c<='\uFFFF'))
            return true;
        else if ((c>='\u0020')&&(c<='\u005B'))
            return true;        
        /*else if ((c>='\u005D')&&(c<='\u10FFFF'))
            return true;        
         *else if ((c>='\u10000')&&(c<='\u10FFFF'))
            return true;        */
        else return false;
    }
    
  
      
    // [37]
    public boolean echaracter(char c){
        if(c=='\t')
            return true;
        else if(c=='\n')
            return true;
        else if(c=='\r')
            return true;
        else if (character(c))
            return true;
        else return false;
    }
    
  
    
    // [38]
    public boolean hex(char c){
        if(digit(c))
            return true;
        else if ((c>='\u0041')&&(c<='\u0046'))
            return true;
        else return false;
    }
    
        
    // [39]
    public boolean ucharacter(char c){
        if(c==' ')
            return false;
        else 
         if (character(c)&&(c!='\u003E'))
            return true;
        else return false;
    }
  
      
    // [40]
    public boolean scharacter(char c){
        if(c=='"')
            return true;
        else  if (echaracter(c))//last modified 11/10/2006: &&(c!='\u0022'))
            return true;
        else return false;
    }
    
  
    private Vector prefixesNames;
    private Vector prefixes;
    private String graph;
    private MultiGraph RDFgraph;
    private int index;
    private boolean Debug;
    private int nomOfErrors;
    private static int bNom=1000;
    private int graphNum;
    private Vector exceptions;
    private int NumRDFGraphsURIs;
    private Vector strs;
    private int strsIndex;
    private long startLoadTimeS;
    private long elapsedLoadTimeS; 
    private boolean isLocalFile;
    private long numOfTriples=0;
}