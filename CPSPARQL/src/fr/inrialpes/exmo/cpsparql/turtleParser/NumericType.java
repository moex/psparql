/*
 * NumericType.java
 *
 * Created on April 3, 2006, 11:25 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.turtleParser;

/**
 *
 * @author faisal
 */
public class NumericType {
    
    /** Creates a new instance of NumericType */
    public NumericType(String str) {
        Debug=false;
        type= new String("");
        if (integer(str))
            type="<http://www.w3.org/2001/XMLSchema#integer>";
        else if (decimal(str))
            type="<http://www.w3.org/2001/XMLSchema#decimal>";
        else if (double1(str))
            type="<http://www.w3.org/2001/XMLSchema#double>";
        else type="";
    }
    
    public String getType(){
        return type;
    }
    
    public boolean integer(String token){
       if (Debug)  System.out.println("we are at integer()"); 
       int i;
       boolean plus;
       if(token.length()>0){
        if ((token.charAt(0)=='-') || (token.charAt(0)=='+'))
            i=1;
        else i=0;
        plus=true;
        plus =digit(token.charAt(i++));
        while ((plus) && (i<token.length()))             //token.charAt(i) !='\0'
            plus = digit(token.charAt(i++));
       }
       else return false;
        if (Debug)  System.out.println("we exit at integer()"); 
        return plus;
    }
    
    

    
    public boolean double1(String token){
        if (Debug)  System.out.println("we are at double1()"); 
        boolean double1;
        String token1="",token2="";
        int pos=-1;
        pos = token.indexOf('e');
        if (pos<0)
            pos = token.indexOf('E');
        if (pos<0)
            double1=false;
        else{
            for(int j=0;j<token.length(); j++)
                if (j <pos)
                    token1+=token.charAt(j);
                else token2+=token.charAt(j);
            token1+='\0';
            token2+='\0';
            if(decimal(token1))
                if (exponent(token2))
                    double1=true;
                else double1=false;
            else if(integer(token1))
                    if (exponent(token2))
                        double1=true;
                    else double1=false;
            else double1=false;
        }
        if (Debug)  System.out.println("we exit at double1()"); 
        return double1;        
    }
    
    
    public boolean digit(char c){
        if((c >= '\u0030') && (c<='\u0039'))
            return true;
        else return false;     
    } /*end letter*/
   

    
    public boolean decimal(String token){
        if (Debug)  System.out.println("we are at decimal()"); 
        boolean decimal1, firstchoice=false,secondchoice=false;
        int i=0;
        if ((token.charAt(0)=='-') || (token.charAt(0)=='+'))
            i=1;
        else i=0;
        if (token.charAt(i)=='.'){ 
            boolean plus=true;
            plus =digit(token.charAt(++i));
            while ((plus) && (token.charAt(++i)!='\0'))
                plus = digit(token.charAt(i));
            if (plus)
                decimal1=true;
            else decimal1= false;
            firstchoice=decimal1;
          /*  System.out.println(firstchoice);*/
        }    
        else if (digit(token.charAt(i))){
                   /* System.out.println(" ???????");*/
                    int pos = token.indexOf('.');
                    if (pos >0){
                        String token1="", token2="";
                        for(int j=0;j<token.length();j++)
                            if (j<pos)
                                token1+=token.charAt(j);
                            else if (j>pos)
                                token2+=token.charAt(j);
                        token1+='\0';
                        token2+='\0';
                         if ((integer(token1)) && (integer(token2) || token2.equals("\0")))
                            secondchoice=true;
                        else secondchoice=false;
                        }
        }
        else decimal1=false;
        if (firstchoice || secondchoice)
            decimal1=true;
        else 
            decimal1=false;
        if (Debug)  System.out.println("we exit at decimal()"); 
        return decimal1;
    }
    
    

    
    public boolean exponent(String token){
       if (Debug)  System.out.println("we are at exponent()"); 
       boolean exponent;
        int i=0;
        if ((token.charAt(0)=='e') || (token.charAt(0)=='E')){
            i++;
             if ((token.charAt(i)=='-') || (token.charAt(i)=='+'))
                 i++;
            String token1="";
            for(int j=i; j<token.length(); j++)  //(token.charAt(j)!='\0')
                token1+=token.charAt(j);
            token1+='\0';
            if(integer(token1))
                exponent=true;
            else exponent=false;
        }
        else exponent=false;
        if (Debug)  System.out.println("we are at exponent()"); 
        return exponent;     
    }
    
    private boolean Debug=true;
    private String type;
}
