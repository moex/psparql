/*
 * GraphPattern.java
 *
 * Created on January 6, 2006, 4:04 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.query;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Graph;

/**
 *
 * @author faisal
 */
public class GraphPattern {
    
    /** Creates a new instance of GraphPattern */
    public GraphPattern(){
        currentNamedGraph=-1;
        defaultGraph=-1;
        namedGraphParameter= new String[maxGraph];
        namedGraphPattern = new Graph [maxGraph];
        defaultGraphPattern = new Graph();
    }
    
    public GraphPattern(GraphPattern GP) {
        this.defaultGraphPattern=GP.defaultGraphPattern;
        this.defaultGraph =GP.defaultGraph;
        for(int j=1;j<=GP.currentNamedGraph;j++){
            this.namedGraphPattern[j]= new Graph(GP.namedGraphPattern[j]);
            this.namedGraphParameter[j]= new String(GP.namedGraphParameter[j]);
        }
        this.currentNamedGraph=GP.currentNamedGraph;
    }
    
    public void setDefaultGraph(Graph g){
        defaultGraphPattern=g;
        defaultGraph++;
    }
    
    public void setNamedGraph(Graph g, String Parameter){
        namedGraphParameter[currentNamedGraph+1]= new String(Parameter);
        namedGraphPattern[++currentNamedGraph]=g;
    }
    
    public void incrementNamedGraphs(){
        ++currentNamedGraph;
    }
    
    public GraphPattern getGraphPattern(){
        return this;
    }
    
     public Graph getDefaultGraph(){
        return defaultGraphPattern;
    }
   
    
    public String getTextGraphPattern(){
        return textGraphPattern;
    }
    
    public void printGraphs(){
        textGraphPattern = new String("");
        //System.out.println("__________________________________________________________________________________________________________________");
        textGraphPattern+="__________________________________________________________________________________________________________________\n";
        //System.out.println("The Triples of the Default Graph are: ");
        textGraphPattern+="The Triples of the Default Graph are: \n";
        defaultGraphPattern.print("G");
        textGraphPattern+=defaultGraphPattern.getTextGraph();
        for(int j=0;j<=currentNamedGraph;j++){
            //System.out.println("__________________________________________________________________________________________________________________");
            textGraphPattern+="__________________________________________________________________________________________________________________\n";
            //System.out.println("The Triples of the Named <Graph["+j+"],"+namedGraphParameter[j]+">  are: ");
            namedGraphPattern[j].print("G");
            textGraphPattern+=namedGraphPattern[j].getTextGraph();
        }
    }
   
    private int currentNamedGraph =-1;
    private int defaultGraph=-1;
    private int maxGraph =20;
    private Graph defaultGraphPattern;
    private String namedGraphParameter [];
    private Graph namedGraphPattern [];
    private String textGraphPattern;
    
}
