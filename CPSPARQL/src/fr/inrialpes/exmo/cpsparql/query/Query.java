/*
 * query.java
 *
 * Created on January 3, 2006, 12:46 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.query;
import fr.inrialpes.exmo.cpsparql.findpathprojections.Graph;
import java.net.URI;
import java.util.Vector;

/**
 *
 * @author faisal
 */
public class Query {
            
    /** Creates a new instance of query */
    public Query() {
        initialize();
    
    }
   
    public void initialize(){
        constructTemplateUri = "";
        orderConditions = new Vector();
        order=false;
        selectIndex=1;
        distinct=false;
        textGraphSet =  new String("");
        base=new String("");        
        prefixesNames=new String[maxprefixes];
        prefixes = new String [maxprefixes];
        for(int j=0;j<= prefixesNom;j++)
            prefixes[j]=new String("");
        for(int j=0;j<= prefixesNom;j++)
            prefixesNames[j]=new String("");
        prefixesNames[0]="rdf:";
        prefixes[0]="<http://www.w3.org/1999/02/22-rdf-syntax-ns#>";
        prefixesNames[1]="rdfs:";
        prefixes[1]="<http://www.w3.org/2000/01/rdf-schema#>";
        prefixesNames[2]="xsd:";
        prefixes[2]="<http://www.w3.org/2001/XMLSchema#>";
        prefixesNames[3]="fn:";
        prefixes[3]="<http://www.w3.org/2004/07/xpath-functions>";
        graphSet = new GraphSet();        
        prefixesNom=3;
        currentVarNom=-1;
        fromNom=-1;
        fromNamedNom=-1;
        base="";
        selectVariables = new String [maxVariables];
        fromIRIs = new Vector();
        fromNamedIRIs = new Vector();
        Template =new Graph();
        limitClause=-1;
        offsetClause=0;
        orderClause=new String("");
    }
    
     /** Creates a new instance of query */
    public Query(Query q) {
        this.orderConditions =q.orderConditions;
        this.order=q.order;
        this.selectIndex=q.selectIndex;
        this.distinct = q.distinct;
        this.base= new String(q.base);
        for(int j=0;j<= q.prefixesNom;j++)
            this.prefixesNames[j]=new String(q.prefixesNames[j]);
        for(int j=0;j<= q.prefixesNom;j++)
            this.prefixes[j]=new String(q.prefixes[j]);
        this.prefixesNom=q.prefixesNom;
        this.fromIRIs=new Vector(q.fromIRIs);
        this.fromNom=q.fromNom;
        this.fromNamedIRIs=new Vector(q.fromNamedIRIs);
        this.fromNamedNom=q.fromNamedNom;
        for(int j=0;j<= q.currentVarNom ;j++)
            this.selectVariables[j]=new String(q.selectVariables[j]);
        this.currentVarNom=q.currentVarNom;
        this.graphSet= new GraphSet(q.graphSet);
    }
   
    public void setOrder(){
        order=true;
    }
    
    public boolean getOrder(){
        return order;
    }
    
     public void setBase(String base){
        this.base= new String(base);
    }
     
    
    public void setPrefix(String prefix, String prefixName){
        prefixes[++prefixesNom]= new String(prefix);
        prefixesNames[prefixesNom]= new String(prefixName);
    }
    
    public void setVariables(String variable){
        selectVariables[++currentVarNom]= new String(variable);
    }
    
    public void setFromIRI(String iri){
        fromIRIs.add(iri);
    }
      
    public void setFromNamedIRI(String iri){
        fromNamedIRIs.add(iri);
    }
    
    public void setGraphSet(GraphSet GS){
        this.graphSet=GS;
    }
    
    public void setLimitClause(int limit){
        limitClause=limit;
    }
    
    public void setOffsetClause(int offset){
        offsetClause=offset;
    }
    
    public void setTemplate(Graph template){        
        this.Template=template;
    }
    
    public void setOrderClause(String orderC){
        orderClause=orderC;
    }
    
    public String getOrderClause(){
        return orderClause;
    }
    
    public void setOrderCondition(OrderCondition orderC){
        orderConditions.add(orderC);
    }
    
    public Vector getOrderConditions(){
        return orderConditions;
    }
    
    
    public Vector getFromIRIs(){
        return fromIRIs;
    }
    
     public Vector getFromNamedIRIs(){
        return fromNamedIRIs;
    }
    
    public String getBase(){
        return this.base;
    }
    
    public String [] getPrefixesNames(){
        return prefixesNames;
    }
    
    public String [] getPrefixes(){
        return prefixes;
    }
    
    public GraphSet getGraphSet(){
        return graphSet;
    }
   
    public int Type(){
        return queryType;
    }
            
     public  String getTextGraphSet(){
        return textGraphSet;
    }
     
     public void setQueryType(int t){
         queryType=t;
     }
    
     public String [] selectVar(){
         return selectVariables;
     }
     
     public int varNom(){
         return currentVarNom;
     }
     
     public Graph Template(){
         return Template;
     }
     
     public int Limit(){
         return limitClause;
     }
     
      public int Offset(){
         return offsetClause;
     }
     
      public boolean distinct(){
          return distinct;
      }
      
      public void setDistinct(boolean d){
          distinct=d;
      }
    
      
      public void setSelectIndexStar(int star){
          selectIndex=star;
      }
      
      public int getSelectIndexStar(){
          return selectIndex;
      }
 
      public void setConstructTemplateUri(String uri){
          constructTemplateUri = uri;
      }
      
      
      public String getConstructTemplateUri(){
          return constructTemplateUri;
      }
      
      
    public void print(){
         textGraphSet+="__________________________________________________________________________________________________________________"+"\n";
         textGraphSet+="BASE="+base+"\n";
         textGraphSet+="--------------"+"\n";
                  
         //System.out.println("__________________________________________________________________________________________________________________");
         //System.out.println("BASE="+base);
         //System.out.println("--------------");
         for(int j=0;j<= prefixesNom;j++){
           // System.out.println("PREFIX["+j+"]   "+prefixesNames[j]+"=   "+prefixes[j]);
            textGraphSet+="PREFIX["+j+"]   "+prefixesNames[j]+"  =   "+prefixes[j]+"\n";
         }
         //System.out.println("--------------");
         textGraphSet+="--------------"+"\n";
         //System.out.println(" The graph Template:");
         textGraphSet+=" The graph Template:\n";
         Template.print("G");
         textGraphSet+= Template.getTextGraph();         
         //System.out.println("--------------");
         textGraphSet+="--------------\n";
         //System.out.println("SELECT VARIABLES: ");
         textGraphSet+="SELECT VARIABLES: \n";
         for(int j=0;j<= currentVarNom;j++){
            //System.out.println("V["+j+"]="+selectVariables[j]);
            textGraphSet+="V["+j+"]="+selectVariables[j]+"\n";
         }
         //System.out.println("--------------");
         textGraphSet+="--------------\n";
         //System.out.println("FROM IRIrefs: ");
         textGraphSet+="FROM IRIrefs: \n";
         for(int j=0;j< fromIRIs.size();j++){
            //System.out.println("IRI["+j+"]="+fromIRIs[j]);
            textGraphSet+="IRI["+j+"]="+(String)fromIRIs.get(j)+"\n";
         }
         //System.out.println("--------------");
         textGraphSet+="--------------\n";
         //System.out.println("FROM NAMED IRIrefs: ");
         textGraphSet+="FROM NAMED IRIrefs: \n";
         for(int j=0;j<= fromNamedNom;j++){
            //System.out.println("IRI["+j+"]="+fromNamedIRIs[j]);
            textGraphSet+="IRI["+j+"]="+(String)fromNamedIRIs.get(j)+"\n";
         }
         graphSet.printGraphSet();
         textGraphSet+=graphSet.getTextGraphSet();
         //System.out.println("LIMIT Clause="+limitClause);
         textGraphSet+="LIMIT Clause = "+limitClause+"\n";
         //System.out.println("OFFSET Clause="+offsetClause);
         textGraphSet+="OFFSET Clause = "+offsetClause+"\n";
         //System.out.println("ORDER BY="+orderCondition);
         String s;
        /* if(ASC==1)
             s="ASC";
         else s= "DESC";*/
         textGraphSet+="ORDER BY = "+orderClause+"\n";
         OrderCondition oc;
         for(int i=0;i<orderConditions.size();i++){
             oc =(OrderCondition)orderConditions.get(i);
             textGraphSet+="array["+i+"]:\n";
             textGraphSet+="        Direction ="+oc.getDirection()+"\n";
             textGraphSet+="        Type ="+oc.getType()+"\n";
             textGraphSet+="        Expression ="+oc.getExpression()+"\n";
             textGraphSet+="\n";             
         }
         //System.out.println("__________________________________________________________________________________________________________________");
         textGraphSet+="__________________________________________________________________________________________________________________\n";
         
        
    }
    
    private int maxprefixes =20;
    private int maxVariables =40;
    private int maxFromNamed =20;
    public int prefixesNom=-1;
    public int currentVarNom=-1;
    public int fromNom=-1;
    public int fromNamedNom=-1;
    public String base;
    public String prefixesNames[]=new String[maxprefixes];
    public String prefixes []= new String [maxprefixes];
    public String selectVariables []= new String [maxVariables];
    public Vector fromIRIs;
    public Vector fromNamedIRIs;
    private GraphSet graphSet;
    private Graph Template;
    private int limitClause=-1;
    private int offsetClause=0;
    private String orderClause;
    private int queryType;    
    private String textGraphSet; 
    private boolean distinct;   
    private int selectIndex;
    private boolean order;
    private Vector orderConditions;
    private String constructTemplateUri;
}
