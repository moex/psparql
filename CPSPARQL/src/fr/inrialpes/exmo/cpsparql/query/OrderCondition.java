/*
 * OrderCondition.java
 *
 * Created on March 2, 2006, 4:26 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.query;

/**
 *
 * @author faisal
 */
public class OrderCondition {
    
    /** Creates a new instance of OrderCondition */
    public OrderCondition() {
        direction =1;
        type = new String("");
        expression = new String("");
        location=-1;
    }
    
    /** Creates a new instance of OrderCondition */
    public OrderCondition(int d,String t,String exp) {
        direction =d;
        type = new String(t);
        expression = new String(exp);
        location =-1;
    }
    
    /** Creates a new instance of OrderCondition */
    public OrderCondition(OrderCondition oc) {
        this.direction =oc.direction;
        this.expression =oc.expression;
        this.type = oc.type;
        this.location = oc.location;
    }
    
    
    public void setDirection(int i){
        direction =i;
    }
    
    public int getDirection(){
        return direction;
    }
    
    public void setType(String t){
        type =t;
    }
    
    public String getType(){
        return type;
    }
    public void setExpression(String exp){
        expression=exp;
    }
    
    public String getExpression(){
        return expression;
    }
    
    public int getLocation(){
        return location;
    }
    
    public void setLocation(int loc){
        location =loc;
    }
    
    private int direction;
    private String type;
    private String expression;     // will be changed after.  !!!!!!!!!!!!!!!!!!!
    private int location;  
    
}
