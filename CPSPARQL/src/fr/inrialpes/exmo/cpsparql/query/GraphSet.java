/*
 * GraphSet.java
 *
 * Created on January 6, 2006, 4:23 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package fr.inrialpes.exmo.cpsparql.query;



/**
 *
 * @author faisal
 */
public class GraphSet {
    
    /** Creates a new instance of GraphSet */
    public GraphSet(GraphSet GS) {
        for(int j=0;j<=GS.graphPatternNom;j++)
            this.graphPatterns[j]= new GraphPattern(GS.graphPatterns[j]);        
        this.graphPatternNom= GS.graphPatternNom;
    }
    
    public GraphSet(){
        graphPatterns = new GraphPattern [maxGraphPatterns];
        graphPatternNom=-1;
    }
    
    public void setGraphPattern(GraphPattern GP){
        graphPatterns[++graphPatternNom]=GP;
    }
    
    public void incrementGraphPatternNom(){
        ++graphPatternNom;
        System.out.println(graphPatternNom+" we are at increment graph Pattern........................");                            
    }

    public int getGraphPatternNom(){
        return graphPatternNom;
    }
    
    public GraphPattern [] getGraphPattern(){
        return graphPatterns;
    }
    
    public String getTextGraphSet(){
        return textGraphSet;
    }
    
    public void printGraphSet(){
        textGraphSet = new String("");
        if (graphPatternNom <0) System.out.println(" Query Set is Empty...");
        for(int j=0;j<=graphPatternNom;j++){
            //System.out.println("__________________________________________________________________________________________________________________");
            textGraphSet+="__________________________________________________________________________________________________________________\n";
            //System.out.println("The  GraphPattern["+j+"] contains: ");
            textGraphSet+="The  GraphPattern["+j+"] contains: \n";
            graphPatterns[j].printGraphs();            
            textGraphSet+=graphPatterns[j].getTextGraphPattern();
        }
    }
   
    private int graphPatternNom=-1;
    private int maxGraphPatterns =20;
    private GraphPattern graphPatterns [];
    private String textGraphSet;
    
}
