/*
 * A Servlet able to answer PSQPARL queries
 * (C) INRIA, 2009
 * Author: Jerome Euzenat
 *
 * This servlet is based on Faisal Alkhateeb "EchoServlet"
 *
 */

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MainInterface.PSPARQLEngine;

/**
 * Simple demonstration for an Applet <-> Servlet communication.
 */
public class PSPARQLServlet extends HttpServlet {
    /**
     * Get a String-object from the applet and send it back.
     */
    public void doPost( HttpServletRequest request, HttpServletResponse response )
	throws ServletException, IOException {
	String format = request.getParameter("format");
	try {
	    // If format = html
	    if ( format.equals("html") ){
		// Read query
		String query = request.getParameter("query");
		// Eval
		PSPARQLEngine engine = new PSPARQLEngine();
		String result = new String(engine.buildRun(query));
		// Print
		response.setContentType("text/html");
		System.setOut(new PrintStream(response.getOutputStream()));
		System.out.println("<html><head><title>Query result</title></head><body>");
		System.out.println("<h1>((C)P)SPARQL Query evaluation</h1>");
		System.out.println("<h2>Query</h2>");
		printQueryForm( query );
		System.out.println("<h2>Result</h2>");
		System.out.println("<p><textarea style=\"width: 100%;\" rows=\"10\" cols=\"80\" readonly=\"1\">");
		System.out.println(result);
		System.out.println("</textarea></p>");
		System.out.println("<small>");
		System.out.println("<a href=\"http://exmo.inrialpes.fr/software/psparql\">More on CPSPARQL</a>.");
		System.out.println("</small>");
		System.out.println("</body></html>");
		System.out.flush();
		System.out.close();
	    } else { // Old EchoServlet code
		response.setContentType("application/x-java-serialized-object");
		// Read
		InputStream in = request.getInputStream();
		ObjectInputStream inputFromApplet = new ObjectInputStream(in);
		String query = (String)inputFromApplet.readObject();
		// Eval
		PSPARQLEngine engine = new PSPARQLEngine();
		String result = new String(engine.buildRun(query));
		// Print
		OutputStream outstr = response.getOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(outstr);
		oos.writeObject(result);
		oos.flush();
		oos.close();
	    }
	} catch (Exception e) { e.printStackTrace(); }
    }

    public void printQueryForm ( String query ){
	System.out.println("<p><form action=\"PSPARQLServlet\" method=\"post\">");
	System.out.println("<div style=\"text-align: center; font-size=120%;\">");
	System.out.println("<input type=\"hidden\" name=\"format\" value=\"html\" />");
	System.out.println("<textarea style=\"width: 100%;\" name=\"query\" rows=\"10\" cols=\"80\">");
	System.out.println(query);
	System.out.println("</textarea>");
	System.out.println("<br />");
	System.out.println("<input style=\"width: 45%;\" type=\"reset\" value=\"Reset\"/>");
	System.out.println("<input style=\"width: 45%;\" type=\"submit\" value=\"Evaluate\"/>");
	System.out.println("</div>");
	System.out.println("</form>");
	System.out.println("</p>");
    }
}
